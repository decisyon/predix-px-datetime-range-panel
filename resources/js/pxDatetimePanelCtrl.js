(function() {
	'use strict';
	
	/**
	* Controller to manage the datetimepanel widget.
	* link: https://www.predix-ui.com/?show=px-datetime-range-panel&type=component
	*/
	function PxDateTimeRangePanelCtrl($scope, $timeout, $element){
		
		var ctrl = this,
			dateTimeRangePanel = $('px-datetime-range-panel', $element),
			target = $scope.DECISYON.target,
			ctx = target.content.ctx,
			baseName = ctx.baseName.value,
			refObjId = ctx.instanceReference.value,
			mshPage = target.page,
			startDateParamToExport = baseName + '_StartDate',
			startDateEpocParamToExport = baseName + '_StartDateEpoch',
			endDateParamToExport = baseName + '_EndDate',
			endDateEpocParamToExport = baseName + '_EndDateEpoch',
			DEFAULT_TIMEZONE = moment.tz(moment.tz.guess())._z.name,
			DEFAULT_TIMEFORMAT = 'HH:mm:ss A',
			EMPTY_PARAM_VALUE = 'PARAM_VALUE_NOT_FOUND',
			FORMAT_DATE_PARAM = 'YYYYMMDD',
			FORMAT_UNIX_TIMESTAMP = 'x';
			
		/* Managing of the custom settings from context */
		ctrl.manageCustomSettingsFromContext = function(newContext) {
			ctrl.hasFutureDatesBlocked = newContext.$pxblockFutureDates.value;
			ctrl.hasPastDatesBlocked = newContext.$pxblockPastDates.value;
			ctrl.hasTimeHidden = newContext.$pxhideTime.value;
			ctrl.timeZone = (newContext.$pxtimeZone.value.value !== 'AUTO') ? newContext.$pxtimeZone.value.value : DEFAULT_TIMEZONE;
			ctrl.hidePresets = newContext.$pxhidePresets.value;
			ctrl.setAttributesToWidget(newContext);
		};
		
		/* Set specific properties to Predix component */
		ctrl.setAttributesToWidget = function(newContext) {
			ctrl.setBooleanAttributeOnlyIsTrue('block-future-dates', ctrl.hasFutureDatesBlocked);
			ctrl.setBooleanAttributeOnlyIsTrue('block-past-dates', ctrl.hasPastDatesBlocked);
			ctrl.setBooleanAttributeOnlyIsTrue('hide-time', ctrl.hasTimeHidden);
			ctrl.setBooleanAttributeOnlyIsTrue('hide-presets', ctrl.hidePresets);
			ctrl.manageTimeFormat(newContext);
			ctrl.manageTimeZone(newContext);
		};
		
		/* Set the boolean attribute only if true because Predix component doesn't want the attribute in its DOM node if false*/
		ctrl.setBooleanAttributeOnlyIsTrue = function(attribute, boolValue) {
			if (boolValue){
				dateTimeRangePanel.attr(attribute, boolValue);
			}
		};
		
		/* Managing of time format property */
		ctrl.manageTimeFormat = function(newContext) {
			var timeFormat = (newContext.$pxtimeFormat.value !== '') ? newContext.$pxtimeFormat.value : DEFAULT_TIMEFORMAT,
				isTimeFormatValid = ctrl.isTimeFormatValid(timeFormat, ctrl.timeZone),
				validTimeFormat = isTimeFormatValid ? timeFormat : DEFAULT_TIMEFORMAT;
			dateTimeRangePanel.attr('time-format', validTimeFormat);
			if (!isTimeFormatValid){
				console.log('Datetime Range Panel: invalid time format ' + timeFormat + '. It will be use the default time format: ' + DEFAULT_TIMEFORMAT);
				
			}
		};
		
		/* Check if time format is valid by using today's day to prevent XSS attacks */
		ctrl.isTimeFormatValid = function(timeFormat, timeZone) {
			var typedInMoment = Px.moment.tz(Px.moment().toISOString(), timeFormat, timeZone),
				invalidAt = typedInMoment.invalidAt();
			return (invalidAt !== -1 || !typedInMoment.isValid() ||!typedInMoment._isValid) ? false: true;
		};
		
		/* Managing of time zone property */
		ctrl.manageTimeZone = function(newContext) {
			dateTimeRangePanel.attr('time-zone', ctrl.timeZone);
		};
		
		/* Function to set the parameters on the page. */
		ctrl.sendParams = function(startDate, startDateEpoch, endDate, endDateEpoch) {
			if (mshPage){
				target.sendParamChanged(startDateParamToExport, startDate);
				target.sendParamChanged(endDateParamToExport, endDate);
				target.sendParamChanged(startDateEpocParamToExport, startDateEpoch);
				target.sendParamChanged(endDateEpocParamToExport, endDateEpoch);
			}
		};
		
		/* Function will be fired once the polymer component are loaded. */
		ctrl.whenPredixDateTimePanelLibraryLoaded = function() {
			ctrl.manageCustomSettingsFromContext(ctx);
			ctrl.manageImportedParams(ctx);
			ctrl.setEventHandler();
		};
		
		/* Action to fire on cancel click*/
		ctrl.actionOnCancel = function(target) {
			if (ctrl.selectedPreviousStartDate === undefined || ctrl.selectedPreviousEndDate === undefined){
				ctrl.selectedPreviousStartDate = target.fromBaseDate.toISOString();
				ctrl.selectedPreviousEndDate = target.toBaseDate.toISOString();
			}
			ctrl.setRangeProperty(ctrl.selectedPreviousStartDate, ctrl.selectedPreviousEndDate);
		};
		
		/* Action to fire on apply click*/
		ctrl.actionOnApply = function(target) {
			var selectedStartDate = target.fromMoment.toISOString(),
					selectedEndDate = target.toMoment.toISOString(),
					startDate = Px.moment(selectedStartDate).format(FORMAT_DATE_PARAM),
					startDateEpoch = Px.moment.utc(selectedStartDate).format(FORMAT_UNIX_TIMESTAMP),
					endDate = Px.moment(selectedEndDate).format(FORMAT_DATE_PARAM),
					endDateEpoch = Px.moment.utc(selectedEndDate).format(FORMAT_UNIX_TIMESTAMP);
			ctrl.selectedPreviousStartDate = selectedStartDate;
			ctrl.selectedPreviousEndDate = selectedEndDate;
			ctrl.sendParams(startDate, startDateEpoch, endDate, endDateEpoch);
		};
		
		/* Set event listeners to widget elements */
		ctrl.setEventHandler = function() {
			ctrl.setEventToDatetimeRangePanel();
		};
		
		/* Set event listeners to widget datetime panel */
		ctrl.setEventToDatetimeRangePanel = function() {
			dateTimeRangePanel[0].addEventListener('px-datetime-button-clicked', function(e) {
				if (e.detail.action){   // when click on apply button
					ctrl.actionOnApply(this);
				}else {  // when click on cancel button
					ctrl.actionOnCancel(this);
				}
			});
		};
		
		/* Check imported parameters: if defined, if empty */
		ctrl.getImportedParamFromContext = function(importedParam) {
			return (angular.isDefined(importedParam) && !angular.equals(importedParam.value, EMPTY_PARAM_VALUE)) ? importedParam.value : '';
		};
		
		/* Check of imported dates, both params for start-date and params for end-date
		- if paramDate and paramDateEpoch are invalid, use today's date;
		- if paramDateEpoch is valid and attribute hide-time is false, use paramDateEpoch;
		- if paramDateEpoch is invalid and paramDate is valid and hide-time is true, use paramDate
		*/
		ctrl.getValidDateToImport = function(pDate, pDateEpoch) {
			var isValidDate = moment(pDate,FORMAT_DATE_PARAM, true).isValid(),
				isValidDateEpoch = parseInt(pDateEpoch) && moment(pDateEpoch,FORMAT_UNIX_TIMESTAMP, true).isValid(),
				dateToYYYYMMDD = (isValidDate) ? parseInt(ctrl.getDateInYYYYMMDD(pDate)) : '',
				dateEpochToYYYYMMDD = (isValidDateEpoch) ? parseInt(ctrl.getDateInYYYYMMDD(new Date(pDateEpoch))) : '',
				isSameDate = (dateToYYYYMMDD === dateEpochToYYYYMMDD),
				useDateEpoch = isValidDateEpoch && (!ctrl.hasTimeHidden || isSameDate),
				useDate = isValidDate && (ctrl.hasTimeHidden || !isValidDateEpoch);
			return (useDateEpoch) ? ctrl.getDateEpochToImport(pDateEpoch) : (useDate ? ctrl.getDateToImport(pDate) : Px.moment().toISOString());
		};
		
		/* Return the date in input in YYYYMMDD format */
		ctrl.getDateInYYYYMMDD = function(pDate) {
			return parseInt(moment(pDate).format(FORMAT_DATE_PARAM));
		};
		
		/* Return the date after adding 7 days from date in input */
		ctrl.getDateAfterAddingSevenDays = function(pDate) {
			return moment(moment(pDate).add(7,'day').format(FORMAT_UNIX_TIMESTAMP), FORMAT_UNIX_TIMESTAMP).toISOString();
		};
		
		/* Return the date after subtracting 7 days from date in input */
		ctrl.getDateAfterSubtractingSevenDays = function(pDate) {
			return moment(moment(pDate).subtract(7,'day').format(FORMAT_UNIX_TIMESTAMP), FORMAT_UNIX_TIMESTAMP).toISOString();
		};
		
		/* Return the valide date by adding or substracting 7 days if necessary */
		ctrl.getValidDateForRange = function(isDateToChange, originalDate, validDateToSet, canAddDays) {
			if (isDateToChange){
				return canAddDays ? ctrl.getDateAfterAddingSevenDays(validDateToSet) : ctrl.getDateAfterSubtractingSevenDays(validDateToSet);
			}
			return originalDate;
		};
		
		/* Managing of both the param start-date and the param end-date.
		- if both the past dates and the future dates are blocked, use today's date
		- if the past dates aren't blocked, substract 7 days from param start-date
		- if the past dates are blocked and the future dates aren'blocked, add 7 days to param end-date
		*/
		ctrl.manageImportedDatesForRange = function(startDate, endDate) {
			var todayDate = Px.moment().toISOString(),
				todayDateInYYYYMMDD = ctrl.getDateInYYYYMMDD(todayDate),
				isSameDate = ctrl.getDateInYYYYMMDD(startDate) === ctrl.getDateInYYYYMMDD(endDate),
				isStartDateGreaterThanToday = ctrl.getDateInYYYYMMDD(startDate) > todayDateInYYYYMMDD,
				isStartDateLessThanToday = ctrl.getDateInYYYYMMDD(startDate) < todayDateInYYYYMMDD,
				isEndDateGreaterThanToday = ctrl.getDateInYYYYMMDD(endDate) > todayDateInYYYYMMDD,
				isEndDateLessThanToday = ctrl.getDateInYYYYMMDD(endDate) < todayDateInYYYYMMDD,
				isStartDateGreterThanEndDate = ctrl.getDateInYYYYMMDD(startDate) > ctrl.getDateInYYYYMMDD(endDate),
				validStartDate = startDate,
				validEndDate = endDate;
				
			if (ctrl.hasPastDatesBlocked && ctrl.hasFutureDatesBlocked){
				validStartDate = todayDate;
				validEndDate = todayDate;
			}else {
				if (!ctrl.hasPastDatesBlocked && !ctrl.hasFutureDatesBlocked){
					validStartDate = ctrl.getValidDateForRange(isSameDate, startDate, todayDate, false);
					validEndDate =  ctrl.getValidDateForRange(isStartDateGreterThanEndDate, endDate, startDate, true);
				}else if (ctrl.hasPastDatesBlocked && !ctrl.hasFutureDatesBlocked){
					if (isStartDateLessThanToday){
						validStartDate = todayDate;
						validEndDate = ctrl.getValidDateForRange(isEndDateLessThanToday, endDate, todayDate, true);
					}else {
						validEndDate = ctrl.getValidDateForRange(isEndDateLessThanToday, endDate, startDate, true);
					}
				}else if (!ctrl.hasPastDatesBlocked && ctrl.hasFutureDatesBlocked){
					if (isEndDateLessThanToday){
						validStartDate = ctrl.getValidDateForRange(isStartDateGreaterThanToday, startDate, endDate, false);
					}else {
						if (isEndDateGreaterThanToday){
							validEndDate = todayDate;
							validStartDate = ctrl.getValidDateForRange(isStartDateGreaterThanToday, startDate, todayDate, false);
						}
					}
				}
			}
			ctrl.setRangeProperty(validStartDate, validEndDate);
		};
		
		/* Set the range setting to Predix component */
		ctrl.setRangeProperty = function(startDate, endDate) {
			var fromHandler = Px.moment(startDate),
				toHandler = Px.moment(endDate),
				fromMonth = fromHandler.format('M'),
				toMonth = toHandler.format('M'),
				toDay = toHandler.format('D'),
				newtoBase = '',
				presetHandler = '',
				newFromBase = '';
			if (fromMonth === toMonth) {
				newFromBase = Px.moment(toHandler, dateTimeRangePanel[0].timeZone).subtract(toDay - 1, 'day');
				newtoBase = newFromBase;
			} else {
				newFromBase = Px.moment(toHandler, dateTimeRangePanel[0].timeZone).subtract(toMonth - fromMonth, 'month');
				newtoBase = Px.moment(fromHandler, dateTimeRangePanel[0].timeZone).add(toMonth - fromMonth, 'month');
			}
			dateTimeRangePanel[0].set('fromBaseDate',newFromBase);
			dateTimeRangePanel[0].set('toBaseDate',newtoBase);
			dateTimeRangePanel[0].set('fromMoment',fromHandler);
			dateTimeRangePanel[0].set('toMoment',toHandler);
			presetHandler = dateTimeRangePanel[0]._presetSelected.bind(dateTimeRangePanel[0]);
			dateTimeRangePanel[0].addEventListener('px-preset-selected', presetHandler);
		};
		
		/* Return the date epoch in ISO format string to use to export parameter */
		ctrl.getDateEpochToImport = function(dateEpoch) {
			return moment(dateEpoch, FORMAT_UNIX_TIMESTAMP).toISOString();
		};
		
		/* Return the date in ISO format string to use to export parameter */
		ctrl.getDateToImport = function(date) {
			return moment(moment(date, FORMAT_DATE_PARAM).utc().format(FORMAT_UNIX_TIMESTAMP), FORMAT_UNIX_TIMESTAMP).toISOString();
		};
		
		/* Managing of date parameters to import  */
		ctrl.manageImportedParams = function(context) {
			var SUFFIX_IMPORTED_PARAMS = '_PARAM_',
				startDateToImport = ctrl.getImportedParamFromContext(context[SUFFIX_IMPORTED_PARAMS + startDateParamToExport]),
				startDateEpochToImport = ctrl.getImportedParamFromContext(context[SUFFIX_IMPORTED_PARAMS + startDateEpocParamToExport]),
				endDateToImport = ctrl.getImportedParamFromContext(context[SUFFIX_IMPORTED_PARAMS + endDateParamToExport]),
				endDateEpochToImport = ctrl.getImportedParamFromContext(context[SUFFIX_IMPORTED_PARAMS + endDateEpocParamToExport]),
				startDateValidToImport = ctrl.getValidDateToImport(startDateToImport, startDateEpochToImport),
				endDateValidToImport = ctrl.getValidDateToImport(endDateToImport, endDateEpochToImport);
			ctrl.manageImportedDatesForRange(startDateValidToImport, endDateValidToImport);
		};
		
		/* Watch on widget context to observe parameter changing after loading */
		ctrl.setListenerOnDataChange = function() {
			$scope.$watch('DECISYON.target.content', function(newValue, oldValue) {
				if (!angular.equals(newValue.ctx, oldValue.ctx)) {
					ctrl.manageCustomSettingsFromContext(newValue.ctx);
					ctrl.manageImportedParams(newValue.ctx);
				}
			},true);
		};
		
		/* Initialize */
		ctrl.inizialize = function() {
			ctrl.widgetID = 'pxDateTimePanel_' + refObjId;
			ctrl.setListenerOnDataChange();
		};
		
		ctrl.inizialize();
	}
	
	PxDateTimeRangePanelCtrl.$inject = ['$scope', '$timeout', '$element'];
	
	DECISYON.ng.register.controller('pxDatetimeRangePanelCtrl', PxDateTimeRangePanelCtrl);

}());