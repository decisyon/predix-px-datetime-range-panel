//! moment.js locale configuration
//! locale : dhivehi (dv)
//! author : Jawish Hameed : https://github.com/jawish

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var months = [
        'ÃÂÃÂ¬ÃÂÃÂªÃÂÃÂ¦ÃÂÃÂ©',
        'ÃÂÃÂ¬ÃÂÃÂ°ÃÂÃÂªÃÂÃÂ¦ÃÂÃÂ©',
        'ÃÂÃÂ§ÃÂÃÂ¨ÃÂÃÂª',
        'ÃÂÃÂ­ÃÂÃÂ°ÃÂÃÂ©ÃÂÃÂª',
        'ÃÂÃÂ­',
        'ÃÂÃÂ«ÃÂÃÂ°',
        'ÃÂÃÂªÃÂÃÂ¦ÃÂÃÂ¨',
        'ÃÂÃÂ¯ÃÂÃÂ¦ÃÂÃÂ°ÃÂÃÂª',
        'ÃÂÃÂ¬ÃÂÃÂ°ÃÂÃÂ¬ÃÂÃÂ°ÃÂÃÂ¦ÃÂÃÂª',
        'ÃÂÃÂ®ÃÂÃÂ°ÃÂÃÂ¯ÃÂÃÂ¦ÃÂÃÂª',
        'ÃÂÃÂ®ÃÂÃÂ¬ÃÂÃÂ°ÃÂÃÂ¦ÃÂÃÂª',
        'ÃÂÃÂ¨ÃÂÃÂ¬ÃÂÃÂ°ÃÂÃÂ¦ÃÂÃÂª'
    ], weekdays = [
        'ÃÂÃÂ§ÃÂÃÂ¨ÃÂÃÂ°ÃÂÃÂ¦',
        'ÃÂÃÂ¯ÃÂÃÂ¦',
        'ÃÂÃÂ¦ÃÂÃÂ°ÃÂÃÂ§ÃÂÃÂ¦',
        'ÃÂÃÂªÃÂÃÂ¦',
        'ÃÂÃÂªÃÂÃÂ§ÃÂÃÂ°ÃÂÃÂ¦ÃÂÃÂ¨',
        'ÃÂÃÂªÃÂÃÂªÃÂÃÂª',
        'ÃÂÃÂ®ÃÂÃÂ¨ÃÂÃÂ¨ÃÂÃÂª'
    ];

    var dv = moment.defineLocale('dv', {
        months : months,
        monthsShort : months,
        weekdays : weekdays,
        weekdaysShort : weekdays,
        weekdaysMin : 'ÃÂÃÂ§ÃÂÃÂ¨_ÃÂÃÂ¯ÃÂÃÂ¦_ÃÂÃÂ¦ÃÂÃÂ°_ÃÂÃÂªÃÂÃÂ¦_ÃÂÃÂªÃÂÃÂ§_ÃÂÃÂªÃÂÃÂª_ÃÂÃÂ®ÃÂÃÂ¨'.split('_'),
        longDateFormat : {

            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'D/M/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        meridiemParse: /ÃÂÃÂ|ÃÂÃÂ/,
        isPM : function (input) {
            return 'ÃÂÃÂ' === input;
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 12) {
                return 'ÃÂÃÂ';
            } else {
                return 'ÃÂÃÂ';
            }
        },
        calendar : {
            sameDay : '[ÃÂÃÂ¨ÃÂÃÂ¦ÃÂÃÂª] LT',
            nextDay : '[ÃÂÃÂ§ÃÂÃÂ¦ÃÂÃÂ§] LT',
            nextWeek : 'dddd LT',
            lastDay : '[ÃÂÃÂ¨ÃÂÃÂ°ÃÂÃÂ¬] LT',
            lastWeek : '[ÃÂÃÂ§ÃÂÃÂ¨ÃÂÃÂªÃÂÃÂ¨] dddd LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂÃÂ¬ÃÂÃÂ­ÃÂÃÂ¦ÃÂÃÂ¨ %s',
            past : 'ÃÂÃÂªÃÂÃÂ¨ÃÂÃÂ° %s',
            s : 'ÃÂÃÂ¨ÃÂÃÂªÃÂÃÂ°ÃÂÃÂªÃÂÃÂ®ÃÂÃÂ¬ÃÂÃÂ°',
            m : 'ÃÂÃÂ¨ÃÂÃÂ¨ÃÂÃÂ¬ÃÂÃÂ°',
            mm : 'ÃÂÃÂ¨ÃÂÃÂ¨ÃÂÃÂª %d',
            h : 'ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂ¨ÃÂÃÂ¬ÃÂÃÂ°',
            hh : 'ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂ¨ÃÂÃÂª %d',
            d : 'ÃÂÃÂªÃÂÃÂ¦ÃÂÃÂ¬ÃÂÃÂ°',
            dd : 'ÃÂÃÂªÃÂÃÂ¦ÃÂÃÂ° %d',
            M : 'ÃÂÃÂ¦ÃÂÃÂ¬ÃÂÃÂ°',
            MM : 'ÃÂÃÂ¦ÃÂÃÂ° %d',
            y : 'ÃÂÃÂ¦ÃÂÃÂ¦ÃÂÃÂ¬ÃÂÃÂ°',
            yy : 'ÃÂÃÂ¦ÃÂÃÂ¦ÃÂÃÂª %d'
        },
        preparse: function (string) {
            return string.replace(/ÃÂ/g, ',');
        },
        postformat: function (string) {
            return string.replace(/,/g, 'ÃÂ');
        },
        week : {
            dow : 7,  // Sunday is the first day of the week.
            doy : 12  // The week that contains Jan 1st is the first week of the year.
        }
    });

    return dv;

}));