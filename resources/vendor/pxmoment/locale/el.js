//! moment.js locale configuration
//! locale : modern greek (el)
//! author : Aggelos Karalias : https://github.com/mehiel

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';

    function isFunction(input) {
        return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
    }


    var el = moment.defineLocale('el', {
        monthsNominativeEl : 'ÃÂÃÂ±ÃÂ½ÃÂ¿ÃÂÃÂ¬ÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂ¦ÃÂµÃÂ²ÃÂÃÂ¿ÃÂÃÂ¬ÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂ¬ÃÂÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂÃÂÃÂ¯ÃÂ»ÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂ¬ÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂÃÂ½ÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂÃÂ»ÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂÃÂ³ÃÂ¿ÃÂÃÂÃÂÃÂ¿ÃÂ_ÃÂ£ÃÂµÃÂÃÂÃÂ­ÃÂ¼ÃÂ²ÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂºÃÂÃÂÃÂ²ÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂ­ÃÂ¼ÃÂ²ÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂµÃÂºÃÂ­ÃÂ¼ÃÂ²ÃÂÃÂ¹ÃÂ¿ÃÂ'.split('_'),
        monthsGenitiveEl : 'ÃÂÃÂ±ÃÂ½ÃÂ¿ÃÂÃÂ±ÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂ¦ÃÂµÃÂ²ÃÂÃÂ¿ÃÂÃÂ±ÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂ±ÃÂÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂÃÂÃÂ¹ÃÂ»ÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂ±ÃÂÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂÃÂ½ÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂÃÂ»ÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂÃÂ³ÃÂ¿ÃÂÃÂÃÂÃÂ¿ÃÂ_ÃÂ£ÃÂµÃÂÃÂÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂºÃÂÃÂÃÂ²ÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂµÃÂºÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¯ÃÂ¿ÃÂ'.split('_'),
        months : function (momentToFormat, format) {
            if (/D/.test(format.substring(0, format.indexOf('MMMM')))) { // if there is a day number before 'MMMM'
                return this._monthsGenitiveEl[momentToFormat.month()];
            } else {
                return this._monthsNominativeEl[momentToFormat.month()];
            }
        },
        monthsShort : 'ÃÂÃÂ±ÃÂ½_ÃÂ¦ÃÂµÃÂ²_ÃÂÃÂ±ÃÂ_ÃÂÃÂÃÂ_ÃÂÃÂ±ÃÂ_ÃÂÃÂ¿ÃÂÃÂ½_ÃÂÃÂ¿ÃÂÃÂ»_ÃÂÃÂÃÂ³_ÃÂ£ÃÂµÃÂ_ÃÂÃÂºÃÂ_ÃÂÃÂ¿ÃÂµ_ÃÂÃÂµÃÂº'.split('_'),
        weekdays : 'ÃÂÃÂÃÂÃÂ¹ÃÂ±ÃÂºÃÂ®_ÃÂÃÂµÃÂÃÂÃÂ­ÃÂÃÂ±_ÃÂ¤ÃÂÃÂ¯ÃÂÃÂ·_ÃÂ¤ÃÂµÃÂÃÂ¬ÃÂÃÂÃÂ·_ÃÂ ÃÂ­ÃÂ¼ÃÂÃÂÃÂ·_ÃÂ ÃÂ±ÃÂÃÂ±ÃÂÃÂºÃÂµÃÂÃÂ®_ÃÂ£ÃÂ¬ÃÂ²ÃÂ²ÃÂ±ÃÂÃÂ¿'.split('_'),
        weekdaysShort : 'ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ_ÃÂ¤ÃÂÃÂ¹_ÃÂ¤ÃÂµÃÂ_ÃÂ ÃÂµÃÂ¼_ÃÂ ÃÂ±ÃÂ_ÃÂ£ÃÂ±ÃÂ²'.split('_'),
        weekdaysMin : 'ÃÂÃÂ_ÃÂÃÂµ_ÃÂ¤ÃÂ_ÃÂ¤ÃÂµ_ÃÂ ÃÂµ_ÃÂ ÃÂ±_ÃÂ£ÃÂ±'.split('_'),
        meridiem : function (hours, minutes, isLower) {
            if (hours > 11) {
                return isLower ? 'ÃÂ¼ÃÂ¼' : 'ÃÂÃÂ';
            } else {
                return isLower ? 'ÃÂÃÂ¼' : 'ÃÂ ÃÂ';
            }
        },
        isPM : function (input) {
            return ((input + '').toLowerCase()[0] === 'ÃÂ¼');
        },
        meridiemParse : /[ÃÂ ÃÂ]\.?ÃÂ?\.?/i,
        longDateFormat : {
            LT : 'h:mm A',
            LTS : 'h:mm:ss A',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY h:mm A',
            LLLL : 'dddd, D MMMM YYYY h:mm A'
        },
        calendarEl : {
            sameDay : '[ÃÂ£ÃÂ®ÃÂ¼ÃÂµÃÂÃÂ± {}] LT',
            nextDay : '[ÃÂÃÂÃÂÃÂ¹ÃÂ¿ {}] LT',
            nextWeek : 'dddd [{}] LT',
            lastDay : '[ÃÂ§ÃÂ¸ÃÂµÃÂ {}] LT',
            lastWeek : function () {
                switch (this.day()) {
                    case 6:
                        return '[ÃÂÃÂ¿ ÃÂÃÂÃÂ¿ÃÂ·ÃÂ³ÃÂ¿ÃÂÃÂ¼ÃÂµÃÂ½ÃÂ¿] dddd [{}] LT';
                    default:
                        return '[ÃÂÃÂ·ÃÂ½ ÃÂÃÂÃÂ¿ÃÂ·ÃÂ³ÃÂ¿ÃÂÃÂ¼ÃÂµÃÂ½ÃÂ·] dddd [{}] LT';
                }
            },
            sameElse : 'L'
        },
        calendar : function (key, mom) {
            var output = this._calendarEl[key],
                hours = mom && mom.hours();
            if (isFunction(output)) {
                output = output.apply(mom);
            }
            return output.replace('{}', (hours % 12 === 1 ? 'ÃÂÃÂÃÂ·' : 'ÃÂÃÂÃÂ¹ÃÂ'));
        },
        relativeTime : {
            future : 'ÃÂÃÂµ %s',
            past : '%s ÃÂÃÂÃÂ¹ÃÂ½',
            s : 'ÃÂ»ÃÂ¯ÃÂ³ÃÂ± ÃÂ´ÃÂµÃÂÃÂÃÂµÃÂÃÂÃÂ»ÃÂµÃÂÃÂÃÂ±',
            m : 'ÃÂ­ÃÂ½ÃÂ± ÃÂ»ÃÂµÃÂÃÂÃÂ',
            mm : '%d ÃÂ»ÃÂµÃÂÃÂÃÂ¬',
            h : 'ÃÂ¼ÃÂ¯ÃÂ± ÃÂÃÂÃÂ±',
            hh : '%d ÃÂÃÂÃÂµÃÂ',
            d : 'ÃÂ¼ÃÂ¯ÃÂ± ÃÂ¼ÃÂ­ÃÂÃÂ±',
            dd : '%d ÃÂ¼ÃÂ­ÃÂÃÂµÃÂ',
            M : 'ÃÂ­ÃÂ½ÃÂ±ÃÂ ÃÂ¼ÃÂ®ÃÂ½ÃÂ±ÃÂ',
            MM : '%d ÃÂ¼ÃÂ®ÃÂ½ÃÂµÃÂ',
            y : 'ÃÂ­ÃÂ½ÃÂ±ÃÂ ÃÂÃÂÃÂÃÂ½ÃÂ¿ÃÂ',
            yy : '%d ÃÂÃÂÃÂÃÂ½ÃÂ¹ÃÂ±'
        },
        ordinalParse: /\d{1,2}ÃÂ·/,
        ordinal: '%dÃÂ·',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4st is the first week of the year.
        }
    });

    return el;

}));