//! moment.js locale configuration
//! locale : Persian (fa)
//! author : Ebrahim Byagowi : https://github.com/ebraminio

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var symbolMap = {
        '1': 'ÃÂ±',
        '2': 'ÃÂ²',
        '3': 'ÃÂ³',
        '4': 'ÃÂ´',
        '5': 'ÃÂµ',
        '6': 'ÃÂ¶',
        '7': 'ÃÂ·',
        '8': 'ÃÂ¸',
        '9': 'ÃÂ¹',
        '0': 'ÃÂ°'
    }, numberMap = {
        'ÃÂ±': '1',
        'ÃÂ²': '2',
        'ÃÂ³': '3',
        'ÃÂ´': '4',
        'ÃÂµ': '5',
        'ÃÂ¶': '6',
        'ÃÂ·': '7',
        'ÃÂ¸': '8',
        'ÃÂ¹': '9',
        'ÃÂ°': '0'
    };

    var fa = moment.defineLocale('fa', {
        months : 'ÃÂÃÂ§ÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ¢ÃÂÃÂ±ÃÂÃÂ_ÃÂÃÂ_ÃÂÃÂÃÂ¦ÃÂ_ÃÂÃÂÃÂ¦ÃÂÃÂ_ÃÂ§ÃÂÃÂª_ÃÂ³ÃÂ¾ÃÂªÃÂ§ÃÂÃÂ¨ÃÂ±_ÃÂ§ÃÂ©ÃÂªÃÂ¨ÃÂ±_ÃÂÃÂÃÂ§ÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂ³ÃÂ§ÃÂÃÂ¨ÃÂ±'.split('_'),
        monthsShort : 'ÃÂÃÂ§ÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ¢ÃÂÃÂ±ÃÂÃÂ_ÃÂÃÂ_ÃÂÃÂÃÂ¦ÃÂ_ÃÂÃÂÃÂ¦ÃÂÃÂ_ÃÂ§ÃÂÃÂª_ÃÂ³ÃÂ¾ÃÂªÃÂ§ÃÂÃÂ¨ÃÂ±_ÃÂ§ÃÂ©ÃÂªÃÂ¨ÃÂ±_ÃÂÃÂÃÂ§ÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂ³ÃÂ§ÃÂÃÂ¨ÃÂ±'.split('_'),
        weekdays : 'ÃÂÃÂ©\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¯ÃÂÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ³ÃÂ\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂÃÂÃÂ§ÃÂ±ÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¾ÃÂÃÂ¬\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¬ÃÂÃÂ¹ÃÂ_ÃÂ´ÃÂÃÂ¨ÃÂ'.split('_'),
        weekdaysShort : 'ÃÂÃÂ©\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¯ÃÂÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ³ÃÂ\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂÃÂÃÂ§ÃÂ±ÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¾ÃÂÃÂ¬\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¬ÃÂÃÂ¹ÃÂ_ÃÂ´ÃÂÃÂ¨ÃÂ'.split('_'),
        weekdaysMin : 'ÃÂ_ÃÂ¯_ÃÂ³_ÃÂ_ÃÂ¾_ÃÂ¬_ÃÂ´'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        meridiemParse: /ÃÂÃÂ¨ÃÂ ÃÂ§ÃÂ² ÃÂ¸ÃÂÃÂ±|ÃÂ¨ÃÂ¹ÃÂ¯ ÃÂ§ÃÂ² ÃÂ¸ÃÂÃÂ±/,
        isPM: function (input) {
            return /ÃÂ¨ÃÂ¹ÃÂ¯ ÃÂ§ÃÂ² ÃÂ¸ÃÂÃÂ±/.test(input);
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 12) {
                return 'ÃÂÃÂ¨ÃÂ ÃÂ§ÃÂ² ÃÂ¸ÃÂÃÂ±';
            } else {
                return 'ÃÂ¨ÃÂ¹ÃÂ¯ ÃÂ§ÃÂ² ÃÂ¸ÃÂÃÂ±';
            }
        },
        calendar : {
            sameDay : '[ÃÂ§ÃÂÃÂ±ÃÂÃÂ² ÃÂ³ÃÂ§ÃÂ¹ÃÂª] LT',
            nextDay : '[ÃÂÃÂ±ÃÂ¯ÃÂ§ ÃÂ³ÃÂ§ÃÂ¹ÃÂª] LT',
            nextWeek : 'dddd [ÃÂ³ÃÂ§ÃÂ¹ÃÂª] LT',
            lastDay : '[ÃÂ¯ÃÂÃÂ±ÃÂÃÂ² ÃÂ³ÃÂ§ÃÂ¹ÃÂª] LT',
            lastWeek : 'dddd [ÃÂ¾ÃÂÃÂ´] [ÃÂ³ÃÂ§ÃÂ¹ÃÂª] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂ¯ÃÂ± %s',
            past : '%s ÃÂ¾ÃÂÃÂ´',
            s : 'ÃÂÃÂÃÂ¯ÃÂÃÂ ÃÂ«ÃÂ§ÃÂÃÂÃÂ',
            m : 'ÃÂÃÂ© ÃÂ¯ÃÂÃÂÃÂÃÂ',
            mm : '%d ÃÂ¯ÃÂÃÂÃÂÃÂ',
            h : 'ÃÂÃÂ© ÃÂ³ÃÂ§ÃÂ¹ÃÂª',
            hh : '%d ÃÂ³ÃÂ§ÃÂ¹ÃÂª',
            d : 'ÃÂÃÂ© ÃÂ±ÃÂÃÂ²',
            dd : '%d ÃÂ±ÃÂÃÂ²',
            M : 'ÃÂÃÂ© ÃÂÃÂ§ÃÂ',
            MM : '%d ÃÂÃÂ§ÃÂ',
            y : 'ÃÂÃÂ© ÃÂ³ÃÂ§ÃÂ',
            yy : '%d ÃÂ³ÃÂ§ÃÂ'
        },
        preparse: function (string) {
            return string.replace(/[ÃÂ°-ÃÂ¹]/g, function (match) {
                return numberMap[match];
            }).replace(/ÃÂ/g, ',');
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return symbolMap[match];
            }).replace(/,/g, 'ÃÂ');
        },
        ordinalParse: /\d{1,2}ÃÂ/,
        ordinal : '%dÃÂ',
        week : {
            dow : 6, // Saturday is the first day of the week.
            doy : 12 // The week that contains Jan 1st is the first week of the year.
        }
    });

    return fa;

}));