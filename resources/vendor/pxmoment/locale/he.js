//! moment.js locale configuration
//! locale : Hebrew (he)
//! author : Tomer Cohen : https://github.com/tomer
//! author : Moshe Simantov : https://github.com/DevelopmentIL
//! author : Tal Ater : https://github.com/TalAter

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var he = moment.defineLocale('he', {
        months : 'ÃÂÃÂ ÃÂÃÂÃÂ¨_ÃÂ¤ÃÂÃÂ¨ÃÂÃÂÃÂ¨_ÃÂÃÂ¨ÃÂ¥_ÃÂÃÂ¤ÃÂ¨ÃÂÃÂ_ÃÂÃÂÃÂ_ÃÂÃÂÃÂ ÃÂ_ÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂ¡ÃÂ_ÃÂ¡ÃÂ¤ÃÂÃÂÃÂÃÂ¨_ÃÂÃÂÃÂ§ÃÂÃÂÃÂÃÂ¨_ÃÂ ÃÂÃÂÃÂÃÂÃÂ¨_ÃÂÃÂ¦ÃÂÃÂÃÂ¨'.split('_'),
        monthsShort : 'ÃÂÃÂ ÃÂÃÂ³_ÃÂ¤ÃÂÃÂ¨ÃÂ³_ÃÂÃÂ¨ÃÂ¥_ÃÂÃÂ¤ÃÂ¨ÃÂ³_ÃÂÃÂÃÂ_ÃÂÃÂÃÂ ÃÂ_ÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂ³_ÃÂ¡ÃÂ¤ÃÂÃÂ³_ÃÂÃÂÃÂ§ÃÂ³_ÃÂ ÃÂÃÂÃÂ³_ÃÂÃÂ¦ÃÂÃÂ³'.split('_'),
        weekdays : 'ÃÂ¨ÃÂÃÂ©ÃÂÃÂ_ÃÂ©ÃÂ ÃÂ_ÃÂ©ÃÂÃÂÃÂ©ÃÂ_ÃÂ¨ÃÂÃÂÃÂ¢ÃÂ_ÃÂÃÂÃÂÃÂ©ÃÂ_ÃÂ©ÃÂÃÂ©ÃÂ_ÃÂ©ÃÂÃÂª'.split('_'),
        weekdaysShort : 'ÃÂÃÂ³_ÃÂÃÂ³_ÃÂÃÂ³_ÃÂÃÂ³_ÃÂÃÂ³_ÃÂÃÂ³_ÃÂ©ÃÂ³'.split('_'),
        weekdaysMin : 'ÃÂ_ÃÂ_ÃÂ_ÃÂ_ÃÂ_ÃÂ_ÃÂ©'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D [ÃÂ]MMMM YYYY',
            LLL : 'D [ÃÂ]MMMM YYYY HH:mm',
            LLLL : 'dddd, D [ÃÂ]MMMM YYYY HH:mm',
            l : 'D/M/YYYY',
            ll : 'D MMM YYYY',
            lll : 'D MMM YYYY HH:mm',
            llll : 'ddd, D MMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[ÃÂÃÂÃÂÃÂ ÃÂÃÂ¾]LT',
            nextDay : '[ÃÂÃÂÃÂ¨ ÃÂÃÂ¾]LT',
            nextWeek : 'dddd [ÃÂÃÂ©ÃÂ¢ÃÂ] LT',
            lastDay : '[ÃÂÃÂªÃÂÃÂÃÂ ÃÂÃÂ¾]LT',
            lastWeek : '[ÃÂÃÂÃÂÃÂ] dddd [ÃÂÃÂÃÂÃÂ¨ÃÂÃÂ ÃÂÃÂ©ÃÂ¢ÃÂ] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂÃÂ¢ÃÂÃÂ %s',
            past : 'ÃÂÃÂ¤ÃÂ ÃÂ %s',
            s : 'ÃÂÃÂ¡ÃÂ¤ÃÂ¨ ÃÂ©ÃÂ ÃÂÃÂÃÂª',
            m : 'ÃÂÃÂ§ÃÂ',
            mm : '%d ÃÂÃÂ§ÃÂÃÂª',
            h : 'ÃÂ©ÃÂ¢ÃÂ',
            hh : function (number) {
                if (number === 2) {
                    return 'ÃÂ©ÃÂ¢ÃÂªÃÂÃÂÃÂ';
                }
                return number + ' ÃÂ©ÃÂ¢ÃÂÃÂª';
            },
            d : 'ÃÂÃÂÃÂ',
            dd : function (number) {
                if (number === 2) {
                    return 'ÃÂÃÂÃÂÃÂÃÂÃÂ';
                }
                return number + ' ÃÂÃÂÃÂÃÂ';
            },
            M : 'ÃÂÃÂÃÂÃÂ©',
            MM : function (number) {
                if (number === 2) {
                    return 'ÃÂÃÂÃÂÃÂ©ÃÂÃÂÃÂ';
                }
                return number + ' ÃÂÃÂÃÂÃÂ©ÃÂÃÂ';
            },
            y : 'ÃÂ©ÃÂ ÃÂ',
            yy : function (number) {
                if (number === 2) {
                    return 'ÃÂ©ÃÂ ÃÂªÃÂÃÂÃÂ';
                } else if (number % 10 === 0 && number !== 10) {
                    return number + ' ÃÂ©ÃÂ ÃÂ';
                }
                return number + ' ÃÂ©ÃÂ ÃÂÃÂ';
            }
        },
        meridiemParse: /ÃÂÃÂÃÂ"ÃÂ¦|ÃÂÃÂ¤ÃÂ ÃÂ"ÃÂ¦|ÃÂÃÂÃÂ¨ÃÂ ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂÃÂ|ÃÂÃÂ¤ÃÂ ÃÂ ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂÃÂ|ÃÂÃÂ¤ÃÂ ÃÂÃÂª ÃÂÃÂÃÂ§ÃÂ¨|ÃÂÃÂÃÂÃÂ§ÃÂ¨|ÃÂÃÂ¢ÃÂ¨ÃÂ/i,
        isPM : function (input) {
            return /^(ÃÂÃÂÃÂ"ÃÂ¦|ÃÂÃÂÃÂ¨ÃÂ ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂÃÂ|ÃÂÃÂ¢ÃÂ¨ÃÂ)$/.test(input);
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 5) {
                return 'ÃÂÃÂ¤ÃÂ ÃÂÃÂª ÃÂÃÂÃÂ§ÃÂ¨';
            } else if (hour < 10) {
                return 'ÃÂÃÂÃÂÃÂ§ÃÂ¨';
            } else if (hour < 12) {
                return isLower ? 'ÃÂÃÂ¤ÃÂ ÃÂ"ÃÂ¦' : 'ÃÂÃÂ¤ÃÂ ÃÂ ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂÃÂ';
            } else if (hour < 18) {
                return isLower ? 'ÃÂÃÂÃÂ"ÃÂ¦' : 'ÃÂÃÂÃÂ¨ÃÂ ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂÃÂ';
            } else {
                return 'ÃÂÃÂ¢ÃÂ¨ÃÂ';
            }
        }
    });

    return he;

}));