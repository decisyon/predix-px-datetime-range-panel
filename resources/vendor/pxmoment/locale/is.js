//! moment.js locale configuration
//! locale : icelandic (is)
//! author : Hinrik ÃÂrn SigurÃÂ°sson : https://github.com/hinrik

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    function plural(n) {
        if (n % 100 === 11) {
            return true;
        } else if (n % 10 === 1) {
            return false;
        }
        return true;
    }
    function translate(number, withoutSuffix, key, isFuture) {
        var result = number + ' ';
        switch (key) {
        case 's':
            return withoutSuffix || isFuture ? 'nokkrar sekÃÂºndur' : 'nokkrum sekÃÂºndum';
        case 'm':
            return withoutSuffix ? 'mÃÂ­nÃÂºta' : 'mÃÂ­nÃÂºtu';
        case 'mm':
            if (plural(number)) {
                return result + (withoutSuffix || isFuture ? 'mÃÂ­nÃÂºtur' : 'mÃÂ­nÃÂºtum');
            } else if (withoutSuffix) {
                return result + 'mÃÂ­nÃÂºta';
            }
            return result + 'mÃÂ­nÃÂºtu';
        case 'hh':
            if (plural(number)) {
                return result + (withoutSuffix || isFuture ? 'klukkustundir' : 'klukkustundum');
            }
            return result + 'klukkustund';
        case 'd':
            if (withoutSuffix) {
                return 'dagur';
            }
            return isFuture ? 'dag' : 'degi';
        case 'dd':
            if (plural(number)) {
                if (withoutSuffix) {
                    return result + 'dagar';
                }
                return result + (isFuture ? 'daga' : 'dÃÂ¶gum');
            } else if (withoutSuffix) {
                return result + 'dagur';
            }
            return result + (isFuture ? 'dag' : 'degi');
        case 'M':
            if (withoutSuffix) {
                return 'mÃÂ¡nuÃÂ°ur';
            }
            return isFuture ? 'mÃÂ¡nuÃÂ°' : 'mÃÂ¡nuÃÂ°i';
        case 'MM':
            if (plural(number)) {
                if (withoutSuffix) {
                    return result + 'mÃÂ¡nuÃÂ°ir';
                }
                return result + (isFuture ? 'mÃÂ¡nuÃÂ°i' : 'mÃÂ¡nuÃÂ°um');
            } else if (withoutSuffix) {
                return result + 'mÃÂ¡nuÃÂ°ur';
            }
            return result + (isFuture ? 'mÃÂ¡nuÃÂ°' : 'mÃÂ¡nuÃÂ°i');
        case 'y':
            return withoutSuffix || isFuture ? 'ÃÂ¡r' : 'ÃÂ¡ri';
        case 'yy':
            if (plural(number)) {
                return result + (withoutSuffix || isFuture ? 'ÃÂ¡r' : 'ÃÂ¡rum');
            }
            return result + (withoutSuffix || isFuture ? 'ÃÂ¡r' : 'ÃÂ¡ri');
        }
    }

    var is = moment.defineLocale('is', {
        months : 'janÃÂºar_febrÃÂºar_mars_aprÃÂ­l_maÃÂ­_jÃÂºnÃÂ­_jÃÂºlÃÂ­_ÃÂ¡gÃÂºst_september_oktÃÂ³ber_nÃÂ³vember_desember'.split('_'),
        monthsShort : 'jan_feb_mar_apr_maÃÂ­_jÃÂºn_jÃÂºl_ÃÂ¡gÃÂº_sep_okt_nÃÂ³v_des'.split('_'),
        weekdays : 'sunnudagur_mÃÂ¡nudagur_ÃÂ¾riÃÂ°judagur_miÃÂ°vikudagur_fimmtudagur_fÃÂ¶studagur_laugardagur'.split('_'),
        weekdaysShort : 'sun_mÃÂ¡n_ÃÂ¾ri_miÃÂ°_fim_fÃÂ¶s_lau'.split('_'),
        weekdaysMin : 'Su_MÃÂ¡_ÃÂr_Mi_Fi_FÃÂ¶_La'.split('_'),
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY [kl.] H:mm',
            LLLL : 'dddd, D. MMMM YYYY [kl.] H:mm'
        },
        calendar : {
            sameDay : '[ÃÂ­ dag kl.] LT',
            nextDay : '[ÃÂ¡ morgun kl.] LT',
            nextWeek : 'dddd [kl.] LT',
            lastDay : '[ÃÂ­ gÃÂ¦r kl.] LT',
            lastWeek : '[sÃÂ­ÃÂ°asta] dddd [kl.] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'eftir %s',
            past : 'fyrir %s sÃÂ­ÃÂ°an',
            s : translate,
            m : translate,
            mm : translate,
            h : 'klukkustund',
            hh : translate,
            d : translate,
            dd : translate,
            M : translate,
            MM : translate,
            y : translate,
            yy : translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    return is;

}));