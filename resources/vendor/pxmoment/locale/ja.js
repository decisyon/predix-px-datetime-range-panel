//! moment.js locale configuration
//! locale : japanese (ja)
//! author : LI Long : https://github.com/baryon

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var ja = moment.defineLocale('ja', {
        months : '1Ã¦ÂÂ_2Ã¦ÂÂ_3Ã¦ÂÂ_4Ã¦ÂÂ_5Ã¦ÂÂ_6Ã¦ÂÂ_7Ã¦ÂÂ_8Ã¦ÂÂ_9Ã¦ÂÂ_10Ã¦ÂÂ_11Ã¦ÂÂ_12Ã¦ÂÂ'.split('_'),
        monthsShort : '1Ã¦ÂÂ_2Ã¦ÂÂ_3Ã¦ÂÂ_4Ã¦ÂÂ_5Ã¦ÂÂ_6Ã¦ÂÂ_7Ã¦ÂÂ_8Ã¦ÂÂ_9Ã¦ÂÂ_10Ã¦ÂÂ_11Ã¦ÂÂ_12Ã¦ÂÂ'.split('_'),
        weekdays : 'Ã¦ÂÂ¥Ã¦ÂÂÃ¦ÂÂ¥_Ã¦ÂÂÃ¦ÂÂÃ¦ÂÂ¥_Ã§ÂÂ«Ã¦ÂÂÃ¦ÂÂ¥_Ã¦Â°Â´Ã¦ÂÂÃ¦ÂÂ¥_Ã¦ÂÂ¨Ã¦ÂÂÃ¦ÂÂ¥_Ã©ÂÂÃ¦ÂÂÃ¦ÂÂ¥_Ã¥ÂÂÃ¦ÂÂÃ¦ÂÂ¥'.split('_'),
        weekdaysShort : 'Ã¦ÂÂ¥_Ã¦ÂÂ_Ã§ÂÂ«_Ã¦Â°Â´_Ã¦ÂÂ¨_Ã©ÂÂ_Ã¥ÂÂ'.split('_'),
        weekdaysMin : 'Ã¦ÂÂ¥_Ã¦ÂÂ_Ã§ÂÂ«_Ã¦Â°Â´_Ã¦ÂÂ¨_Ã©ÂÂ_Ã¥ÂÂ'.split('_'),
        longDateFormat : {
            LT : 'AhÃ¦ÂÂmÃ¥ÂÂ',
            LTS : 'AhÃ¦ÂÂmÃ¥ÂÂsÃ§Â§Â',
            L : 'YYYY/MM/DD',
            LL : 'YYYYÃ¥Â¹Â´MÃ¦ÂÂDÃ¦ÂÂ¥',
            LLL : 'YYYYÃ¥Â¹Â´MÃ¦ÂÂDÃ¦ÂÂ¥AhÃ¦ÂÂmÃ¥ÂÂ',
            LLLL : 'YYYYÃ¥Â¹Â´MÃ¦ÂÂDÃ¦ÂÂ¥AhÃ¦ÂÂmÃ¥ÂÂ dddd'
        },
        meridiemParse: /Ã¥ÂÂÃ¥ÂÂ|Ã¥ÂÂÃ¥Â¾Â/i,
        isPM : function (input) {
            return input === 'Ã¥ÂÂÃ¥Â¾Â';
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 12) {
                return 'Ã¥ÂÂÃ¥ÂÂ';
            } else {
                return 'Ã¥ÂÂÃ¥Â¾Â';
            }
        },
        calendar : {
            sameDay : '[Ã¤Â»ÂÃ¦ÂÂ¥] LT',
            nextDay : '[Ã¦ÂÂÃ¦ÂÂ¥] LT',
            nextWeek : '[Ã¦ÂÂ¥Ã©ÂÂ±]dddd LT',
            lastDay : '[Ã¦ÂÂ¨Ã¦ÂÂ¥] LT',
            lastWeek : '[Ã¥ÂÂÃ©ÂÂ±]dddd LT',
            sameElse : 'L'
        },
        ordinalParse : /\d{1,2}Ã¦ÂÂ¥/,
        ordinal : function (number, period) {
            switch (period) {
            case 'd':
            case 'D':
            case 'DDD':
                return number + 'Ã¦ÂÂ¥';
            default:
                return number;
            }
        },
        relativeTime : {
            future : '%sÃ¥Â¾Â',
            past : '%sÃ¥ÂÂ',
            s : 'Ã¦ÂÂ°Ã§Â§Â',
            m : '1Ã¥ÂÂ',
            mm : '%dÃ¥ÂÂ',
            h : '1Ã¦ÂÂÃ©ÂÂ',
            hh : '%dÃ¦ÂÂÃ©ÂÂ',
            d : '1Ã¦ÂÂ¥',
            dd : '%dÃ¦ÂÂ¥',
            M : '1Ã£ÂÂ¶Ã¦ÂÂ',
            MM : '%dÃ£ÂÂ¶Ã¦ÂÂ',
            y : '1Ã¥Â¹Â´',
            yy : '%dÃ¥Â¹Â´'
        }
    });

    return ja;

}));