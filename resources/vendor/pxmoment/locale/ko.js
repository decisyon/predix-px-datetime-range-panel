//! moment.js locale configuration
//! locale : korean (ko)
//!
//! authors
//!
//! - Kyungwook, Park : https://github.com/kyungw00k
//! - Jeeeyul Lee <jeeeyul@gmail.com>

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var ko = moment.defineLocale('ko', {
        months : '1Ã¬ÂÂ_2Ã¬ÂÂ_3Ã¬ÂÂ_4Ã¬ÂÂ_5Ã¬ÂÂ_6Ã¬ÂÂ_7Ã¬ÂÂ_8Ã¬ÂÂ_9Ã¬ÂÂ_10Ã¬ÂÂ_11Ã¬ÂÂ_12Ã¬ÂÂ'.split('_'),
        monthsShort : '1Ã¬ÂÂ_2Ã¬ÂÂ_3Ã¬ÂÂ_4Ã¬ÂÂ_5Ã¬ÂÂ_6Ã¬ÂÂ_7Ã¬ÂÂ_8Ã¬ÂÂ_9Ã¬ÂÂ_10Ã¬ÂÂ_11Ã¬ÂÂ_12Ã¬ÂÂ'.split('_'),
        weekdays : 'Ã¬ÂÂ¼Ã¬ÂÂÃ¬ÂÂ¼_Ã¬ÂÂÃ¬ÂÂÃ¬ÂÂ¼_Ã­ÂÂÃ¬ÂÂÃ¬ÂÂ¼_Ã¬ÂÂÃ¬ÂÂÃ¬ÂÂ¼_Ã«ÂªÂ©Ã¬ÂÂÃ¬ÂÂ¼_ÃªÂ¸ÂÃ¬ÂÂÃ¬ÂÂ¼_Ã­ÂÂ Ã¬ÂÂÃ¬ÂÂ¼'.split('_'),
        weekdaysShort : 'Ã¬ÂÂ¼_Ã¬ÂÂ_Ã­ÂÂ_Ã¬ÂÂ_Ã«ÂªÂ©_ÃªÂ¸Â_Ã­ÂÂ '.split('_'),
        weekdaysMin : 'Ã¬ÂÂ¼_Ã¬ÂÂ_Ã­ÂÂ_Ã¬ÂÂ_Ã«ÂªÂ©_ÃªÂ¸Â_Ã­ÂÂ '.split('_'),
        longDateFormat : {
            LT : 'A hÃ¬ÂÂ mÃ«Â¶Â',
            LTS : 'A hÃ¬ÂÂ mÃ«Â¶Â sÃ¬Â´Â',
            L : 'YYYY.MM.DD',
            LL : 'YYYYÃ«ÂÂ MMMM DÃ¬ÂÂ¼',
            LLL : 'YYYYÃ«ÂÂ MMMM DÃ¬ÂÂ¼ A hÃ¬ÂÂ mÃ«Â¶Â',
            LLLL : 'YYYYÃ«ÂÂ MMMM DÃ¬ÂÂ¼ dddd A hÃ¬ÂÂ mÃ«Â¶Â'
        },
        calendar : {
            sameDay : 'Ã¬ÂÂ¤Ã«ÂÂ LT',
            nextDay : 'Ã«ÂÂ´Ã¬ÂÂ¼ LT',
            nextWeek : 'dddd LT',
            lastDay : 'Ã¬ÂÂ´Ã¬Â Â LT',
            lastWeek : 'Ã¬Â§ÂÃ«ÂÂÃ¬Â£Â¼ dddd LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s Ã­ÂÂ',
            past : '%s Ã¬Â Â',
            s : 'Ã«ÂªÂ Ã¬Â´Â',
            ss : '%dÃ¬Â´Â',
            m : 'Ã¬ÂÂ¼Ã«Â¶Â',
            mm : '%dÃ«Â¶Â',
            h : 'Ã­ÂÂ Ã¬ÂÂÃªÂ°Â',
            hh : '%dÃ¬ÂÂÃªÂ°Â',
            d : 'Ã­ÂÂÃ«Â£Â¨',
            dd : '%dÃ¬ÂÂ¼',
            M : 'Ã­ÂÂ Ã«ÂÂ¬',
            MM : '%dÃ«ÂÂ¬',
            y : 'Ã¬ÂÂ¼ Ã«ÂÂ',
            yy : '%dÃ«ÂÂ'
        },
        ordinalParse : /\d{1,2}Ã¬ÂÂ¼/,
        ordinal : '%dÃ¬ÂÂ¼',
        meridiemParse : /Ã¬ÂÂ¤Ã¬Â Â|Ã¬ÂÂ¤Ã­ÂÂ/,
        isPM : function (token) {
            return token === 'Ã¬ÂÂ¤Ã­ÂÂ';
        },
        meridiem : function (hour, minute, isUpper) {
            return hour < 12 ? 'Ã¬ÂÂ¤Ã¬Â Â' : 'Ã¬ÂÂ¤Ã­ÂÂ';
        }
    });

    return ko;

}));