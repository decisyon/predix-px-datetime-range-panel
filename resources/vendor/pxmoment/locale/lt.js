//! moment.js locale configuration
//! locale : Lithuanian (lt)
//! author : Mindaugas MozÃÂ«ras : https://github.com/mmozuras

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var units = {
        'm' : 'minutÃÂ_minutÃÂs_minutÃÂ',
        'mm': 'minutÃÂs_minuÃÂiÃÂ³_minutes',
        'h' : 'valanda_valandos_valandÃÂ',
        'hh': 'valandos_valandÃÂ³_valandas',
        'd' : 'diena_dienos_dienÃÂ',
        'dd': 'dienos_dienÃÂ³_dienas',
        'M' : 'mÃÂnuo_mÃÂnesio_mÃÂnesÃÂ¯',
        'MM': 'mÃÂnesiai_mÃÂnesiÃÂ³_mÃÂnesius',
        'y' : 'metai_metÃÂ³_metus',
        'yy': 'metai_metÃÂ³_metus'
    };
    function translateSeconds(number, withoutSuffix, key, isFuture) {
        if (withoutSuffix) {
            return 'kelios sekundÃÂs';
        } else {
            return isFuture ? 'keliÃÂ³ sekundÃÂ¾iÃÂ³' : 'kelias sekundes';
        }
    }
    function translateSingular(number, withoutSuffix, key, isFuture) {
        return withoutSuffix ? forms(key)[0] : (isFuture ? forms(key)[1] : forms(key)[2]);
    }
    function special(number) {
        return number % 10 === 0 || (number > 10 && number < 20);
    }
    function forms(key) {
        return units[key].split('_');
    }
    function translate(number, withoutSuffix, key, isFuture) {
        var result = number + ' ';
        if (number === 1) {
            return result + translateSingular(number, withoutSuffix, key[0], isFuture);
        } else if (withoutSuffix) {
            return result + (special(number) ? forms(key)[1] : forms(key)[0]);
        } else {
            if (isFuture) {
                return result + forms(key)[1];
            } else {
                return result + (special(number) ? forms(key)[1] : forms(key)[2]);
            }
        }
    }
    var lt = moment.defineLocale('lt', {
        months : {
            format: 'sausio_vasario_kovo_balandÃÂ¾io_geguÃÂ¾ÃÂs_birÃÂ¾elio_liepos_rugpjÃÂ«ÃÂio_rugsÃÂjo_spalio_lapkriÃÂio_gruodÃÂ¾io'.split('_'),
            standalone: 'sausis_vasaris_kovas_balandis_geguÃÂ¾ÃÂ_birÃÂ¾elis_liepa_rugpjÃÂ«tis_rugsÃÂjis_spalis_lapkritis_gruodis'.split('_')
        },
        monthsShort : 'sau_vas_kov_bal_geg_bir_lie_rgp_rgs_spa_lap_grd'.split('_'),
        weekdays : {
            format: 'sekmadienÃÂ¯_pirmadienÃÂ¯_antradienÃÂ¯_treÃÂiadienÃÂ¯_ketvirtadienÃÂ¯_penktadienÃÂ¯_ÃÂ¡eÃÂ¡tadienÃÂ¯'.split('_'),
            standalone: 'sekmadienis_pirmadienis_antradienis_treÃÂiadienis_ketvirtadienis_penktadienis_ÃÂ¡eÃÂ¡tadienis'.split('_'),
            isFormat: /dddd HH:mm/
        },
        weekdaysShort : 'Sek_Pir_Ant_Tre_Ket_Pen_ÃÂ eÃÂ¡'.split('_'),
        weekdaysMin : 'S_P_A_T_K_Pn_ÃÂ '.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'YYYY-MM-DD',
            LL : 'YYYY [m.] MMMM D [d.]',
            LLL : 'YYYY [m.] MMMM D [d.], HH:mm [val.]',
            LLLL : 'YYYY [m.] MMMM D [d.], dddd, HH:mm [val.]',
            l : 'YYYY-MM-DD',
            ll : 'YYYY [m.] MMMM D [d.]',
            lll : 'YYYY [m.] MMMM D [d.], HH:mm [val.]',
            llll : 'YYYY [m.] MMMM D [d.], ddd, HH:mm [val.]'
        },
        calendar : {
            sameDay : '[ÃÂ iandien] LT',
            nextDay : '[Rytoj] LT',
            nextWeek : 'dddd LT',
            lastDay : '[Vakar] LT',
            lastWeek : '[PraÃÂjusÃÂ¯] dddd LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'po %s',
            past : 'prieÃÂ¡ %s',
            s : translateSeconds,
            m : translateSingular,
            mm : translate,
            h : translateSingular,
            hh : translate,
            d : translateSingular,
            dd : translate,
            M : translateSingular,
            MM : translate,
            y : translateSingular,
            yy : translate
        },
        ordinalParse: /\d{1,2}-oji/,
        ordinal : function (number) {
            return number + '-oji';
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    return lt;

}));