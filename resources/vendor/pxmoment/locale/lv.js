//! moment.js locale configuration
//! locale : latvian (lv)
//! author : Kristaps Karlsons : https://github.com/skakri
//! author : JÃÂnis Elmeris : https://github.com/JanisE

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var units = {
        'm': 'minÃÂ«tes_minÃÂ«tÃÂm_minÃÂ«te_minÃÂ«tes'.split('_'),
        'mm': 'minÃÂ«tes_minÃÂ«tÃÂm_minÃÂ«te_minÃÂ«tes'.split('_'),
        'h': 'stundas_stundÃÂm_stunda_stundas'.split('_'),
        'hh': 'stundas_stundÃÂm_stunda_stundas'.split('_'),
        'd': 'dienas_dienÃÂm_diena_dienas'.split('_'),
        'dd': 'dienas_dienÃÂm_diena_dienas'.split('_'),
        'M': 'mÃÂneÃÂ¡a_mÃÂneÃÂ¡iem_mÃÂnesis_mÃÂneÃÂ¡i'.split('_'),
        'MM': 'mÃÂneÃÂ¡a_mÃÂneÃÂ¡iem_mÃÂnesis_mÃÂneÃÂ¡i'.split('_'),
        'y': 'gada_gadiem_gads_gadi'.split('_'),
        'yy': 'gada_gadiem_gads_gadi'.split('_')
    };
    /**
     * @param withoutSuffix boolean true = a length of time; false = before/after a period of time.
     */
    function format(forms, number, withoutSuffix) {
        if (withoutSuffix) {
            // E.g. "21 minÃÂ«te", "3 minÃÂ«tes".
            return number % 10 === 1 && number !== 11 ? forms[2] : forms[3];
        } else {
            // E.g. "21 minÃÂ«tes" as in "pÃÂc 21 minÃÂ«tes".
            // E.g. "3 minÃÂ«tÃÂm" as in "pÃÂc 3 minÃÂ«tÃÂm".
            return number % 10 === 1 && number !== 11 ? forms[0] : forms[1];
        }
    }
    function relativeTimeWithPlural(number, withoutSuffix, key) {
        return number + ' ' + format(units[key], number, withoutSuffix);
    }
    function relativeTimeWithSingular(number, withoutSuffix, key) {
        return format(units[key], number, withoutSuffix);
    }
    function relativeSeconds(number, withoutSuffix) {
        return withoutSuffix ? 'daÃÂ¾as sekundes' : 'daÃÂ¾ÃÂm sekundÃÂm';
    }

    var lv = moment.defineLocale('lv', {
        months : 'janvÃÂris_februÃÂris_marts_aprÃÂ«lis_maijs_jÃÂ«nijs_jÃÂ«lijs_augusts_septembris_oktobris_novembris_decembris'.split('_'),
        monthsShort : 'jan_feb_mar_apr_mai_jÃÂ«n_jÃÂ«l_aug_sep_okt_nov_dec'.split('_'),
        weekdays : 'svÃÂtdiena_pirmdiena_otrdiena_treÃÂ¡diena_ceturtdiena_piektdiena_sestdiena'.split('_'),
        weekdaysShort : 'Sv_P_O_T_C_Pk_S'.split('_'),
        weekdaysMin : 'Sv_P_O_T_C_Pk_S'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY.',
            LL : 'YYYY. [gada] D. MMMM',
            LLL : 'YYYY. [gada] D. MMMM, HH:mm',
            LLLL : 'YYYY. [gada] D. MMMM, dddd, HH:mm'
        },
        calendar : {
            sameDay : '[ÃÂ odien pulksten] LT',
            nextDay : '[RÃÂ«t pulksten] LT',
            nextWeek : 'dddd [pulksten] LT',
            lastDay : '[Vakar pulksten] LT',
            lastWeek : '[PagÃÂjuÃÂ¡ÃÂ] dddd [pulksten] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'pÃÂc %s',
            past : 'pirms %s',
            s : relativeSeconds,
            m : relativeTimeWithSingular,
            mm : relativeTimeWithPlural,
            h : relativeTimeWithSingular,
            hh : relativeTimeWithPlural,
            d : relativeTimeWithSingular,
            dd : relativeTimeWithPlural,
            M : relativeTimeWithSingular,
            MM : relativeTimeWithPlural,
            y : relativeTimeWithSingular,
            yy : relativeTimeWithPlural
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    return lv;

}));