//! moment.js locale configuration
//! locale : punjabi india (pa-in)
//! author : Harpreet Singh : https://github.com/harpreetkhalsagtbit

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var symbolMap = {
        '1': 'Ã Â©Â§',
        '2': 'Ã Â©Â¨',
        '3': 'Ã Â©Â©',
        '4': 'Ã Â©Âª',
        '5': 'Ã Â©Â«',
        '6': 'Ã Â©Â¬',
        '7': 'Ã Â©Â­',
        '8': 'Ã Â©Â®',
        '9': 'Ã Â©Â¯',
        '0': 'Ã Â©Â¦'
    },
    numberMap = {
        'Ã Â©Â§': '1',
        'Ã Â©Â¨': '2',
        'Ã Â©Â©': '3',
        'Ã Â©Âª': '4',
        'Ã Â©Â«': '5',
        'Ã Â©Â¬': '6',
        'Ã Â©Â­': '7',
        'Ã Â©Â®': '8',
        'Ã Â©Â¯': '9',
        'Ã Â©Â¦': '0'
    };

    var pa_in = moment.defineLocale('pa-in', {
        // There are months name as per Nanakshahi Calender but they are not used as rigidly in modern Punjabi.
        months : 'Ã Â¨ÂÃ Â¨Â¨Ã Â¨ÂµÃ Â¨Â°Ã Â©Â_Ã Â¨Â«Ã Â¨Â¼Ã Â¨Â°Ã Â¨ÂµÃ Â¨Â°Ã Â©Â_Ã Â¨Â®Ã Â¨Â¾Ã Â¨Â°Ã Â¨Â_Ã Â¨ÂÃ Â¨ÂªÃ Â©ÂÃ Â¨Â°Ã Â©ÂÃ Â¨Â²_Ã Â¨Â®Ã Â¨Â_Ã Â¨ÂÃ Â©ÂÃ Â¨Â¨_Ã Â¨ÂÃ Â©ÂÃ Â¨Â²Ã Â¨Â¾Ã Â¨Â_Ã Â¨ÂÃ Â¨ÂÃ Â¨Â¸Ã Â¨Â¤_Ã Â¨Â¸Ã Â¨Â¤Ã Â©Â°Ã Â¨Â¬Ã Â¨Â°_Ã Â¨ÂÃ Â¨ÂÃ Â¨Â¤Ã Â©ÂÃ Â¨Â¬Ã Â¨Â°_Ã Â¨Â¨Ã Â¨ÂµÃ Â©Â°Ã Â¨Â¬Ã Â¨Â°_Ã Â¨Â¦Ã Â¨Â¸Ã Â©Â°Ã Â¨Â¬Ã Â¨Â°'.split('_'),
        monthsShort : 'Ã Â¨ÂÃ Â¨Â¨Ã Â¨ÂµÃ Â¨Â°Ã Â©Â_Ã Â¨Â«Ã Â¨Â¼Ã Â¨Â°Ã Â¨ÂµÃ Â¨Â°Ã Â©Â_Ã Â¨Â®Ã Â¨Â¾Ã Â¨Â°Ã Â¨Â_Ã Â¨ÂÃ Â¨ÂªÃ Â©ÂÃ Â¨Â°Ã Â©ÂÃ Â¨Â²_Ã Â¨Â®Ã Â¨Â_Ã Â¨ÂÃ Â©ÂÃ Â¨Â¨_Ã Â¨ÂÃ Â©ÂÃ Â¨Â²Ã Â¨Â¾Ã Â¨Â_Ã Â¨ÂÃ Â¨ÂÃ Â¨Â¸Ã Â¨Â¤_Ã Â¨Â¸Ã Â¨Â¤Ã Â©Â°Ã Â¨Â¬Ã Â¨Â°_Ã Â¨ÂÃ Â¨ÂÃ Â¨Â¤Ã Â©ÂÃ Â¨Â¬Ã Â¨Â°_Ã Â¨Â¨Ã Â¨ÂµÃ Â©Â°Ã Â¨Â¬Ã Â¨Â°_Ã Â¨Â¦Ã Â¨Â¸Ã Â©Â°Ã Â¨Â¬Ã Â¨Â°'.split('_'),
        weekdays : 'Ã Â¨ÂÃ Â¨Â¤Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨Â¸Ã Â©ÂÃ Â¨Â®Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨Â®Ã Â©Â°Ã Â¨ÂÃ Â¨Â²Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨Â¬Ã Â©ÂÃ Â¨Â§Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â©ÂÃ Â©Â±Ã Â¨ÂÃ Â¨Â°Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¨Ã Â©ÂÃ Â¨ÂÃ Â¨Â°Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°'.split('_'),
        weekdaysShort : 'Ã Â¨ÂÃ Â¨Â¤_Ã Â¨Â¸Ã Â©ÂÃ Â¨Â®_Ã Â¨Â®Ã Â©Â°Ã Â¨ÂÃ Â¨Â²_Ã Â¨Â¬Ã Â©ÂÃ Â¨Â§_Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â©ÂÃ Â¨ÂÃ Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¨Ã Â©Â'.split('_'),
        weekdaysMin : 'Ã Â¨ÂÃ Â¨Â¤_Ã Â¨Â¸Ã Â©ÂÃ Â¨Â®_Ã Â¨Â®Ã Â©Â°Ã Â¨ÂÃ Â¨Â²_Ã Â¨Â¬Ã Â©ÂÃ Â¨Â§_Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â©ÂÃ Â¨ÂÃ Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¨Ã Â©Â'.split('_'),
        longDateFormat : {
            LT : 'A h:mm Ã Â¨ÂµÃ Â¨ÂÃ Â©Â',
            LTS : 'A h:mm:ss Ã Â¨ÂµÃ Â¨ÂÃ Â©Â',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY, A h:mm Ã Â¨ÂµÃ Â¨ÂÃ Â©Â',
            LLLL : 'dddd, D MMMM YYYY, A h:mm Ã Â¨ÂµÃ Â¨ÂÃ Â©Â'
        },
        calendar : {
            sameDay : '[Ã Â¨ÂÃ Â¨Â] LT',
            nextDay : '[Ã Â¨ÂÃ Â¨Â²] LT',
            nextWeek : 'dddd, LT',
            lastDay : '[Ã Â¨ÂÃ Â¨Â²] LT',
            lastWeek : '[Ã Â¨ÂªÃ Â¨Â¿Ã Â¨ÂÃ Â¨Â²Ã Â©Â] dddd, LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s Ã Â¨ÂµÃ Â¨Â¿Ã Â©Â±Ã Â¨Â',
            past : '%s Ã Â¨ÂªÃ Â¨Â¿Ã Â¨ÂÃ Â¨Â²Ã Â©Â',
            s : 'Ã Â¨ÂÃ Â©ÂÃ Â¨Â Ã Â¨Â¸Ã Â¨ÂÃ Â¨Â¿Ã Â©Â°Ã Â¨Â',
            m : 'Ã Â¨ÂÃ Â¨Â Ã Â¨Â®Ã Â¨Â¿Ã Â©Â°Ã Â¨Â',
            mm : '%d Ã Â¨Â®Ã Â¨Â¿Ã Â©Â°Ã Â¨Â',
            h : 'Ã Â¨ÂÃ Â©Â±Ã Â¨Â Ã Â¨ÂÃ Â©Â°Ã Â¨ÂÃ Â¨Â¾',
            hh : '%d Ã Â¨ÂÃ Â©Â°Ã Â¨ÂÃ Â©Â',
            d : 'Ã Â¨ÂÃ Â©Â±Ã Â¨Â Ã Â¨Â¦Ã Â¨Â¿Ã Â¨Â¨',
            dd : '%d Ã Â¨Â¦Ã Â¨Â¿Ã Â¨Â¨',
            M : 'Ã Â¨ÂÃ Â©Â±Ã Â¨Â Ã Â¨Â®Ã Â¨Â¹Ã Â©ÂÃ Â¨Â¨Ã Â¨Â¾',
            MM : '%d Ã Â¨Â®Ã Â¨Â¹Ã Â©ÂÃ Â¨Â¨Ã Â©Â',
            y : 'Ã Â¨ÂÃ Â©Â±Ã Â¨Â Ã Â¨Â¸Ã Â¨Â¾Ã Â¨Â²',
            yy : '%d Ã Â¨Â¸Ã Â¨Â¾Ã Â¨Â²'
        },
        preparse: function (string) {
            return string.replace(/[Ã Â©Â§Ã Â©Â¨Ã Â©Â©Ã Â©ÂªÃ Â©Â«Ã Â©Â¬Ã Â©Â­Ã Â©Â®Ã Â©Â¯Ã Â©Â¦]/g, function (match) {
                return numberMap[match];
            });
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return symbolMap[match];
            });
        },
        // Punjabi notation for meridiems are quite fuzzy in practice. While there exists
        // a rigid notion of a 'Pahar' it is not used as rigidly in modern Punjabi.
        meridiemParse: /Ã Â¨Â°Ã Â¨Â¾Ã Â¨Â¤|Ã Â¨Â¸Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°|Ã Â¨Â¦Ã Â©ÂÃ Â¨ÂªÃ Â¨Â¹Ã Â¨Â¿Ã Â¨Â°|Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¾Ã Â¨Â®/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'Ã Â¨Â°Ã Â¨Â¾Ã Â¨Â¤') {
                return hour < 4 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â¨Â¸Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°') {
                return hour;
            } else if (meridiem === 'Ã Â¨Â¦Ã Â©ÂÃ Â¨ÂªÃ Â¨Â¹Ã Â¨Â¿Ã Â¨Â°') {
                return hour >= 10 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¾Ã Â¨Â®') {
                return hour + 12;
            }
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'Ã Â¨Â°Ã Â¨Â¾Ã Â¨Â¤';
            } else if (hour < 10) {
                return 'Ã Â¨Â¸Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°';
            } else if (hour < 17) {
                return 'Ã Â¨Â¦Ã Â©ÂÃ Â¨ÂªÃ Â¨Â¹Ã Â¨Â¿Ã Â¨Â°';
            } else if (hour < 20) {
                return 'Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¾Ã Â¨Â®';
            } else {
                return 'Ã Â¨Â°Ã Â¨Â¾Ã Â¨Â¤';
            }
        },
        week : {
            dow : 0, // Sunday is the first day of the week.
            doy : 6  // The week that contains Jan 1st is the first week of the year.
        }
    });

    return pa_in;

}));