//! moment.js locale configuration
//! locale : brazilian portuguese (pt-br)
//! author : Caio Ribeiro Pereira : https://github.com/caio-ribeiro-pereira

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var pt_br = moment.defineLocale('pt-br', {
        months : 'Janeiro_Fevereiro_MarÃÂ§o_Abril_Maio_Junho_Julho_Agosto_Setembro_Outubro_Novembro_Dezembro'.split('_'),
        monthsShort : 'Jan_Fev_Mar_Abr_Mai_Jun_Jul_Ago_Set_Out_Nov_Dez'.split('_'),
        weekdays : 'Domingo_Segunda-feira_TerÃÂ§a-feira_Quarta-feira_Quinta-feira_Sexta-feira_SÃÂ¡bado'.split('_'),
        weekdaysShort : 'Dom_Seg_Ter_Qua_Qui_Sex_SÃÂ¡b'.split('_'),
        weekdaysMin : 'Dom_2ÃÂª_3ÃÂª_4ÃÂª_5ÃÂª_6ÃÂª_SÃÂ¡b'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D [de] MMMM [de] YYYY',
            LLL : 'D [de] MMMM [de] YYYY [ÃÂ s] HH:mm',
            LLLL : 'dddd, D [de] MMMM [de] YYYY [ÃÂ s] HH:mm'
        },
        calendar : {
            sameDay: '[Hoje ÃÂ s] LT',
            nextDay: '[AmanhÃÂ£ ÃÂ s] LT',
            nextWeek: 'dddd [ÃÂ s] LT',
            lastDay: '[Ontem ÃÂ s] LT',
            lastWeek: function () {
                return (this.day() === 0 || this.day() === 6) ?
                    '[ÃÂltimo] dddd [ÃÂ s] LT' : // Saturday + Sunday
                    '[ÃÂltima] dddd [ÃÂ s] LT'; // Monday - Friday
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'em %s',
            past : '%s atrÃÂ¡s',
            s : 'poucos segundos',
            m : 'um minuto',
            mm : '%d minutos',
            h : 'uma hora',
            hh : '%d horas',
            d : 'um dia',
            dd : '%d dias',
            M : 'um mÃÂªs',
            MM : '%d meses',
            y : 'um ano',
            yy : '%d anos'
        },
        ordinalParse: /\d{1,2}ÃÂº/,
        ordinal : '%dÃÂº'
    });

    return pt_br;

}));