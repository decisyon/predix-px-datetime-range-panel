//! moment.js locale configuration
//! locale : Northern Sami (se)
//! authors : BÃÂ¥rd Rolstad Henriksen : https://github.com/karamell

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';



    var se = moment.defineLocale('se', {
        months : 'oÃÂÃÂajagemÃÂ¡nnu_guovvamÃÂ¡nnu_njukÃÂamÃÂ¡nnu_cuoÃÂomÃÂ¡nnu_miessemÃÂ¡nnu_geassemÃÂ¡nnu_suoidnemÃÂ¡nnu_borgemÃÂ¡nnu_ÃÂakÃÂamÃÂ¡nnu_golggotmÃÂ¡nnu_skÃÂ¡bmamÃÂ¡nnu_juovlamÃÂ¡nnu'.split('_'),
        monthsShort : 'oÃÂÃÂj_guov_njuk_cuo_mies_geas_suoi_borg_ÃÂakÃÂ_golg_skÃÂ¡b_juov'.split('_'),
        weekdays : 'sotnabeaivi_vuossÃÂ¡rga_maÃÂÃÂebÃÂ¡rga_gaskavahkku_duorastat_bearjadat_lÃÂ¡vvardat'.split('_'),
        weekdaysShort : 'sotn_vuos_maÃÂ_gask_duor_bear_lÃÂ¡v'.split('_'),
        weekdaysMin : 's_v_m_g_d_b_L'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'MMMM D. [b.] YYYY',
            LLL : 'MMMM D. [b.] YYYY [ti.] HH:mm',
            LLLL : 'dddd, MMMM D. [b.] YYYY [ti.] HH:mm'
        },
        calendar : {
            sameDay: '[otne ti] LT',
            nextDay: '[ihttin ti] LT',
            nextWeek: 'dddd [ti] LT',
            lastDay: '[ikte ti] LT',
            lastWeek: '[ovddit] dddd [ti] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : '%s geaÃÂ¾es',
            past : 'maÃÂit %s',
            s : 'moadde sekunddat',
            m : 'okta minuhta',
            mm : '%d minuhtat',
            h : 'okta diimmu',
            hh : '%d diimmut',
            d : 'okta beaivi',
            dd : '%d beaivvit',
            M : 'okta mÃÂ¡nnu',
            MM : '%d mÃÂ¡nut',
            y : 'okta jahki',
            yy : '%d jagit'
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    return se;

}));