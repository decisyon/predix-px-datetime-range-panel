//! moment.js locale configuration
//! locale : turkish (tr)
//! authors : Erhan Gundogan : https://github.com/erhangundogan,
//!           Burak YiÃÂit Kaya: https://github.com/BYK

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var suffixes = {
        1: '\'inci',
        5: '\'inci',
        8: '\'inci',
        70: '\'inci',
        80: '\'inci',
        2: '\'nci',
        7: '\'nci',
        20: '\'nci',
        50: '\'nci',
        3: '\'ÃÂ¼ncÃÂ¼',
        4: '\'ÃÂ¼ncÃÂ¼',
        100: '\'ÃÂ¼ncÃÂ¼',
        6: '\'ncÃÂ±',
        9: '\'uncu',
        10: '\'uncu',
        30: '\'uncu',
        60: '\'ÃÂ±ncÃÂ±',
        90: '\'ÃÂ±ncÃÂ±'
    };

    var tr = moment.defineLocale('tr', {
        months : 'Ocak_ÃÂubat_Mart_Nisan_MayÃÂ±s_Haziran_Temmuz_AÃÂustos_EylÃÂ¼l_Ekim_KasÃÂ±m_AralÃÂ±k'.split('_'),
        monthsShort : 'Oca_ÃÂub_Mar_Nis_May_Haz_Tem_AÃÂu_Eyl_Eki_Kas_Ara'.split('_'),
        weekdays : 'Pazar_Pazartesi_SalÃÂ±_ÃÂarÃÂamba_PerÃÂembe_Cuma_Cumartesi'.split('_'),
        weekdaysShort : 'Paz_Pts_Sal_ÃÂar_Per_Cum_Cts'.split('_'),
        weekdaysMin : 'Pz_Pt_Sa_ÃÂa_Pe_Cu_Ct'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[bugÃÂ¼n saat] LT',
            nextDay : '[yarÃÂ±n saat] LT',
            nextWeek : '[haftaya] dddd [saat] LT',
            lastDay : '[dÃÂ¼n] LT',
            lastWeek : '[geÃÂ§en hafta] dddd [saat] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s sonra',
            past : '%s ÃÂ¶nce',
            s : 'birkaÃÂ§ saniye',
            m : 'bir dakika',
            mm : '%d dakika',
            h : 'bir saat',
            hh : '%d saat',
            d : 'bir gÃÂ¼n',
            dd : '%d gÃÂ¼n',
            M : 'bir ay',
            MM : '%d ay',
            y : 'bir yÃÂ±l',
            yy : '%d yÃÂ±l'
        },
        ordinalParse: /\d{1,2}'(inci|nci|ÃÂ¼ncÃÂ¼|ncÃÂ±|uncu|ÃÂ±ncÃÂ±)/,
        ordinal : function (number) {
            if (number === 0) {  // special case for zero
                return number + '\'ÃÂ±ncÃÂ±';
            }
            var a = number % 10,
                b = number % 100 - a,
                c = number >= 100 ? 100 : null;
            return number + (suffixes[a] || suffixes[b] || suffixes[c]);
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    return tr;

}));