//! moment.js locale configuration
//! locale : talossan (tzl)
//! author : Robin van der Vliet : https://github.com/robin0van0der0v with the help of IustÃÂ¬ Canun

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';



    // After the year there should be a slash and the amount of years since December 26, 1979 in Roman numerals.
    // This is currently too difficult (maybe even impossible) to add.
    var tzl = moment.defineLocale('tzl', {
        months : 'Januar_Fevraglh_MarÃÂ§_AvrÃÂ¯u_Mai_GÃÂ¼n_Julia_Guscht_Setemvar_ListopÃÂ¤ts_Noemvar_Zecemvar'.split('_'),
        monthsShort : 'Jan_Fev_Mar_Avr_Mai_GÃÂ¼n_Jul_Gus_Set_Lis_Noe_Zec'.split('_'),
        weekdays : 'SÃÂºladi_LÃÂºneÃÂ§i_Maitzi_MÃÂ¡rcuri_XhÃÂºadi_ViÃÂ©nerÃÂ§i_SÃÂ¡turi'.split('_'),
        weekdaysShort : 'SÃÂºl_LÃÂºn_Mai_MÃÂ¡r_XhÃÂº_ViÃÂ©_SÃÂ¡t'.split('_'),
        weekdaysMin : 'SÃÂº_LÃÂº_Ma_MÃÂ¡_Xh_Vi_SÃÂ¡'.split('_'),
        longDateFormat : {
            LT : 'HH.mm',
            LTS : 'HH.mm.ss',
            L : 'DD.MM.YYYY',
            LL : 'D. MMMM [dallas] YYYY',
            LLL : 'D. MMMM [dallas] YYYY HH.mm',
            LLLL : 'dddd, [li] D. MMMM [dallas] YYYY HH.mm'
        },
        meridiemParse: /d\'o|d\'a/i,
        isPM : function (input) {
            return 'd\'o' === input.toLowerCase();
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours > 11) {
                return isLower ? 'd\'o' : 'D\'O';
            } else {
                return isLower ? 'd\'a' : 'D\'A';
            }
        },
        calendar : {
            sameDay : '[oxhi ÃÂ ] LT',
            nextDay : '[demÃÂ  ÃÂ ] LT',
            nextWeek : 'dddd [ÃÂ ] LT',
            lastDay : '[ieiri ÃÂ ] LT',
            lastWeek : '[sÃÂ¼r el] dddd [lasteu ÃÂ ] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'osprei %s',
            past : 'ja%s',
            s : processRelativeTime,
            m : processRelativeTime,
            mm : processRelativeTime,
            h : processRelativeTime,
            hh : processRelativeTime,
            d : processRelativeTime,
            dd : processRelativeTime,
            M : processRelativeTime,
            MM : processRelativeTime,
            y : processRelativeTime,
            yy : processRelativeTime
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    function processRelativeTime(number, withoutSuffix, key, isFuture) {
        var format = {
            's': ['viensas secunds', '\'iensas secunds'],
            'm': ['\'n mÃÂ­ut', '\'iens mÃÂ­ut'],
            'mm': [number + ' mÃÂ­uts', '' + number + ' mÃÂ­uts'],
            'h': ['\'n ÃÂ¾ora', '\'iensa ÃÂ¾ora'],
            'hh': [number + ' ÃÂ¾oras', '' + number + ' ÃÂ¾oras'],
            'd': ['\'n ziua', '\'iensa ziua'],
            'dd': [number + ' ziuas', '' + number + ' ziuas'],
            'M': ['\'n mes', '\'iens mes'],
            'MM': [number + ' mesen', '' + number + ' mesen'],
            'y': ['\'n ar', '\'iens ar'],
            'yy': [number + ' ars', '' + number + ' ars']
        };
        return isFuture ? format[key][0] : (withoutSuffix ? format[key][0] : format[key][1]);
    }

    return tzl;

}));