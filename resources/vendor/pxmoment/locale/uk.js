//! moment.js locale configuration
//! locale : ukrainian (uk)
//! author : zemlanin : https://github.com/zemlanin
//! Author : Menelion ElensÃÂºle : https://github.com/Oire

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    function plural(word, num) {
        var forms = word.split('_');
        return num % 10 === 1 && num % 100 !== 11 ? forms[0] : (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) ? forms[1] : forms[2]);
    }
    function relativeTimeWithPlural(number, withoutSuffix, key) {
        var format = {
            'mm': withoutSuffix ? 'ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ°_ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ¸_ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½' : 'ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ_ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ¸_ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½',
            'hh': withoutSuffix ? 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°_ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ¸_ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½' : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ_ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ¸_ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½',
            'dd': 'ÃÂ´ÃÂµÃÂ½ÃÂ_ÃÂ´ÃÂ½ÃÂ_ÃÂ´ÃÂ½ÃÂÃÂ²',
            'MM': 'ÃÂ¼ÃÂÃÂÃÂÃÂÃÂ_ÃÂ¼ÃÂÃÂÃÂÃÂÃÂ_ÃÂ¼ÃÂÃÂÃÂÃÂÃÂÃÂ²',
            'yy': 'ÃÂÃÂÃÂº_ÃÂÃÂ¾ÃÂºÃÂ¸_ÃÂÃÂ¾ÃÂºÃÂÃÂ²'
        };
        if (key === 'm') {
            return withoutSuffix ? 'ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ°' : 'ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ';
        }
        else if (key === 'h') {
            return withoutSuffix ? 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°' : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ';
        }
        else {
            return number + ' ' + plural(format[key], +number);
        }
    }
    function weekdaysCaseReplace(m, format) {
        var weekdays = {
            'nominative': 'ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂ_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂ¾ÃÂº_ÃÂ²ÃÂÃÂ²ÃÂÃÂ¾ÃÂÃÂ¾ÃÂº_ÃÂÃÂµÃÂÃÂµÃÂ´ÃÂ°_ÃÂÃÂµÃÂÃÂ²ÃÂµÃÂ_ÃÂ¿Ã¢ÂÂÃÂÃÂÃÂ½ÃÂ¸ÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ°'.split('_'),
            'accusative': 'ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂ_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂ¾ÃÂº_ÃÂ²ÃÂÃÂ²ÃÂÃÂ¾ÃÂÃÂ¾ÃÂº_ÃÂÃÂµÃÂÃÂµÃÂ´ÃÂ_ÃÂÃÂµÃÂÃÂ²ÃÂµÃÂ_ÃÂ¿Ã¢ÂÂÃÂÃÂÃÂ½ÃÂ¸ÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ'.split('_'),
            'genitive': 'ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂ_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂºÃÂ°_ÃÂ²ÃÂÃÂ²ÃÂÃÂ¾ÃÂÃÂºÃÂ°_ÃÂÃÂµÃÂÃÂµÃÂ´ÃÂ¸_ÃÂÃÂµÃÂÃÂ²ÃÂµÃÂÃÂ³ÃÂ°_ÃÂ¿Ã¢ÂÂÃÂÃÂÃÂ½ÃÂ¸ÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ¸'.split('_')
        },
        nounCase = (/(\[[ÃÂÃÂ²ÃÂ£ÃÂ]\]) ?dddd/).test(format) ?
            'accusative' :
            ((/\[?(?:ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ»ÃÂ¾ÃÂ|ÃÂ½ÃÂ°ÃÂÃÂÃÂÃÂ¿ÃÂ½ÃÂ¾ÃÂ)? ?\] ?dddd/).test(format) ?
                'genitive' :
                'nominative');
        return weekdays[nounCase][m.day()];
    }
    function processHoursFunction(str) {
        return function () {
            return str + 'ÃÂ¾' + (this.hours() === 11 ? 'ÃÂ±' : '') + '] LT';
        };
    }

    var uk = moment.defineLocale('uk', {
        months : {
            'format': 'ÃÂÃÂÃÂÃÂ½ÃÂ_ÃÂ»ÃÂÃÂÃÂ¾ÃÂ³ÃÂ¾_ÃÂ±ÃÂµÃÂÃÂµÃÂ·ÃÂ½ÃÂ_ÃÂºÃÂ²ÃÂÃÂÃÂ½ÃÂ_ÃÂÃÂÃÂ°ÃÂ²ÃÂ½ÃÂ_ÃÂÃÂµÃÂÃÂ²ÃÂ½ÃÂ_ÃÂ»ÃÂ¸ÃÂ¿ÃÂ½ÃÂ_ÃÂÃÂµÃÂÃÂ¿ÃÂ½ÃÂ_ÃÂ²ÃÂµÃÂÃÂµÃÂÃÂ½ÃÂ_ÃÂ¶ÃÂ¾ÃÂ²ÃÂÃÂ½ÃÂ_ÃÂ»ÃÂ¸ÃÂÃÂÃÂ¾ÃÂ¿ÃÂ°ÃÂ´ÃÂ°_ÃÂ³ÃÂÃÂÃÂ´ÃÂ½ÃÂ'.split('_'),
            'standalone': 'ÃÂÃÂÃÂÃÂµÃÂ½ÃÂ_ÃÂ»ÃÂÃÂÃÂ¸ÃÂ¹_ÃÂ±ÃÂµÃÂÃÂµÃÂ·ÃÂµÃÂ½ÃÂ_ÃÂºÃÂ²ÃÂÃÂÃÂµÃÂ½ÃÂ_ÃÂÃÂÃÂ°ÃÂ²ÃÂµÃÂ½ÃÂ_ÃÂÃÂµÃÂÃÂ²ÃÂµÃÂ½ÃÂ_ÃÂ»ÃÂ¸ÃÂ¿ÃÂµÃÂ½ÃÂ_ÃÂÃÂµÃÂÃÂ¿ÃÂµÃÂ½ÃÂ_ÃÂ²ÃÂµÃÂÃÂµÃÂÃÂµÃÂ½ÃÂ_ÃÂ¶ÃÂ¾ÃÂ²ÃÂÃÂµÃÂ½ÃÂ_ÃÂ»ÃÂ¸ÃÂÃÂÃÂ¾ÃÂ¿ÃÂ°ÃÂ´_ÃÂ³ÃÂÃÂÃÂ´ÃÂµÃÂ½ÃÂ'.split('_')
        },
        monthsShort : 'ÃÂÃÂÃÂ_ÃÂ»ÃÂÃÂ_ÃÂ±ÃÂµÃÂ_ÃÂºÃÂ²ÃÂÃÂ_ÃÂÃÂÃÂ°ÃÂ²_ÃÂÃÂµÃÂÃÂ²_ÃÂ»ÃÂ¸ÃÂ¿_ÃÂÃÂµÃÂÃÂ¿_ÃÂ²ÃÂµÃÂ_ÃÂ¶ÃÂ¾ÃÂ²ÃÂ_ÃÂ»ÃÂ¸ÃÂÃÂ_ÃÂ³ÃÂÃÂÃÂ´'.split('_'),
        weekdays : weekdaysCaseReplace,
        weekdaysShort : 'ÃÂ½ÃÂ´_ÃÂ¿ÃÂ½_ÃÂ²ÃÂ_ÃÂÃÂ_ÃÂÃÂ_ÃÂ¿ÃÂ_ÃÂÃÂ±'.split('_'),
        weekdaysMin : 'ÃÂ½ÃÂ´_ÃÂ¿ÃÂ½_ÃÂ²ÃÂ_ÃÂÃÂ_ÃÂÃÂ_ÃÂ¿ÃÂ_ÃÂÃÂ±'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY ÃÂ.',
            LLL : 'D MMMM YYYY ÃÂ., HH:mm',
            LLLL : 'dddd, D MMMM YYYY ÃÂ., HH:mm'
        },
        calendar : {
            sameDay: processHoursFunction('[ÃÂ¡ÃÂÃÂ¾ÃÂ³ÃÂ¾ÃÂ´ÃÂ½ÃÂ '),
            nextDay: processHoursFunction('[ÃÂÃÂ°ÃÂ²ÃÂÃÂÃÂ° '),
            lastDay: processHoursFunction('[ÃÂÃÂÃÂ¾ÃÂÃÂ° '),
            nextWeek: processHoursFunction('[ÃÂ£] dddd ['),
            lastWeek: function () {
                switch (this.day()) {
                case 0:
                case 3:
                case 5:
                case 6:
                    return processHoursFunction('[ÃÂÃÂ¸ÃÂ½ÃÂÃÂ»ÃÂ¾ÃÂ] dddd [').call(this);
                case 1:
                case 2:
                case 4:
                    return processHoursFunction('[ÃÂÃÂ¸ÃÂ½ÃÂÃÂ»ÃÂ¾ÃÂ³ÃÂ¾] dddd [').call(this);
                }
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'ÃÂ·ÃÂ° %s',
            past : '%s ÃÂÃÂ¾ÃÂ¼ÃÂ',
            s : 'ÃÂ´ÃÂµÃÂºÃÂÃÂ»ÃÂÃÂºÃÂ° ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´',
            m : relativeTimeWithPlural,
            mm : relativeTimeWithPlural,
            h : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ',
            hh : relativeTimeWithPlural,
            d : 'ÃÂ´ÃÂµÃÂ½ÃÂ',
            dd : relativeTimeWithPlural,
            M : 'ÃÂ¼ÃÂÃÂÃÂÃÂÃÂ',
            MM : relativeTimeWithPlural,
            y : 'ÃÂÃÂÃÂº',
            yy : relativeTimeWithPlural
        },
        // M. E.: those two are virtually unused but a user might want to implement them for his/her website for some reason
        meridiemParse: /ÃÂ½ÃÂ¾ÃÂÃÂ|ÃÂÃÂ°ÃÂ½ÃÂºÃÂ|ÃÂ´ÃÂ½ÃÂ|ÃÂ²ÃÂµÃÂÃÂ¾ÃÂÃÂ°/,
        isPM: function (input) {
            return /^(ÃÂ´ÃÂ½ÃÂ|ÃÂ²ÃÂµÃÂÃÂ¾ÃÂÃÂ°)$/.test(input);
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'ÃÂ½ÃÂ¾ÃÂÃÂ';
            } else if (hour < 12) {
                return 'ÃÂÃÂ°ÃÂ½ÃÂºÃÂ';
            } else if (hour < 17) {
                return 'ÃÂ´ÃÂ½ÃÂ';
            } else {
                return 'ÃÂ²ÃÂµÃÂÃÂ¾ÃÂÃÂ°';
            }
        },
        ordinalParse: /\d{1,2}-(ÃÂ¹|ÃÂ³ÃÂ¾)/,
        ordinal: function (number, period) {
            switch (period) {
            case 'M':
            case 'd':
            case 'DDD':
            case 'w':
            case 'W':
                return number + '-ÃÂ¹';
            case 'D':
                return number + '-ÃÂ³ÃÂ¾';
            default:
                return number;
            }
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    return uk;

}));