//! moment.js locale configuration
//! locale : vietnamese (vi)
//! author : Bang Nguyen : https://github.com/bangnk

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var vi = moment.defineLocale('vi', {
        months : 'thÃÂ¡ng 1_thÃÂ¡ng 2_thÃÂ¡ng 3_thÃÂ¡ng 4_thÃÂ¡ng 5_thÃÂ¡ng 6_thÃÂ¡ng 7_thÃÂ¡ng 8_thÃÂ¡ng 9_thÃÂ¡ng 10_thÃÂ¡ng 11_thÃÂ¡ng 12'.split('_'),
        monthsShort : 'Th01_Th02_Th03_Th04_Th05_Th06_Th07_Th08_Th09_Th10_Th11_Th12'.split('_'),
        monthsParseExact : true,
        weekdays : 'chÃ¡Â»Â§ nhÃ¡ÂºÂ­t_thÃ¡Â»Â© hai_thÃ¡Â»Â© ba_thÃ¡Â»Â© tÃÂ°_thÃ¡Â»Â© nÃÂm_thÃ¡Â»Â© sÃÂ¡u_thÃ¡Â»Â© bÃ¡ÂºÂ£y'.split('_'),
        weekdaysShort : 'CN_T2_T3_T4_T5_T6_T7'.split('_'),
        weekdaysMin : 'CN_T2_T3_T4_T5_T6_T7'.split('_'),
        weekdaysParseExact : true,
        meridiemParse: /sa|ch/i,
        isPM : function (input) {
            return /^ch$/i.test(input);
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours < 12) {
                return isLower ? 'sa' : 'SA';
            } else {
                return isLower ? 'ch' : 'CH';
            }
        },
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM [nÃÂm] YYYY',
            LLL : 'D MMMM [nÃÂm] YYYY HH:mm',
            LLLL : 'dddd, D MMMM [nÃÂm] YYYY HH:mm',
            l : 'DD/M/YYYY',
            ll : 'D MMM YYYY',
            lll : 'D MMM YYYY HH:mm',
            llll : 'ddd, D MMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[HÃÂ´m nay lÃÂºc] LT',
            nextDay: '[NgÃÂ y mai lÃÂºc] LT',
            nextWeek: 'dddd [tuÃ¡ÂºÂ§n tÃ¡Â»Âi lÃÂºc] LT',
            lastDay: '[HÃÂ´m qua lÃÂºc] LT',
            lastWeek: 'dddd [tuÃ¡ÂºÂ§n rÃ¡Â»Âi lÃÂºc] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : '%s tÃ¡Â»Âi',
            past : '%s trÃÂ°Ã¡Â»Âc',
            s : 'vÃÂ i giÃÂ¢y',
            m : 'mÃ¡Â»Ât phÃÂºt',
            mm : '%d phÃÂºt',
            h : 'mÃ¡Â»Ât giÃ¡Â»Â',
            hh : '%d giÃ¡Â»Â',
            d : 'mÃ¡Â»Ât ngÃÂ y',
            dd : '%d ngÃÂ y',
            M : 'mÃ¡Â»Ât thÃÂ¡ng',
            MM : '%d thÃÂ¡ng',
            y : 'mÃ¡Â»Ât nÃÂm',
            yy : '%d nÃÂm'
        },
        ordinalParse: /\d{1,2}/,
        ordinal : function (number) {
            return number;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    return vi;

}));