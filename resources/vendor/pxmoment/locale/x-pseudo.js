//! moment.js locale configuration
//! locale : pseudo (x-pseudo)
//! author : Andrew Hood : https://github.com/andrewhood125

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var x_pseudo = moment.defineLocale('x-pseudo', {
        months : 'J~ÃÂ¡ÃÂ±ÃÂºÃÂ¡~rÃÂ½_F~ÃÂ©brÃÂº~ÃÂ¡rÃÂ½_~MÃÂ¡rc~h_ÃÂp~rÃÂ­l_~MÃÂ¡ÃÂ½_~JÃÂºÃÂ±ÃÂ©~_JÃÂºl~ÃÂ½_ÃÂÃÂº~gÃÂºst~_SÃÂ©p~tÃÂ©mb~ÃÂ©r_ÃÂ~ctÃÂ³b~ÃÂ©r_ÃÂ~ÃÂ³vÃÂ©m~bÃÂ©r_~DÃÂ©cÃÂ©~mbÃÂ©r'.split('_'),
        monthsShort : 'J~ÃÂ¡ÃÂ±_~FÃÂ©b_~MÃÂ¡r_~ÃÂpr_~MÃÂ¡ÃÂ½_~JÃÂºÃÂ±_~JÃÂºl_~ÃÂÃÂºg_~SÃÂ©p_~ÃÂct_~ÃÂÃÂ³v_~DÃÂ©c'.split('_'),
        monthsParseExact : true,
        weekdays : 'S~ÃÂºÃÂ±dÃÂ¡~ÃÂ½_MÃÂ³~ÃÂ±dÃÂ¡ÃÂ½~_TÃÂºÃÂ©~sdÃÂ¡ÃÂ½~_WÃÂ©d~ÃÂ±ÃÂ©sd~ÃÂ¡ÃÂ½_T~hÃÂºrs~dÃÂ¡ÃÂ½_~FrÃÂ­d~ÃÂ¡ÃÂ½_S~ÃÂ¡tÃÂºr~dÃÂ¡ÃÂ½'.split('_'),
        weekdaysShort : 'S~ÃÂºÃÂ±_~MÃÂ³ÃÂ±_~TÃÂºÃÂ©_~WÃÂ©d_~ThÃÂº_~FrÃÂ­_~SÃÂ¡t'.split('_'),
        weekdaysMin : 'S~ÃÂº_MÃÂ³~_TÃÂº_~WÃÂ©_T~h_Fr~_SÃÂ¡'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[T~ÃÂ³dÃÂ¡~ÃÂ½ ÃÂ¡t] LT',
            nextDay : '[T~ÃÂ³mÃÂ³~rrÃÂ³~w ÃÂ¡t] LT',
            nextWeek : 'dddd [ÃÂ¡t] LT',
            lastDay : '[ÃÂ~ÃÂ©st~ÃÂ©rdÃÂ¡~ÃÂ½ ÃÂ¡t] LT',
            lastWeek : '[L~ÃÂ¡st] dddd [ÃÂ¡t] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂ­~ÃÂ± %s',
            past : '%s ÃÂ¡~gÃÂ³',
            s : 'ÃÂ¡ ~fÃÂ©w ~sÃÂ©cÃÂ³~ÃÂ±ds',
            m : 'ÃÂ¡ ~mÃÂ­ÃÂ±~ÃÂºtÃÂ©',
            mm : '%d m~ÃÂ­ÃÂ±ÃÂº~tÃÂ©s',
            h : 'ÃÂ¡~ÃÂ± hÃÂ³~ÃÂºr',
            hh : '%d h~ÃÂ³ÃÂºrs',
            d : 'ÃÂ¡ ~dÃÂ¡ÃÂ½',
            dd : '%d d~ÃÂ¡ÃÂ½s',
            M : 'ÃÂ¡ ~mÃÂ³ÃÂ±~th',
            MM : '%d m~ÃÂ³ÃÂ±t~hs',
            y : 'ÃÂ¡ ~ÃÂ½ÃÂ©ÃÂ¡r',
            yy : '%d ÃÂ½~ÃÂ©ÃÂ¡rs'
        },
        ordinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    return x_pseudo;

}));