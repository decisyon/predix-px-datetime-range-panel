//! moment.js locale configuration
//! locale : traditional chinese (zh-tw)
//! author : Ben : https://github.com/ben-lin

;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';


    var zh_tw = moment.defineLocale('zh-tw', {
        months : 'Ã¤Â¸ÂÃ¦ÂÂ_Ã¤ÂºÂÃ¦ÂÂ_Ã¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂÃ¦ÂÂ_Ã¤ÂºÂÃ¦ÂÂ_Ã¥ÂÂ­Ã¦ÂÂ_Ã¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂ«Ã¦ÂÂ_Ã¤Â¹ÂÃ¦ÂÂ_Ã¥ÂÂÃ¦ÂÂ_Ã¥ÂÂÃ¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂÃ¤ÂºÂÃ¦ÂÂ'.split('_'),
        monthsShort : '1Ã¦ÂÂ_2Ã¦ÂÂ_3Ã¦ÂÂ_4Ã¦ÂÂ_5Ã¦ÂÂ_6Ã¦ÂÂ_7Ã¦ÂÂ_8Ã¦ÂÂ_9Ã¦ÂÂ_10Ã¦ÂÂ_11Ã¦ÂÂ_12Ã¦ÂÂ'.split('_'),
        weekdays : 'Ã¦ÂÂÃ¦ÂÂÃ¦ÂÂ¥_Ã¦ÂÂÃ¦ÂÂÃ¤Â¸Â_Ã¦ÂÂÃ¦ÂÂÃ¤ÂºÂ_Ã¦ÂÂÃ¦ÂÂÃ¤Â¸Â_Ã¦ÂÂÃ¦ÂÂÃ¥ÂÂ_Ã¦ÂÂÃ¦ÂÂÃ¤ÂºÂ_Ã¦ÂÂÃ¦ÂÂÃ¥ÂÂ­'.split('_'),
        weekdaysShort : 'Ã©ÂÂ±Ã¦ÂÂ¥_Ã©ÂÂ±Ã¤Â¸Â_Ã©ÂÂ±Ã¤ÂºÂ_Ã©ÂÂ±Ã¤Â¸Â_Ã©ÂÂ±Ã¥ÂÂ_Ã©ÂÂ±Ã¤ÂºÂ_Ã©ÂÂ±Ã¥ÂÂ­'.split('_'),
        weekdaysMin : 'Ã¦ÂÂ¥_Ã¤Â¸Â_Ã¤ÂºÂ_Ã¤Â¸Â_Ã¥ÂÂ_Ã¤ÂºÂ_Ã¥ÂÂ­'.split('_'),
        longDateFormat : {
            LT : 'AhÃ©Â»ÂmmÃ¥ÂÂ',
            LTS : 'AhÃ©Â»ÂmÃ¥ÂÂsÃ§Â§Â',
            L : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
            LL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
            LLL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥AhÃ©Â»ÂmmÃ¥ÂÂ',
            LLLL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥ddddAhÃ©Â»ÂmmÃ¥ÂÂ',
            l : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
            ll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
            lll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥AhÃ©Â»ÂmmÃ¥ÂÂ',
            llll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥ddddAhÃ©Â»ÂmmÃ¥ÂÂ'
        },
        meridiemParse: /Ã¦ÂÂ©Ã¤Â¸Â|Ã¤Â¸ÂÃ¥ÂÂ|Ã¤Â¸Â­Ã¥ÂÂ|Ã¤Â¸ÂÃ¥ÂÂ|Ã¦ÂÂÃ¤Â¸Â/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'Ã¦ÂÂ©Ã¤Â¸Â' || meridiem === 'Ã¤Â¸ÂÃ¥ÂÂ') {
                return hour;
            } else if (meridiem === 'Ã¤Â¸Â­Ã¥ÂÂ') {
                return hour >= 11 ? hour : hour + 12;
            } else if (meridiem === 'Ã¤Â¸ÂÃ¥ÂÂ' || meridiem === 'Ã¦ÂÂÃ¤Â¸Â') {
                return hour + 12;
            }
        },
        meridiem : function (hour, minute, isLower) {
            var hm = hour * 100 + minute;
            if (hm < 900) {
                return 'Ã¦ÂÂ©Ã¤Â¸Â';
            } else if (hm < 1130) {
                return 'Ã¤Â¸ÂÃ¥ÂÂ';
            } else if (hm < 1230) {
                return 'Ã¤Â¸Â­Ã¥ÂÂ';
            } else if (hm < 1800) {
                return 'Ã¤Â¸ÂÃ¥ÂÂ';
            } else {
                return 'Ã¦ÂÂÃ¤Â¸Â';
            }
        },
        calendar : {
            sameDay : '[Ã¤Â»ÂÃ¥Â¤Â©]LT',
            nextDay : '[Ã¦ÂÂÃ¥Â¤Â©]LT',
            nextWeek : '[Ã¤Â¸Â]ddddLT',
            lastDay : '[Ã¦ÂÂ¨Ã¥Â¤Â©]LT',
            lastWeek : '[Ã¤Â¸Â]ddddLT',
            sameElse : 'L'
        },
        ordinalParse: /\d{1,2}(Ã¦ÂÂ¥|Ã¦ÂÂ|Ã©ÂÂ±)/,
        ordinal : function (number, period) {
            switch (period) {
            case 'd' :
            case 'D' :
            case 'DDD' :
                return number + 'Ã¦ÂÂ¥';
            case 'M' :
                return number + 'Ã¦ÂÂ';
            case 'w' :
            case 'W' :
                return number + 'Ã©ÂÂ±';
            default :
                return number;
            }
        },
        relativeTime : {
            future : '%sÃ¥ÂÂ§',
            past : '%sÃ¥ÂÂ',
            s : 'Ã¥Â¹Â¾Ã§Â§Â',
            m : '1Ã¥ÂÂÃ©ÂÂ',
            mm : '%dÃ¥ÂÂÃ©ÂÂ',
            h : '1Ã¥Â°ÂÃ¦ÂÂ',
            hh : '%dÃ¥Â°ÂÃ¦ÂÂ',
            d : '1Ã¥Â¤Â©',
            dd : '%dÃ¥Â¤Â©',
            M : '1Ã¥ÂÂÃ¦ÂÂ',
            MM : '%dÃ¥ÂÂÃ¦ÂÂ',
            y : '1Ã¥Â¹Â´',
            yy : '%dÃ¥Â¹Â´'
        }
    });

    return zh_tw;

}));