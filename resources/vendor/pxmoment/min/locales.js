;(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined'
       && typeof require === 'function' ? factory(require('../moment')) :
   typeof define === 'function' && define.amd ? define(['moment'], factory) :
   factory(global.moment)
}(this, function (moment) { 'use strict';

    //! moment.js locale configuration
    //! locale : afrikaans (af)
    //! author : Werner Mollentze : https://github.com/wernerm

    var af = moment.defineLocale('af', {
        months : 'Januarie_Februarie_Maart_April_Mei_Junie_Julie_Augustus_September_Oktober_November_Desember'.split('_'),
        monthsShort : 'Jan_Feb_Mar_Apr_Mei_Jun_Jul_Aug_Sep_Okt_Nov_Des'.split('_'),
        weekdays : 'Sondag_Maandag_Dinsdag_Woensdag_Donderdag_Vrydag_Saterdag'.split('_'),
        weekdaysShort : 'Son_Maa_Din_Woe_Don_Vry_Sat'.split('_'),
        weekdaysMin : 'So_Ma_Di_Wo_Do_Vr_Sa'.split('_'),
        meridiemParse: /vm|nm/i,
        isPM : function (input) {
            return /^nm$/i.test(input);
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours < 12) {
                return isLower ? 'vm' : 'VM';
            } else {
                return isLower ? 'nm' : 'NM';
            }
        },
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[Vandag om] LT',
            nextDay : '[MÃÂ´re om] LT',
            nextWeek : 'dddd [om] LT',
            lastDay : '[Gister om] LT',
            lastWeek : '[Laas] dddd [om] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'oor %s',
            past : '%s gelede',
            s : '\'n paar sekondes',
            m : '\'n minuut',
            mm : '%d minute',
            h : '\'n uur',
            hh : '%d ure',
            d : '\'n dag',
            dd : '%d dae',
            M : '\'n maand',
            MM : '%d maande',
            y : '\'n jaar',
            yy : '%d jaar'
        },
        ordinalParse: /\d{1,2}(ste|de)/,
        ordinal : function (number) {
            return number + ((number === 1 || number === 8 || number >= 20) ? 'ste' : 'de'); // Thanks to Joris RÃÂ¶ling : https://github.com/jjupiter
        },
        week : {
            dow : 1, // Maandag is die eerste dag van die week.
            doy : 4  // Die week wat die 4de Januarie bevat is die eerste week van die jaar.
        }
    });

    //! moment.js locale configuration
    //! locale : Moroccan Arabic (ar-ma)
    //! author : ElFadili Yassine : https://github.com/ElFadiliY
    //! author : Abdel Said : https://github.com/abdelsaid

    var ar_ma = moment.defineLocale('ar-ma', {
        months : 'ÃÂÃÂÃÂ§ÃÂÃÂ±_ÃÂÃÂ¨ÃÂ±ÃÂ§ÃÂÃÂ±_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ£ÃÂ¨ÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂ_ÃÂÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂÃÂ²_ÃÂºÃÂ´ÃÂª_ÃÂ´ÃÂªÃÂÃÂ¨ÃÂ±_ÃÂ£ÃÂÃÂªÃÂÃÂ¨ÃÂ±_ÃÂÃÂÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂ¬ÃÂÃÂ¨ÃÂ±'.split('_'),
        monthsShort : 'ÃÂÃÂÃÂ§ÃÂÃÂ±_ÃÂÃÂ¨ÃÂ±ÃÂ§ÃÂÃÂ±_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ£ÃÂ¨ÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂ_ÃÂÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂÃÂ²_ÃÂºÃÂ´ÃÂª_ÃÂ´ÃÂªÃÂÃÂ¨ÃÂ±_ÃÂ£ÃÂÃÂªÃÂÃÂ¨ÃÂ±_ÃÂÃÂÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂ¬ÃÂÃÂ¨ÃÂ±'.split('_'),
        weekdays : 'ÃÂ§ÃÂÃÂ£ÃÂ­ÃÂ¯_ÃÂ§ÃÂÃÂ¥ÃÂªÃÂÃÂÃÂ_ÃÂ§ÃÂÃÂ«ÃÂÃÂ§ÃÂ«ÃÂ§ÃÂ¡_ÃÂ§ÃÂÃÂ£ÃÂ±ÃÂ¨ÃÂ¹ÃÂ§ÃÂ¡_ÃÂ§ÃÂÃÂ®ÃÂÃÂÃÂ³_ÃÂ§ÃÂÃÂ¬ÃÂÃÂ¹ÃÂ©_ÃÂ§ÃÂÃÂ³ÃÂ¨ÃÂª'.split('_'),
        weekdaysShort : 'ÃÂ§ÃÂ­ÃÂ¯_ÃÂ§ÃÂªÃÂÃÂÃÂ_ÃÂ«ÃÂÃÂ§ÃÂ«ÃÂ§ÃÂ¡_ÃÂ§ÃÂ±ÃÂ¨ÃÂ¹ÃÂ§ÃÂ¡_ÃÂ®ÃÂÃÂÃÂ³_ÃÂ¬ÃÂÃÂ¹ÃÂ©_ÃÂ³ÃÂ¨ÃÂª'.split('_'),
        weekdaysMin : 'ÃÂ­_ÃÂ_ÃÂ«_ÃÂ±_ÃÂ®_ÃÂ¬_ÃÂ³'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[ÃÂ§ÃÂÃÂÃÂÃÂ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            nextDay: '[ÃÂºÃÂ¯ÃÂ§ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            nextWeek: 'dddd [ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            lastDay: '[ÃÂ£ÃÂÃÂ³ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            lastWeek: 'dddd [ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'ÃÂÃÂ %s',
            past : 'ÃÂÃÂÃÂ° %s',
            s : 'ÃÂ«ÃÂÃÂ§ÃÂ',
            m : 'ÃÂ¯ÃÂÃÂÃÂÃÂ©',
            mm : '%d ÃÂ¯ÃÂÃÂ§ÃÂ¦ÃÂ',
            h : 'ÃÂ³ÃÂ§ÃÂ¹ÃÂ©',
            hh : '%d ÃÂ³ÃÂ§ÃÂ¹ÃÂ§ÃÂª',
            d : 'ÃÂÃÂÃÂ',
            dd : '%d ÃÂ£ÃÂÃÂ§ÃÂ',
            M : 'ÃÂ´ÃÂÃÂ±',
            MM : '%d ÃÂ£ÃÂ´ÃÂÃÂ±',
            y : 'ÃÂ³ÃÂÃÂ©',
            yy : '%d ÃÂ³ÃÂÃÂÃÂ§ÃÂª'
        },
        week : {
            dow : 6, // Saturday is the first day of the week.
            doy : 12  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Arabic Saudi Arabia (ar-sa)
    //! author : Suhail Alkowaileet : https://github.com/xsoh

    var ar_sa__symbolMap = {
        '1': 'ÃÂ¡',
        '2': 'ÃÂ¢',
        '3': 'ÃÂ£',
        '4': 'ÃÂ¤',
        '5': 'ÃÂ¥',
        '6': 'ÃÂ¦',
        '7': 'ÃÂ§',
        '8': 'ÃÂ¨',
        '9': 'ÃÂ©',
        '0': 'ÃÂ '
    }, ar_sa__numberMap = {
        'ÃÂ¡': '1',
        'ÃÂ¢': '2',
        'ÃÂ£': '3',
        'ÃÂ¤': '4',
        'ÃÂ¥': '5',
        'ÃÂ¦': '6',
        'ÃÂ§': '7',
        'ÃÂ¨': '8',
        'ÃÂ©': '9',
        'ÃÂ ': '0'
    };

    var ar_sa = moment.defineLocale('ar-sa', {
        months : 'ÃÂÃÂÃÂ§ÃÂÃÂ±_ÃÂÃÂ¨ÃÂ±ÃÂ§ÃÂÃÂ±_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ£ÃÂ¨ÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂ_ÃÂ£ÃÂºÃÂ³ÃÂ·ÃÂ³_ÃÂ³ÃÂ¨ÃÂªÃÂÃÂ¨ÃÂ±_ÃÂ£ÃÂÃÂªÃÂÃÂ¨ÃÂ±_ÃÂÃÂÃÂÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂÃÂ³ÃÂÃÂ¨ÃÂ±'.split('_'),
        monthsShort : 'ÃÂÃÂÃÂ§ÃÂÃÂ±_ÃÂÃÂ¨ÃÂ±ÃÂ§ÃÂÃÂ±_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ£ÃÂ¨ÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂ_ÃÂ£ÃÂºÃÂ³ÃÂ·ÃÂ³_ÃÂ³ÃÂ¨ÃÂªÃÂÃÂ¨ÃÂ±_ÃÂ£ÃÂÃÂªÃÂÃÂ¨ÃÂ±_ÃÂÃÂÃÂÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂÃÂ³ÃÂÃÂ¨ÃÂ±'.split('_'),
        weekdays : 'ÃÂ§ÃÂÃÂ£ÃÂ­ÃÂ¯_ÃÂ§ÃÂÃÂ¥ÃÂ«ÃÂÃÂÃÂ_ÃÂ§ÃÂÃÂ«ÃÂÃÂ§ÃÂ«ÃÂ§ÃÂ¡_ÃÂ§ÃÂÃÂ£ÃÂ±ÃÂ¨ÃÂ¹ÃÂ§ÃÂ¡_ÃÂ§ÃÂÃÂ®ÃÂÃÂÃÂ³_ÃÂ§ÃÂÃÂ¬ÃÂÃÂ¹ÃÂ©_ÃÂ§ÃÂÃÂ³ÃÂ¨ÃÂª'.split('_'),
        weekdaysShort : 'ÃÂ£ÃÂ­ÃÂ¯_ÃÂ¥ÃÂ«ÃÂÃÂÃÂ_ÃÂ«ÃÂÃÂ§ÃÂ«ÃÂ§ÃÂ¡_ÃÂ£ÃÂ±ÃÂ¨ÃÂ¹ÃÂ§ÃÂ¡_ÃÂ®ÃÂÃÂÃÂ³_ÃÂ¬ÃÂÃÂ¹ÃÂ©_ÃÂ³ÃÂ¨ÃÂª'.split('_'),
        weekdaysMin : 'ÃÂ­_ÃÂ_ÃÂ«_ÃÂ±_ÃÂ®_ÃÂ¬_ÃÂ³'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        meridiemParse: /ÃÂµ|ÃÂ/,
        isPM : function (input) {
            return 'ÃÂ' === input;
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 12) {
                return 'ÃÂµ';
            } else {
                return 'ÃÂ';
            }
        },
        calendar : {
            sameDay: '[ÃÂ§ÃÂÃÂÃÂÃÂ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            nextDay: '[ÃÂºÃÂ¯ÃÂ§ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            nextWeek: 'dddd [ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            lastDay: '[ÃÂ£ÃÂÃÂ³ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            lastWeek: 'dddd [ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'ÃÂÃÂ %s',
            past : 'ÃÂÃÂÃÂ° %s',
            s : 'ÃÂ«ÃÂÃÂ§ÃÂ',
            m : 'ÃÂ¯ÃÂÃÂÃÂÃÂ©',
            mm : '%d ÃÂ¯ÃÂÃÂ§ÃÂ¦ÃÂ',
            h : 'ÃÂ³ÃÂ§ÃÂ¹ÃÂ©',
            hh : '%d ÃÂ³ÃÂ§ÃÂ¹ÃÂ§ÃÂª',
            d : 'ÃÂÃÂÃÂ',
            dd : '%d ÃÂ£ÃÂÃÂ§ÃÂ',
            M : 'ÃÂ´ÃÂÃÂ±',
            MM : '%d ÃÂ£ÃÂ´ÃÂÃÂ±',
            y : 'ÃÂ³ÃÂÃÂ©',
            yy : '%d ÃÂ³ÃÂÃÂÃÂ§ÃÂª'
        },
        preparse: function (string) {
            return string.replace(/[ÃÂ¡ÃÂ¢ÃÂ£ÃÂ¤ÃÂ¥ÃÂ¦ÃÂ§ÃÂ¨ÃÂ©ÃÂ ]/g, function (match) {
                return ar_sa__numberMap[match];
            }).replace(/ÃÂ/g, ',');
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return ar_sa__symbolMap[match];
            }).replace(/,/g, 'ÃÂ');
        },
        week : {
            dow : 6, // Saturday is the first day of the week.
            doy : 12  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale  : Tunisian Arabic (ar-tn)

    var ar_tn = moment.defineLocale('ar-tn', {
        months: 'ÃÂ¬ÃÂ§ÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂ±ÃÂ_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ£ÃÂÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂ_ÃÂ¬ÃÂÃÂ§ÃÂ_ÃÂ¬ÃÂÃÂÃÂÃÂÃÂ©_ÃÂ£ÃÂÃÂª_ÃÂ³ÃÂ¨ÃÂªÃÂÃÂ¨ÃÂ±_ÃÂ£ÃÂÃÂªÃÂÃÂ¨ÃÂ±_ÃÂÃÂÃÂÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂÃÂ³ÃÂÃÂ¨ÃÂ±'.split('_'),
        monthsShort: 'ÃÂ¬ÃÂ§ÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂ±ÃÂ_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ£ÃÂÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂ_ÃÂ¬ÃÂÃÂ§ÃÂ_ÃÂ¬ÃÂÃÂÃÂÃÂÃÂ©_ÃÂ£ÃÂÃÂª_ÃÂ³ÃÂ¨ÃÂªÃÂÃÂ¨ÃÂ±_ÃÂ£ÃÂÃÂªÃÂÃÂ¨ÃÂ±_ÃÂÃÂÃÂÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂÃÂ³ÃÂÃÂ¨ÃÂ±'.split('_'),
        weekdays: 'ÃÂ§ÃÂÃÂ£ÃÂ­ÃÂ¯_ÃÂ§ÃÂÃÂ¥ÃÂ«ÃÂÃÂÃÂ_ÃÂ§ÃÂÃÂ«ÃÂÃÂ§ÃÂ«ÃÂ§ÃÂ¡_ÃÂ§ÃÂÃÂ£ÃÂ±ÃÂ¨ÃÂ¹ÃÂ§ÃÂ¡_ÃÂ§ÃÂÃÂ®ÃÂÃÂÃÂ³_ÃÂ§ÃÂÃÂ¬ÃÂÃÂ¹ÃÂ©_ÃÂ§ÃÂÃÂ³ÃÂ¨ÃÂª'.split('_'),
        weekdaysShort: 'ÃÂ£ÃÂ­ÃÂ¯_ÃÂ¥ÃÂ«ÃÂÃÂÃÂ_ÃÂ«ÃÂÃÂ§ÃÂ«ÃÂ§ÃÂ¡_ÃÂ£ÃÂ±ÃÂ¨ÃÂ¹ÃÂ§ÃÂ¡_ÃÂ®ÃÂÃÂÃÂ³_ÃÂ¬ÃÂÃÂ¹ÃÂ©_ÃÂ³ÃÂ¨ÃÂª'.split('_'),
        weekdaysMin: 'ÃÂ­_ÃÂ_ÃÂ«_ÃÂ±_ÃÂ®_ÃÂ¬_ÃÂ³'.split('_'),
        weekdaysParseExact : true,
        longDateFormat: {
            LT: 'HH:mm',
            LTS: 'HH:mm:ss',
            L: 'DD/MM/YYYY',
            LL: 'D MMMM YYYY',
            LLL: 'D MMMM YYYY HH:mm',
            LLLL: 'dddd D MMMM YYYY HH:mm'
        },
        calendar: {
            sameDay: '[ÃÂ§ÃÂÃÂÃÂÃÂ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            nextDay: '[ÃÂºÃÂ¯ÃÂ§ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            nextWeek: 'dddd [ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            lastDay: '[ÃÂ£ÃÂÃÂ³ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            lastWeek: 'dddd [ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            sameElse: 'L'
        },
        relativeTime: {
            future: 'ÃÂÃÂ %s',
            past: 'ÃÂÃÂÃÂ° %s',
            s: 'ÃÂ«ÃÂÃÂ§ÃÂ',
            m: 'ÃÂ¯ÃÂÃÂÃÂÃÂ©',
            mm: '%d ÃÂ¯ÃÂÃÂ§ÃÂ¦ÃÂ',
            h: 'ÃÂ³ÃÂ§ÃÂ¹ÃÂ©',
            hh: '%d ÃÂ³ÃÂ§ÃÂ¹ÃÂ§ÃÂª',
            d: 'ÃÂÃÂÃÂ',
            dd: '%d ÃÂ£ÃÂÃÂ§ÃÂ',
            M: 'ÃÂ´ÃÂÃÂ±',
            MM: '%d ÃÂ£ÃÂ´ÃÂÃÂ±',
            y: 'ÃÂ³ÃÂÃÂ©',
            yy: '%d ÃÂ³ÃÂÃÂÃÂ§ÃÂª'
        },
        week: {
            dow: 1, // Monday is the first day of the week.
            doy: 4 // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! Locale: Arabic (ar)
    //! Author: Abdel Said: https://github.com/abdelsaid
    //! Changes in months, weekdays: Ahmed Elkhatib
    //! Native plural forms: forabi https://github.com/forabi

    var ar__symbolMap = {
        '1': 'ÃÂ¡',
        '2': 'ÃÂ¢',
        '3': 'ÃÂ£',
        '4': 'ÃÂ¤',
        '5': 'ÃÂ¥',
        '6': 'ÃÂ¦',
        '7': 'ÃÂ§',
        '8': 'ÃÂ¨',
        '9': 'ÃÂ©',
        '0': 'ÃÂ '
    }, ar__numberMap = {
        'ÃÂ¡': '1',
        'ÃÂ¢': '2',
        'ÃÂ£': '3',
        'ÃÂ¤': '4',
        'ÃÂ¥': '5',
        'ÃÂ¦': '6',
        'ÃÂ§': '7',
        'ÃÂ¨': '8',
        'ÃÂ©': '9',
        'ÃÂ ': '0'
    }, pluralForm = function (n) {
        return n === 0 ? 0 : n === 1 ? 1 : n === 2 ? 2 : n % 100 >= 3 && n % 100 <= 10 ? 3 : n % 100 >= 11 ? 4 : 5;
    }, plurals = {
        s : ['ÃÂ£ÃÂÃÂ ÃÂÃÂ ÃÂ«ÃÂ§ÃÂÃÂÃÂ©', 'ÃÂ«ÃÂ§ÃÂÃÂÃÂ© ÃÂÃÂ§ÃÂ­ÃÂ¯ÃÂ©', ['ÃÂ«ÃÂ§ÃÂÃÂÃÂªÃÂ§ÃÂ', 'ÃÂ«ÃÂ§ÃÂÃÂÃÂªÃÂÃÂ'], '%d ÃÂ«ÃÂÃÂ§ÃÂ', '%d ÃÂ«ÃÂ§ÃÂÃÂÃÂ©', '%d ÃÂ«ÃÂ§ÃÂÃÂÃÂ©'],
        m : ['ÃÂ£ÃÂÃÂ ÃÂÃÂ ÃÂ¯ÃÂÃÂÃÂÃÂ©', 'ÃÂ¯ÃÂÃÂÃÂÃÂ© ÃÂÃÂ§ÃÂ­ÃÂ¯ÃÂ©', ['ÃÂ¯ÃÂÃÂÃÂÃÂªÃÂ§ÃÂ', 'ÃÂ¯ÃÂÃÂÃÂÃÂªÃÂÃÂ'], '%d ÃÂ¯ÃÂÃÂ§ÃÂ¦ÃÂ', '%d ÃÂ¯ÃÂÃÂÃÂÃÂ©', '%d ÃÂ¯ÃÂÃÂÃÂÃÂ©'],
        h : ['ÃÂ£ÃÂÃÂ ÃÂÃÂ ÃÂ³ÃÂ§ÃÂ¹ÃÂ©', 'ÃÂ³ÃÂ§ÃÂ¹ÃÂ© ÃÂÃÂ§ÃÂ­ÃÂ¯ÃÂ©', ['ÃÂ³ÃÂ§ÃÂ¹ÃÂªÃÂ§ÃÂ', 'ÃÂ³ÃÂ§ÃÂ¹ÃÂªÃÂÃÂ'], '%d ÃÂ³ÃÂ§ÃÂ¹ÃÂ§ÃÂª', '%d ÃÂ³ÃÂ§ÃÂ¹ÃÂ©', '%d ÃÂ³ÃÂ§ÃÂ¹ÃÂ©'],
        d : ['ÃÂ£ÃÂÃÂ ÃÂÃÂ ÃÂÃÂÃÂ', 'ÃÂÃÂÃÂ ÃÂÃÂ§ÃÂ­ÃÂ¯', ['ÃÂÃÂÃÂÃÂ§ÃÂ', 'ÃÂÃÂÃÂÃÂÃÂ'], '%d ÃÂ£ÃÂÃÂ§ÃÂ', '%d ÃÂÃÂÃÂÃÂÃÂ§', '%d ÃÂÃÂÃÂ'],
        M : ['ÃÂ£ÃÂÃÂ ÃÂÃÂ ÃÂ´ÃÂÃÂ±', 'ÃÂ´ÃÂÃÂ± ÃÂÃÂ§ÃÂ­ÃÂ¯', ['ÃÂ´ÃÂÃÂ±ÃÂ§ÃÂ', 'ÃÂ´ÃÂÃÂ±ÃÂÃÂ'], '%d ÃÂ£ÃÂ´ÃÂÃÂ±', '%d ÃÂ´ÃÂÃÂ±ÃÂ§', '%d ÃÂ´ÃÂÃÂ±'],
        y : ['ÃÂ£ÃÂÃÂ ÃÂÃÂ ÃÂ¹ÃÂ§ÃÂ', 'ÃÂ¹ÃÂ§ÃÂ ÃÂÃÂ§ÃÂ­ÃÂ¯', ['ÃÂ¹ÃÂ§ÃÂÃÂ§ÃÂ', 'ÃÂ¹ÃÂ§ÃÂÃÂÃÂ'], '%d ÃÂ£ÃÂ¹ÃÂÃÂ§ÃÂ', '%d ÃÂ¹ÃÂ§ÃÂÃÂÃÂ§', '%d ÃÂ¹ÃÂ§ÃÂ']
    }, pluralize = function (u) {
        return function (number, withoutSuffix, string, isFuture) {
            var f = pluralForm(number),
                str = plurals[u][pluralForm(number)];
            if (f === 2) {
                str = str[withoutSuffix ? 0 : 1];
            }
            return str.replace(/%d/i, number);
        };
    }, ar__months = [
        'ÃÂÃÂ§ÃÂÃÂÃÂ ÃÂ§ÃÂÃÂ«ÃÂ§ÃÂÃÂ ÃÂÃÂÃÂ§ÃÂÃÂ±',
        'ÃÂ´ÃÂ¨ÃÂ§ÃÂ· ÃÂÃÂ¨ÃÂ±ÃÂ§ÃÂÃÂ±',
        'ÃÂ¢ÃÂ°ÃÂ§ÃÂ± ÃÂÃÂ§ÃÂ±ÃÂ³',
        'ÃÂÃÂÃÂ³ÃÂ§ÃÂ ÃÂ£ÃÂ¨ÃÂ±ÃÂÃÂ',
        'ÃÂ£ÃÂÃÂ§ÃÂ± ÃÂÃÂ§ÃÂÃÂ',
        'ÃÂ­ÃÂ²ÃÂÃÂ±ÃÂ§ÃÂ ÃÂÃÂÃÂÃÂÃÂ',
        'ÃÂªÃÂÃÂÃÂ² ÃÂÃÂÃÂÃÂÃÂ',
        'ÃÂ¢ÃÂ¨ ÃÂ£ÃÂºÃÂ³ÃÂ·ÃÂ³',
        'ÃÂ£ÃÂÃÂÃÂÃÂ ÃÂ³ÃÂ¨ÃÂªÃÂÃÂ¨ÃÂ±',
        'ÃÂªÃÂ´ÃÂ±ÃÂÃÂ ÃÂ§ÃÂÃÂ£ÃÂÃÂ ÃÂ£ÃÂÃÂªÃÂÃÂ¨ÃÂ±',
        'ÃÂªÃÂ´ÃÂ±ÃÂÃÂ ÃÂ§ÃÂÃÂ«ÃÂ§ÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂ¨ÃÂ±',
        'ÃÂÃÂ§ÃÂÃÂÃÂ ÃÂ§ÃÂÃÂ£ÃÂÃÂ ÃÂ¯ÃÂÃÂ³ÃÂÃÂ¨ÃÂ±'
    ];

    var ar = moment.defineLocale('ar', {
        months : ar__months,
        monthsShort : ar__months,
        weekdays : 'ÃÂ§ÃÂÃÂ£ÃÂ­ÃÂ¯_ÃÂ§ÃÂÃÂ¥ÃÂ«ÃÂÃÂÃÂ_ÃÂ§ÃÂÃÂ«ÃÂÃÂ§ÃÂ«ÃÂ§ÃÂ¡_ÃÂ§ÃÂÃÂ£ÃÂ±ÃÂ¨ÃÂ¹ÃÂ§ÃÂ¡_ÃÂ§ÃÂÃÂ®ÃÂÃÂÃÂ³_ÃÂ§ÃÂÃÂ¬ÃÂÃÂ¹ÃÂ©_ÃÂ§ÃÂÃÂ³ÃÂ¨ÃÂª'.split('_'),
        weekdaysShort : 'ÃÂ£ÃÂ­ÃÂ¯_ÃÂ¥ÃÂ«ÃÂÃÂÃÂ_ÃÂ«ÃÂÃÂ§ÃÂ«ÃÂ§ÃÂ¡_ÃÂ£ÃÂ±ÃÂ¨ÃÂ¹ÃÂ§ÃÂ¡_ÃÂ®ÃÂÃÂÃÂ³_ÃÂ¬ÃÂÃÂ¹ÃÂ©_ÃÂ³ÃÂ¨ÃÂª'.split('_'),
        weekdaysMin : 'ÃÂ­_ÃÂ_ÃÂ«_ÃÂ±_ÃÂ®_ÃÂ¬_ÃÂ³'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'D/\u200FM/\u200FYYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        meridiemParse: /ÃÂµ|ÃÂ/,
        isPM : function (input) {
            return 'ÃÂ' === input;
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 12) {
                return 'ÃÂµ';
            } else {
                return 'ÃÂ';
            }
        },
        calendar : {
            sameDay: '[ÃÂ§ÃÂÃÂÃÂÃÂ ÃÂ¹ÃÂÃÂ¯ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            nextDay: '[ÃÂºÃÂ¯ÃÂÃÂ§ ÃÂ¹ÃÂÃÂ¯ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            nextWeek: 'dddd [ÃÂ¹ÃÂÃÂ¯ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            lastDay: '[ÃÂ£ÃÂÃÂ³ ÃÂ¹ÃÂÃÂ¯ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            lastWeek: 'dddd [ÃÂ¹ÃÂÃÂ¯ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'ÃÂ¨ÃÂ¹ÃÂ¯ %s',
            past : 'ÃÂÃÂÃÂ° %s',
            s : pluralize('s'),
            m : pluralize('m'),
            mm : pluralize('m'),
            h : pluralize('h'),
            hh : pluralize('h'),
            d : pluralize('d'),
            dd : pluralize('d'),
            M : pluralize('M'),
            MM : pluralize('M'),
            y : pluralize('y'),
            yy : pluralize('y')
        },
        preparse: function (string) {
            return string.replace(/\u200f/g, '').replace(/[ÃÂ¡ÃÂ¢ÃÂ£ÃÂ¤ÃÂ¥ÃÂ¦ÃÂ§ÃÂ¨ÃÂ©ÃÂ ]/g, function (match) {
                return ar__numberMap[match];
            }).replace(/ÃÂ/g, ',');
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return ar__symbolMap[match];
            }).replace(/,/g, 'ÃÂ');
        },
        week : {
            dow : 6, // Saturday is the first day of the week.
            doy : 12  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : azerbaijani (az)
    //! author : topchiyev : https://github.com/topchiyev

    var az__suffixes = {
        1: '-inci',
        5: '-inci',
        8: '-inci',
        70: '-inci',
        80: '-inci',
        2: '-nci',
        7: '-nci',
        20: '-nci',
        50: '-nci',
        3: '-ÃÂ¼ncÃÂ¼',
        4: '-ÃÂ¼ncÃÂ¼',
        100: '-ÃÂ¼ncÃÂ¼',
        6: '-ncÃÂ±',
        9: '-uncu',
        10: '-uncu',
        30: '-uncu',
        60: '-ÃÂ±ncÃÂ±',
        90: '-ÃÂ±ncÃÂ±'
    };

    var az = moment.defineLocale('az', {
        months : 'yanvar_fevral_mart_aprel_may_iyun_iyul_avqust_sentyabr_oktyabr_noyabr_dekabr'.split('_'),
        monthsShort : 'yan_fev_mar_apr_may_iyn_iyl_avq_sen_okt_noy_dek'.split('_'),
        weekdays : 'Bazar_Bazar ertÃÂsi_ÃÂÃÂrÃÂÃÂnbÃÂ axÃÂamÃÂ±_ÃÂÃÂrÃÂÃÂnbÃÂ_CÃÂ¼mÃÂ axÃÂamÃÂ±_CÃÂ¼mÃÂ_ÃÂÃÂnbÃÂ'.split('_'),
        weekdaysShort : 'Baz_BzE_ÃÂAx_ÃÂÃÂr_CAx_CÃÂ¼m_ÃÂÃÂn'.split('_'),
        weekdaysMin : 'Bz_BE_ÃÂA_ÃÂÃÂ_CA_CÃÂ¼_ÃÂÃÂ'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[bugÃÂ¼n saat] LT',
            nextDay : '[sabah saat] LT',
            nextWeek : '[gÃÂlÃÂn hÃÂftÃÂ] dddd [saat] LT',
            lastDay : '[dÃÂ¼nÃÂn] LT',
            lastWeek : '[keÃÂ§ÃÂn hÃÂftÃÂ] dddd [saat] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s sonra',
            past : '%s ÃÂvvÃÂl',
            s : 'birneÃÂ§ÃÂ saniyyÃÂ',
            m : 'bir dÃÂqiqÃÂ',
            mm : '%d dÃÂqiqÃÂ',
            h : 'bir saat',
            hh : '%d saat',
            d : 'bir gÃÂ¼n',
            dd : '%d gÃÂ¼n',
            M : 'bir ay',
            MM : '%d ay',
            y : 'bir il',
            yy : '%d il'
        },
        meridiemParse: /gecÃÂ|sÃÂhÃÂr|gÃÂ¼ndÃÂ¼z|axÃÂam/,
        isPM : function (input) {
            return /^(gÃÂ¼ndÃÂ¼z|axÃÂam)$/.test(input);
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'gecÃÂ';
            } else if (hour < 12) {
                return 'sÃÂhÃÂr';
            } else if (hour < 17) {
                return 'gÃÂ¼ndÃÂ¼z';
            } else {
                return 'axÃÂam';
            }
        },
        ordinalParse: /\d{1,2}-(ÃÂ±ncÃÂ±|inci|nci|ÃÂ¼ncÃÂ¼|ncÃÂ±|uncu)/,
        ordinal : function (number) {
            if (number === 0) {  // special case for zero
                return number + '-ÃÂ±ncÃÂ±';
            }
            var a = number % 10,
                b = number % 100 - a,
                c = number >= 100 ? 100 : null;
            return number + (az__suffixes[a] || az__suffixes[b] || az__suffixes[c]);
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : belarusian (be)
    //! author : Dmitry Demidov : https://github.com/demidov91
    //! author: Praleska: http://praleska.pro/
    //! Author : Menelion ElensÃÂºle : https://github.com/Oire

    function be__plural(word, num) {
        var forms = word.split('_');
        return num % 10 === 1 && num % 100 !== 11 ? forms[0] : (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) ? forms[1] : forms[2]);
    }
    function be__relativeTimeWithPlural(number, withoutSuffix, key) {
        var format = {
            'mm': withoutSuffix ? 'ÃÂÃÂ²ÃÂÃÂ»ÃÂÃÂ½ÃÂ°_ÃÂÃÂ²ÃÂÃÂ»ÃÂÃÂ½ÃÂ_ÃÂÃÂ²ÃÂÃÂ»ÃÂÃÂ½' : 'ÃÂÃÂ²ÃÂÃÂ»ÃÂÃÂ½ÃÂ_ÃÂÃÂ²ÃÂÃÂ»ÃÂÃÂ½ÃÂ_ÃÂÃÂ²ÃÂÃÂ»ÃÂÃÂ½',
            'hh': withoutSuffix ? 'ÃÂ³ÃÂ°ÃÂ´ÃÂ·ÃÂÃÂ½ÃÂ°_ÃÂ³ÃÂ°ÃÂ´ÃÂ·ÃÂÃÂ½ÃÂ_ÃÂ³ÃÂ°ÃÂ´ÃÂ·ÃÂÃÂ½' : 'ÃÂ³ÃÂ°ÃÂ´ÃÂ·ÃÂÃÂ½ÃÂ_ÃÂ³ÃÂ°ÃÂ´ÃÂ·ÃÂÃÂ½ÃÂ_ÃÂ³ÃÂ°ÃÂ´ÃÂ·ÃÂÃÂ½',
            'dd': 'ÃÂ´ÃÂ·ÃÂµÃÂ½ÃÂ_ÃÂ´ÃÂ½ÃÂ_ÃÂ´ÃÂ·ÃÂÃÂ½',
            'MM': 'ÃÂ¼ÃÂµÃÂÃÂÃÂ_ÃÂ¼ÃÂµÃÂÃÂÃÂÃÂ_ÃÂ¼ÃÂµÃÂÃÂÃÂÃÂ°ÃÂ',
            'yy': 'ÃÂ³ÃÂ¾ÃÂ´_ÃÂ³ÃÂ°ÃÂ´ÃÂ_ÃÂ³ÃÂ°ÃÂ´ÃÂ¾ÃÂ'
        };
        if (key === 'm') {
            return withoutSuffix ? 'ÃÂÃÂ²ÃÂÃÂ»ÃÂÃÂ½ÃÂ°' : 'ÃÂÃÂ²ÃÂÃÂ»ÃÂÃÂ½ÃÂ';
        }
        else if (key === 'h') {
            return withoutSuffix ? 'ÃÂ³ÃÂ°ÃÂ´ÃÂ·ÃÂÃÂ½ÃÂ°' : 'ÃÂ³ÃÂ°ÃÂ´ÃÂ·ÃÂÃÂ½ÃÂ';
        }
        else {
            return number + ' ' + be__plural(format[key], +number);
        }
    }

    var be = moment.defineLocale('be', {
        months : {
            format: 'ÃÂÃÂÃÂÃÂ´ÃÂ·ÃÂµÃÂ½ÃÂ_ÃÂ»ÃÂÃÂÃÂ°ÃÂ³ÃÂ°_ÃÂÃÂ°ÃÂºÃÂ°ÃÂ²ÃÂÃÂºÃÂ°_ÃÂºÃÂÃÂ°ÃÂÃÂ°ÃÂ²ÃÂÃÂºÃÂ°_ÃÂÃÂÃÂ°ÃÂÃÂ½ÃÂ_ÃÂÃÂÃÂÃÂ²ÃÂµÃÂ½ÃÂ_ÃÂ»ÃÂÃÂ¿ÃÂµÃÂ½ÃÂ_ÃÂ¶ÃÂ½ÃÂÃÂÃÂ½ÃÂ_ÃÂ²ÃÂµÃÂÃÂ°ÃÂÃÂ½ÃÂ_ÃÂºÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂºÃÂ°_ÃÂ»ÃÂÃÂÃÂÃÂ°ÃÂ¿ÃÂ°ÃÂ´ÃÂ°_ÃÂÃÂ½ÃÂµÃÂ¶ÃÂ½ÃÂ'.split('_'),
            standalone: 'ÃÂÃÂÃÂÃÂ´ÃÂ·ÃÂµÃÂ½ÃÂ_ÃÂ»ÃÂÃÂÃÂ_ÃÂÃÂ°ÃÂºÃÂ°ÃÂ²ÃÂÃÂº_ÃÂºÃÂÃÂ°ÃÂÃÂ°ÃÂ²ÃÂÃÂº_ÃÂÃÂÃÂ°ÃÂ²ÃÂµÃÂ½ÃÂ_ÃÂÃÂÃÂÃÂ²ÃÂµÃÂ½ÃÂ_ÃÂ»ÃÂÃÂ¿ÃÂµÃÂ½ÃÂ_ÃÂ¶ÃÂ½ÃÂÃÂ²ÃÂµÃÂ½ÃÂ_ÃÂ²ÃÂµÃÂÃÂ°ÃÂÃÂµÃÂ½ÃÂ_ÃÂºÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂº_ÃÂ»ÃÂÃÂÃÂÃÂ°ÃÂ¿ÃÂ°ÃÂ´_ÃÂÃÂ½ÃÂµÃÂ¶ÃÂ°ÃÂ½ÃÂ'.split('_')
        },
        monthsShort : 'ÃÂÃÂÃÂÃÂ´_ÃÂ»ÃÂÃÂ_ÃÂÃÂ°ÃÂº_ÃÂºÃÂÃÂ°ÃÂ_ÃÂÃÂÃÂ°ÃÂ²_ÃÂÃÂÃÂÃÂ²_ÃÂ»ÃÂÃÂ¿_ÃÂ¶ÃÂ½ÃÂÃÂ²_ÃÂ²ÃÂµÃÂ_ÃÂºÃÂ°ÃÂÃÂ_ÃÂ»ÃÂÃÂÃÂ_ÃÂÃÂ½ÃÂµÃÂ¶'.split('_'),
        weekdays : {
            format: 'ÃÂ½ÃÂÃÂ´ÃÂ·ÃÂµÃÂ»ÃÂ_ÃÂ¿ÃÂ°ÃÂ½ÃÂÃÂ´ÃÂ·ÃÂµÃÂ»ÃÂ°ÃÂº_ÃÂ°ÃÂÃÂÃÂ¾ÃÂÃÂ°ÃÂº_ÃÂÃÂµÃÂÃÂ°ÃÂ´ÃÂ_ÃÂÃÂ°ÃÂÃÂ²ÃÂµÃÂ_ÃÂ¿ÃÂÃÂÃÂ½ÃÂÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ'.split('_'),
            standalone: 'ÃÂ½ÃÂÃÂ´ÃÂ·ÃÂµÃÂ»ÃÂ_ÃÂ¿ÃÂ°ÃÂ½ÃÂÃÂ´ÃÂ·ÃÂµÃÂ»ÃÂ°ÃÂº_ÃÂ°ÃÂÃÂÃÂ¾ÃÂÃÂ°ÃÂº_ÃÂÃÂµÃÂÃÂ°ÃÂ´ÃÂ°_ÃÂÃÂ°ÃÂÃÂ²ÃÂµÃÂ_ÃÂ¿ÃÂÃÂÃÂ½ÃÂÃÂÃÂ°_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ°'.split('_'),
            isFormat: /\[ ?[ÃÂÃÂ²] ?(?:ÃÂ¼ÃÂÃÂ½ÃÂÃÂ»ÃÂÃÂ|ÃÂ½ÃÂ°ÃÂÃÂÃÂÃÂ¿ÃÂ½ÃÂÃÂ)? ?\] ?dddd/
        },
        weekdaysShort : 'ÃÂ½ÃÂ´_ÃÂ¿ÃÂ½_ÃÂ°ÃÂ_ÃÂÃÂ_ÃÂÃÂ_ÃÂ¿ÃÂ_ÃÂÃÂ±'.split('_'),
        weekdaysMin : 'ÃÂ½ÃÂ´_ÃÂ¿ÃÂ½_ÃÂ°ÃÂ_ÃÂÃÂ_ÃÂÃÂ_ÃÂ¿ÃÂ_ÃÂÃÂ±'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY ÃÂ³.',
            LLL : 'D MMMM YYYY ÃÂ³., HH:mm',
            LLLL : 'dddd, D MMMM YYYY ÃÂ³., HH:mm'
        },
        calendar : {
            sameDay: '[ÃÂ¡ÃÂÃÂ½ÃÂ½ÃÂ ÃÂ] LT',
            nextDay: '[ÃÂÃÂ°ÃÂÃÂÃÂÃÂ° ÃÂ] LT',
            lastDay: '[ÃÂ£ÃÂÃÂ¾ÃÂÃÂ° ÃÂ] LT',
            nextWeek: function () {
                return '[ÃÂ£] dddd [ÃÂ] LT';
            },
            lastWeek: function () {
                switch (this.day()) {
                case 0:
                case 3:
                case 5:
                case 6:
                    return '[ÃÂ£ ÃÂ¼ÃÂÃÂ½ÃÂÃÂ»ÃÂÃÂ] dddd [ÃÂ] LT';
                case 1:
                case 2:
                case 4:
                    return '[ÃÂ£ ÃÂ¼ÃÂÃÂ½ÃÂÃÂ»ÃÂ] dddd [ÃÂ] LT';
                }
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'ÃÂ¿ÃÂÃÂ°ÃÂ· %s',
            past : '%s ÃÂÃÂ°ÃÂ¼ÃÂ',
            s : 'ÃÂ½ÃÂµÃÂºÃÂ°ÃÂ»ÃÂÃÂºÃÂ ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´',
            m : be__relativeTimeWithPlural,
            mm : be__relativeTimeWithPlural,
            h : be__relativeTimeWithPlural,
            hh : be__relativeTimeWithPlural,
            d : 'ÃÂ´ÃÂ·ÃÂµÃÂ½ÃÂ',
            dd : be__relativeTimeWithPlural,
            M : 'ÃÂ¼ÃÂµÃÂÃÂÃÂ',
            MM : be__relativeTimeWithPlural,
            y : 'ÃÂ³ÃÂ¾ÃÂ´',
            yy : be__relativeTimeWithPlural
        },
        meridiemParse: /ÃÂ½ÃÂ¾ÃÂÃÂ|ÃÂÃÂ°ÃÂ½ÃÂÃÂÃÂ|ÃÂ´ÃÂ½ÃÂ|ÃÂ²ÃÂµÃÂÃÂ°ÃÂÃÂ°/,
        isPM : function (input) {
            return /^(ÃÂ´ÃÂ½ÃÂ|ÃÂ²ÃÂµÃÂÃÂ°ÃÂÃÂ°)$/.test(input);
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'ÃÂ½ÃÂ¾ÃÂÃÂ';
            } else if (hour < 12) {
                return 'ÃÂÃÂ°ÃÂ½ÃÂÃÂÃÂ';
            } else if (hour < 17) {
                return 'ÃÂ´ÃÂ½ÃÂ';
            } else {
                return 'ÃÂ²ÃÂµÃÂÃÂ°ÃÂÃÂ°';
            }
        },
        ordinalParse: /\d{1,2}-(ÃÂ|ÃÂ|ÃÂ³ÃÂ°)/,
        ordinal: function (number, period) {
            switch (period) {
            case 'M':
            case 'd':
            case 'DDD':
            case 'w':
            case 'W':
                return (number % 10 === 2 || number % 10 === 3) && (number % 100 !== 12 && number % 100 !== 13) ? number + '-ÃÂ' : number + '-ÃÂ';
            case 'D':
                return number + '-ÃÂ³ÃÂ°';
            default:
                return number;
            }
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : bulgarian (bg)
    //! author : Krasen Borisov : https://github.com/kraz

    var bg = moment.defineLocale('bg', {
        months : 'ÃÂÃÂ½ÃÂÃÂ°ÃÂÃÂ¸_ÃÂÃÂµÃÂ²ÃÂÃÂÃÂ°ÃÂÃÂ¸_ÃÂ¼ÃÂ°ÃÂÃÂ_ÃÂ°ÃÂ¿ÃÂÃÂ¸ÃÂ»_ÃÂ¼ÃÂ°ÃÂ¹_ÃÂÃÂ½ÃÂ¸_ÃÂÃÂ»ÃÂ¸_ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ¿ÃÂÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¸_ÃÂ¾ÃÂºÃÂÃÂ¾ÃÂ¼ÃÂ²ÃÂÃÂ¸_ÃÂ½ÃÂ¾ÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¸_ÃÂ´ÃÂµÃÂºÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¸'.split('_'),
        monthsShort : 'ÃÂÃÂ½ÃÂ_ÃÂÃÂµÃÂ²_ÃÂ¼ÃÂ°ÃÂ_ÃÂ°ÃÂ¿ÃÂ_ÃÂ¼ÃÂ°ÃÂ¹_ÃÂÃÂ½ÃÂ¸_ÃÂÃÂ»ÃÂ¸_ÃÂ°ÃÂ²ÃÂ³_ÃÂÃÂµÃÂ¿_ÃÂ¾ÃÂºÃÂ_ÃÂ½ÃÂ¾ÃÂµ_ÃÂ´ÃÂµÃÂº'.split('_'),
        weekdays : 'ÃÂ½ÃÂµÃÂ´ÃÂµÃÂ»ÃÂ_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂµÃÂ»ÃÂ½ÃÂ¸ÃÂº_ÃÂ²ÃÂÃÂ¾ÃÂÃÂ½ÃÂ¸ÃÂº_ÃÂÃÂÃÂÃÂ´ÃÂ°_ÃÂÃÂµÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂº_ÃÂ¿ÃÂµÃÂÃÂÃÂº_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ°'.split('_'),
        weekdaysShort : 'ÃÂ½ÃÂµÃÂ´_ÃÂ¿ÃÂ¾ÃÂ½_ÃÂ²ÃÂÃÂ¾_ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ_ÃÂ¿ÃÂµÃÂ_ÃÂÃÂÃÂ±'.split('_'),
        weekdaysMin : 'ÃÂ½ÃÂ´_ÃÂ¿ÃÂ½_ÃÂ²ÃÂ_ÃÂÃÂ_ÃÂÃÂ_ÃÂ¿ÃÂ_ÃÂÃÂ±'.split('_'),
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'D.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY H:mm',
            LLLL : 'dddd, D MMMM YYYY H:mm'
        },
        calendar : {
            sameDay : '[ÃÂÃÂ½ÃÂµÃÂ ÃÂ²] LT',
            nextDay : '[ÃÂ£ÃÂÃÂÃÂµ ÃÂ²] LT',
            nextWeek : 'dddd [ÃÂ²] LT',
            lastDay : '[ÃÂÃÂÃÂµÃÂÃÂ° ÃÂ²] LT',
            lastWeek : function () {
                switch (this.day()) {
                case 0:
                case 3:
                case 6:
                    return '[ÃÂ ÃÂ¸ÃÂ·ÃÂ¼ÃÂ¸ÃÂ½ÃÂ°ÃÂ»ÃÂ°ÃÂÃÂ°] dddd [ÃÂ²] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[ÃÂ ÃÂ¸ÃÂ·ÃÂ¼ÃÂ¸ÃÂ½ÃÂ°ÃÂ»ÃÂ¸ÃÂ] dddd [ÃÂ²] LT';
                }
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂÃÂ»ÃÂµÃÂ´ %s',
            past : 'ÃÂ¿ÃÂÃÂµÃÂ´ÃÂ¸ %s',
            s : 'ÃÂ½ÃÂÃÂºÃÂ¾ÃÂ»ÃÂºÃÂ¾ ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´ÃÂ¸',
            m : 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ°',
            mm : '%d ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ¸',
            h : 'ÃÂÃÂ°ÃÂ',
            hh : '%d ÃÂÃÂ°ÃÂÃÂ°',
            d : 'ÃÂ´ÃÂµÃÂ½',
            dd : '%d ÃÂ´ÃÂ½ÃÂ¸',
            M : 'ÃÂ¼ÃÂµÃÂÃÂµÃÂ',
            MM : '%d ÃÂ¼ÃÂµÃÂÃÂµÃÂÃÂ°',
            y : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°',
            yy : '%d ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ¸'
        },
        ordinalParse: /\d{1,2}-(ÃÂµÃÂ²|ÃÂµÃÂ½|ÃÂÃÂ¸|ÃÂ²ÃÂ¸|ÃÂÃÂ¸|ÃÂ¼ÃÂ¸)/,
        ordinal : function (number) {
            var lastDigit = number % 10,
                last2Digits = number % 100;
            if (number === 0) {
                return number + '-ÃÂµÃÂ²';
            } else if (last2Digits === 0) {
                return number + '-ÃÂµÃÂ½';
            } else if (last2Digits > 10 && last2Digits < 20) {
                return number + '-ÃÂÃÂ¸';
            } else if (lastDigit === 1) {
                return number + '-ÃÂ²ÃÂ¸';
            } else if (lastDigit === 2) {
                return number + '-ÃÂÃÂ¸';
            } else if (lastDigit === 7 || lastDigit === 8) {
                return number + '-ÃÂ¼ÃÂ¸';
            } else {
                return number + '-ÃÂÃÂ¸';
            }
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Bengali (bn)
    //! author : Kaushik Gandhi : https://github.com/kaushikgandhi

    var bn__symbolMap = {
        '1': 'Ã Â§Â§',
        '2': 'Ã Â§Â¨',
        '3': 'Ã Â§Â©',
        '4': 'Ã Â§Âª',
        '5': 'Ã Â§Â«',
        '6': 'Ã Â§Â¬',
        '7': 'Ã Â§Â­',
        '8': 'Ã Â§Â®',
        '9': 'Ã Â§Â¯',
        '0': 'Ã Â§Â¦'
    },
    bn__numberMap = {
        'Ã Â§Â§': '1',
        'Ã Â§Â¨': '2',
        'Ã Â§Â©': '3',
        'Ã Â§Âª': '4',
        'Ã Â§Â«': '5',
        'Ã Â§Â¬': '6',
        'Ã Â§Â­': '7',
        'Ã Â§Â®': '8',
        'Ã Â§Â¯': '9',
        'Ã Â§Â¦': '0'
    };

    var bn = moment.defineLocale('bn', {
        months : 'Ã Â¦ÂÃ Â¦Â¾Ã Â¦Â¨Ã Â§ÂÃ Â§ÂÃ Â¦Â¾Ã Â¦Â°Ã Â§Â_Ã Â¦Â«Ã Â§ÂÃ Â¦Â¬Ã Â§ÂÃ Â§ÂÃ Â¦Â¾Ã Â¦Â°Ã Â§Â_Ã Â¦Â®Ã Â¦Â¾Ã Â¦Â°Ã Â§ÂÃ Â¦Â_Ã Â¦ÂÃ Â¦ÂªÃ Â§ÂÃ Â¦Â°Ã Â¦Â¿Ã Â¦Â²_Ã Â¦Â®Ã Â§Â_Ã Â¦ÂÃ Â§ÂÃ Â¦Â¨_Ã Â¦ÂÃ Â§ÂÃ Â¦Â²Ã Â¦Â¾Ã Â¦Â_Ã Â¦ÂÃ Â¦ÂÃ Â¦Â¾Ã Â¦Â¸Ã Â§ÂÃ Â¦Â_Ã Â¦Â¸Ã Â§ÂÃ Â¦ÂªÃ Â§ÂÃ Â¦ÂÃ Â§ÂÃ Â¦Â®Ã Â§ÂÃ Â¦Â¬Ã Â¦Â°_Ã Â¦ÂÃ Â¦ÂÃ Â§ÂÃ Â¦ÂÃ Â§ÂÃ Â¦Â¬Ã Â¦Â°_Ã Â¦Â¨Ã Â¦Â­Ã Â§ÂÃ Â¦Â®Ã Â§ÂÃ Â¦Â¬Ã Â¦Â°_Ã Â¦Â¡Ã Â¦Â¿Ã Â¦Â¸Ã Â§ÂÃ Â¦Â®Ã Â§ÂÃ Â¦Â¬Ã Â¦Â°'.split('_'),
        monthsShort : 'Ã Â¦ÂÃ Â¦Â¾Ã Â¦Â¨Ã Â§Â_Ã Â¦Â«Ã Â§ÂÃ Â¦Â¬_Ã Â¦Â®Ã Â¦Â¾Ã Â¦Â°Ã Â§ÂÃ Â¦Â_Ã Â¦ÂÃ Â¦ÂªÃ Â¦Â°_Ã Â¦Â®Ã Â§Â_Ã Â¦ÂÃ Â§ÂÃ Â¦Â¨_Ã Â¦ÂÃ Â§ÂÃ Â¦Â²_Ã Â¦ÂÃ Â¦Â_Ã Â¦Â¸Ã Â§ÂÃ Â¦ÂªÃ Â§ÂÃ Â¦Â_Ã Â¦ÂÃ Â¦ÂÃ Â§ÂÃ Â¦ÂÃ Â§Â_Ã Â¦Â¨Ã Â¦Â­_Ã Â¦Â¡Ã Â¦Â¿Ã Â¦Â¸Ã Â§ÂÃ Â¦Â®Ã Â§Â'.split('_'),
        weekdays : 'Ã Â¦Â°Ã Â¦Â¬Ã Â¦Â¿Ã Â¦Â¬Ã Â¦Â¾Ã Â¦Â°_Ã Â¦Â¸Ã Â§ÂÃ Â¦Â®Ã Â¦Â¬Ã Â¦Â¾Ã Â¦Â°_Ã Â¦Â®Ã Â¦ÂÃ Â§ÂÃ Â¦ÂÃ Â¦Â²Ã Â¦Â¬Ã Â¦Â¾Ã Â¦Â°_Ã Â¦Â¬Ã Â§ÂÃ Â¦Â§Ã Â¦Â¬Ã Â¦Â¾Ã Â¦Â°_Ã Â¦Â¬Ã Â§ÂÃ Â¦Â¹Ã Â¦Â¸Ã Â§ÂÃ Â¦ÂªÃ Â¦Â¤Ã Â§ÂÃ Â¦Â¤Ã Â¦Â¿Ã Â¦Â¬Ã Â¦Â¾Ã Â¦Â°_Ã Â¦Â¶Ã Â§ÂÃ Â¦ÂÃ Â§ÂÃ Â¦Â°Ã Â¦Â¬Ã Â¦Â¾Ã Â¦Â°_Ã Â¦Â¶Ã Â¦Â¨Ã Â¦Â¿Ã Â¦Â¬Ã Â¦Â¾Ã Â¦Â°'.split('_'),
        weekdaysShort : 'Ã Â¦Â°Ã Â¦Â¬Ã Â¦Â¿_Ã Â¦Â¸Ã Â§ÂÃ Â¦Â®_Ã Â¦Â®Ã Â¦ÂÃ Â§ÂÃ Â¦ÂÃ Â¦Â²_Ã Â¦Â¬Ã Â§ÂÃ Â¦Â§_Ã Â¦Â¬Ã Â§ÂÃ Â¦Â¹Ã Â¦Â¸Ã Â§ÂÃ Â¦ÂªÃ Â¦Â¤Ã Â§ÂÃ Â¦Â¤Ã Â¦Â¿_Ã Â¦Â¶Ã Â§ÂÃ Â¦ÂÃ Â§ÂÃ Â¦Â°_Ã Â¦Â¶Ã Â¦Â¨Ã Â¦Â¿'.split('_'),
        weekdaysMin : 'Ã Â¦Â°Ã Â¦Â¬_Ã Â¦Â¸Ã Â¦Â®_Ã Â¦Â®Ã Â¦ÂÃ Â§ÂÃ Â¦Â_Ã Â¦Â¬Ã Â§Â_Ã Â¦Â¬Ã Â§ÂÃ Â¦Â°Ã Â¦Â¿Ã Â¦Â¹_Ã Â¦Â¶Ã Â§Â_Ã Â¦Â¶Ã Â¦Â¨Ã Â¦Â¿'.split('_'),
        longDateFormat : {
            LT : 'A h:mm Ã Â¦Â¸Ã Â¦Â®Ã Â§Â',
            LTS : 'A h:mm:ss Ã Â¦Â¸Ã Â¦Â®Ã Â§Â',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY, A h:mm Ã Â¦Â¸Ã Â¦Â®Ã Â§Â',
            LLLL : 'dddd, D MMMM YYYY, A h:mm Ã Â¦Â¸Ã Â¦Â®Ã Â§Â'
        },
        calendar : {
            sameDay : '[Ã Â¦ÂÃ Â¦Â] LT',
            nextDay : '[Ã Â¦ÂÃ Â¦ÂÃ Â¦Â¾Ã Â¦Â®Ã Â§ÂÃ Â¦ÂÃ Â¦Â¾Ã Â¦Â²] LT',
            nextWeek : 'dddd, LT',
            lastDay : '[Ã Â¦ÂÃ Â¦Â¤Ã Â¦ÂÃ Â¦Â¾Ã Â¦Â²] LT',
            lastWeek : '[Ã Â¦ÂÃ Â¦Â¤] dddd, LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s Ã Â¦ÂªÃ Â¦Â°Ã Â§Â',
            past : '%s Ã Â¦ÂÃ Â¦ÂÃ Â§Â',
            s : 'Ã Â¦ÂÃ Â§ÂÃ Â§ÂÃ Â¦Â Ã Â¦Â¸Ã Â§ÂÃ Â¦ÂÃ Â§ÂÃ Â¦Â¨Ã Â§ÂÃ Â¦Â¡',
            m : 'Ã Â¦ÂÃ Â¦Â Ã Â¦Â®Ã Â¦Â¿Ã Â¦Â¨Ã Â¦Â¿Ã Â¦Â',
            mm : '%d Ã Â¦Â®Ã Â¦Â¿Ã Â¦Â¨Ã Â¦Â¿Ã Â¦Â',
            h : 'Ã Â¦ÂÃ Â¦Â Ã Â¦ÂÃ Â¦Â¨Ã Â§ÂÃ Â¦ÂÃ Â¦Â¾',
            hh : '%d Ã Â¦ÂÃ Â¦Â¨Ã Â§ÂÃ Â¦ÂÃ Â¦Â¾',
            d : 'Ã Â¦ÂÃ Â¦Â Ã Â¦Â¦Ã Â¦Â¿Ã Â¦Â¨',
            dd : '%d Ã Â¦Â¦Ã Â¦Â¿Ã Â¦Â¨',
            M : 'Ã Â¦ÂÃ Â¦Â Ã Â¦Â®Ã Â¦Â¾Ã Â¦Â¸',
            MM : '%d Ã Â¦Â®Ã Â¦Â¾Ã Â¦Â¸',
            y : 'Ã Â¦ÂÃ Â¦Â Ã Â¦Â¬Ã Â¦ÂÃ Â¦Â°',
            yy : '%d Ã Â¦Â¬Ã Â¦ÂÃ Â¦Â°'
        },
        preparse: function (string) {
            return string.replace(/[Ã Â§Â§Ã Â§Â¨Ã Â§Â©Ã Â§ÂªÃ Â§Â«Ã Â§Â¬Ã Â§Â­Ã Â§Â®Ã Â§Â¯Ã Â§Â¦]/g, function (match) {
                return bn__numberMap[match];
            });
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return bn__symbolMap[match];
            });
        },
        meridiemParse: /Ã Â¦Â°Ã Â¦Â¾Ã Â¦Â¤|Ã Â¦Â¸Ã Â¦ÂÃ Â¦Â¾Ã Â¦Â²|Ã Â¦Â¦Ã Â§ÂÃ Â¦ÂªÃ Â§ÂÃ Â¦Â°|Ã Â¦Â¬Ã Â¦Â¿Ã Â¦ÂÃ Â¦Â¾Ã Â¦Â²|Ã Â¦Â°Ã Â¦Â¾Ã Â¦Â¤/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if ((meridiem === 'Ã Â¦Â°Ã Â¦Â¾Ã Â¦Â¤' && hour >= 4) ||
                    (meridiem === 'Ã Â¦Â¦Ã Â§ÂÃ Â¦ÂªÃ Â§ÂÃ Â¦Â°' && hour < 5) ||
                    meridiem === 'Ã Â¦Â¬Ã Â¦Â¿Ã Â¦ÂÃ Â¦Â¾Ã Â¦Â²') {
                return hour + 12;
            } else {
                return hour;
            }
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'Ã Â¦Â°Ã Â¦Â¾Ã Â¦Â¤';
            } else if (hour < 10) {
                return 'Ã Â¦Â¸Ã Â¦ÂÃ Â¦Â¾Ã Â¦Â²';
            } else if (hour < 17) {
                return 'Ã Â¦Â¦Ã Â§ÂÃ Â¦ÂªÃ Â§ÂÃ Â¦Â°';
            } else if (hour < 20) {
                return 'Ã Â¦Â¬Ã Â¦Â¿Ã Â¦ÂÃ Â¦Â¾Ã Â¦Â²';
            } else {
                return 'Ã Â¦Â°Ã Â¦Â¾Ã Â¦Â¤';
            }
        },
        week : {
            dow : 0, // Sunday is the first day of the week.
            doy : 6  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : tibetan (bo)
    //! author : Thupten N. Chakrishar : https://github.com/vajradog

    var bo__symbolMap = {
        '1': 'Ã Â¼Â¡',
        '2': 'Ã Â¼Â¢',
        '3': 'Ã Â¼Â£',
        '4': 'Ã Â¼Â¤',
        '5': 'Ã Â¼Â¥',
        '6': 'Ã Â¼Â¦',
        '7': 'Ã Â¼Â§',
        '8': 'Ã Â¼Â¨',
        '9': 'Ã Â¼Â©',
        '0': 'Ã Â¼Â '
    },
    bo__numberMap = {
        'Ã Â¼Â¡': '1',
        'Ã Â¼Â¢': '2',
        'Ã Â¼Â£': '3',
        'Ã Â¼Â¤': '4',
        'Ã Â¼Â¥': '5',
        'Ã Â¼Â¦': '6',
        'Ã Â¼Â§': '7',
        'Ã Â¼Â¨': '8',
        'Ã Â¼Â©': '9',
        'Ã Â¼Â ': '0'
    };

    var bo = moment.defineLocale('bo', {
        months : 'Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¼_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½Â¦Ã Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¦Ã Â½Â´Ã Â½ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½Â£Ã Â¾ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â¾Â²Ã Â½Â´Ã Â½ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â´Ã Â½ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¢Ã Â¾ÂÃ Â¾Â±Ã Â½ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â´Ã Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â´Ã Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â´Ã Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â´Ã Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½Â¦Ã Â¼ÂÃ Â½Â'.split('_'),
        monthsShort : 'Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¼_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½Â¦Ã Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¦Ã Â½Â´Ã Â½ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½Â£Ã Â¾ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â¾Â²Ã Â½Â´Ã Â½ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â´Ã Â½ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¢Ã Â¾ÂÃ Â¾Â±Ã Â½ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â´Ã Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â´Ã Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â´Ã Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½ÂÃ Â¼ÂÃ Â½Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â´Ã Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½Â¦Ã Â¼ÂÃ Â½Â'.split('_'),
        weekdays : 'Ã Â½ÂÃ Â½ÂÃ Â½Â Ã Â¼ÂÃ Â½ÂÃ Â½Â²Ã Â¼ÂÃ Â½ÂÃ Â¼Â_Ã Â½ÂÃ Â½ÂÃ Â½Â Ã Â¼ÂÃ Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼Â_Ã Â½ÂÃ Â½ÂÃ Â½Â Ã Â¼ÂÃ Â½ÂÃ Â½Â²Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â¢Ã Â¼Â_Ã Â½ÂÃ Â½ÂÃ Â½Â Ã Â¼ÂÃ Â½Â£Ã Â¾Â·Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â¼Â_Ã Â½ÂÃ Â½ÂÃ Â½Â Ã Â¼ÂÃ Â½ÂÃ Â½Â´Ã Â½Â¢Ã Â¼ÂÃ Â½ÂÃ Â½Â´_Ã Â½ÂÃ Â½ÂÃ Â½Â Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½Â¦Ã Â½ÂÃ Â½Â¦Ã Â¼Â_Ã Â½ÂÃ Â½ÂÃ Â½Â Ã Â¼ÂÃ Â½Â¦Ã Â¾Â¤Ã Â½ÂºÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â¼Â'.split('_'),
        weekdaysShort : 'Ã Â½ÂÃ Â½Â²Ã Â¼ÂÃ Â½ÂÃ Â¼Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼Â_Ã Â½ÂÃ Â½Â²Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â¢Ã Â¼Â_Ã Â½Â£Ã Â¾Â·Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â¼Â_Ã Â½ÂÃ Â½Â´Ã Â½Â¢Ã Â¼ÂÃ Â½ÂÃ Â½Â´_Ã Â½ÂÃ Â¼ÂÃ Â½Â¦Ã Â½ÂÃ Â½Â¦Ã Â¼Â_Ã Â½Â¦Ã Â¾Â¤Ã Â½ÂºÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â¼Â'.split('_'),
        weekdaysMin : 'Ã Â½ÂÃ Â½Â²Ã Â¼ÂÃ Â½ÂÃ Â¼Â_Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼Â_Ã Â½ÂÃ Â½Â²Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â¢Ã Â¼Â_Ã Â½Â£Ã Â¾Â·Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â¼Â_Ã Â½ÂÃ Â½Â´Ã Â½Â¢Ã Â¼ÂÃ Â½ÂÃ Â½Â´_Ã Â½ÂÃ Â¼ÂÃ Â½Â¦Ã Â½ÂÃ Â½Â¦Ã Â¼Â_Ã Â½Â¦Ã Â¾Â¤Ã Â½ÂºÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â¼Â'.split('_'),
        longDateFormat : {
            LT : 'A h:mm',
            LTS : 'A h:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY, A h:mm',
            LLLL : 'dddd, D MMMM YYYY, A h:mm'
        },
        calendar : {
            sameDay : '[Ã Â½ÂÃ Â½Â²Ã Â¼ÂÃ Â½Â¢Ã Â½Â²Ã Â½Â] LT',
            nextDay : '[Ã Â½Â¦Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â²Ã Â½Â] LT',
            nextWeek : '[Ã Â½ÂÃ Â½ÂÃ Â½Â´Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â¾Â²Ã Â½ÂÃ Â¼ÂÃ Â½Â¢Ã Â¾ÂÃ Â½ÂºÃ Â½Â¦Ã Â¼ÂÃ Â½Â], LT',
            lastDay : '[Ã Â½ÂÃ Â¼ÂÃ Â½Â¦Ã Â½Â] LT',
            lastWeek : '[Ã Â½ÂÃ Â½ÂÃ Â½Â´Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â¾Â²Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â Ã Â¼ÂÃ Â½Â] dddd, LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s Ã Â½Â£Ã Â¼Â',
            past : '%s Ã Â½Â¦Ã Â¾ÂÃ Â½ÂÃ Â¼ÂÃ Â½Â£',
            s : 'Ã Â½Â£Ã Â½ÂÃ Â¼ÂÃ Â½Â¦Ã Â½Â',
            m : 'Ã Â½Â¦Ã Â¾ÂÃ Â½Â¢Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½Â',
            mm : '%d Ã Â½Â¦Ã Â¾ÂÃ Â½Â¢Ã Â¼ÂÃ Â½Â',
            h : 'Ã Â½ÂÃ Â½Â´Ã Â¼ÂÃ Â½ÂÃ Â½Â¼Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½Â',
            hh : '%d Ã Â½ÂÃ Â½Â´Ã Â¼ÂÃ Â½ÂÃ Â½Â¼Ã Â½Â',
            d : 'Ã Â½ÂÃ Â½Â²Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½Â',
            dd : '%d Ã Â½ÂÃ Â½Â²Ã Â½ÂÃ Â¼Â',
            M : 'Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½Â',
            MM : '%d Ã Â½ÂÃ Â¾Â³Ã Â¼ÂÃ Â½Â',
            y : 'Ã Â½Â£Ã Â½Â¼Ã Â¼ÂÃ Â½ÂÃ Â½ÂÃ Â½Â²Ã Â½Â',
            yy : '%d Ã Â½Â£Ã Â½Â¼'
        },
        preparse: function (string) {
            return string.replace(/[Ã Â¼Â¡Ã Â¼Â¢Ã Â¼Â£Ã Â¼Â¤Ã Â¼Â¥Ã Â¼Â¦Ã Â¼Â§Ã Â¼Â¨Ã Â¼Â©Ã Â¼Â ]/g, function (match) {
                return bo__numberMap[match];
            });
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return bo__symbolMap[match];
            });
        },
        meridiemParse: /Ã Â½ÂÃ Â½ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¼|Ã Â½ÂÃ Â½Â¼Ã Â½ÂÃ Â½Â¦Ã Â¼ÂÃ Â½ÂÃ Â½Â¦|Ã Â½ÂÃ Â½Â²Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â´Ã Â½Â|Ã Â½ÂÃ Â½ÂÃ Â½Â¼Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â|Ã Â½ÂÃ Â½ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¼/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if ((meridiem === 'Ã Â½ÂÃ Â½ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¼' && hour >= 4) ||
                    (meridiem === 'Ã Â½ÂÃ Â½Â²Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â´Ã Â½Â' && hour < 5) ||
                    meridiem === 'Ã Â½ÂÃ Â½ÂÃ Â½Â¼Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â') {
                return hour + 12;
            } else {
                return hour;
            }
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'Ã Â½ÂÃ Â½ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¼';
            } else if (hour < 10) {
                return 'Ã Â½ÂÃ Â½Â¼Ã Â½ÂÃ Â½Â¦Ã Â¼ÂÃ Â½ÂÃ Â½Â¦';
            } else if (hour < 17) {
                return 'Ã Â½ÂÃ Â½Â²Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â´Ã Â½Â';
            } else if (hour < 20) {
                return 'Ã Â½ÂÃ Â½ÂÃ Â½Â¼Ã Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â';
            } else {
                return 'Ã Â½ÂÃ Â½ÂÃ Â½ÂÃ Â¼ÂÃ Â½ÂÃ Â½Â¼';
            }
        },
        week : {
            dow : 0, // Sunday is the first day of the week.
            doy : 6  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : breton (br)
    //! author : Jean-Baptiste Le Duigou : https://github.com/jbleduigou

    function relativeTimeWithMutation(number, withoutSuffix, key) {
        var format = {
            'mm': 'munutenn',
            'MM': 'miz',
            'dd': 'devezh'
        };
        return number + ' ' + mutation(format[key], number);
    }
    function specialMutationForYears(number) {
        switch (lastNumber(number)) {
        case 1:
        case 3:
        case 4:
        case 5:
        case 9:
            return number + ' bloaz';
        default:
            return number + ' vloaz';
        }
    }
    function lastNumber(number) {
        if (number > 9) {
            return lastNumber(number % 10);
        }
        return number;
    }
    function mutation(text, number) {
        if (number === 2) {
            return softMutation(text);
        }
        return text;
    }
    function softMutation(text) {
        var mutationTable = {
            'm': 'v',
            'b': 'v',
            'd': 'z'
        };
        if (mutationTable[text.charAt(0)] === undefined) {
            return text;
        }
        return mutationTable[text.charAt(0)] + text.substring(1);
    }

    var br = moment.defineLocale('br', {
        months : 'Genver_C\'hwevrer_Meurzh_Ebrel_Mae_Mezheven_Gouere_Eost_Gwengolo_Here_Du_Kerzu'.split('_'),
        monthsShort : 'Gen_C\'hwe_Meu_Ebr_Mae_Eve_Gou_Eos_Gwe_Her_Du_Ker'.split('_'),
        weekdays : 'Sul_Lun_Meurzh_Merc\'her_Yaou_Gwener_Sadorn'.split('_'),
        weekdaysShort : 'Sul_Lun_Meu_Mer_Yao_Gwe_Sad'.split('_'),
        weekdaysMin : 'Su_Lu_Me_Mer_Ya_Gw_Sa'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'h[e]mm A',
            LTS : 'h[e]mm:ss A',
            L : 'DD/MM/YYYY',
            LL : 'D [a viz] MMMM YYYY',
            LLL : 'D [a viz] MMMM YYYY h[e]mm A',
            LLLL : 'dddd, D [a viz] MMMM YYYY h[e]mm A'
        },
        calendar : {
            sameDay : '[Hiziv da] LT',
            nextDay : '[Warc\'hoazh da] LT',
            nextWeek : 'dddd [da] LT',
            lastDay : '[Dec\'h da] LT',
            lastWeek : 'dddd [paset da] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'a-benn %s',
            past : '%s \'zo',
            s : 'un nebeud segondennoÃÂ¹',
            m : 'ur vunutenn',
            mm : relativeTimeWithMutation,
            h : 'un eur',
            hh : '%d eur',
            d : 'un devezh',
            dd : relativeTimeWithMutation,
            M : 'ur miz',
            MM : relativeTimeWithMutation,
            y : 'ur bloaz',
            yy : specialMutationForYears
        },
        ordinalParse: /\d{1,2}(aÃÂ±|vet)/,
        ordinal : function (number) {
            var output = (number === 1) ? 'aÃÂ±' : 'vet';
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : bosnian (bs)
    //! author : Nedim Cholich : https://github.com/frontyard
    //! based on (hr) translation by Bojan MarkoviÃÂ

    function bs__translate(number, withoutSuffix, key) {
        var result = number + ' ';
        switch (key) {
        case 'm':
            return withoutSuffix ? 'jedna minuta' : 'jedne minute';
        case 'mm':
            if (number === 1) {
                result += 'minuta';
            } else if (number === 2 || number === 3 || number === 4) {
                result += 'minute';
            } else {
                result += 'minuta';
            }
            return result;
        case 'h':
            return withoutSuffix ? 'jedan sat' : 'jednog sata';
        case 'hh':
            if (number === 1) {
                result += 'sat';
            } else if (number === 2 || number === 3 || number === 4) {
                result += 'sata';
            } else {
                result += 'sati';
            }
            return result;
        case 'dd':
            if (number === 1) {
                result += 'dan';
            } else {
                result += 'dana';
            }
            return result;
        case 'MM':
            if (number === 1) {
                result += 'mjesec';
            } else if (number === 2 || number === 3 || number === 4) {
                result += 'mjeseca';
            } else {
                result += 'mjeseci';
            }
            return result;
        case 'yy':
            if (number === 1) {
                result += 'godina';
            } else if (number === 2 || number === 3 || number === 4) {
                result += 'godine';
            } else {
                result += 'godina';
            }
            return result;
        }
    }

    var bs = moment.defineLocale('bs', {
        months : 'januar_februar_mart_april_maj_juni_juli_august_septembar_oktobar_novembar_decembar'.split('_'),
        monthsShort : 'jan._feb._mar._apr._maj._jun._jul._aug._sep._okt._nov._dec.'.split('_'),
        monthsParseExact: true,
        weekdays : 'nedjelja_ponedjeljak_utorak_srijeda_ÃÂetvrtak_petak_subota'.split('_'),
        weekdaysShort : 'ned._pon._uto._sri._ÃÂet._pet._sub.'.split('_'),
        weekdaysMin : 'ne_po_ut_sr_ÃÂe_pe_su'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD. MM. YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY H:mm',
            LLLL : 'dddd, D. MMMM YYYY H:mm'
        },
        calendar : {
            sameDay  : '[danas u] LT',
            nextDay  : '[sutra u] LT',
            nextWeek : function () {
                switch (this.day()) {
                case 0:
                    return '[u] [nedjelju] [u] LT';
                case 3:
                    return '[u] [srijedu] [u] LT';
                case 6:
                    return '[u] [subotu] [u] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[u] dddd [u] LT';
                }
            },
            lastDay  : '[juÃÂer u] LT',
            lastWeek : function () {
                switch (this.day()) {
                case 0:
                case 3:
                    return '[proÃÂ¡lu] dddd [u] LT';
                case 6:
                    return '[proÃÂ¡le] [subote] [u] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[proÃÂ¡li] dddd [u] LT';
                }
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : 'za %s',
            past   : 'prije %s',
            s      : 'par sekundi',
            m      : bs__translate,
            mm     : bs__translate,
            h      : bs__translate,
            hh     : bs__translate,
            d      : 'dan',
            dd     : bs__translate,
            M      : 'mjesec',
            MM     : bs__translate,
            y      : 'godinu',
            yy     : bs__translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : catalan (ca)
    //! author : Juan G. Hurtado : https://github.com/juanghurtado

    var ca = moment.defineLocale('ca', {
        months : 'gener_febrer_marÃÂ§_abril_maig_juny_juliol_agost_setembre_octubre_novembre_desembre'.split('_'),
        monthsShort : 'gen._febr._mar._abr._mai._jun._jul._ag._set._oct._nov._des.'.split('_'),
        monthsParseExact : true,
        weekdays : 'diumenge_dilluns_dimarts_dimecres_dijous_divendres_dissabte'.split('_'),
        weekdaysShort : 'dg._dl._dt._dc._dj._dv._ds.'.split('_'),
        weekdaysMin : 'Dg_Dl_Dt_Dc_Dj_Dv_Ds'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY H:mm',
            LLLL : 'dddd D MMMM YYYY H:mm'
        },
        calendar : {
            sameDay : function () {
                return '[avui a ' + ((this.hours() !== 1) ? 'les' : 'la') + '] LT';
            },
            nextDay : function () {
                return '[demÃÂ  a ' + ((this.hours() !== 1) ? 'les' : 'la') + '] LT';
            },
            nextWeek : function () {
                return 'dddd [a ' + ((this.hours() !== 1) ? 'les' : 'la') + '] LT';
            },
            lastDay : function () {
                return '[ahir a ' + ((this.hours() !== 1) ? 'les' : 'la') + '] LT';
            },
            lastWeek : function () {
                return '[el] dddd [passat a ' + ((this.hours() !== 1) ? 'les' : 'la') + '] LT';
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : 'en %s',
            past : 'fa %s',
            s : 'uns segons',
            m : 'un minut',
            mm : '%d minuts',
            h : 'una hora',
            hh : '%d hores',
            d : 'un dia',
            dd : '%d dies',
            M : 'un mes',
            MM : '%d mesos',
            y : 'un any',
            yy : '%d anys'
        },
        ordinalParse: /\d{1,2}(r|n|t|ÃÂ¨|a)/,
        ordinal : function (number, period) {
            var output = (number === 1) ? 'r' :
                (number === 2) ? 'n' :
                (number === 3) ? 'r' :
                (number === 4) ? 't' : 'ÃÂ¨';
            if (period === 'w' || period === 'W') {
                output = 'a';
            }
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : czech (cs)
    //! author : petrbela : https://github.com/petrbela

    var cs__months = 'leden_ÃÂºnor_bÃÂezen_duben_kvÃÂten_ÃÂerven_ÃÂervenec_srpen_zÃÂ¡ÃÂÃÂ­_ÃÂÃÂ­jen_listopad_prosinec'.split('_'),
        cs__monthsShort = 'led_ÃÂºno_bÃÂe_dub_kvÃÂ_ÃÂvn_ÃÂvc_srp_zÃÂ¡ÃÂ_ÃÂÃÂ­j_lis_pro'.split('_');
    function cs__plural(n) {
        return (n > 1) && (n < 5) && (~~(n / 10) !== 1);
    }
    function cs__translate(number, withoutSuffix, key, isFuture) {
        var result = number + ' ';
        switch (key) {
        case 's':  // a few seconds / in a few seconds / a few seconds ago
            return (withoutSuffix || isFuture) ? 'pÃÂ¡r sekund' : 'pÃÂ¡r sekundami';
        case 'm':  // a minute / in a minute / a minute ago
            return withoutSuffix ? 'minuta' : (isFuture ? 'minutu' : 'minutou');
        case 'mm': // 9 minutes / in 9 minutes / 9 minutes ago
            if (withoutSuffix || isFuture) {
                return result + (cs__plural(number) ? 'minuty' : 'minut');
            } else {
                return result + 'minutami';
            }
            break;
        case 'h':  // an hour / in an hour / an hour ago
            return withoutSuffix ? 'hodina' : (isFuture ? 'hodinu' : 'hodinou');
        case 'hh': // 9 hours / in 9 hours / 9 hours ago
            if (withoutSuffix || isFuture) {
                return result + (cs__plural(number) ? 'hodiny' : 'hodin');
            } else {
                return result + 'hodinami';
            }
            break;
        case 'd':  // a day / in a day / a day ago
            return (withoutSuffix || isFuture) ? 'den' : 'dnem';
        case 'dd': // 9 days / in 9 days / 9 days ago
            if (withoutSuffix || isFuture) {
                return result + (cs__plural(number) ? 'dny' : 'dnÃÂ­');
            } else {
                return result + 'dny';
            }
            break;
        case 'M':  // a month / in a month / a month ago
            return (withoutSuffix || isFuture) ? 'mÃÂsÃÂ­c' : 'mÃÂsÃÂ­cem';
        case 'MM': // 9 months / in 9 months / 9 months ago
            if (withoutSuffix || isFuture) {
                return result + (cs__plural(number) ? 'mÃÂsÃÂ­ce' : 'mÃÂsÃÂ­cÃÂ¯');
            } else {
                return result + 'mÃÂsÃÂ­ci';
            }
            break;
        case 'y':  // a year / in a year / a year ago
            return (withoutSuffix || isFuture) ? 'rok' : 'rokem';
        case 'yy': // 9 years / in 9 years / 9 years ago
            if (withoutSuffix || isFuture) {
                return result + (cs__plural(number) ? 'roky' : 'let');
            } else {
                return result + 'lety';
            }
            break;
        }
    }

    var cs = moment.defineLocale('cs', {
        months : cs__months,
        monthsShort : cs__monthsShort,
        monthsParse : (function (months, monthsShort) {
            var i, _monthsParse = [];
            for (i = 0; i < 12; i++) {
                // use custom parser to solve problem with July (ÃÂervenec)
                _monthsParse[i] = new RegExp('^' + months[i] + '$|^' + monthsShort[i] + '$', 'i');
            }
            return _monthsParse;
        }(cs__months, cs__monthsShort)),
        shortMonthsParse : (function (monthsShort) {
            var i, _shortMonthsParse = [];
            for (i = 0; i < 12; i++) {
                _shortMonthsParse[i] = new RegExp('^' + monthsShort[i] + '$', 'i');
            }
            return _shortMonthsParse;
        }(cs__monthsShort)),
        longMonthsParse : (function (months) {
            var i, _longMonthsParse = [];
            for (i = 0; i < 12; i++) {
                _longMonthsParse[i] = new RegExp('^' + months[i] + '$', 'i');
            }
            return _longMonthsParse;
        }(cs__months)),
        weekdays : 'nedÃÂle_pondÃÂlÃÂ­_ÃÂºterÃÂ½_stÃÂeda_ÃÂtvrtek_pÃÂ¡tek_sobota'.split('_'),
        weekdaysShort : 'ne_po_ÃÂºt_st_ÃÂt_pÃÂ¡_so'.split('_'),
        weekdaysMin : 'ne_po_ÃÂºt_st_ÃÂt_pÃÂ¡_so'.split('_'),
        longDateFormat : {
            LT: 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY H:mm',
            LLLL : 'dddd D. MMMM YYYY H:mm'
        },
        calendar : {
            sameDay: '[dnes v] LT',
            nextDay: '[zÃÂ­tra v] LT',
            nextWeek: function () {
                switch (this.day()) {
                case 0:
                    return '[v nedÃÂli v] LT';
                case 1:
                case 2:
                    return '[v] dddd [v] LT';
                case 3:
                    return '[ve stÃÂedu v] LT';
                case 4:
                    return '[ve ÃÂtvrtek v] LT';
                case 5:
                    return '[v pÃÂ¡tek v] LT';
                case 6:
                    return '[v sobotu v] LT';
                }
            },
            lastDay: '[vÃÂera v] LT',
            lastWeek: function () {
                switch (this.day()) {
                case 0:
                    return '[minulou nedÃÂli v] LT';
                case 1:
                case 2:
                    return '[minulÃÂ©] dddd [v] LT';
                case 3:
                    return '[minulou stÃÂedu v] LT';
                case 4:
                case 5:
                    return '[minulÃÂ½] dddd [v] LT';
                case 6:
                    return '[minulou sobotu v] LT';
                }
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'za %s',
            past : 'pÃÂed %s',
            s : cs__translate,
            m : cs__translate,
            mm : cs__translate,
            h : cs__translate,
            hh : cs__translate,
            d : cs__translate,
            dd : cs__translate,
            M : cs__translate,
            MM : cs__translate,
            y : cs__translate,
            yy : cs__translate
        },
        ordinalParse : /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : chuvash (cv)
    //! author : Anatoly Mironov : https://github.com/mirontoli

    var cv = moment.defineLocale('cv', {
        months : 'ÃÂºÃÂÃÂÃÂ»ÃÂ°ÃÂ_ÃÂ½ÃÂ°ÃÂÃÂÃÂ_ÃÂ¿ÃÂÃÂ_ÃÂ°ÃÂºÃÂ°_ÃÂ¼ÃÂ°ÃÂ¹_ÃÂ«ÃÂÃÂÃÂÃÂ¼ÃÂµ_ÃÂÃÂÃÂ_ÃÂ«ÃÂÃÂÃÂ»ÃÂ°_ÃÂ°ÃÂ²ÃÂÃÂ½_ÃÂÃÂ¿ÃÂ°_ÃÂÃÂ³ÃÂº_ÃÂÃÂ°ÃÂÃÂÃÂ°ÃÂ²'.split('_'),
        monthsShort : 'ÃÂºÃÂÃÂ_ÃÂ½ÃÂ°ÃÂ_ÃÂ¿ÃÂÃÂ_ÃÂ°ÃÂºÃÂ°_ÃÂ¼ÃÂ°ÃÂ¹_ÃÂ«ÃÂÃÂ_ÃÂÃÂÃÂ_ÃÂ«ÃÂÃÂ_ÃÂ°ÃÂ²ÃÂ½_ÃÂÃÂ¿ÃÂ°_ÃÂÃÂ³ÃÂº_ÃÂÃÂ°ÃÂ'.split('_'),
        weekdays : 'ÃÂ²ÃÂÃÂÃÂÃÂ°ÃÂÃÂ½ÃÂ¸ÃÂºÃÂÃÂ½_ÃÂÃÂÃÂ½ÃÂÃÂ¸ÃÂºÃÂÃÂ½_ÃÂÃÂÃÂ»ÃÂ°ÃÂÃÂ¸ÃÂºÃÂÃÂ½_ÃÂÃÂ½ÃÂºÃÂÃÂ½_ÃÂºÃÂÃÂ«ÃÂ½ÃÂµÃÂÃÂ½ÃÂ¸ÃÂºÃÂÃÂ½_ÃÂÃÂÃÂ½ÃÂµÃÂºÃÂÃÂ½_ÃÂÃÂÃÂ¼ÃÂ°ÃÂÃÂºÃÂÃÂ½'.split('_'),
        weekdaysShort : 'ÃÂ²ÃÂÃÂ_ÃÂÃÂÃÂ½_ÃÂÃÂÃÂ»_ÃÂÃÂ½_ÃÂºÃÂÃÂ«_ÃÂÃÂÃÂ½_ÃÂÃÂÃÂ¼'.split('_'),
        weekdaysMin : 'ÃÂ²ÃÂ_ÃÂÃÂ½_ÃÂÃÂ_ÃÂÃÂ½_ÃÂºÃÂ«_ÃÂÃÂ_ÃÂÃÂ¼'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD-MM-YYYY',
            LL : 'YYYY [ÃÂ«ÃÂÃÂ»ÃÂÃÂ¸] MMMM [ÃÂÃÂ¹ÃÂÃÂÃÂÃÂ½] D[-ÃÂ¼ÃÂÃÂÃÂ]',
            LLL : 'YYYY [ÃÂ«ÃÂÃÂ»ÃÂÃÂ¸] MMMM [ÃÂÃÂ¹ÃÂÃÂÃÂÃÂ½] D[-ÃÂ¼ÃÂÃÂÃÂ], HH:mm',
            LLLL : 'dddd, YYYY [ÃÂ«ÃÂÃÂ»ÃÂÃÂ¸] MMMM [ÃÂÃÂ¹ÃÂÃÂÃÂÃÂ½] D[-ÃÂ¼ÃÂÃÂÃÂ], HH:mm'
        },
        calendar : {
            sameDay: '[ÃÂÃÂ°ÃÂÃÂ½] LT [ÃÂÃÂµÃÂÃÂµÃÂÃÂÃÂµ]',
            nextDay: '[ÃÂ«ÃÂÃÂ°ÃÂ½] LT [ÃÂÃÂµÃÂÃÂµÃÂÃÂÃÂµ]',
            lastDay: '[ÃÂÃÂ½ÃÂµÃÂ] LT [ÃÂÃÂµÃÂÃÂµÃÂÃÂÃÂµ]',
            nextWeek: '[ÃÂªÃÂ¸ÃÂÃÂµÃÂ] dddd LT [ÃÂÃÂµÃÂÃÂµÃÂÃÂÃÂµ]',
            lastWeek: '[ÃÂÃÂÃÂÃÂ½ÃÂ] dddd LT [ÃÂÃÂµÃÂÃÂµÃÂÃÂÃÂµ]',
            sameElse: 'L'
        },
        relativeTime : {
            future : function (output) {
                var affix = /ÃÂÃÂµÃÂÃÂµÃÂ$/i.exec(output) ? 'ÃÂÃÂµÃÂ½' : /ÃÂ«ÃÂÃÂ»$/i.exec(output) ? 'ÃÂÃÂ°ÃÂ½' : 'ÃÂÃÂ°ÃÂ½';
                return output + affix;
            },
            past : '%s ÃÂºÃÂ°ÃÂÃÂ»ÃÂ»ÃÂ°',
            s : 'ÃÂ¿ÃÂÃÂ-ÃÂ¸ÃÂº ÃÂ«ÃÂµÃÂºÃÂºÃÂÃÂ½ÃÂ',
            m : 'ÃÂ¿ÃÂÃÂ ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ',
            mm : '%d ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ',
            h : 'ÃÂ¿ÃÂÃÂ ÃÂÃÂµÃÂÃÂµÃÂ',
            hh : '%d ÃÂÃÂµÃÂÃÂµÃÂ',
            d : 'ÃÂ¿ÃÂÃÂ ÃÂºÃÂÃÂ½',
            dd : '%d ÃÂºÃÂÃÂ½',
            M : 'ÃÂ¿ÃÂÃÂ ÃÂÃÂ¹ÃÂÃÂ',
            MM : '%d ÃÂÃÂ¹ÃÂÃÂ',
            y : 'ÃÂ¿ÃÂÃÂ ÃÂ«ÃÂÃÂ»',
            yy : '%d ÃÂ«ÃÂÃÂ»'
        },
        ordinalParse: /\d{1,2}-ÃÂ¼ÃÂÃÂ/,
        ordinal : '%d-ÃÂ¼ÃÂÃÂ',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Welsh (cy)
    //! author : Robert Allen

    var cy = moment.defineLocale('cy', {
        months: 'Ionawr_Chwefror_Mawrth_Ebrill_Mai_Mehefin_Gorffennaf_Awst_Medi_Hydref_Tachwedd_Rhagfyr'.split('_'),
        monthsShort: 'Ion_Chwe_Maw_Ebr_Mai_Meh_Gor_Aws_Med_Hyd_Tach_Rhag'.split('_'),
        weekdays: 'Dydd Sul_Dydd Llun_Dydd Mawrth_Dydd Mercher_Dydd Iau_Dydd Gwener_Dydd Sadwrn'.split('_'),
        weekdaysShort: 'Sul_Llun_Maw_Mer_Iau_Gwe_Sad'.split('_'),
        weekdaysMin: 'Su_Ll_Ma_Me_Ia_Gw_Sa'.split('_'),
        weekdaysParseExact : true,
        // time formats are the same as en-gb
        longDateFormat: {
            LT: 'HH:mm',
            LTS : 'HH:mm:ss',
            L: 'DD/MM/YYYY',
            LL: 'D MMMM YYYY',
            LLL: 'D MMMM YYYY HH:mm',
            LLLL: 'dddd, D MMMM YYYY HH:mm'
        },
        calendar: {
            sameDay: '[Heddiw am] LT',
            nextDay: '[Yfory am] LT',
            nextWeek: 'dddd [am] LT',
            lastDay: '[Ddoe am] LT',
            lastWeek: 'dddd [diwethaf am] LT',
            sameElse: 'L'
        },
        relativeTime: {
            future: 'mewn %s',
            past: '%s yn ÃÂ´l',
            s: 'ychydig eiliadau',
            m: 'munud',
            mm: '%d munud',
            h: 'awr',
            hh: '%d awr',
            d: 'diwrnod',
            dd: '%d diwrnod',
            M: 'mis',
            MM: '%d mis',
            y: 'blwyddyn',
            yy: '%d flynedd'
        },
        ordinalParse: /\d{1,2}(fed|ain|af|il|ydd|ed|eg)/,
        // traditional ordinal numbers above 31 are not commonly used in colloquial Welsh
        ordinal: function (number) {
            var b = number,
                output = '',
                lookup = [
                    '', 'af', 'il', 'ydd', 'ydd', 'ed', 'ed', 'ed', 'fed', 'fed', 'fed', // 1af to 10fed
                    'eg', 'fed', 'eg', 'eg', 'fed', 'eg', 'eg', 'fed', 'eg', 'fed' // 11eg to 20fed
                ];
            if (b > 20) {
                if (b === 40 || b === 50 || b === 60 || b === 80 || b === 100) {
                    output = 'fed'; // not 30ain, 70ain or 90ain
                } else {
                    output = 'ain';
                }
            } else if (b > 0) {
                output = lookup[b];
            }
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : danish (da)
    //! author : Ulrik Nielsen : https://github.com/mrbase

    var da = moment.defineLocale('da', {
        months : 'januar_februar_marts_april_maj_juni_juli_august_september_oktober_november_december'.split('_'),
        monthsShort : 'jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec'.split('_'),
        weekdays : 'sÃÂ¸ndag_mandag_tirsdag_onsdag_torsdag_fredag_lÃÂ¸rdag'.split('_'),
        weekdaysShort : 'sÃÂ¸n_man_tir_ons_tor_fre_lÃÂ¸r'.split('_'),
        weekdaysMin : 'sÃÂ¸_ma_ti_on_to_fr_lÃÂ¸'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY HH:mm',
            LLLL : 'dddd [d.] D. MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[I dag kl.] LT',
            nextDay : '[I morgen kl.] LT',
            nextWeek : 'dddd [kl.] LT',
            lastDay : '[I gÃÂ¥r kl.] LT',
            lastWeek : '[sidste] dddd [kl] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'om %s',
            past : '%s siden',
            s : 'fÃÂ¥ sekunder',
            m : 'et minut',
            mm : '%d minutter',
            h : 'en time',
            hh : '%d timer',
            d : 'en dag',
            dd : '%d dage',
            M : 'en mÃÂ¥ned',
            MM : '%d mÃÂ¥neder',
            y : 'et ÃÂ¥r',
            yy : '%d ÃÂ¥r'
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : austrian german (de-at)
    //! author : lluchs : https://github.com/lluchs
    //! author: Menelion ElensÃÂºle: https://github.com/Oire
    //! author : Martin Groller : https://github.com/MadMG
    //! author : Mikolaj Dadela : https://github.com/mik01aj

    function de_at__processRelativeTime(number, withoutSuffix, key, isFuture) {
        var format = {
            'm': ['eine Minute', 'einer Minute'],
            'h': ['eine Stunde', 'einer Stunde'],
            'd': ['ein Tag', 'einem Tag'],
            'dd': [number + ' Tage', number + ' Tagen'],
            'M': ['ein Monat', 'einem Monat'],
            'MM': [number + ' Monate', number + ' Monaten'],
            'y': ['ein Jahr', 'einem Jahr'],
            'yy': [number + ' Jahre', number + ' Jahren']
        };
        return withoutSuffix ? format[key][0] : format[key][1];
    }

    var de_at = moment.defineLocale('de-at', {
        months : 'JÃÂ¤nner_Februar_MÃÂ¤rz_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember'.split('_'),
        monthsShort : 'JÃÂ¤n._Febr._Mrz._Apr._Mai_Jun._Jul._Aug._Sept._Okt._Nov._Dez.'.split('_'),
        monthsParseExact : true,
        weekdays : 'Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag'.split('_'),
        weekdaysShort : 'So._Mo._Di._Mi._Do._Fr._Sa.'.split('_'),
        weekdaysMin : 'So_Mo_Di_Mi_Do_Fr_Sa'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT: 'HH:mm',
            LTS: 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY HH:mm',
            LLLL : 'dddd, D. MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[heute um] LT [Uhr]',
            sameElse: 'L',
            nextDay: '[morgen um] LT [Uhr]',
            nextWeek: 'dddd [um] LT [Uhr]',
            lastDay: '[gestern um] LT [Uhr]',
            lastWeek: '[letzten] dddd [um] LT [Uhr]'
        },
        relativeTime : {
            future : 'in %s',
            past : 'vor %s',
            s : 'ein paar Sekunden',
            m : de_at__processRelativeTime,
            mm : '%d Minuten',
            h : de_at__processRelativeTime,
            hh : '%d Stunden',
            d : de_at__processRelativeTime,
            dd : de_at__processRelativeTime,
            M : de_at__processRelativeTime,
            MM : de_at__processRelativeTime,
            y : de_at__processRelativeTime,
            yy : de_at__processRelativeTime
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : german (de)
    //! author : lluchs : https://github.com/lluchs
    //! author: Menelion ElensÃÂºle: https://github.com/Oire
    //! author : Mikolaj Dadela : https://github.com/mik01aj

    function de__processRelativeTime(number, withoutSuffix, key, isFuture) {
        var format = {
            'm': ['eine Minute', 'einer Minute'],
            'h': ['eine Stunde', 'einer Stunde'],
            'd': ['ein Tag', 'einem Tag'],
            'dd': [number + ' Tage', number + ' Tagen'],
            'M': ['ein Monat', 'einem Monat'],
            'MM': [number + ' Monate', number + ' Monaten'],
            'y': ['ein Jahr', 'einem Jahr'],
            'yy': [number + ' Jahre', number + ' Jahren']
        };
        return withoutSuffix ? format[key][0] : format[key][1];
    }

    var de = moment.defineLocale('de', {
        months : 'Januar_Februar_MÃÂ¤rz_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember'.split('_'),
        monthsShort : 'Jan._Febr._Mrz._Apr._Mai_Jun._Jul._Aug._Sept._Okt._Nov._Dez.'.split('_'),
        monthsParseExact : true,
        weekdays : 'Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag'.split('_'),
        weekdaysShort : 'So._Mo._Di._Mi._Do._Fr._Sa.'.split('_'),
        weekdaysMin : 'So_Mo_Di_Mi_Do_Fr_Sa'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT: 'HH:mm',
            LTS: 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY HH:mm',
            LLLL : 'dddd, D. MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[heute um] LT [Uhr]',
            sameElse: 'L',
            nextDay: '[morgen um] LT [Uhr]',
            nextWeek: 'dddd [um] LT [Uhr]',
            lastDay: '[gestern um] LT [Uhr]',
            lastWeek: '[letzten] dddd [um] LT [Uhr]'
        },
        relativeTime : {
            future : 'in %s',
            past : 'vor %s',
            s : 'ein paar Sekunden',
            m : de__processRelativeTime,
            mm : '%d Minuten',
            h : de__processRelativeTime,
            hh : '%d Stunden',
            d : de__processRelativeTime,
            dd : de__processRelativeTime,
            M : de__processRelativeTime,
            MM : de__processRelativeTime,
            y : de__processRelativeTime,
            yy : de__processRelativeTime
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : dhivehi (dv)
    //! author : Jawish Hameed : https://github.com/jawish

    var dv__months = [
        'ÃÂÃÂ¬ÃÂÃÂªÃÂÃÂ¦ÃÂÃÂ©',
        'ÃÂÃÂ¬ÃÂÃÂ°ÃÂÃÂªÃÂÃÂ¦ÃÂÃÂ©',
        'ÃÂÃÂ§ÃÂÃÂ¨ÃÂÃÂª',
        'ÃÂÃÂ­ÃÂÃÂ°ÃÂÃÂ©ÃÂÃÂª',
        'ÃÂÃÂ­',
        'ÃÂÃÂ«ÃÂÃÂ°',
        'ÃÂÃÂªÃÂÃÂ¦ÃÂÃÂ¨',
        'ÃÂÃÂ¯ÃÂÃÂ¦ÃÂÃÂ°ÃÂÃÂª',
        'ÃÂÃÂ¬ÃÂÃÂ°ÃÂÃÂ¬ÃÂÃÂ°ÃÂÃÂ¦ÃÂÃÂª',
        'ÃÂÃÂ®ÃÂÃÂ°ÃÂÃÂ¯ÃÂÃÂ¦ÃÂÃÂª',
        'ÃÂÃÂ®ÃÂÃÂ¬ÃÂÃÂ°ÃÂÃÂ¦ÃÂÃÂª',
        'ÃÂÃÂ¨ÃÂÃÂ¬ÃÂÃÂ°ÃÂÃÂ¦ÃÂÃÂª'
    ], dv__weekdays = [
        'ÃÂÃÂ§ÃÂÃÂ¨ÃÂÃÂ°ÃÂÃÂ¦',
        'ÃÂÃÂ¯ÃÂÃÂ¦',
        'ÃÂÃÂ¦ÃÂÃÂ°ÃÂÃÂ§ÃÂÃÂ¦',
        'ÃÂÃÂªÃÂÃÂ¦',
        'ÃÂÃÂªÃÂÃÂ§ÃÂÃÂ°ÃÂÃÂ¦ÃÂÃÂ¨',
        'ÃÂÃÂªÃÂÃÂªÃÂÃÂª',
        'ÃÂÃÂ®ÃÂÃÂ¨ÃÂÃÂ¨ÃÂÃÂª'
    ];

    var dv = moment.defineLocale('dv', {
        months : dv__months,
        monthsShort : dv__months,
        weekdays : dv__weekdays,
        weekdaysShort : dv__weekdays,
        weekdaysMin : 'ÃÂÃÂ§ÃÂÃÂ¨_ÃÂÃÂ¯ÃÂÃÂ¦_ÃÂÃÂ¦ÃÂÃÂ°_ÃÂÃÂªÃÂÃÂ¦_ÃÂÃÂªÃÂÃÂ§_ÃÂÃÂªÃÂÃÂª_ÃÂÃÂ®ÃÂÃÂ¨'.split('_'),
        longDateFormat : {

            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'D/M/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        meridiemParse: /ÃÂÃÂ|ÃÂÃÂ/,
        isPM : function (input) {
            return 'ÃÂÃÂ' === input;
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 12) {
                return 'ÃÂÃÂ';
            } else {
                return 'ÃÂÃÂ';
            }
        },
        calendar : {
            sameDay : '[ÃÂÃÂ¨ÃÂÃÂ¦ÃÂÃÂª] LT',
            nextDay : '[ÃÂÃÂ§ÃÂÃÂ¦ÃÂÃÂ§] LT',
            nextWeek : 'dddd LT',
            lastDay : '[ÃÂÃÂ¨ÃÂÃÂ°ÃÂÃÂ¬] LT',
            lastWeek : '[ÃÂÃÂ§ÃÂÃÂ¨ÃÂÃÂªÃÂÃÂ¨] dddd LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂÃÂ¬ÃÂÃÂ­ÃÂÃÂ¦ÃÂÃÂ¨ %s',
            past : 'ÃÂÃÂªÃÂÃÂ¨ÃÂÃÂ° %s',
            s : 'ÃÂÃÂ¨ÃÂÃÂªÃÂÃÂ°ÃÂÃÂªÃÂÃÂ®ÃÂÃÂ¬ÃÂÃÂ°',
            m : 'ÃÂÃÂ¨ÃÂÃÂ¨ÃÂÃÂ¬ÃÂÃÂ°',
            mm : 'ÃÂÃÂ¨ÃÂÃÂ¨ÃÂÃÂª %d',
            h : 'ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂ¨ÃÂÃÂ¬ÃÂÃÂ°',
            hh : 'ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂ¨ÃÂÃÂª %d',
            d : 'ÃÂÃÂªÃÂÃÂ¦ÃÂÃÂ¬ÃÂÃÂ°',
            dd : 'ÃÂÃÂªÃÂÃÂ¦ÃÂÃÂ° %d',
            M : 'ÃÂÃÂ¦ÃÂÃÂ¬ÃÂÃÂ°',
            MM : 'ÃÂÃÂ¦ÃÂÃÂ° %d',
            y : 'ÃÂÃÂ¦ÃÂÃÂ¦ÃÂÃÂ¬ÃÂÃÂ°',
            yy : 'ÃÂÃÂ¦ÃÂÃÂ¦ÃÂÃÂª %d'
        },
        preparse: function (string) {
            return string.replace(/ÃÂ/g, ',');
        },
        postformat: function (string) {
            return string.replace(/,/g, 'ÃÂ');
        },
        week : {
            dow : 7,  // Sunday is the first day of the week.
            doy : 12  // The week that contains Jan 1st is the first week of the year.
        }
    });

    function isFunction(input) {
        return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
    }

    //! moment.js locale configuration
    //! locale : modern greek (el)
    //! author : Aggelos Karalias : https://github.com/mehiel

    var el = moment.defineLocale('el', {
        monthsNominativeEl : 'ÃÂÃÂ±ÃÂ½ÃÂ¿ÃÂÃÂ¬ÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂ¦ÃÂµÃÂ²ÃÂÃÂ¿ÃÂÃÂ¬ÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂ¬ÃÂÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂÃÂÃÂ¯ÃÂ»ÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂ¬ÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂÃÂ½ÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂÃÂ»ÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂÃÂ³ÃÂ¿ÃÂÃÂÃÂÃÂ¿ÃÂ_ÃÂ£ÃÂµÃÂÃÂÃÂ­ÃÂ¼ÃÂ²ÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂºÃÂÃÂÃÂ²ÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂ­ÃÂ¼ÃÂ²ÃÂÃÂ¹ÃÂ¿ÃÂ_ÃÂÃÂµÃÂºÃÂ­ÃÂ¼ÃÂ²ÃÂÃÂ¹ÃÂ¿ÃÂ'.split('_'),
        monthsGenitiveEl : 'ÃÂÃÂ±ÃÂ½ÃÂ¿ÃÂÃÂ±ÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂ¦ÃÂµÃÂ²ÃÂÃÂ¿ÃÂÃÂ±ÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂ±ÃÂÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂÃÂÃÂ¹ÃÂ»ÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂ±ÃÂÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂÃÂ½ÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂÃÂ»ÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂÃÂ³ÃÂ¿ÃÂÃÂÃÂÃÂ¿ÃÂ_ÃÂ£ÃÂµÃÂÃÂÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂºÃÂÃÂÃÂ²ÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂ¿ÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¯ÃÂ¿ÃÂ_ÃÂÃÂµÃÂºÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¯ÃÂ¿ÃÂ'.split('_'),
        months : function (momentToFormat, format) {
            if (/D/.test(format.substring(0, format.indexOf('MMMM')))) { // if there is a day number before 'MMMM'
                return this._monthsGenitiveEl[momentToFormat.month()];
            } else {
                return this._monthsNominativeEl[momentToFormat.month()];
            }
        },
        monthsShort : 'ÃÂÃÂ±ÃÂ½_ÃÂ¦ÃÂµÃÂ²_ÃÂÃÂ±ÃÂ_ÃÂÃÂÃÂ_ÃÂÃÂ±ÃÂ_ÃÂÃÂ¿ÃÂÃÂ½_ÃÂÃÂ¿ÃÂÃÂ»_ÃÂÃÂÃÂ³_ÃÂ£ÃÂµÃÂ_ÃÂÃÂºÃÂ_ÃÂÃÂ¿ÃÂµ_ÃÂÃÂµÃÂº'.split('_'),
        weekdays : 'ÃÂÃÂÃÂÃÂ¹ÃÂ±ÃÂºÃÂ®_ÃÂÃÂµÃÂÃÂÃÂ­ÃÂÃÂ±_ÃÂ¤ÃÂÃÂ¯ÃÂÃÂ·_ÃÂ¤ÃÂµÃÂÃÂ¬ÃÂÃÂÃÂ·_ÃÂ ÃÂ­ÃÂ¼ÃÂÃÂÃÂ·_ÃÂ ÃÂ±ÃÂÃÂ±ÃÂÃÂºÃÂµÃÂÃÂ®_ÃÂ£ÃÂ¬ÃÂ²ÃÂ²ÃÂ±ÃÂÃÂ¿'.split('_'),
        weekdaysShort : 'ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ_ÃÂ¤ÃÂÃÂ¹_ÃÂ¤ÃÂµÃÂ_ÃÂ ÃÂµÃÂ¼_ÃÂ ÃÂ±ÃÂ_ÃÂ£ÃÂ±ÃÂ²'.split('_'),
        weekdaysMin : 'ÃÂÃÂ_ÃÂÃÂµ_ÃÂ¤ÃÂ_ÃÂ¤ÃÂµ_ÃÂ ÃÂµ_ÃÂ ÃÂ±_ÃÂ£ÃÂ±'.split('_'),
        meridiem : function (hours, minutes, isLower) {
            if (hours > 11) {
                return isLower ? 'ÃÂ¼ÃÂ¼' : 'ÃÂÃÂ';
            } else {
                return isLower ? 'ÃÂÃÂ¼' : 'ÃÂ ÃÂ';
            }
        },
        isPM : function (input) {
            return ((input + '').toLowerCase()[0] === 'ÃÂ¼');
        },
        meridiemParse : /[ÃÂ ÃÂ]\.?ÃÂ?\.?/i,
        longDateFormat : {
            LT : 'h:mm A',
            LTS : 'h:mm:ss A',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY h:mm A',
            LLLL : 'dddd, D MMMM YYYY h:mm A'
        },
        calendarEl : {
            sameDay : '[ÃÂ£ÃÂ®ÃÂ¼ÃÂµÃÂÃÂ± {}] LT',
            nextDay : '[ÃÂÃÂÃÂÃÂ¹ÃÂ¿ {}] LT',
            nextWeek : 'dddd [{}] LT',
            lastDay : '[ÃÂ§ÃÂ¸ÃÂµÃÂ {}] LT',
            lastWeek : function () {
                switch (this.day()) {
                    case 6:
                        return '[ÃÂÃÂ¿ ÃÂÃÂÃÂ¿ÃÂ·ÃÂ³ÃÂ¿ÃÂÃÂ¼ÃÂµÃÂ½ÃÂ¿] dddd [{}] LT';
                    default:
                        return '[ÃÂÃÂ·ÃÂ½ ÃÂÃÂÃÂ¿ÃÂ·ÃÂ³ÃÂ¿ÃÂÃÂ¼ÃÂµÃÂ½ÃÂ·] dddd [{}] LT';
                }
            },
            sameElse : 'L'
        },
        calendar : function (key, mom) {
            var output = this._calendarEl[key],
                hours = mom && mom.hours();
            if (isFunction(output)) {
                output = output.apply(mom);
            }
            return output.replace('{}', (hours % 12 === 1 ? 'ÃÂÃÂÃÂ·' : 'ÃÂÃÂÃÂ¹ÃÂ'));
        },
        relativeTime : {
            future : 'ÃÂÃÂµ %s',
            past : '%s ÃÂÃÂÃÂ¹ÃÂ½',
            s : 'ÃÂ»ÃÂ¯ÃÂ³ÃÂ± ÃÂ´ÃÂµÃÂÃÂÃÂµÃÂÃÂÃÂ»ÃÂµÃÂÃÂÃÂ±',
            m : 'ÃÂ­ÃÂ½ÃÂ± ÃÂ»ÃÂµÃÂÃÂÃÂ',
            mm : '%d ÃÂ»ÃÂµÃÂÃÂÃÂ¬',
            h : 'ÃÂ¼ÃÂ¯ÃÂ± ÃÂÃÂÃÂ±',
            hh : '%d ÃÂÃÂÃÂµÃÂ',
            d : 'ÃÂ¼ÃÂ¯ÃÂ± ÃÂ¼ÃÂ­ÃÂÃÂ±',
            dd : '%d ÃÂ¼ÃÂ­ÃÂÃÂµÃÂ',
            M : 'ÃÂ­ÃÂ½ÃÂ±ÃÂ ÃÂ¼ÃÂ®ÃÂ½ÃÂ±ÃÂ',
            MM : '%d ÃÂ¼ÃÂ®ÃÂ½ÃÂµÃÂ',
            y : 'ÃÂ­ÃÂ½ÃÂ±ÃÂ ÃÂÃÂÃÂÃÂ½ÃÂ¿ÃÂ',
            yy : '%d ÃÂÃÂÃÂÃÂ½ÃÂ¹ÃÂ±'
        },
        ordinalParse: /\d{1,2}ÃÂ·/,
        ordinal: '%dÃÂ·',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : australian english (en-au)

    var en_au = moment.defineLocale('en-au', {
        months : 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
        monthsShort : 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
        weekdays : 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
        weekdaysShort : 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
        weekdaysMin : 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'),
        longDateFormat : {
            LT : 'h:mm A',
            LTS : 'h:mm:ss A',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY h:mm A',
            LLLL : 'dddd, D MMMM YYYY h:mm A'
        },
        calendar : {
            sameDay : '[Today at] LT',
            nextDay : '[Tomorrow at] LT',
            nextWeek : 'dddd [at] LT',
            lastDay : '[Yesterday at] LT',
            lastWeek : '[Last] dddd [at] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'in %s',
            past : '%s ago',
            s : 'a few seconds',
            m : 'a minute',
            mm : '%d minutes',
            h : 'an hour',
            hh : '%d hours',
            d : 'a day',
            dd : '%d days',
            M : 'a month',
            MM : '%d months',
            y : 'a year',
            yy : '%d years'
        },
        ordinalParse: /\d{1,2}(st|nd|rd|th)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : canadian english (en-ca)
    //! author : Jonathan Abourbih : https://github.com/jonbca

    var en_ca = moment.defineLocale('en-ca', {
        months : 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
        monthsShort : 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
        weekdays : 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
        weekdaysShort : 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
        weekdaysMin : 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'),
        longDateFormat : {
            LT : 'h:mm A',
            LTS : 'h:mm:ss A',
            L : 'YYYY-MM-DD',
            LL : 'MMMM D, YYYY',
            LLL : 'MMMM D, YYYY h:mm A',
            LLLL : 'dddd, MMMM D, YYYY h:mm A'
        },
        calendar : {
            sameDay : '[Today at] LT',
            nextDay : '[Tomorrow at] LT',
            nextWeek : 'dddd [at] LT',
            lastDay : '[Yesterday at] LT',
            lastWeek : '[Last] dddd [at] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'in %s',
            past : '%s ago',
            s : 'a few seconds',
            m : 'a minute',
            mm : '%d minutes',
            h : 'an hour',
            hh : '%d hours',
            d : 'a day',
            dd : '%d days',
            M : 'a month',
            MM : '%d months',
            y : 'a year',
            yy : '%d years'
        },
        ordinalParse: /\d{1,2}(st|nd|rd|th)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
            return number + output;
        }
    });

    //! moment.js locale configuration
    //! locale : great britain english (en-gb)
    //! author : Chris Gedrim : https://github.com/chrisgedrim

    var en_gb = moment.defineLocale('en-gb', {
        months : 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
        monthsShort : 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
        weekdays : 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
        weekdaysShort : 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
        weekdaysMin : 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[Today at] LT',
            nextDay : '[Tomorrow at] LT',
            nextWeek : 'dddd [at] LT',
            lastDay : '[Yesterday at] LT',
            lastWeek : '[Last] dddd [at] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'in %s',
            past : '%s ago',
            s : 'a few seconds',
            m : 'a minute',
            mm : '%d minutes',
            h : 'an hour',
            hh : '%d hours',
            d : 'a day',
            dd : '%d days',
            M : 'a month',
            MM : '%d months',
            y : 'a year',
            yy : '%d years'
        },
        ordinalParse: /\d{1,2}(st|nd|rd|th)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Irish english (en-ie)
    //! author : Chris Cartlidge : https://github.com/chriscartlidge

    var en_ie = moment.defineLocale('en-ie', {
        months : 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
        monthsShort : 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
        weekdays : 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
        weekdaysShort : 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
        weekdaysMin : 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD-MM-YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[Today at] LT',
            nextDay : '[Tomorrow at] LT',
            nextWeek : 'dddd [at] LT',
            lastDay : '[Yesterday at] LT',
            lastWeek : '[Last] dddd [at] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'in %s',
            past : '%s ago',
            s : 'a few seconds',
            m : 'a minute',
            mm : '%d minutes',
            h : 'an hour',
            hh : '%d hours',
            d : 'a day',
            dd : '%d days',
            M : 'a month',
            MM : '%d months',
            y : 'a year',
            yy : '%d years'
        },
        ordinalParse: /\d{1,2}(st|nd|rd|th)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : New Zealand english (en-nz)

    var en_nz = moment.defineLocale('en-nz', {
        months : 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
        monthsShort : 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
        weekdays : 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
        weekdaysShort : 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
        weekdaysMin : 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'),
        longDateFormat : {
            LT : 'h:mm A',
            LTS : 'h:mm:ss A',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY h:mm A',
            LLLL : 'dddd, D MMMM YYYY h:mm A'
        },
        calendar : {
            sameDay : '[Today at] LT',
            nextDay : '[Tomorrow at] LT',
            nextWeek : 'dddd [at] LT',
            lastDay : '[Yesterday at] LT',
            lastWeek : '[Last] dddd [at] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'in %s',
            past : '%s ago',
            s : 'a few seconds',
            m : 'a minute',
            mm : '%d minutes',
            h : 'an hour',
            hh : '%d hours',
            d : 'a day',
            dd : '%d days',
            M : 'a month',
            MM : '%d months',
            y : 'a year',
            yy : '%d years'
        },
        ordinalParse: /\d{1,2}(st|nd|rd|th)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : esperanto (eo)
    //! author : Colin Dean : https://github.com/colindean
    //! komento: Mi estas malcerta se mi korekte traktis akuzativojn en tiu traduko.
    //!          Se ne, bonvolu korekti kaj avizi min por ke mi povas lerni!

    var eo = moment.defineLocale('eo', {
        months : 'januaro_februaro_marto_aprilo_majo_junio_julio_aÃÂ­gusto_septembro_oktobro_novembro_decembro'.split('_'),
        monthsShort : 'jan_feb_mar_apr_maj_jun_jul_aÃÂ­g_sep_okt_nov_dec'.split('_'),
        weekdays : 'DimanÃÂo_Lundo_Mardo_Merkredo_ÃÂ´aÃÂ­do_Vendredo_Sabato'.split('_'),
        weekdaysShort : 'Dim_Lun_Mard_Merk_ÃÂ´aÃÂ­_Ven_Sab'.split('_'),
        weekdaysMin : 'Di_Lu_Ma_Me_ÃÂ´a_Ve_Sa'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'YYYY-MM-DD',
            LL : 'D[-an de] MMMM, YYYY',
            LLL : 'D[-an de] MMMM, YYYY HH:mm',
            LLLL : 'dddd, [la] D[-an de] MMMM, YYYY HH:mm'
        },
        meridiemParse: /[ap]\.t\.m/i,
        isPM: function (input) {
            return input.charAt(0).toLowerCase() === 'p';
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours > 11) {
                return isLower ? 'p.t.m.' : 'P.T.M.';
            } else {
                return isLower ? 'a.t.m.' : 'A.T.M.';
            }
        },
        calendar : {
            sameDay : '[HodiaÃÂ­ je] LT',
            nextDay : '[MorgaÃÂ­ je] LT',
            nextWeek : 'dddd [je] LT',
            lastDay : '[HieraÃÂ­ je] LT',
            lastWeek : '[pasinta] dddd [je] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'je %s',
            past : 'antaÃÂ­ %s',
            s : 'sekundoj',
            m : 'minuto',
            mm : '%d minutoj',
            h : 'horo',
            hh : '%d horoj',
            d : 'tago',//ne 'diurno', ÃÂar estas uzita por proksimumo
            dd : '%d tagoj',
            M : 'monato',
            MM : '%d monatoj',
            y : 'jaro',
            yy : '%d jaroj'
        },
        ordinalParse: /\d{1,2}a/,
        ordinal : '%da',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : spanish (es)
    //! author : Julio NapurÃÂ­ : https://github.com/julionc

    var monthsShortDot = 'ene._feb._mar._abr._may._jun._jul._ago._sep._oct._nov._dic.'.split('_'),
        es__monthsShort = 'ene_feb_mar_abr_may_jun_jul_ago_sep_oct_nov_dic'.split('_');

    var es = moment.defineLocale('es', {
        months : 'enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre'.split('_'),
        monthsShort : function (m, format) {
            if (/-MMM-/.test(format)) {
                return es__monthsShort[m.month()];
            } else {
                return monthsShortDot[m.month()];
            }
        },
        monthsParseExact : true,
        weekdays : 'domingo_lunes_martes_miÃÂ©rcoles_jueves_viernes_sÃÂ¡bado'.split('_'),
        weekdaysShort : 'dom._lun._mar._miÃÂ©._jue._vie._sÃÂ¡b.'.split('_'),
        weekdaysMin : 'do_lu_ma_mi_ju_vi_sÃÂ¡'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D [de] MMMM [de] YYYY',
            LLL : 'D [de] MMMM [de] YYYY H:mm',
            LLLL : 'dddd, D [de] MMMM [de] YYYY H:mm'
        },
        calendar : {
            sameDay : function () {
                return '[hoy a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
            },
            nextDay : function () {
                return '[maÃÂ±ana a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
            },
            nextWeek : function () {
                return 'dddd [a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
            },
            lastDay : function () {
                return '[ayer a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
            },
            lastWeek : function () {
                return '[el] dddd [pasado a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : 'en %s',
            past : 'hace %s',
            s : 'unos segundos',
            m : 'un minuto',
            mm : '%d minutos',
            h : 'una hora',
            hh : '%d horas',
            d : 'un dÃÂ­a',
            dd : '%d dÃÂ­as',
            M : 'un mes',
            MM : '%d meses',
            y : 'un aÃÂ±o',
            yy : '%d aÃÂ±os'
        },
        ordinalParse : /\d{1,2}ÃÂº/,
        ordinal : '%dÃÂº',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : estonian (et)
    //! author : Henry Kehlmann : https://github.com/madhenry
    //! improvements : Illimar Tambek : https://github.com/ragulka

    function et__processRelativeTime(number, withoutSuffix, key, isFuture) {
        var format = {
            's' : ['mÃÂµne sekundi', 'mÃÂµni sekund', 'paar sekundit'],
            'm' : ['ÃÂ¼he minuti', 'ÃÂ¼ks minut'],
            'mm': [number + ' minuti', number + ' minutit'],
            'h' : ['ÃÂ¼he tunni', 'tund aega', 'ÃÂ¼ks tund'],
            'hh': [number + ' tunni', number + ' tundi'],
            'd' : ['ÃÂ¼he pÃÂ¤eva', 'ÃÂ¼ks pÃÂ¤ev'],
            'M' : ['kuu aja', 'kuu aega', 'ÃÂ¼ks kuu'],
            'MM': [number + ' kuu', number + ' kuud'],
            'y' : ['ÃÂ¼he aasta', 'aasta', 'ÃÂ¼ks aasta'],
            'yy': [number + ' aasta', number + ' aastat']
        };
        if (withoutSuffix) {
            return format[key][2] ? format[key][2] : format[key][1];
        }
        return isFuture ? format[key][0] : format[key][1];
    }

    var et = moment.defineLocale('et', {
        months        : 'jaanuar_veebruar_mÃÂ¤rts_aprill_mai_juuni_juuli_august_september_oktoober_november_detsember'.split('_'),
        monthsShort   : 'jaan_veebr_mÃÂ¤rts_apr_mai_juuni_juuli_aug_sept_okt_nov_dets'.split('_'),
        weekdays      : 'pÃÂ¼hapÃÂ¤ev_esmaspÃÂ¤ev_teisipÃÂ¤ev_kolmapÃÂ¤ev_neljapÃÂ¤ev_reede_laupÃÂ¤ev'.split('_'),
        weekdaysShort : 'P_E_T_K_N_R_L'.split('_'),
        weekdaysMin   : 'P_E_T_K_N_R_L'.split('_'),
        longDateFormat : {
            LT   : 'H:mm',
            LTS : 'H:mm:ss',
            L    : 'DD.MM.YYYY',
            LL   : 'D. MMMM YYYY',
            LLL  : 'D. MMMM YYYY H:mm',
            LLLL : 'dddd, D. MMMM YYYY H:mm'
        },
        calendar : {
            sameDay  : '[TÃÂ¤na,] LT',
            nextDay  : '[Homme,] LT',
            nextWeek : '[JÃÂ¤rgmine] dddd LT',
            lastDay  : '[Eile,] LT',
            lastWeek : '[Eelmine] dddd LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s pÃÂ¤rast',
            past   : '%s tagasi',
            s      : et__processRelativeTime,
            m      : et__processRelativeTime,
            mm     : et__processRelativeTime,
            h      : et__processRelativeTime,
            hh     : et__processRelativeTime,
            d      : et__processRelativeTime,
            dd     : '%d pÃÂ¤eva',
            M      : et__processRelativeTime,
            MM     : et__processRelativeTime,
            y      : et__processRelativeTime,
            yy     : et__processRelativeTime
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : euskara (eu)
    //! author : Eneko Illarramendi : https://github.com/eillarra

    var eu = moment.defineLocale('eu', {
        months : 'urtarrila_otsaila_martxoa_apirila_maiatza_ekaina_uztaila_abuztua_iraila_urria_azaroa_abendua'.split('_'),
        monthsShort : 'urt._ots._mar._api._mai._eka._uzt._abu._ira._urr._aza._abe.'.split('_'),
        monthsParseExact : true,
        weekdays : 'igandea_astelehena_asteartea_asteazkena_osteguna_ostirala_larunbata'.split('_'),
        weekdaysShort : 'ig._al._ar._az._og._ol._lr.'.split('_'),
        weekdaysMin : 'ig_al_ar_az_og_ol_lr'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'YYYY-MM-DD',
            LL : 'YYYY[ko] MMMM[ren] D[a]',
            LLL : 'YYYY[ko] MMMM[ren] D[a] HH:mm',
            LLLL : 'dddd, YYYY[ko] MMMM[ren] D[a] HH:mm',
            l : 'YYYY-M-D',
            ll : 'YYYY[ko] MMM D[a]',
            lll : 'YYYY[ko] MMM D[a] HH:mm',
            llll : 'ddd, YYYY[ko] MMM D[a] HH:mm'
        },
        calendar : {
            sameDay : '[gaur] LT[etan]',
            nextDay : '[bihar] LT[etan]',
            nextWeek : 'dddd LT[etan]',
            lastDay : '[atzo] LT[etan]',
            lastWeek : '[aurreko] dddd LT[etan]',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s barru',
            past : 'duela %s',
            s : 'segundo batzuk',
            m : 'minutu bat',
            mm : '%d minutu',
            h : 'ordu bat',
            hh : '%d ordu',
            d : 'egun bat',
            dd : '%d egun',
            M : 'hilabete bat',
            MM : '%d hilabete',
            y : 'urte bat',
            yy : '%d urte'
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Persian (fa)
    //! author : Ebrahim Byagowi : https://github.com/ebraminio

    var fa__symbolMap = {
        '1': 'ÃÂ±',
        '2': 'ÃÂ²',
        '3': 'ÃÂ³',
        '4': 'ÃÂ´',
        '5': 'ÃÂµ',
        '6': 'ÃÂ¶',
        '7': 'ÃÂ·',
        '8': 'ÃÂ¸',
        '9': 'ÃÂ¹',
        '0': 'ÃÂ°'
    }, fa__numberMap = {
        'ÃÂ±': '1',
        'ÃÂ²': '2',
        'ÃÂ³': '3',
        'ÃÂ´': '4',
        'ÃÂµ': '5',
        'ÃÂ¶': '6',
        'ÃÂ·': '7',
        'ÃÂ¸': '8',
        'ÃÂ¹': '9',
        'ÃÂ°': '0'
    };

    var fa = moment.defineLocale('fa', {
        months : 'ÃÂÃÂ§ÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ¢ÃÂÃÂ±ÃÂÃÂ_ÃÂÃÂ_ÃÂÃÂÃÂ¦ÃÂ_ÃÂÃÂÃÂ¦ÃÂÃÂ_ÃÂ§ÃÂÃÂª_ÃÂ³ÃÂ¾ÃÂªÃÂ§ÃÂÃÂ¨ÃÂ±_ÃÂ§ÃÂ©ÃÂªÃÂ¨ÃÂ±_ÃÂÃÂÃÂ§ÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂ³ÃÂ§ÃÂÃÂ¨ÃÂ±'.split('_'),
        monthsShort : 'ÃÂÃÂ§ÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ¢ÃÂÃÂ±ÃÂÃÂ_ÃÂÃÂ_ÃÂÃÂÃÂ¦ÃÂ_ÃÂÃÂÃÂ¦ÃÂÃÂ_ÃÂ§ÃÂÃÂª_ÃÂ³ÃÂ¾ÃÂªÃÂ§ÃÂÃÂ¨ÃÂ±_ÃÂ§ÃÂ©ÃÂªÃÂ¨ÃÂ±_ÃÂÃÂÃÂ§ÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂ³ÃÂ§ÃÂÃÂ¨ÃÂ±'.split('_'),
        weekdays : 'ÃÂÃÂ©\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¯ÃÂÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ³ÃÂ\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂÃÂÃÂ§ÃÂ±ÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¾ÃÂÃÂ¬\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¬ÃÂÃÂ¹ÃÂ_ÃÂ´ÃÂÃÂ¨ÃÂ'.split('_'),
        weekdaysShort : 'ÃÂÃÂ©\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¯ÃÂÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ³ÃÂ\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂÃÂÃÂ§ÃÂ±ÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¾ÃÂÃÂ¬\u200cÃÂ´ÃÂÃÂ¨ÃÂ_ÃÂ¬ÃÂÃÂ¹ÃÂ_ÃÂ´ÃÂÃÂ¨ÃÂ'.split('_'),
        weekdaysMin : 'ÃÂ_ÃÂ¯_ÃÂ³_ÃÂ_ÃÂ¾_ÃÂ¬_ÃÂ´'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        meridiemParse: /ÃÂÃÂ¨ÃÂ ÃÂ§ÃÂ² ÃÂ¸ÃÂÃÂ±|ÃÂ¨ÃÂ¹ÃÂ¯ ÃÂ§ÃÂ² ÃÂ¸ÃÂÃÂ±/,
        isPM: function (input) {
            return /ÃÂ¨ÃÂ¹ÃÂ¯ ÃÂ§ÃÂ² ÃÂ¸ÃÂÃÂ±/.test(input);
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 12) {
                return 'ÃÂÃÂ¨ÃÂ ÃÂ§ÃÂ² ÃÂ¸ÃÂÃÂ±';
            } else {
                return 'ÃÂ¨ÃÂ¹ÃÂ¯ ÃÂ§ÃÂ² ÃÂ¸ÃÂÃÂ±';
            }
        },
        calendar : {
            sameDay : '[ÃÂ§ÃÂÃÂ±ÃÂÃÂ² ÃÂ³ÃÂ§ÃÂ¹ÃÂª] LT',
            nextDay : '[ÃÂÃÂ±ÃÂ¯ÃÂ§ ÃÂ³ÃÂ§ÃÂ¹ÃÂª] LT',
            nextWeek : 'dddd [ÃÂ³ÃÂ§ÃÂ¹ÃÂª] LT',
            lastDay : '[ÃÂ¯ÃÂÃÂ±ÃÂÃÂ² ÃÂ³ÃÂ§ÃÂ¹ÃÂª] LT',
            lastWeek : 'dddd [ÃÂ¾ÃÂÃÂ´] [ÃÂ³ÃÂ§ÃÂ¹ÃÂª] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂ¯ÃÂ± %s',
            past : '%s ÃÂ¾ÃÂÃÂ´',
            s : 'ÃÂÃÂÃÂ¯ÃÂÃÂ ÃÂ«ÃÂ§ÃÂÃÂÃÂ',
            m : 'ÃÂÃÂ© ÃÂ¯ÃÂÃÂÃÂÃÂ',
            mm : '%d ÃÂ¯ÃÂÃÂÃÂÃÂ',
            h : 'ÃÂÃÂ© ÃÂ³ÃÂ§ÃÂ¹ÃÂª',
            hh : '%d ÃÂ³ÃÂ§ÃÂ¹ÃÂª',
            d : 'ÃÂÃÂ© ÃÂ±ÃÂÃÂ²',
            dd : '%d ÃÂ±ÃÂÃÂ²',
            M : 'ÃÂÃÂ© ÃÂÃÂ§ÃÂ',
            MM : '%d ÃÂÃÂ§ÃÂ',
            y : 'ÃÂÃÂ© ÃÂ³ÃÂ§ÃÂ',
            yy : '%d ÃÂ³ÃÂ§ÃÂ'
        },
        preparse: function (string) {
            return string.replace(/[ÃÂ°-ÃÂ¹]/g, function (match) {
                return fa__numberMap[match];
            }).replace(/ÃÂ/g, ',');
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return fa__symbolMap[match];
            }).replace(/,/g, 'ÃÂ');
        },
        ordinalParse: /\d{1,2}ÃÂ/,
        ordinal : '%dÃÂ',
        week : {
            dow : 6, // Saturday is the first day of the week.
            doy : 12 // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : finnish (fi)
    //! author : Tarmo Aidantausta : https://github.com/bleadof

    var numbersPast = 'nolla yksi kaksi kolme neljÃÂ¤ viisi kuusi seitsemÃÂ¤n kahdeksan yhdeksÃÂ¤n'.split(' '),
        numbersFuture = [
            'nolla', 'yhden', 'kahden', 'kolmen', 'neljÃÂ¤n', 'viiden', 'kuuden',
            numbersPast[7], numbersPast[8], numbersPast[9]
        ];
    function fi__translate(number, withoutSuffix, key, isFuture) {
        var result = '';
        switch (key) {
        case 's':
            return isFuture ? 'muutaman sekunnin' : 'muutama sekunti';
        case 'm':
            return isFuture ? 'minuutin' : 'minuutti';
        case 'mm':
            result = isFuture ? 'minuutin' : 'minuuttia';
            break;
        case 'h':
            return isFuture ? 'tunnin' : 'tunti';
        case 'hh':
            result = isFuture ? 'tunnin' : 'tuntia';
            break;
        case 'd':
            return isFuture ? 'pÃÂ¤ivÃÂ¤n' : 'pÃÂ¤ivÃÂ¤';
        case 'dd':
            result = isFuture ? 'pÃÂ¤ivÃÂ¤n' : 'pÃÂ¤ivÃÂ¤ÃÂ¤';
            break;
        case 'M':
            return isFuture ? 'kuukauden' : 'kuukausi';
        case 'MM':
            result = isFuture ? 'kuukauden' : 'kuukautta';
            break;
        case 'y':
            return isFuture ? 'vuoden' : 'vuosi';
        case 'yy':
            result = isFuture ? 'vuoden' : 'vuotta';
            break;
        }
        result = verbalNumber(number, isFuture) + ' ' + result;
        return result;
    }
    function verbalNumber(number, isFuture) {
        return number < 10 ? (isFuture ? numbersFuture[number] : numbersPast[number]) : number;
    }

    var fi = moment.defineLocale('fi', {
        months : 'tammikuu_helmikuu_maaliskuu_huhtikuu_toukokuu_kesÃÂ¤kuu_heinÃÂ¤kuu_elokuu_syyskuu_lokakuu_marraskuu_joulukuu'.split('_'),
        monthsShort : 'tammi_helmi_maalis_huhti_touko_kesÃÂ¤_heinÃÂ¤_elo_syys_loka_marras_joulu'.split('_'),
        weekdays : 'sunnuntai_maanantai_tiistai_keskiviikko_torstai_perjantai_lauantai'.split('_'),
        weekdaysShort : 'su_ma_ti_ke_to_pe_la'.split('_'),
        weekdaysMin : 'su_ma_ti_ke_to_pe_la'.split('_'),
        longDateFormat : {
            LT : 'HH.mm',
            LTS : 'HH.mm.ss',
            L : 'DD.MM.YYYY',
            LL : 'Do MMMM[ta] YYYY',
            LLL : 'Do MMMM[ta] YYYY, [klo] HH.mm',
            LLLL : 'dddd, Do MMMM[ta] YYYY, [klo] HH.mm',
            l : 'D.M.YYYY',
            ll : 'Do MMM YYYY',
            lll : 'Do MMM YYYY, [klo] HH.mm',
            llll : 'ddd, Do MMM YYYY, [klo] HH.mm'
        },
        calendar : {
            sameDay : '[tÃÂ¤nÃÂ¤ÃÂ¤n] [klo] LT',
            nextDay : '[huomenna] [klo] LT',
            nextWeek : 'dddd [klo] LT',
            lastDay : '[eilen] [klo] LT',
            lastWeek : '[viime] dddd[na] [klo] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s pÃÂ¤ÃÂ¤stÃÂ¤',
            past : '%s sitten',
            s : fi__translate,
            m : fi__translate,
            mm : fi__translate,
            h : fi__translate,
            hh : fi__translate,
            d : fi__translate,
            dd : fi__translate,
            M : fi__translate,
            MM : fi__translate,
            y : fi__translate,
            yy : fi__translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : faroese (fo)
    //! author : Ragnar Johannesen : https://github.com/ragnar123

    var fo = moment.defineLocale('fo', {
        months : 'januar_februar_mars_aprÃÂ­l_mai_juni_juli_august_september_oktober_november_desember'.split('_'),
        monthsShort : 'jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des'.split('_'),
        weekdays : 'sunnudagur_mÃÂ¡nadagur_tÃÂ½sdagur_mikudagur_hÃÂ³sdagur_frÃÂ­ggjadagur_leygardagur'.split('_'),
        weekdaysShort : 'sun_mÃÂ¡n_tÃÂ½s_mik_hÃÂ³s_frÃÂ­_ley'.split('_'),
        weekdaysMin : 'su_mÃÂ¡_tÃÂ½_mi_hÃÂ³_fr_le'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D. MMMM, YYYY HH:mm'
        },
        calendar : {
            sameDay : '[ÃÂ dag kl.] LT',
            nextDay : '[ÃÂ morgin kl.] LT',
            nextWeek : 'dddd [kl.] LT',
            lastDay : '[ÃÂ gjÃÂ¡r kl.] LT',
            lastWeek : '[sÃÂ­ÃÂ°stu] dddd [kl] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'um %s',
            past : '%s sÃÂ­ÃÂ°ani',
            s : 'fÃÂ¡ sekund',
            m : 'ein minutt',
            mm : '%d minuttir',
            h : 'ein tÃÂ­mi',
            hh : '%d tÃÂ­mar',
            d : 'ein dagur',
            dd : '%d dagar',
            M : 'ein mÃÂ¡naÃÂ°i',
            MM : '%d mÃÂ¡naÃÂ°ir',
            y : 'eitt ÃÂ¡r',
            yy : '%d ÃÂ¡r'
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : canadian french (fr-ca)
    //! author : Jonathan Abourbih : https://github.com/jonbca

    var fr_ca = moment.defineLocale('fr-ca', {
        months : 'janvier_fÃÂ©vrier_mars_avril_mai_juin_juillet_aoÃÂ»t_septembre_octobre_novembre_dÃÂ©cembre'.split('_'),
        monthsShort : 'janv._fÃÂ©vr._mars_avr._mai_juin_juil._aoÃÂ»t_sept._oct._nov._dÃÂ©c.'.split('_'),
        monthsParseExact : true,
        weekdays : 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split('_'),
        weekdaysShort : 'dim._lun._mar._mer._jeu._ven._sam.'.split('_'),
        weekdaysMin : 'Di_Lu_Ma_Me_Je_Ve_Sa'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'YYYY-MM-DD',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[Aujourd\'hui ÃÂ ] LT',
            nextDay: '[Demain ÃÂ ] LT',
            nextWeek: 'dddd [ÃÂ ] LT',
            lastDay: '[Hier ÃÂ ] LT',
            lastWeek: 'dddd [dernier ÃÂ ] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'dans %s',
            past : 'il y a %s',
            s : 'quelques secondes',
            m : 'une minute',
            mm : '%d minutes',
            h : 'une heure',
            hh : '%d heures',
            d : 'un jour',
            dd : '%d jours',
            M : 'un mois',
            MM : '%d mois',
            y : 'un an',
            yy : '%d ans'
        },
        ordinalParse: /\d{1,2}(er|e)/,
        ordinal : function (number) {
            return number + (number === 1 ? 'er' : 'e');
        }
    });

    //! moment.js locale configuration
    //! locale : swiss french (fr)
    //! author : Gaspard Bucher : https://github.com/gaspard

    var fr_ch = moment.defineLocale('fr-ch', {
        months : 'janvier_fÃÂ©vrier_mars_avril_mai_juin_juillet_aoÃÂ»t_septembre_octobre_novembre_dÃÂ©cembre'.split('_'),
        monthsShort : 'janv._fÃÂ©vr._mars_avr._mai_juin_juil._aoÃÂ»t_sept._oct._nov._dÃÂ©c.'.split('_'),
        monthsParseExact : true,
        weekdays : 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split('_'),
        weekdaysShort : 'dim._lun._mar._mer._jeu._ven._sam.'.split('_'),
        weekdaysMin : 'Di_Lu_Ma_Me_Je_Ve_Sa'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[Aujourd\'hui ÃÂ ] LT',
            nextDay: '[Demain ÃÂ ] LT',
            nextWeek: 'dddd [ÃÂ ] LT',
            lastDay: '[Hier ÃÂ ] LT',
            lastWeek: 'dddd [dernier ÃÂ ] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'dans %s',
            past : 'il y a %s',
            s : 'quelques secondes',
            m : 'une minute',
            mm : '%d minutes',
            h : 'une heure',
            hh : '%d heures',
            d : 'un jour',
            dd : '%d jours',
            M : 'un mois',
            MM : '%d mois',
            y : 'un an',
            yy : '%d ans'
        },
        ordinalParse: /\d{1,2}(er|e)/,
        ordinal : function (number) {
            return number + (number === 1 ? 'er' : 'e');
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : french (fr)
    //! author : John Fischer : https://github.com/jfroffice

    var fr = moment.defineLocale('fr', {
        months : 'janvier_fÃÂ©vrier_mars_avril_mai_juin_juillet_aoÃÂ»t_septembre_octobre_novembre_dÃÂ©cembre'.split('_'),
        monthsShort : 'janv._fÃÂ©vr._mars_avr._mai_juin_juil._aoÃÂ»t_sept._oct._nov._dÃÂ©c.'.split('_'),
        monthsParseExact : true,
        weekdays : 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split('_'),
        weekdaysShort : 'dim._lun._mar._mer._jeu._ven._sam.'.split('_'),
        weekdaysMin : 'Di_Lu_Ma_Me_Je_Ve_Sa'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[Aujourd\'hui ÃÂ ] LT',
            nextDay: '[Demain ÃÂ ] LT',
            nextWeek: 'dddd [ÃÂ ] LT',
            lastDay: '[Hier ÃÂ ] LT',
            lastWeek: 'dddd [dernier ÃÂ ] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'dans %s',
            past : 'il y a %s',
            s : 'quelques secondes',
            m : 'une minute',
            mm : '%d minutes',
            h : 'une heure',
            hh : '%d heures',
            d : 'un jour',
            dd : '%d jours',
            M : 'un mois',
            MM : '%d mois',
            y : 'un an',
            yy : '%d ans'
        },
        ordinalParse: /\d{1,2}(er|)/,
        ordinal : function (number) {
            return number + (number === 1 ? 'er' : '');
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : frisian (fy)
    //! author : Robin van der Vliet : https://github.com/robin0van0der0v

    var fy__monthsShortWithDots = 'jan._feb._mrt._apr._mai_jun._jul._aug._sep._okt._nov._des.'.split('_'),
        fy__monthsShortWithoutDots = 'jan_feb_mrt_apr_mai_jun_jul_aug_sep_okt_nov_des'.split('_');

    var fy = moment.defineLocale('fy', {
        months : 'jannewaris_febrewaris_maart_april_maaie_juny_july_augustus_septimber_oktober_novimber_desimber'.split('_'),
        monthsShort : function (m, format) {
            if (/-MMM-/.test(format)) {
                return fy__monthsShortWithoutDots[m.month()];
            } else {
                return fy__monthsShortWithDots[m.month()];
            }
        },
        monthsParseExact : true,
        weekdays : 'snein_moandei_tiisdei_woansdei_tongersdei_freed_sneon'.split('_'),
        weekdaysShort : 'si._mo._ti._wo._to._fr._so.'.split('_'),
        weekdaysMin : 'Si_Mo_Ti_Wo_To_Fr_So'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD-MM-YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[hjoed om] LT',
            nextDay: '[moarn om] LT',
            nextWeek: 'dddd [om] LT',
            lastDay: '[juster om] LT',
            lastWeek: '[ÃÂ´frÃÂ»ne] dddd [om] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'oer %s',
            past : '%s lyn',
            s : 'in pear sekonden',
            m : 'ien minÃÂºt',
            mm : '%d minuten',
            h : 'ien oere',
            hh : '%d oeren',
            d : 'ien dei',
            dd : '%d dagen',
            M : 'ien moanne',
            MM : '%d moannen',
            y : 'ien jier',
            yy : '%d jierren'
        },
        ordinalParse: /\d{1,2}(ste|de)/,
        ordinal : function (number) {
            return number + ((number === 1 || number === 8 || number >= 20) ? 'ste' : 'de');
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : great britain scottish gealic (gd)
    //! author : Jon Ashdown : https://github.com/jonashdown

    var gd__months = [
        'Am Faoilleach', 'An Gearran', 'Am MÃÂ rt', 'An Giblean', 'An CÃÂ¨itean', 'An t-ÃÂgmhios', 'An t-Iuchar', 'An LÃÂ¹nastal', 'An t-Sultain', 'An DÃÂ mhair', 'An t-Samhain', 'An DÃÂ¹bhlachd'
    ];

    var gd__monthsShort = ['Faoi', 'Gear', 'MÃÂ rt', 'Gibl', 'CÃÂ¨it', 'ÃÂgmh', 'Iuch', 'LÃÂ¹n', 'Sult', 'DÃÂ mh', 'Samh', 'DÃÂ¹bh'];

    var gd__weekdays = ['DidÃÂ²mhnaich', 'Diluain', 'DimÃÂ irt', 'Diciadain', 'Diardaoin', 'Dihaoine', 'Disathairne'];

    var weekdaysShort = ['Did', 'Dil', 'Dim', 'Dic', 'Dia', 'Dih', 'Dis'];

    var weekdaysMin = ['DÃÂ²', 'Lu', 'MÃÂ ', 'Ci', 'Ar', 'Ha', 'Sa'];

    var gd = moment.defineLocale('gd', {
        months : gd__months,
        monthsShort : gd__monthsShort,
        monthsParseExact : true,
        weekdays : gd__weekdays,
        weekdaysShort : weekdaysShort,
        weekdaysMin : weekdaysMin,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[An-diugh aig] LT',
            nextDay : '[A-mÃÂ ireach aig] LT',
            nextWeek : 'dddd [aig] LT',
            lastDay : '[An-dÃÂ¨ aig] LT',
            lastWeek : 'dddd [seo chaidh] [aig] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ann an %s',
            past : 'bho chionn %s',
            s : 'beagan diogan',
            m : 'mionaid',
            mm : '%d mionaidean',
            h : 'uair',
            hh : '%d uairean',
            d : 'latha',
            dd : '%d latha',
            M : 'mÃÂ¬os',
            MM : '%d mÃÂ¬osan',
            y : 'bliadhna',
            yy : '%d bliadhna'
        },
        ordinalParse : /\d{1,2}(d|na|mh)/,
        ordinal : function (number) {
            var output = number === 1 ? 'd' : number % 10 === 2 ? 'na' : 'mh';
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : galician (gl)
    //! author : Juan G. Hurtado : https://github.com/juanghurtado

    var gl = moment.defineLocale('gl', {
        months : 'Xaneiro_Febreiro_Marzo_Abril_Maio_XuÃÂ±o_Xullo_Agosto_Setembro_Outubro_Novembro_Decembro'.split('_'),
        monthsShort : 'Xan._Feb._Mar._Abr._Mai._XuÃÂ±._Xul._Ago._Set._Out._Nov._Dec.'.split('_'),
        monthsParseExact: true,
        weekdays : 'Domingo_Luns_Martes_MÃÂ©rcores_Xoves_Venres_SÃÂ¡bado'.split('_'),
        weekdaysShort : 'Dom._Lun._Mar._MÃÂ©r._Xov._Ven._SÃÂ¡b.'.split('_'),
        weekdaysMin : 'Do_Lu_Ma_MÃÂ©_Xo_Ve_SÃÂ¡'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY H:mm',
            LLLL : 'dddd D MMMM YYYY H:mm'
        },
        calendar : {
            sameDay : function () {
                return '[hoxe ' + ((this.hours() !== 1) ? 'ÃÂ¡s' : 'ÃÂ¡') + '] LT';
            },
            nextDay : function () {
                return '[maÃÂ±ÃÂ¡ ' + ((this.hours() !== 1) ? 'ÃÂ¡s' : 'ÃÂ¡') + '] LT';
            },
            nextWeek : function () {
                return 'dddd [' + ((this.hours() !== 1) ? 'ÃÂ¡s' : 'a') + '] LT';
            },
            lastDay : function () {
                return '[onte ' + ((this.hours() !== 1) ? 'ÃÂ¡' : 'a') + '] LT';
            },
            lastWeek : function () {
                return '[o] dddd [pasado ' + ((this.hours() !== 1) ? 'ÃÂ¡s' : 'a') + '] LT';
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : function (str) {
                if (str === 'uns segundos') {
                    return 'nuns segundos';
                }
                return 'en ' + str;
            },
            past : 'hai %s',
            s : 'uns segundos',
            m : 'un minuto',
            mm : '%d minutos',
            h : 'unha hora',
            hh : '%d horas',
            d : 'un dÃÂ­a',
            dd : '%d dÃÂ­as',
            M : 'un mes',
            MM : '%d meses',
            y : 'un ano',
            yy : '%d anos'
        },
        ordinalParse : /\d{1,2}ÃÂº/,
        ordinal : '%dÃÂº',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Hebrew (he)
    //! author : Tomer Cohen : https://github.com/tomer
    //! author : Moshe Simantov : https://github.com/DevelopmentIL
    //! author : Tal Ater : https://github.com/TalAter

    var he = moment.defineLocale('he', {
        months : 'ÃÂÃÂ ÃÂÃÂÃÂ¨_ÃÂ¤ÃÂÃÂ¨ÃÂÃÂÃÂ¨_ÃÂÃÂ¨ÃÂ¥_ÃÂÃÂ¤ÃÂ¨ÃÂÃÂ_ÃÂÃÂÃÂ_ÃÂÃÂÃÂ ÃÂ_ÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂ¡ÃÂ_ÃÂ¡ÃÂ¤ÃÂÃÂÃÂÃÂ¨_ÃÂÃÂÃÂ§ÃÂÃÂÃÂÃÂ¨_ÃÂ ÃÂÃÂÃÂÃÂÃÂ¨_ÃÂÃÂ¦ÃÂÃÂÃÂ¨'.split('_'),
        monthsShort : 'ÃÂÃÂ ÃÂÃÂ³_ÃÂ¤ÃÂÃÂ¨ÃÂ³_ÃÂÃÂ¨ÃÂ¥_ÃÂÃÂ¤ÃÂ¨ÃÂ³_ÃÂÃÂÃÂ_ÃÂÃÂÃÂ ÃÂ_ÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂ³_ÃÂ¡ÃÂ¤ÃÂÃÂ³_ÃÂÃÂÃÂ§ÃÂ³_ÃÂ ÃÂÃÂÃÂ³_ÃÂÃÂ¦ÃÂÃÂ³'.split('_'),
        weekdays : 'ÃÂ¨ÃÂÃÂ©ÃÂÃÂ_ÃÂ©ÃÂ ÃÂ_ÃÂ©ÃÂÃÂÃÂ©ÃÂ_ÃÂ¨ÃÂÃÂÃÂ¢ÃÂ_ÃÂÃÂÃÂÃÂ©ÃÂ_ÃÂ©ÃÂÃÂ©ÃÂ_ÃÂ©ÃÂÃÂª'.split('_'),
        weekdaysShort : 'ÃÂÃÂ³_ÃÂÃÂ³_ÃÂÃÂ³_ÃÂÃÂ³_ÃÂÃÂ³_ÃÂÃÂ³_ÃÂ©ÃÂ³'.split('_'),
        weekdaysMin : 'ÃÂ_ÃÂ_ÃÂ_ÃÂ_ÃÂ_ÃÂ_ÃÂ©'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D [ÃÂ]MMMM YYYY',
            LLL : 'D [ÃÂ]MMMM YYYY HH:mm',
            LLLL : 'dddd, D [ÃÂ]MMMM YYYY HH:mm',
            l : 'D/M/YYYY',
            ll : 'D MMM YYYY',
            lll : 'D MMM YYYY HH:mm',
            llll : 'ddd, D MMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[ÃÂÃÂÃÂÃÂ ÃÂÃÂ¾]LT',
            nextDay : '[ÃÂÃÂÃÂ¨ ÃÂÃÂ¾]LT',
            nextWeek : 'dddd [ÃÂÃÂ©ÃÂ¢ÃÂ] LT',
            lastDay : '[ÃÂÃÂªÃÂÃÂÃÂ ÃÂÃÂ¾]LT',
            lastWeek : '[ÃÂÃÂÃÂÃÂ] dddd [ÃÂÃÂÃÂÃÂ¨ÃÂÃÂ ÃÂÃÂ©ÃÂ¢ÃÂ] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂÃÂ¢ÃÂÃÂ %s',
            past : 'ÃÂÃÂ¤ÃÂ ÃÂ %s',
            s : 'ÃÂÃÂ¡ÃÂ¤ÃÂ¨ ÃÂ©ÃÂ ÃÂÃÂÃÂª',
            m : 'ÃÂÃÂ§ÃÂ',
            mm : '%d ÃÂÃÂ§ÃÂÃÂª',
            h : 'ÃÂ©ÃÂ¢ÃÂ',
            hh : function (number) {
                if (number === 2) {
                    return 'ÃÂ©ÃÂ¢ÃÂªÃÂÃÂÃÂ';
                }
                return number + ' ÃÂ©ÃÂ¢ÃÂÃÂª';
            },
            d : 'ÃÂÃÂÃÂ',
            dd : function (number) {
                if (number === 2) {
                    return 'ÃÂÃÂÃÂÃÂÃÂÃÂ';
                }
                return number + ' ÃÂÃÂÃÂÃÂ';
            },
            M : 'ÃÂÃÂÃÂÃÂ©',
            MM : function (number) {
                if (number === 2) {
                    return 'ÃÂÃÂÃÂÃÂ©ÃÂÃÂÃÂ';
                }
                return number + ' ÃÂÃÂÃÂÃÂ©ÃÂÃÂ';
            },
            y : 'ÃÂ©ÃÂ ÃÂ',
            yy : function (number) {
                if (number === 2) {
                    return 'ÃÂ©ÃÂ ÃÂªÃÂÃÂÃÂ';
                } else if (number % 10 === 0 && number !== 10) {
                    return number + ' ÃÂ©ÃÂ ÃÂ';
                }
                return number + ' ÃÂ©ÃÂ ÃÂÃÂ';
            }
        },
        meridiemParse: /ÃÂÃÂÃÂ"ÃÂ¦|ÃÂÃÂ¤ÃÂ ÃÂ"ÃÂ¦|ÃÂÃÂÃÂ¨ÃÂ ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂÃÂ|ÃÂÃÂ¤ÃÂ ÃÂ ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂÃÂ|ÃÂÃÂ¤ÃÂ ÃÂÃÂª ÃÂÃÂÃÂ§ÃÂ¨|ÃÂÃÂÃÂÃÂ§ÃÂ¨|ÃÂÃÂ¢ÃÂ¨ÃÂ/i,
        isPM : function (input) {
            return /^(ÃÂÃÂÃÂ"ÃÂ¦|ÃÂÃÂÃÂ¨ÃÂ ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂÃÂ|ÃÂÃÂ¢ÃÂ¨ÃÂ)$/.test(input);
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 5) {
                return 'ÃÂÃÂ¤ÃÂ ÃÂÃÂª ÃÂÃÂÃÂ§ÃÂ¨';
            } else if (hour < 10) {
                return 'ÃÂÃÂÃÂÃÂ§ÃÂ¨';
            } else if (hour < 12) {
                return isLower ? 'ÃÂÃÂ¤ÃÂ ÃÂ"ÃÂ¦' : 'ÃÂÃÂ¤ÃÂ ÃÂ ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂÃÂ';
            } else if (hour < 18) {
                return isLower ? 'ÃÂÃÂÃÂ"ÃÂ¦' : 'ÃÂÃÂÃÂ¨ÃÂ ÃÂÃÂ¦ÃÂÃÂ¨ÃÂÃÂÃÂ';
            } else {
                return 'ÃÂÃÂ¢ÃÂ¨ÃÂ';
            }
        }
    });

    //! moment.js locale configuration
    //! locale : hindi (hi)
    //! author : Mayank Singhal : https://github.com/mayanksinghal

    var hi__symbolMap = {
        '1': 'Ã Â¥Â§',
        '2': 'Ã Â¥Â¨',
        '3': 'Ã Â¥Â©',
        '4': 'Ã Â¥Âª',
        '5': 'Ã Â¥Â«',
        '6': 'Ã Â¥Â¬',
        '7': 'Ã Â¥Â­',
        '8': 'Ã Â¥Â®',
        '9': 'Ã Â¥Â¯',
        '0': 'Ã Â¥Â¦'
    },
    hi__numberMap = {
        'Ã Â¥Â§': '1',
        'Ã Â¥Â¨': '2',
        'Ã Â¥Â©': '3',
        'Ã Â¥Âª': '4',
        'Ã Â¥Â«': '5',
        'Ã Â¥Â¬': '6',
        'Ã Â¥Â­': '7',
        'Ã Â¥Â®': '8',
        'Ã Â¥Â¯': '9',
        'Ã Â¥Â¦': '0'
    };

    var hi = moment.defineLocale('hi', {
        months : 'Ã Â¤ÂÃ Â¤Â¨Ã Â¤ÂµÃ Â¤Â°Ã Â¥Â_Ã Â¤Â«Ã Â¤Â¼Ã Â¤Â°Ã Â¤ÂµÃ Â¤Â°Ã Â¥Â_Ã Â¤Â®Ã Â¤Â¾Ã Â¤Â°Ã Â¥ÂÃ Â¤Â_Ã Â¤ÂÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â°Ã Â¥ÂÃ Â¤Â²_Ã Â¤Â®Ã Â¤Â_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â¨_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â²Ã Â¤Â¾Ã Â¤Â_Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¸Ã Â¥ÂÃ Â¤Â¤_Ã Â¤Â¸Ã Â¤Â¿Ã Â¤Â¤Ã Â¤Â®Ã Â¥ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤Â¨Ã Â¤ÂµÃ Â¤Â®Ã Â¥ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤Â¦Ã Â¤Â¿Ã Â¤Â¸Ã Â¤Â®Ã Â¥ÂÃ Â¤Â¬Ã Â¤Â°'.split('_'),
        monthsShort : 'Ã Â¤ÂÃ Â¤Â¨._Ã Â¤Â«Ã Â¤Â¼Ã Â¤Â°._Ã Â¤Â®Ã Â¤Â¾Ã Â¤Â°Ã Â¥ÂÃ Â¤Â_Ã Â¤ÂÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â°Ã Â¥Â._Ã Â¤Â®Ã Â¤Â_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â¨_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â²._Ã Â¤ÂÃ Â¤Â._Ã Â¤Â¸Ã Â¤Â¿Ã Â¤Â¤._Ã Â¤ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¥Â._Ã Â¤Â¨Ã Â¤Âµ._Ã Â¤Â¦Ã Â¤Â¿Ã Â¤Â¸.'.split('_'),
        monthsParseExact: true,
        weekdays : 'Ã Â¤Â°Ã Â¤ÂµÃ Â¤Â¿Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â®Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â®Ã Â¤ÂÃ Â¤ÂÃ Â¤Â²Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¬Ã Â¥ÂÃ Â¤Â§Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â°Ã Â¥ÂÃ Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¶Ã Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â°Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¶Ã Â¤Â¨Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°'.split('_'),
        weekdaysShort : 'Ã Â¤Â°Ã Â¤ÂµÃ Â¤Â¿_Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â®_Ã Â¤Â®Ã Â¤ÂÃ Â¤ÂÃ Â¤Â²_Ã Â¤Â¬Ã Â¥ÂÃ Â¤Â§_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â°Ã Â¥Â_Ã Â¤Â¶Ã Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â°_Ã Â¤Â¶Ã Â¤Â¨Ã Â¤Â¿'.split('_'),
        weekdaysMin : 'Ã Â¤Â°_Ã Â¤Â¸Ã Â¥Â_Ã Â¤Â®Ã Â¤Â_Ã Â¤Â¬Ã Â¥Â_Ã Â¤ÂÃ Â¥Â_Ã Â¤Â¶Ã Â¥Â_Ã Â¤Â¶'.split('_'),
        longDateFormat : {
            LT : 'A h:mm Ã Â¤Â¬Ã Â¤ÂÃ Â¥Â',
            LTS : 'A h:mm:ss Ã Â¤Â¬Ã Â¤ÂÃ Â¥Â',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY, A h:mm Ã Â¤Â¬Ã Â¤ÂÃ Â¥Â',
            LLLL : 'dddd, D MMMM YYYY, A h:mm Ã Â¤Â¬Ã Â¤ÂÃ Â¥Â'
        },
        calendar : {
            sameDay : '[Ã Â¤ÂÃ Â¤Â] LT',
            nextDay : '[Ã Â¤ÂÃ Â¤Â²] LT',
            nextWeek : 'dddd, LT',
            lastDay : '[Ã Â¤ÂÃ Â¤Â²] LT',
            lastWeek : '[Ã Â¤ÂªÃ Â¤Â¿Ã Â¤ÂÃ Â¤Â²Ã Â¥Â] dddd, LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s Ã Â¤Â®Ã Â¥ÂÃ Â¤Â',
            past : '%s Ã Â¤ÂªÃ Â¤Â¹Ã Â¤Â²Ã Â¥Â',
            s : 'Ã Â¤ÂÃ Â¥ÂÃ Â¤Â Ã Â¤Â¹Ã Â¥Â Ã Â¤ÂÃ Â¥ÂÃ Â¤Â·Ã Â¤Â£',
            m : 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â',
            mm : '%d Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â',
            h : 'Ã Â¤ÂÃ Â¤Â Ã Â¤ÂÃ Â¤ÂÃ Â¤ÂÃ Â¤Â¾',
            hh : '%d Ã Â¤ÂÃ Â¤ÂÃ Â¤ÂÃ Â¥Â',
            d : 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â¦Ã Â¤Â¿Ã Â¤Â¨',
            dd : '%d Ã Â¤Â¦Ã Â¤Â¿Ã Â¤Â¨',
            M : 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â®Ã Â¤Â¹Ã Â¥ÂÃ Â¤Â¨Ã Â¥Â',
            MM : '%d Ã Â¤Â®Ã Â¤Â¹Ã Â¥ÂÃ Â¤Â¨Ã Â¥Â',
            y : 'Ã Â¤ÂÃ Â¤Â Ã Â¤ÂµÃ Â¤Â°Ã Â¥ÂÃ Â¤Â·',
            yy : '%d Ã Â¤ÂµÃ Â¤Â°Ã Â¥ÂÃ Â¤Â·'
        },
        preparse: function (string) {
            return string.replace(/[Ã Â¥Â§Ã Â¥Â¨Ã Â¥Â©Ã Â¥ÂªÃ Â¥Â«Ã Â¥Â¬Ã Â¥Â­Ã Â¥Â®Ã Â¥Â¯Ã Â¥Â¦]/g, function (match) {
                return hi__numberMap[match];
            });
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return hi__symbolMap[match];
            });
        },
        // Hindi notation for meridiems are quite fuzzy in practice. While there exists
        // a rigid notion of a 'Pahar' it is not used as rigidly in modern Hindi.
        meridiemParse: /Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤|Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â¬Ã Â¤Â¹|Ã Â¤Â¦Ã Â¥ÂÃ Â¤ÂªÃ Â¤Â¹Ã Â¤Â°|Ã Â¤Â¶Ã Â¤Â¾Ã Â¤Â®/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤') {
                return hour < 4 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â¬Ã Â¤Â¹') {
                return hour;
            } else if (meridiem === 'Ã Â¤Â¦Ã Â¥ÂÃ Â¤ÂªÃ Â¤Â¹Ã Â¤Â°') {
                return hour >= 10 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â¤Â¶Ã Â¤Â¾Ã Â¤Â®') {
                return hour + 12;
            }
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤';
            } else if (hour < 10) {
                return 'Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â¬Ã Â¤Â¹';
            } else if (hour < 17) {
                return 'Ã Â¤Â¦Ã Â¥ÂÃ Â¤ÂªÃ Â¤Â¹Ã Â¤Â°';
            } else if (hour < 20) {
                return 'Ã Â¤Â¶Ã Â¤Â¾Ã Â¤Â®';
            } else {
                return 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤';
            }
        },
        week : {
            dow : 0, // Sunday is the first day of the week.
            doy : 6  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : hrvatski (hr)
    //! author : Bojan MarkoviÃÂ : https://github.com/bmarkovic

    function hr__translate(number, withoutSuffix, key) {
        var result = number + ' ';
        switch (key) {
        case 'm':
            return withoutSuffix ? 'jedna minuta' : 'jedne minute';
        case 'mm':
            if (number === 1) {
                result += 'minuta';
            } else if (number === 2 || number === 3 || number === 4) {
                result += 'minute';
            } else {
                result += 'minuta';
            }
            return result;
        case 'h':
            return withoutSuffix ? 'jedan sat' : 'jednog sata';
        case 'hh':
            if (number === 1) {
                result += 'sat';
            } else if (number === 2 || number === 3 || number === 4) {
                result += 'sata';
            } else {
                result += 'sati';
            }
            return result;
        case 'dd':
            if (number === 1) {
                result += 'dan';
            } else {
                result += 'dana';
            }
            return result;
        case 'MM':
            if (number === 1) {
                result += 'mjesec';
            } else if (number === 2 || number === 3 || number === 4) {
                result += 'mjeseca';
            } else {
                result += 'mjeseci';
            }
            return result;
        case 'yy':
            if (number === 1) {
                result += 'godina';
            } else if (number === 2 || number === 3 || number === 4) {
                result += 'godine';
            } else {
                result += 'godina';
            }
            return result;
        }
    }

    var hr = moment.defineLocale('hr', {
        months : {
            format: 'sijeÃÂnja_veljaÃÂe_oÃÂ¾ujka_travnja_svibnja_lipnja_srpnja_kolovoza_rujna_listopada_studenoga_prosinca'.split('_'),
            standalone: 'sijeÃÂanj_veljaÃÂa_oÃÂ¾ujak_travanj_svibanj_lipanj_srpanj_kolovoz_rujan_listopad_studeni_prosinac'.split('_')
        },
        monthsShort : 'sij._velj._oÃÂ¾u._tra._svi._lip._srp._kol._ruj._lis._stu._pro.'.split('_'),
        monthsParseExact: true,
        weekdays : 'nedjelja_ponedjeljak_utorak_srijeda_ÃÂetvrtak_petak_subota'.split('_'),
        weekdaysShort : 'ned._pon._uto._sri._ÃÂet._pet._sub.'.split('_'),
        weekdaysMin : 'ne_po_ut_sr_ÃÂe_pe_su'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD. MM. YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY H:mm',
            LLLL : 'dddd, D. MMMM YYYY H:mm'
        },
        calendar : {
            sameDay  : '[danas u] LT',
            nextDay  : '[sutra u] LT',
            nextWeek : function () {
                switch (this.day()) {
                case 0:
                    return '[u] [nedjelju] [u] LT';
                case 3:
                    return '[u] [srijedu] [u] LT';
                case 6:
                    return '[u] [subotu] [u] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[u] dddd [u] LT';
                }
            },
            lastDay  : '[juÃÂer u] LT',
            lastWeek : function () {
                switch (this.day()) {
                case 0:
                case 3:
                    return '[proÃÂ¡lu] dddd [u] LT';
                case 6:
                    return '[proÃÂ¡le] [subote] [u] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[proÃÂ¡li] dddd [u] LT';
                }
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : 'za %s',
            past   : 'prije %s',
            s      : 'par sekundi',
            m      : hr__translate,
            mm     : hr__translate,
            h      : hr__translate,
            hh     : hr__translate,
            d      : 'dan',
            dd     : hr__translate,
            M      : 'mjesec',
            MM     : hr__translate,
            y      : 'godinu',
            yy     : hr__translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : hungarian (hu)
    //! author : Adam Brunner : https://github.com/adambrunner

    var weekEndings = 'vasÃÂ¡rnap hÃÂ©tfÃÂn kedden szerdÃÂ¡n csÃÂ¼tÃÂ¶rtÃÂ¶kÃÂ¶n pÃÂ©nteken szombaton'.split(' ');
    function hu__translate(number, withoutSuffix, key, isFuture) {
        var num = number,
            suffix;
        switch (key) {
        case 's':
            return (isFuture || withoutSuffix) ? 'nÃÂ©hÃÂ¡ny mÃÂ¡sodperc' : 'nÃÂ©hÃÂ¡ny mÃÂ¡sodperce';
        case 'm':
            return 'egy' + (isFuture || withoutSuffix ? ' perc' : ' perce');
        case 'mm':
            return num + (isFuture || withoutSuffix ? ' perc' : ' perce');
        case 'h':
            return 'egy' + (isFuture || withoutSuffix ? ' ÃÂ³ra' : ' ÃÂ³rÃÂ¡ja');
        case 'hh':
            return num + (isFuture || withoutSuffix ? ' ÃÂ³ra' : ' ÃÂ³rÃÂ¡ja');
        case 'd':
            return 'egy' + (isFuture || withoutSuffix ? ' nap' : ' napja');
        case 'dd':
            return num + (isFuture || withoutSuffix ? ' nap' : ' napja');
        case 'M':
            return 'egy' + (isFuture || withoutSuffix ? ' hÃÂ³nap' : ' hÃÂ³napja');
        case 'MM':
            return num + (isFuture || withoutSuffix ? ' hÃÂ³nap' : ' hÃÂ³napja');
        case 'y':
            return 'egy' + (isFuture || withoutSuffix ? ' ÃÂ©v' : ' ÃÂ©ve');
        case 'yy':
            return num + (isFuture || withoutSuffix ? ' ÃÂ©v' : ' ÃÂ©ve');
        }
        return '';
    }
    function week(isFuture) {
        return (isFuture ? '' : '[mÃÂºlt] ') + '[' + weekEndings[this.day()] + '] LT[-kor]';
    }

    var hu = moment.defineLocale('hu', {
        months : 'januÃÂ¡r_februÃÂ¡r_mÃÂ¡rcius_ÃÂ¡prilis_mÃÂ¡jus_jÃÂºnius_jÃÂºlius_augusztus_szeptember_oktÃÂ³ber_november_december'.split('_'),
        monthsShort : 'jan_feb_mÃÂ¡rc_ÃÂ¡pr_mÃÂ¡j_jÃÂºn_jÃÂºl_aug_szept_okt_nov_dec'.split('_'),
        weekdays : 'vasÃÂ¡rnap_hÃÂ©tfÃÂ_kedd_szerda_csÃÂ¼tÃÂ¶rtÃÂ¶k_pÃÂ©ntek_szombat'.split('_'),
        weekdaysShort : 'vas_hÃÂ©t_kedd_sze_csÃÂ¼t_pÃÂ©n_szo'.split('_'),
        weekdaysMin : 'v_h_k_sze_cs_p_szo'.split('_'),
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'YYYY.MM.DD.',
            LL : 'YYYY. MMMM D.',
            LLL : 'YYYY. MMMM D. H:mm',
            LLLL : 'YYYY. MMMM D., dddd H:mm'
        },
        meridiemParse: /de|du/i,
        isPM: function (input) {
            return input.charAt(1).toLowerCase() === 'u';
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours < 12) {
                return isLower === true ? 'de' : 'DE';
            } else {
                return isLower === true ? 'du' : 'DU';
            }
        },
        calendar : {
            sameDay : '[ma] LT[-kor]',
            nextDay : '[holnap] LT[-kor]',
            nextWeek : function () {
                return week.call(this, true);
            },
            lastDay : '[tegnap] LT[-kor]',
            lastWeek : function () {
                return week.call(this, false);
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s mÃÂºlva',
            past : '%s',
            s : hu__translate,
            m : hu__translate,
            mm : hu__translate,
            h : hu__translate,
            hh : hu__translate,
            d : hu__translate,
            dd : hu__translate,
            M : hu__translate,
            MM : hu__translate,
            y : hu__translate,
            yy : hu__translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Armenian (hy-am)
    //! author : Armendarabyan : https://github.com/armendarabyan

    var hy_am = moment.defineLocale('hy-am', {
        months : {
            format: 'ÃÂ°ÃÂ¸ÃÂÃÂ¶ÃÂ¾ÃÂ¡ÃÂÃÂ«_ÃÂÃÂ¥ÃÂ¿ÃÂÃÂ¾ÃÂ¡ÃÂÃÂ«_ÃÂ´ÃÂ¡ÃÂÃÂ¿ÃÂ«_ÃÂ¡ÃÂºÃÂÃÂ«ÃÂ¬ÃÂ«_ÃÂ´ÃÂ¡ÃÂµÃÂ«ÃÂ½ÃÂ«_ÃÂ°ÃÂ¸ÃÂÃÂ¶ÃÂ«ÃÂ½ÃÂ«_ÃÂ°ÃÂ¸ÃÂÃÂ¬ÃÂ«ÃÂ½ÃÂ«_ÃÂÃÂ£ÃÂ¸ÃÂ½ÃÂ¿ÃÂ¸ÃÂ½ÃÂ«_ÃÂ½ÃÂ¥ÃÂºÃÂ¿ÃÂ¥ÃÂ´ÃÂ¢ÃÂ¥ÃÂÃÂ«_ÃÂ°ÃÂ¸ÃÂ¯ÃÂ¿ÃÂ¥ÃÂ´ÃÂ¢ÃÂ¥ÃÂÃÂ«_ÃÂ¶ÃÂ¸ÃÂµÃÂ¥ÃÂ´ÃÂ¢ÃÂ¥ÃÂÃÂ«_ÃÂ¤ÃÂ¥ÃÂ¯ÃÂ¿ÃÂ¥ÃÂ´ÃÂ¢ÃÂ¥ÃÂÃÂ«'.split('_'),
            standalone: 'ÃÂ°ÃÂ¸ÃÂÃÂ¶ÃÂ¾ÃÂ¡ÃÂ_ÃÂÃÂ¥ÃÂ¿ÃÂÃÂ¾ÃÂ¡ÃÂ_ÃÂ´ÃÂ¡ÃÂÃÂ¿_ÃÂ¡ÃÂºÃÂÃÂ«ÃÂ¬_ÃÂ´ÃÂ¡ÃÂµÃÂ«ÃÂ½_ÃÂ°ÃÂ¸ÃÂÃÂ¶ÃÂ«ÃÂ½_ÃÂ°ÃÂ¸ÃÂÃÂ¬ÃÂ«ÃÂ½_ÃÂÃÂ£ÃÂ¸ÃÂ½ÃÂ¿ÃÂ¸ÃÂ½_ÃÂ½ÃÂ¥ÃÂºÃÂ¿ÃÂ¥ÃÂ´ÃÂ¢ÃÂ¥ÃÂ_ÃÂ°ÃÂ¸ÃÂ¯ÃÂ¿ÃÂ¥ÃÂ´ÃÂ¢ÃÂ¥ÃÂ_ÃÂ¶ÃÂ¸ÃÂµÃÂ¥ÃÂ´ÃÂ¢ÃÂ¥ÃÂ_ÃÂ¤ÃÂ¥ÃÂ¯ÃÂ¿ÃÂ¥ÃÂ´ÃÂ¢ÃÂ¥ÃÂ'.split('_')
        },
        monthsShort : 'ÃÂ°ÃÂ¶ÃÂ¾_ÃÂÃÂ¿ÃÂ_ÃÂ´ÃÂÃÂ¿_ÃÂ¡ÃÂºÃÂ_ÃÂ´ÃÂµÃÂ½_ÃÂ°ÃÂ¶ÃÂ½_ÃÂ°ÃÂ¬ÃÂ½_ÃÂÃÂ£ÃÂ½_ÃÂ½ÃÂºÃÂ¿_ÃÂ°ÃÂ¯ÃÂ¿_ÃÂ¶ÃÂ´ÃÂ¢_ÃÂ¤ÃÂ¯ÃÂ¿'.split('_'),
        weekdays : 'ÃÂ¯ÃÂ«ÃÂÃÂ¡ÃÂ¯ÃÂ«_ÃÂ¥ÃÂÃÂ¯ÃÂ¸ÃÂÃÂ·ÃÂ¡ÃÂ¢ÃÂ©ÃÂ«_ÃÂ¥ÃÂÃÂ¥ÃÂÃÂ·ÃÂ¡ÃÂ¢ÃÂ©ÃÂ«_ÃÂ¹ÃÂ¸ÃÂÃÂ¥ÃÂÃÂ·ÃÂ¡ÃÂ¢ÃÂ©ÃÂ«_ÃÂ°ÃÂ«ÃÂ¶ÃÂ£ÃÂ·ÃÂ¡ÃÂ¢ÃÂ©ÃÂ«_ÃÂ¸ÃÂÃÂÃÂ¢ÃÂ¡ÃÂ©_ÃÂ·ÃÂ¡ÃÂ¢ÃÂ¡ÃÂ©'.split('_'),
        weekdaysShort : 'ÃÂ¯ÃÂÃÂ¯_ÃÂ¥ÃÂÃÂ¯_ÃÂ¥ÃÂÃÂ_ÃÂ¹ÃÂÃÂ_ÃÂ°ÃÂ¶ÃÂ£_ÃÂ¸ÃÂÃÂÃÂ¢_ÃÂ·ÃÂ¢ÃÂ©'.split('_'),
        weekdaysMin : 'ÃÂ¯ÃÂÃÂ¯_ÃÂ¥ÃÂÃÂ¯_ÃÂ¥ÃÂÃÂ_ÃÂ¹ÃÂÃÂ_ÃÂ°ÃÂ¶ÃÂ£_ÃÂ¸ÃÂÃÂÃÂ¢_ÃÂ·ÃÂ¢ÃÂ©'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY ÃÂ©.',
            LLL : 'D MMMM YYYY ÃÂ©., HH:mm',
            LLLL : 'dddd, D MMMM YYYY ÃÂ©., HH:mm'
        },
        calendar : {
            sameDay: '[ÃÂ¡ÃÂµÃÂ½ÃÂÃÂ] LT',
            nextDay: '[ÃÂ¾ÃÂ¡ÃÂ²ÃÂ¨] LT',
            lastDay: '[ÃÂ¥ÃÂÃÂ¥ÃÂ¯] LT',
            nextWeek: function () {
                return 'dddd [ÃÂÃÂÃÂ¨ ÃÂªÃÂ¡ÃÂ´ÃÂ¨] LT';
            },
            lastWeek: function () {
                return '[ÃÂ¡ÃÂ¶ÃÂÃÂ¡ÃÂ®] dddd [ÃÂÃÂÃÂ¨ ÃÂªÃÂ¡ÃÂ´ÃÂ¨] LT';
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : '%s ÃÂ°ÃÂ¥ÃÂ¿ÃÂ¸',
            past : '%s ÃÂ¡ÃÂ¼ÃÂ¡ÃÂ»',
            s : 'ÃÂ´ÃÂ« ÃÂÃÂ¡ÃÂ¶ÃÂ« ÃÂ¾ÃÂ¡ÃÂµÃÂÃÂ¯ÃÂµÃÂ¡ÃÂ¶',
            m : 'ÃÂÃÂ¸ÃÂºÃÂ¥',
            mm : '%d ÃÂÃÂ¸ÃÂºÃÂ¥',
            h : 'ÃÂªÃÂ¡ÃÂ´',
            hh : '%d ÃÂªÃÂ¡ÃÂ´',
            d : 'ÃÂÃÂ',
            dd : '%d ÃÂÃÂ',
            M : 'ÃÂ¡ÃÂ´ÃÂ«ÃÂ½',
            MM : '%d ÃÂ¡ÃÂ´ÃÂ«ÃÂ½',
            y : 'ÃÂ¿ÃÂ¡ÃÂÃÂ«',
            yy : '%d ÃÂ¿ÃÂ¡ÃÂÃÂ«'
        },
        meridiemParse: /ÃÂ£ÃÂ«ÃÂ·ÃÂ¥ÃÂÃÂ¾ÃÂ¡|ÃÂ¡ÃÂ¼ÃÂ¡ÃÂ¾ÃÂ¸ÃÂ¿ÃÂ¾ÃÂ¡|ÃÂÃÂ¥ÃÂÃÂ¥ÃÂ¯ÃÂ¾ÃÂ¡|ÃÂ¥ÃÂÃÂ¥ÃÂ¯ÃÂ¸ÃÂµÃÂ¡ÃÂ¶/,
        isPM: function (input) {
            return /^(ÃÂÃÂ¥ÃÂÃÂ¥ÃÂ¯ÃÂ¾ÃÂ¡|ÃÂ¥ÃÂÃÂ¥ÃÂ¯ÃÂ¸ÃÂµÃÂ¡ÃÂ¶)$/.test(input);
        },
        meridiem : function (hour) {
            if (hour < 4) {
                return 'ÃÂ£ÃÂ«ÃÂ·ÃÂ¥ÃÂÃÂ¾ÃÂ¡';
            } else if (hour < 12) {
                return 'ÃÂ¡ÃÂ¼ÃÂ¡ÃÂ¾ÃÂ¸ÃÂ¿ÃÂ¾ÃÂ¡';
            } else if (hour < 17) {
                return 'ÃÂÃÂ¥ÃÂÃÂ¥ÃÂ¯ÃÂ¾ÃÂ¡';
            } else {
                return 'ÃÂ¥ÃÂÃÂ¥ÃÂ¯ÃÂ¸ÃÂµÃÂ¡ÃÂ¶';
            }
        },
        ordinalParse: /\d{1,2}|\d{1,2}-(ÃÂ«ÃÂ¶|ÃÂÃÂ¤)/,
        ordinal: function (number, period) {
            switch (period) {
            case 'DDD':
            case 'w':
            case 'W':
            case 'DDDo':
                if (number === 1) {
                    return number + '-ÃÂ«ÃÂ¶';
                }
                return number + '-ÃÂÃÂ¤';
            default:
                return number;
            }
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Bahasa Indonesia (id)
    //! author : Mohammad Satrio Utomo : https://github.com/tyok
    //! reference: http://id.wikisource.org/wiki/Pedoman_Umum_Ejaan_Bahasa_Indonesia_yang_Disempurnakan

    var id = moment.defineLocale('id', {
        months : 'Januari_Februari_Maret_April_Mei_Juni_Juli_Agustus_September_Oktober_November_Desember'.split('_'),
        monthsShort : 'Jan_Feb_Mar_Apr_Mei_Jun_Jul_Ags_Sep_Okt_Nov_Des'.split('_'),
        weekdays : 'Minggu_Senin_Selasa_Rabu_Kamis_Jumat_Sabtu'.split('_'),
        weekdaysShort : 'Min_Sen_Sel_Rab_Kam_Jum_Sab'.split('_'),
        weekdaysMin : 'Mg_Sn_Sl_Rb_Km_Jm_Sb'.split('_'),
        longDateFormat : {
            LT : 'HH.mm',
            LTS : 'HH.mm.ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY [pukul] HH.mm',
            LLLL : 'dddd, D MMMM YYYY [pukul] HH.mm'
        },
        meridiemParse: /pagi|siang|sore|malam/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'pagi') {
                return hour;
            } else if (meridiem === 'siang') {
                return hour >= 11 ? hour : hour + 12;
            } else if (meridiem === 'sore' || meridiem === 'malam') {
                return hour + 12;
            }
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours < 11) {
                return 'pagi';
            } else if (hours < 15) {
                return 'siang';
            } else if (hours < 19) {
                return 'sore';
            } else {
                return 'malam';
            }
        },
        calendar : {
            sameDay : '[Hari ini pukul] LT',
            nextDay : '[Besok pukul] LT',
            nextWeek : 'dddd [pukul] LT',
            lastDay : '[Kemarin pukul] LT',
            lastWeek : 'dddd [lalu pukul] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'dalam %s',
            past : '%s yang lalu',
            s : 'beberapa detik',
            m : 'semenit',
            mm : '%d menit',
            h : 'sejam',
            hh : '%d jam',
            d : 'sehari',
            dd : '%d hari',
            M : 'sebulan',
            MM : '%d bulan',
            y : 'setahun',
            yy : '%d tahun'
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : icelandic (is)
    //! author : Hinrik ÃÂrn SigurÃÂ°sson : https://github.com/hinrik

    function is__plural(n) {
        if (n % 100 === 11) {
            return true;
        } else if (n % 10 === 1) {
            return false;
        }
        return true;
    }
    function is__translate(number, withoutSuffix, key, isFuture) {
        var result = number + ' ';
        switch (key) {
        case 's':
            return withoutSuffix || isFuture ? 'nokkrar sekÃÂºndur' : 'nokkrum sekÃÂºndum';
        case 'm':
            return withoutSuffix ? 'mÃÂ­nÃÂºta' : 'mÃÂ­nÃÂºtu';
        case 'mm':
            if (is__plural(number)) {
                return result + (withoutSuffix || isFuture ? 'mÃÂ­nÃÂºtur' : 'mÃÂ­nÃÂºtum');
            } else if (withoutSuffix) {
                return result + 'mÃÂ­nÃÂºta';
            }
            return result + 'mÃÂ­nÃÂºtu';
        case 'hh':
            if (is__plural(number)) {
                return result + (withoutSuffix || isFuture ? 'klukkustundir' : 'klukkustundum');
            }
            return result + 'klukkustund';
        case 'd':
            if (withoutSuffix) {
                return 'dagur';
            }
            return isFuture ? 'dag' : 'degi';
        case 'dd':
            if (is__plural(number)) {
                if (withoutSuffix) {
                    return result + 'dagar';
                }
                return result + (isFuture ? 'daga' : 'dÃÂ¶gum');
            } else if (withoutSuffix) {
                return result + 'dagur';
            }
            return result + (isFuture ? 'dag' : 'degi');
        case 'M':
            if (withoutSuffix) {
                return 'mÃÂ¡nuÃÂ°ur';
            }
            return isFuture ? 'mÃÂ¡nuÃÂ°' : 'mÃÂ¡nuÃÂ°i';
        case 'MM':
            if (is__plural(number)) {
                if (withoutSuffix) {
                    return result + 'mÃÂ¡nuÃÂ°ir';
                }
                return result + (isFuture ? 'mÃÂ¡nuÃÂ°i' : 'mÃÂ¡nuÃÂ°um');
            } else if (withoutSuffix) {
                return result + 'mÃÂ¡nuÃÂ°ur';
            }
            return result + (isFuture ? 'mÃÂ¡nuÃÂ°' : 'mÃÂ¡nuÃÂ°i');
        case 'y':
            return withoutSuffix || isFuture ? 'ÃÂ¡r' : 'ÃÂ¡ri';
        case 'yy':
            if (is__plural(number)) {
                return result + (withoutSuffix || isFuture ? 'ÃÂ¡r' : 'ÃÂ¡rum');
            }
            return result + (withoutSuffix || isFuture ? 'ÃÂ¡r' : 'ÃÂ¡ri');
        }
    }

    var is = moment.defineLocale('is', {
        months : 'janÃÂºar_febrÃÂºar_mars_aprÃÂ­l_maÃÂ­_jÃÂºnÃÂ­_jÃÂºlÃÂ­_ÃÂ¡gÃÂºst_september_oktÃÂ³ber_nÃÂ³vember_desember'.split('_'),
        monthsShort : 'jan_feb_mar_apr_maÃÂ­_jÃÂºn_jÃÂºl_ÃÂ¡gÃÂº_sep_okt_nÃÂ³v_des'.split('_'),
        weekdays : 'sunnudagur_mÃÂ¡nudagur_ÃÂ¾riÃÂ°judagur_miÃÂ°vikudagur_fimmtudagur_fÃÂ¶studagur_laugardagur'.split('_'),
        weekdaysShort : 'sun_mÃÂ¡n_ÃÂ¾ri_miÃÂ°_fim_fÃÂ¶s_lau'.split('_'),
        weekdaysMin : 'Su_MÃÂ¡_ÃÂr_Mi_Fi_FÃÂ¶_La'.split('_'),
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY [kl.] H:mm',
            LLLL : 'dddd, D. MMMM YYYY [kl.] H:mm'
        },
        calendar : {
            sameDay : '[ÃÂ­ dag kl.] LT',
            nextDay : '[ÃÂ¡ morgun kl.] LT',
            nextWeek : 'dddd [kl.] LT',
            lastDay : '[ÃÂ­ gÃÂ¦r kl.] LT',
            lastWeek : '[sÃÂ­ÃÂ°asta] dddd [kl.] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'eftir %s',
            past : 'fyrir %s sÃÂ­ÃÂ°an',
            s : is__translate,
            m : is__translate,
            mm : is__translate,
            h : 'klukkustund',
            hh : is__translate,
            d : is__translate,
            dd : is__translate,
            M : is__translate,
            MM : is__translate,
            y : is__translate,
            yy : is__translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : italian (it)
    //! author : Lorenzo : https://github.com/aliem
    //! author: Mattia Larentis: https://github.com/nostalgiaz

    var it = moment.defineLocale('it', {
        months : 'gennaio_febbraio_marzo_aprile_maggio_giugno_luglio_agosto_settembre_ottobre_novembre_dicembre'.split('_'),
        monthsShort : 'gen_feb_mar_apr_mag_giu_lug_ago_set_ott_nov_dic'.split('_'),
        weekdays : 'Domenica_LunedÃÂ¬_MartedÃÂ¬_MercoledÃÂ¬_GiovedÃÂ¬_VenerdÃÂ¬_Sabato'.split('_'),
        weekdaysShort : 'Dom_Lun_Mar_Mer_Gio_Ven_Sab'.split('_'),
        weekdaysMin : 'Do_Lu_Ma_Me_Gi_Ve_Sa'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[Oggi alle] LT',
            nextDay: '[Domani alle] LT',
            nextWeek: 'dddd [alle] LT',
            lastDay: '[Ieri alle] LT',
            lastWeek: function () {
                switch (this.day()) {
                    case 0:
                        return '[la scorsa] dddd [alle] LT';
                    default:
                        return '[lo scorso] dddd [alle] LT';
                }
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : function (s) {
                return ((/^[0-9].+$/).test(s) ? 'tra' : 'in') + ' ' + s;
            },
            past : '%s fa',
            s : 'alcuni secondi',
            m : 'un minuto',
            mm : '%d minuti',
            h : 'un\'ora',
            hh : '%d ore',
            d : 'un giorno',
            dd : '%d giorni',
            M : 'un mese',
            MM : '%d mesi',
            y : 'un anno',
            yy : '%d anni'
        },
        ordinalParse : /\d{1,2}ÃÂº/,
        ordinal: '%dÃÂº',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : japanese (ja)
    //! author : LI Long : https://github.com/baryon

    var ja = moment.defineLocale('ja', {
        months : '1Ã¦ÂÂ_2Ã¦ÂÂ_3Ã¦ÂÂ_4Ã¦ÂÂ_5Ã¦ÂÂ_6Ã¦ÂÂ_7Ã¦ÂÂ_8Ã¦ÂÂ_9Ã¦ÂÂ_10Ã¦ÂÂ_11Ã¦ÂÂ_12Ã¦ÂÂ'.split('_'),
        monthsShort : '1Ã¦ÂÂ_2Ã¦ÂÂ_3Ã¦ÂÂ_4Ã¦ÂÂ_5Ã¦ÂÂ_6Ã¦ÂÂ_7Ã¦ÂÂ_8Ã¦ÂÂ_9Ã¦ÂÂ_10Ã¦ÂÂ_11Ã¦ÂÂ_12Ã¦ÂÂ'.split('_'),
        weekdays : 'Ã¦ÂÂ¥Ã¦ÂÂÃ¦ÂÂ¥_Ã¦ÂÂÃ¦ÂÂÃ¦ÂÂ¥_Ã§ÂÂ«Ã¦ÂÂÃ¦ÂÂ¥_Ã¦Â°Â´Ã¦ÂÂÃ¦ÂÂ¥_Ã¦ÂÂ¨Ã¦ÂÂÃ¦ÂÂ¥_Ã©ÂÂÃ¦ÂÂÃ¦ÂÂ¥_Ã¥ÂÂÃ¦ÂÂÃ¦ÂÂ¥'.split('_'),
        weekdaysShort : 'Ã¦ÂÂ¥_Ã¦ÂÂ_Ã§ÂÂ«_Ã¦Â°Â´_Ã¦ÂÂ¨_Ã©ÂÂ_Ã¥ÂÂ'.split('_'),
        weekdaysMin : 'Ã¦ÂÂ¥_Ã¦ÂÂ_Ã§ÂÂ«_Ã¦Â°Â´_Ã¦ÂÂ¨_Ã©ÂÂ_Ã¥ÂÂ'.split('_'),
        longDateFormat : {
            LT : 'AhÃ¦ÂÂmÃ¥ÂÂ',
            LTS : 'AhÃ¦ÂÂmÃ¥ÂÂsÃ§Â§Â',
            L : 'YYYY/MM/DD',
            LL : 'YYYYÃ¥Â¹Â´MÃ¦ÂÂDÃ¦ÂÂ¥',
            LLL : 'YYYYÃ¥Â¹Â´MÃ¦ÂÂDÃ¦ÂÂ¥AhÃ¦ÂÂmÃ¥ÂÂ',
            LLLL : 'YYYYÃ¥Â¹Â´MÃ¦ÂÂDÃ¦ÂÂ¥AhÃ¦ÂÂmÃ¥ÂÂ dddd'
        },
        meridiemParse: /Ã¥ÂÂÃ¥ÂÂ|Ã¥ÂÂÃ¥Â¾Â/i,
        isPM : function (input) {
            return input === 'Ã¥ÂÂÃ¥Â¾Â';
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 12) {
                return 'Ã¥ÂÂÃ¥ÂÂ';
            } else {
                return 'Ã¥ÂÂÃ¥Â¾Â';
            }
        },
        calendar : {
            sameDay : '[Ã¤Â»ÂÃ¦ÂÂ¥] LT',
            nextDay : '[Ã¦ÂÂÃ¦ÂÂ¥] LT',
            nextWeek : '[Ã¦ÂÂ¥Ã©ÂÂ±]dddd LT',
            lastDay : '[Ã¦ÂÂ¨Ã¦ÂÂ¥] LT',
            lastWeek : '[Ã¥ÂÂÃ©ÂÂ±]dddd LT',
            sameElse : 'L'
        },
        ordinalParse : /\d{1,2}Ã¦ÂÂ¥/,
        ordinal : function (number, period) {
            switch (period) {
            case 'd':
            case 'D':
            case 'DDD':
                return number + 'Ã¦ÂÂ¥';
            default:
                return number;
            }
        },
        relativeTime : {
            future : '%sÃ¥Â¾Â',
            past : '%sÃ¥ÂÂ',
            s : 'Ã¦ÂÂ°Ã§Â§Â',
            m : '1Ã¥ÂÂ',
            mm : '%dÃ¥ÂÂ',
            h : '1Ã¦ÂÂÃ©ÂÂ',
            hh : '%dÃ¦ÂÂÃ©ÂÂ',
            d : '1Ã¦ÂÂ¥',
            dd : '%dÃ¦ÂÂ¥',
            M : '1Ã£ÂÂ¶Ã¦ÂÂ',
            MM : '%dÃ£ÂÂ¶Ã¦ÂÂ',
            y : '1Ã¥Â¹Â´',
            yy : '%dÃ¥Â¹Â´'
        }
    });

    //! moment.js locale configuration
    //! locale : Boso Jowo (jv)
    //! author : Rony Lantip : https://github.com/lantip
    //! reference: http://jv.wikipedia.org/wiki/Basa_Jawa

    var jv = moment.defineLocale('jv', {
        months : 'Januari_Februari_Maret_April_Mei_Juni_Juli_Agustus_September_Oktober_Nopember_Desember'.split('_'),
        monthsShort : 'Jan_Feb_Mar_Apr_Mei_Jun_Jul_Ags_Sep_Okt_Nop_Des'.split('_'),
        weekdays : 'Minggu_Senen_Seloso_Rebu_Kemis_Jemuwah_Septu'.split('_'),
        weekdaysShort : 'Min_Sen_Sel_Reb_Kem_Jem_Sep'.split('_'),
        weekdaysMin : 'Mg_Sn_Sl_Rb_Km_Jm_Sp'.split('_'),
        longDateFormat : {
            LT : 'HH.mm',
            LTS : 'HH.mm.ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY [pukul] HH.mm',
            LLLL : 'dddd, D MMMM YYYY [pukul] HH.mm'
        },
        meridiemParse: /enjing|siyang|sonten|ndalu/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'enjing') {
                return hour;
            } else if (meridiem === 'siyang') {
                return hour >= 11 ? hour : hour + 12;
            } else if (meridiem === 'sonten' || meridiem === 'ndalu') {
                return hour + 12;
            }
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours < 11) {
                return 'enjing';
            } else if (hours < 15) {
                return 'siyang';
            } else if (hours < 19) {
                return 'sonten';
            } else {
                return 'ndalu';
            }
        },
        calendar : {
            sameDay : '[Dinten puniko pukul] LT',
            nextDay : '[Mbenjang pukul] LT',
            nextWeek : 'dddd [pukul] LT',
            lastDay : '[Kala wingi pukul] LT',
            lastWeek : 'dddd [kepengker pukul] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'wonten ing %s',
            past : '%s ingkang kepengker',
            s : 'sawetawis detik',
            m : 'setunggal menit',
            mm : '%d menit',
            h : 'setunggal jam',
            hh : '%d jam',
            d : 'sedinten',
            dd : '%d dinten',
            M : 'sewulan',
            MM : '%d wulan',
            y : 'setaun',
            yy : '%d taun'
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Georgian (ka)
    //! author : Irakli Janiashvili : https://github.com/irakli-janiashvili

    var ka = moment.defineLocale('ka', {
        months : {
            standalone: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¢Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ¢Ã¡ÂÂ_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂ¥Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ¥Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ'.split('_'),
            format: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¢Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ¢Ã¡ÂÂ¡_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂ¥Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂ¥Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¡'.split('_')
        },
        monthsShort : 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ _Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ _Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂ¥_Ã¡ÂÂÃ¡ÂÂ¥Ã¡ÂÂ¢_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ'.split('_'),
        weekdays : {
            standalone: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ®Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ®Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ'.split('_'),
            format: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ®Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂ®Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡'.split('_'),
            isFormat: /(Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ)/
        },
        weekdaysShort : 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¨_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ®_Ã¡ÂÂ®Ã¡ÂÂ£Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ _Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂ'.split('_'),
        weekdaysMin : 'Ã¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ _Ã¡ÂÂ¡Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ®Ã¡ÂÂ£_Ã¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¨Ã¡ÂÂ'.split('_'),
        longDateFormat : {
            LT : 'h:mm A',
            LTS : 'h:mm:ss A',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY h:mm A',
            LLLL : 'dddd, D MMMM YYYY h:mm A'
        },
        calendar : {
            sameDay : '[Ã¡ÂÂÃ¡ÂÂ¦Ã¡ÂÂÃ¡ÂÂ¡] LT[-Ã¡ÂÂÃ¡ÂÂ]',
            nextDay : '[Ã¡ÂÂ®Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ] LT[-Ã¡ÂÂÃ¡ÂÂ]',
            lastDay : '[Ã¡ÂÂÃ¡ÂÂ£Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂ] LT[-Ã¡ÂÂÃ¡ÂÂ]',
            nextWeek : '[Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ] dddd LT[-Ã¡ÂÂÃ¡ÂÂ]',
            lastWeek : '[Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ] dddd LT-Ã¡ÂÂÃ¡ÂÂ',
            sameElse : 'L'
        },
        relativeTime : {
            future : function (s) {
                return (/(Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¬Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ)/).test(s) ?
                    s.replace(/Ã¡ÂÂ$/, 'Ã¡ÂÂ¨Ã¡ÂÂ') :
                    s + 'Ã¡ÂÂ¨Ã¡ÂÂ';
            },
            past : function (s) {
                if ((/(Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¬Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ|Ã¡ÂÂÃ¡ÂÂ¦Ã¡ÂÂ|Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ)/).test(s)) {
                    return s.replace(/(Ã¡ÂÂ|Ã¡ÂÂ)$/, 'Ã¡ÂÂÃ¡ÂÂ¡ Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂ');
                }
                if ((/Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ/).test(s)) {
                    return s.replace(/Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ$/, 'Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡ Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂ');
                }
            },
            s : 'Ã¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            m : 'Ã¡ÂÂ¬Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ',
            mm : '%d Ã¡ÂÂ¬Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ',
            h : 'Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            hh : '%d Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            d : 'Ã¡ÂÂÃ¡ÂÂ¦Ã¡ÂÂ',
            dd : '%d Ã¡ÂÂÃ¡ÂÂ¦Ã¡ÂÂ',
            M : 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            MM : '%d Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            y : 'Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            yy : '%d Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ'
        },
        ordinalParse: /0|1-Ã¡ÂÂÃ¡ÂÂ|Ã¡ÂÂÃ¡ÂÂ-\d{1,2}|\d{1,2}-Ã¡ÂÂ/,
        ordinal : function (number) {
            if (number === 0) {
                return number;
            }
            if (number === 1) {
                return number + '-Ã¡ÂÂÃ¡ÂÂ';
            }
            if ((number < 20) || (number <= 100 && (number % 20 === 0)) || (number % 100 === 0)) {
                return 'Ã¡ÂÂÃ¡ÂÂ-' + number;
            }
            return number + '-Ã¡ÂÂ';
        },
        week : {
            dow : 1,
            doy : 7
        }
    });

    //! moment.js locale configuration
    //! locale : kazakh (kk)
    //! authors : Nurlan Rakhimzhanov : https://github.com/nurlan

    var kk__suffixes = {
        0: '-ÃÂÃÂ',
        1: '-ÃÂÃÂ',
        2: '-ÃÂÃÂ',
        3: '-ÃÂÃÂ',
        4: '-ÃÂÃÂ',
        5: '-ÃÂÃÂ',
        6: '-ÃÂÃÂ',
        7: '-ÃÂÃÂ',
        8: '-ÃÂÃÂ',
        9: '-ÃÂÃÂ',
        10: '-ÃÂÃÂ',
        20: '-ÃÂÃÂ',
        30: '-ÃÂÃÂ',
        40: '-ÃÂÃÂ',
        50: '-ÃÂÃÂ',
        60: '-ÃÂÃÂ',
        70: '-ÃÂÃÂ',
        80: '-ÃÂÃÂ',
        90: '-ÃÂÃÂ',
        100: '-ÃÂÃÂ'
    };

    var kk = moment.defineLocale('kk', {
        months : 'ÃÂÃÂ°ÃÂ£ÃÂÃÂ°ÃÂ_ÃÂ°ÃÂÃÂ¿ÃÂ°ÃÂ½_ÃÂ½ÃÂ°ÃÂÃÂÃÂÃÂ·_ÃÂÃÂÃÂÃÂÃÂ_ÃÂ¼ÃÂ°ÃÂ¼ÃÂÃÂ_ÃÂ¼ÃÂ°ÃÂÃÂÃÂÃÂ¼_ÃÂÃÂÃÂ»ÃÂ´ÃÂµ_ÃÂÃÂ°ÃÂ¼ÃÂÃÂ·_ÃÂÃÂÃÂÃÂºÃÂ¯ÃÂ¹ÃÂµÃÂº_ÃÂÃÂ°ÃÂ·ÃÂ°ÃÂ½_ÃÂÃÂ°ÃÂÃÂ°ÃÂÃÂ°_ÃÂ¶ÃÂµÃÂ»ÃÂÃÂ¾ÃÂÃÂÃÂ°ÃÂ½'.split('_'),
        monthsShort : 'ÃÂÃÂ°ÃÂ£_ÃÂ°ÃÂÃÂ¿_ÃÂ½ÃÂ°ÃÂ_ÃÂÃÂÃÂ_ÃÂ¼ÃÂ°ÃÂ¼_ÃÂ¼ÃÂ°ÃÂ_ÃÂÃÂÃÂ»_ÃÂÃÂ°ÃÂ¼_ÃÂÃÂÃÂ_ÃÂÃÂ°ÃÂ·_ÃÂÃÂ°ÃÂ_ÃÂ¶ÃÂµÃÂ»'.split('_'),
        weekdays : 'ÃÂ¶ÃÂµÃÂºÃÂÃÂµÃÂ½ÃÂ±ÃÂ_ÃÂ´ÃÂ¯ÃÂ¹ÃÂÃÂµÃÂ½ÃÂ±ÃÂ_ÃÂÃÂµÃÂ¹ÃÂÃÂµÃÂ½ÃÂ±ÃÂ_ÃÂÃÂÃÂÃÂÃÂµÃÂ½ÃÂ±ÃÂ_ÃÂ±ÃÂµÃÂ¹ÃÂÃÂµÃÂ½ÃÂ±ÃÂ_ÃÂ¶ÃÂ±ÃÂ¼ÃÂ°_ÃÂÃÂµÃÂ½ÃÂ±ÃÂ'.split('_'),
        weekdaysShort : 'ÃÂ¶ÃÂµÃÂº_ÃÂ´ÃÂ¯ÃÂ¹_ÃÂÃÂµÃÂ¹_ÃÂÃÂÃÂ_ÃÂ±ÃÂµÃÂ¹_ÃÂ¶ÃÂ±ÃÂ¼_ÃÂÃÂµÃÂ½'.split('_'),
        weekdaysMin : 'ÃÂ¶ÃÂº_ÃÂ´ÃÂ¹_ÃÂÃÂ¹_ÃÂÃÂ_ÃÂ±ÃÂ¹_ÃÂ¶ÃÂ¼_ÃÂÃÂ½'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[ÃÂÃÂ¯ÃÂ³ÃÂÃÂ½ ÃÂÃÂ°ÃÂÃÂ°ÃÂ] LT',
            nextDay : '[ÃÂÃÂÃÂÃÂµÃÂ£ ÃÂÃÂ°ÃÂÃÂ°ÃÂ] LT',
            nextWeek : 'dddd [ÃÂÃÂ°ÃÂÃÂ°ÃÂ] LT',
            lastDay : '[ÃÂÃÂµÃÂÃÂµ ÃÂÃÂ°ÃÂÃÂ°ÃÂ] LT',
            lastWeek : '[ÃÂ¨ÃÂÃÂºÃÂµÃÂ½ ÃÂ°ÃÂ¿ÃÂÃÂ°ÃÂ½ÃÂÃÂ£] dddd [ÃÂÃÂ°ÃÂÃÂ°ÃÂ] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s ÃÂÃÂÃÂÃÂ½ÃÂ´ÃÂµ',
            past : '%s ÃÂ±ÃÂ±ÃÂÃÂÃÂ½',
            s : 'ÃÂ±ÃÂÃÂÃÂ½ÃÂµÃÂÃÂµ ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´',
            m : 'ÃÂ±ÃÂÃÂ ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ',
            mm : '%d ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ',
            h : 'ÃÂ±ÃÂÃÂ ÃÂÃÂ°ÃÂÃÂ°ÃÂ',
            hh : '%d ÃÂÃÂ°ÃÂÃÂ°ÃÂ',
            d : 'ÃÂ±ÃÂÃÂ ÃÂºÃÂ¯ÃÂ½',
            dd : '%d ÃÂºÃÂ¯ÃÂ½',
            M : 'ÃÂ±ÃÂÃÂ ÃÂ°ÃÂ¹',
            MM : '%d ÃÂ°ÃÂ¹',
            y : 'ÃÂ±ÃÂÃÂ ÃÂ¶ÃÂÃÂ»',
            yy : '%d ÃÂ¶ÃÂÃÂ»'
        },
        ordinalParse: /\d{1,2}-(ÃÂÃÂ|ÃÂÃÂ)/,
        ordinal : function (number) {
            var a = number % 10,
                b = number >= 100 ? 100 : null;
            return number + (kk__suffixes[number] || kk__suffixes[a] || kk__suffixes[b]);
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : khmer (km)
    //! author : Kruy Vanna : https://github.com/kruyvanna

    var km = moment.defineLocale('km', {
        months: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ¸Ã¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂ§Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂ¸Ã¡ÂÂ Ã¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¼'.split('_'),
        monthsShort: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ¸Ã¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂ§Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂ¸Ã¡ÂÂ Ã¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂ¶_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¼'.split('_'),
        weekdays: 'Ã¡ÂÂ¢Ã¡ÂÂ¶Ã¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ·Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ'.split('_'),
        weekdaysShort: 'Ã¡ÂÂ¢Ã¡ÂÂ¶Ã¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ·Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ'.split('_'),
        weekdaysMin: 'Ã¡ÂÂ¢Ã¡ÂÂ¶Ã¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ·Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ»Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ'.split('_'),
        longDateFormat: {
            LT: 'HH:mm',
            LTS : 'HH:mm:ss',
            L: 'DD/MM/YYYY',
            LL: 'D MMMM YYYY',
            LLL: 'D MMMM YYYY HH:mm',
            LLLL: 'dddd, D MMMM YYYY HH:mm'
        },
        calendar: {
            sameDay: '[Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ] LT',
            nextDay: '[Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ] LT',
            nextWeek: 'dddd [Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ] LT',
            lastDay: '[Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ·Ã¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ] LT',
            lastWeek: 'dddd [Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶Ã¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ»Ã¡ÂÂ] [Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ] LT',
            sameElse: 'L'
        },
        relativeTime: {
            future: '%sÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            past: '%sÃ¡ÂÂÃ¡ÂÂ»Ã¡ÂÂ',
            s: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ»Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ·Ã¡ÂÂÃ¡ÂÂ¶Ã¡ÂÂÃ¡ÂÂ¸',
            m: 'Ã¡ÂÂÃ¡ÂÂ½Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶Ã¡ÂÂÃ¡ÂÂ¸',
            mm: '%d Ã¡ÂÂÃ¡ÂÂ¶Ã¡ÂÂÃ¡ÂÂ¸',
            h: 'Ã¡ÂÂÃ¡ÂÂ½Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            hh: '%d Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            d: 'Ã¡ÂÂÃ¡ÂÂ½Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            dd: '%d Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            M: 'Ã¡ÂÂÃ¡ÂÂ½Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
            MM: '%d Ã¡ÂÂÃ¡ÂÂ',
            y: 'Ã¡ÂÂÃ¡ÂÂ½Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶Ã¡ÂÂ',
            yy: '%d Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¶Ã¡ÂÂ'
        },
        week: {
            dow: 1, // Monday is the first day of the week.
            doy: 4 // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : korean (ko)
    //!
    //! authors
    //!
    //! - Kyungwook, Park : https://github.com/kyungw00k
    //! - Jeeeyul Lee <jeeeyul@gmail.com>

    var ko = moment.defineLocale('ko', {
        months : '1Ã¬ÂÂ_2Ã¬ÂÂ_3Ã¬ÂÂ_4Ã¬ÂÂ_5Ã¬ÂÂ_6Ã¬ÂÂ_7Ã¬ÂÂ_8Ã¬ÂÂ_9Ã¬ÂÂ_10Ã¬ÂÂ_11Ã¬ÂÂ_12Ã¬ÂÂ'.split('_'),
        monthsShort : '1Ã¬ÂÂ_2Ã¬ÂÂ_3Ã¬ÂÂ_4Ã¬ÂÂ_5Ã¬ÂÂ_6Ã¬ÂÂ_7Ã¬ÂÂ_8Ã¬ÂÂ_9Ã¬ÂÂ_10Ã¬ÂÂ_11Ã¬ÂÂ_12Ã¬ÂÂ'.split('_'),
        weekdays : 'Ã¬ÂÂ¼Ã¬ÂÂÃ¬ÂÂ¼_Ã¬ÂÂÃ¬ÂÂÃ¬ÂÂ¼_Ã­ÂÂÃ¬ÂÂÃ¬ÂÂ¼_Ã¬ÂÂÃ¬ÂÂÃ¬ÂÂ¼_Ã«ÂªÂ©Ã¬ÂÂÃ¬ÂÂ¼_ÃªÂ¸ÂÃ¬ÂÂÃ¬ÂÂ¼_Ã­ÂÂ Ã¬ÂÂÃ¬ÂÂ¼'.split('_'),
        weekdaysShort : 'Ã¬ÂÂ¼_Ã¬ÂÂ_Ã­ÂÂ_Ã¬ÂÂ_Ã«ÂªÂ©_ÃªÂ¸Â_Ã­ÂÂ '.split('_'),
        weekdaysMin : 'Ã¬ÂÂ¼_Ã¬ÂÂ_Ã­ÂÂ_Ã¬ÂÂ_Ã«ÂªÂ©_ÃªÂ¸Â_Ã­ÂÂ '.split('_'),
        longDateFormat : {
            LT : 'A hÃ¬ÂÂ mÃ«Â¶Â',
            LTS : 'A hÃ¬ÂÂ mÃ«Â¶Â sÃ¬Â´Â',
            L : 'YYYY.MM.DD',
            LL : 'YYYYÃ«ÂÂ MMMM DÃ¬ÂÂ¼',
            LLL : 'YYYYÃ«ÂÂ MMMM DÃ¬ÂÂ¼ A hÃ¬ÂÂ mÃ«Â¶Â',
            LLLL : 'YYYYÃ«ÂÂ MMMM DÃ¬ÂÂ¼ dddd A hÃ¬ÂÂ mÃ«Â¶Â'
        },
        calendar : {
            sameDay : 'Ã¬ÂÂ¤Ã«ÂÂ LT',
            nextDay : 'Ã«ÂÂ´Ã¬ÂÂ¼ LT',
            nextWeek : 'dddd LT',
            lastDay : 'Ã¬ÂÂ´Ã¬Â Â LT',
            lastWeek : 'Ã¬Â§ÂÃ«ÂÂÃ¬Â£Â¼ dddd LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s Ã­ÂÂ',
            past : '%s Ã¬Â Â',
            s : 'Ã«ÂªÂ Ã¬Â´Â',
            ss : '%dÃ¬Â´Â',
            m : 'Ã¬ÂÂ¼Ã«Â¶Â',
            mm : '%dÃ«Â¶Â',
            h : 'Ã­ÂÂ Ã¬ÂÂÃªÂ°Â',
            hh : '%dÃ¬ÂÂÃªÂ°Â',
            d : 'Ã­ÂÂÃ«Â£Â¨',
            dd : '%dÃ¬ÂÂ¼',
            M : 'Ã­ÂÂ Ã«ÂÂ¬',
            MM : '%dÃ«ÂÂ¬',
            y : 'Ã¬ÂÂ¼ Ã«ÂÂ',
            yy : '%dÃ«ÂÂ'
        },
        ordinalParse : /\d{1,2}Ã¬ÂÂ¼/,
        ordinal : '%dÃ¬ÂÂ¼',
        meridiemParse : /Ã¬ÂÂ¤Ã¬Â Â|Ã¬ÂÂ¤Ã­ÂÂ/,
        isPM : function (token) {
            return token === 'Ã¬ÂÂ¤Ã­ÂÂ';
        },
        meridiem : function (hour, minute, isUpper) {
            return hour < 12 ? 'Ã¬ÂÂ¤Ã¬Â Â' : 'Ã¬ÂÂ¤Ã­ÂÂ';
        }
    });

    //! moment.js locale configuration
    //! locale : kyrgyz (ky)
    //! author : Chyngyz Arystan uulu : https://github.com/chyngyz


    var ky__suffixes = {
        0: '-ÃÂÃÂ¯',
        1: '-ÃÂÃÂ¸',
        2: '-ÃÂÃÂ¸',
        3: '-ÃÂÃÂ¯',
        4: '-ÃÂÃÂ¯',
        5: '-ÃÂÃÂ¸',
        6: '-ÃÂÃÂ',
        7: '-ÃÂÃÂ¸',
        8: '-ÃÂÃÂ¸',
        9: '-ÃÂÃÂ',
        10: '-ÃÂÃÂ',
        20: '-ÃÂÃÂ',
        30: '-ÃÂÃÂ',
        40: '-ÃÂÃÂ',
        50: '-ÃÂÃÂ¯',
        60: '-ÃÂÃÂ',
        70: '-ÃÂÃÂ¸',
        80: '-ÃÂÃÂ¸',
        90: '-ÃÂÃÂ',
        100: '-ÃÂÃÂ¯'
    };

    var ky = moment.defineLocale('ky', {
        months : 'ÃÂÃÂ½ÃÂ²ÃÂ°ÃÂÃÂ_ÃÂÃÂµÃÂ²ÃÂÃÂ°ÃÂ»ÃÂ_ÃÂ¼ÃÂ°ÃÂÃÂ_ÃÂ°ÃÂ¿ÃÂÃÂµÃÂ»ÃÂ_ÃÂ¼ÃÂ°ÃÂ¹_ÃÂ¸ÃÂÃÂ½ÃÂ_ÃÂ¸ÃÂÃÂ»ÃÂ_ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ½ÃÂÃÂÃÂ±ÃÂÃÂ_ÃÂ¾ÃÂºÃÂÃÂÃÂ±ÃÂÃÂ_ÃÂ½ÃÂ¾ÃÂÃÂ±ÃÂÃÂ_ÃÂ´ÃÂµÃÂºÃÂ°ÃÂ±ÃÂÃÂ'.split('_'),
        monthsShort : 'ÃÂÃÂ½ÃÂ²_ÃÂÃÂµÃÂ²_ÃÂ¼ÃÂ°ÃÂÃÂ_ÃÂ°ÃÂ¿ÃÂ_ÃÂ¼ÃÂ°ÃÂ¹_ÃÂ¸ÃÂÃÂ½ÃÂ_ÃÂ¸ÃÂÃÂ»ÃÂ_ÃÂ°ÃÂ²ÃÂ³_ÃÂÃÂµÃÂ½_ÃÂ¾ÃÂºÃÂ_ÃÂ½ÃÂ¾ÃÂ_ÃÂ´ÃÂµÃÂº'.split('_'),
        weekdays : 'ÃÂÃÂµÃÂºÃÂÃÂµÃÂ¼ÃÂ±ÃÂ¸_ÃÂÃÂ¯ÃÂ¹ÃÂÃÂ©ÃÂ¼ÃÂ±ÃÂ¯_ÃÂ¨ÃÂµÃÂ¹ÃÂÃÂµÃÂ¼ÃÂ±ÃÂ¸_ÃÂ¨ÃÂ°ÃÂÃÂÃÂµÃÂ¼ÃÂ±ÃÂ¸_ÃÂÃÂµÃÂ¹ÃÂÃÂµÃÂ¼ÃÂ±ÃÂ¸_ÃÂÃÂÃÂ¼ÃÂ°_ÃÂÃÂÃÂµÃÂ¼ÃÂ±ÃÂ¸'.split('_'),
        weekdaysShort : 'ÃÂÃÂµÃÂº_ÃÂÃÂ¯ÃÂ¹_ÃÂ¨ÃÂµÃÂ¹_ÃÂ¨ÃÂ°ÃÂ_ÃÂÃÂµÃÂ¹_ÃÂÃÂÃÂ¼_ÃÂÃÂÃÂµ'.split('_'),
        weekdaysMin : 'ÃÂÃÂº_ÃÂÃÂ¹_ÃÂ¨ÃÂ¹_ÃÂ¨ÃÂ_ÃÂÃÂ¹_ÃÂÃÂ¼_ÃÂÃÂ'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[ÃÂÃÂ¯ÃÂ³ÃÂ¯ÃÂ½ ÃÂÃÂ°ÃÂ°ÃÂ] LT',
            nextDay : '[ÃÂ­ÃÂÃÂÃÂµÃÂ£ ÃÂÃÂ°ÃÂ°ÃÂ] LT',
            nextWeek : 'dddd [ÃÂÃÂ°ÃÂ°ÃÂ] LT',
            lastDay : '[ÃÂÃÂµÃÂÃÂµ ÃÂÃÂ°ÃÂ°ÃÂ] LT',
            lastWeek : '[ÃÂ¨ÃÂÃÂºÃÂµÃÂ½ ÃÂ°ÃÂ¿ÃÂÃÂ°ÃÂ½ÃÂÃÂ½] dddd [ÃÂºÃÂ¯ÃÂ½ÃÂ¯] [ÃÂÃÂ°ÃÂ°ÃÂ] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s ÃÂ¸ÃÂÃÂ¸ÃÂ½ÃÂ´ÃÂµ',
            past : '%s ÃÂ¼ÃÂÃÂÃÂÃÂ½',
            s : 'ÃÂ±ÃÂ¸ÃÂÃÂ½ÃÂµÃÂÃÂµ ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´',
            m : 'ÃÂ±ÃÂ¸ÃÂ ÃÂ¼ÃÂ¯ÃÂ½ÃÂ©ÃÂ',
            mm : '%d ÃÂ¼ÃÂ¯ÃÂ½ÃÂ©ÃÂ',
            h : 'ÃÂ±ÃÂ¸ÃÂ ÃÂÃÂ°ÃÂ°ÃÂ',
            hh : '%d ÃÂÃÂ°ÃÂ°ÃÂ',
            d : 'ÃÂ±ÃÂ¸ÃÂ ÃÂºÃÂ¯ÃÂ½',
            dd : '%d ÃÂºÃÂ¯ÃÂ½',
            M : 'ÃÂ±ÃÂ¸ÃÂ ÃÂ°ÃÂ¹',
            MM : '%d ÃÂ°ÃÂ¹',
            y : 'ÃÂ±ÃÂ¸ÃÂ ÃÂ¶ÃÂÃÂ»',
            yy : '%d ÃÂ¶ÃÂÃÂ»'
        },
        ordinalParse: /\d{1,2}-(ÃÂÃÂ¸|ÃÂÃÂ|ÃÂÃÂ¯|ÃÂÃÂ)/,
        ordinal : function (number) {
            var a = number % 10,
                b = number >= 100 ? 100 : null;
            return number + (ky__suffixes[number] || ky__suffixes[a] || ky__suffixes[b]);
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Luxembourgish (lb)
    //! author : mweimerskirch : https://github.com/mweimerskirch, David Raison : https://github.com/kwisatz

    function lb__processRelativeTime(number, withoutSuffix, key, isFuture) {
        var format = {
            'm': ['eng Minutt', 'enger Minutt'],
            'h': ['eng Stonn', 'enger Stonn'],
            'd': ['een Dag', 'engem Dag'],
            'M': ['ee Mount', 'engem Mount'],
            'y': ['ee Joer', 'engem Joer']
        };
        return withoutSuffix ? format[key][0] : format[key][1];
    }
    function processFutureTime(string) {
        var number = string.substr(0, string.indexOf(' '));
        if (eifelerRegelAppliesToNumber(number)) {
            return 'a ' + string;
        }
        return 'an ' + string;
    }
    function processPastTime(string) {
        var number = string.substr(0, string.indexOf(' '));
        if (eifelerRegelAppliesToNumber(number)) {
            return 'viru ' + string;
        }
        return 'virun ' + string;
    }
    /**
     * Returns true if the word before the given number loses the '-n' ending.
     * e.g. 'an 10 Deeg' but 'a 5 Deeg'
     *
     * @param number {integer}
     * @returns {boolean}
     */
    function eifelerRegelAppliesToNumber(number) {
        number = parseInt(number, 10);
        if (isNaN(number)) {
            return false;
        }
        if (number < 0) {
            // Negative Number --> always true
            return true;
        } else if (number < 10) {
            // Only 1 digit
            if (4 <= number && number <= 7) {
                return true;
            }
            return false;
        } else if (number < 100) {
            // 2 digits
            var lastDigit = number % 10, firstDigit = number / 10;
            if (lastDigit === 0) {
                return eifelerRegelAppliesToNumber(firstDigit);
            }
            return eifelerRegelAppliesToNumber(lastDigit);
        } else if (number < 10000) {
            // 3 or 4 digits --> recursively check first digit
            while (number >= 10) {
                number = number / 10;
            }
            return eifelerRegelAppliesToNumber(number);
        } else {
            // Anything larger than 4 digits: recursively check first n-3 digits
            number = number / 1000;
            return eifelerRegelAppliesToNumber(number);
        }
    }

    var lb = moment.defineLocale('lb', {
        months: 'Januar_Februar_MÃÂ¤erz_AbrÃÂ«ll_Mee_Juni_Juli_August_September_Oktober_November_Dezember'.split('_'),
        monthsShort: 'Jan._Febr._Mrz._Abr._Mee_Jun._Jul._Aug._Sept._Okt._Nov._Dez.'.split('_'),
        monthsParseExact : true,
        weekdays: 'Sonndeg_MÃÂ©indeg_DÃÂ«nschdeg_MÃÂ«ttwoch_Donneschdeg_Freideg_Samschdeg'.split('_'),
        weekdaysShort: 'So._MÃÂ©._DÃÂ«._MÃÂ«._Do._Fr._Sa.'.split('_'),
        weekdaysMin: 'So_MÃÂ©_DÃÂ«_MÃÂ«_Do_Fr_Sa'.split('_'),
        weekdaysParseExact : true,
        longDateFormat: {
            LT: 'H:mm [Auer]',
            LTS: 'H:mm:ss [Auer]',
            L: 'DD.MM.YYYY',
            LL: 'D. MMMM YYYY',
            LLL: 'D. MMMM YYYY H:mm [Auer]',
            LLLL: 'dddd, D. MMMM YYYY H:mm [Auer]'
        },
        calendar: {
            sameDay: '[Haut um] LT',
            sameElse: 'L',
            nextDay: '[Muer um] LT',
            nextWeek: 'dddd [um] LT',
            lastDay: '[GÃÂ«schter um] LT',
            lastWeek: function () {
                // Different date string for 'DÃÂ«nschdeg' (Tuesday) and 'Donneschdeg' (Thursday) due to phonological rule
                switch (this.day()) {
                    case 2:
                    case 4:
                        return '[Leschten] dddd [um] LT';
                    default:
                        return '[Leschte] dddd [um] LT';
                }
            }
        },
        relativeTime : {
            future : processFutureTime,
            past : processPastTime,
            s : 'e puer Sekonnen',
            m : lb__processRelativeTime,
            mm : '%d Minutten',
            h : lb__processRelativeTime,
            hh : '%d Stonnen',
            d : lb__processRelativeTime,
            dd : '%d Deeg',
            M : lb__processRelativeTime,
            MM : '%d MÃÂ©int',
            y : lb__processRelativeTime,
            yy : '%d Joer'
        },
        ordinalParse: /\d{1,2}\./,
        ordinal: '%d.',
        week: {
            dow: 1, // Monday is the first day of the week.
            doy: 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : lao (lo)
    //! author : Ryan Hart : https://github.com/ryanhart2

    var lo = moment.defineLocale('lo', {
        months : 'Ã ÂºÂ¡Ã ÂºÂ±Ã ÂºÂÃ ÂºÂÃ ÂºÂ­Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ¸Ã ÂºÂ¡Ã ÂºÂÃ ÂºÂ²_Ã ÂºÂ¡Ã ÂºÂµÃ ÂºÂÃ ÂºÂ²_Ã Â»ÂÃ ÂºÂ¡Ã ÂºÂªÃ ÂºÂ²_Ã ÂºÂÃ ÂºÂ¶Ã ÂºÂÃ ÂºÂªÃ ÂºÂ°Ã ÂºÂÃ ÂºÂ²_Ã ÂºÂ¡Ã ÂºÂ´Ã ÂºÂÃ ÂºÂ¸Ã ÂºÂÃ ÂºÂ²_Ã ÂºÂÃ Â»ÂÃ ÂºÂ¥Ã ÂºÂ°Ã ÂºÂÃ ÂºÂ»Ã ÂºÂ_Ã ÂºÂªÃ ÂºÂ´Ã ÂºÂÃ ÂºÂ«Ã ÂºÂ²_Ã ÂºÂÃ ÂºÂ±Ã ÂºÂÃ ÂºÂÃ ÂºÂ²_Ã ÂºÂÃ ÂºÂ¸Ã ÂºÂ¥Ã ÂºÂ²_Ã ÂºÂÃ ÂºÂ°Ã ÂºÂÃ ÂºÂ´Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ±Ã ÂºÂÃ ÂºÂ§Ã ÂºÂ²'.split('_'),
        monthsShort : 'Ã ÂºÂ¡Ã ÂºÂ±Ã ÂºÂÃ ÂºÂÃ ÂºÂ­Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ¸Ã ÂºÂ¡Ã ÂºÂÃ ÂºÂ²_Ã ÂºÂ¡Ã ÂºÂµÃ ÂºÂÃ ÂºÂ²_Ã Â»ÂÃ ÂºÂ¡Ã ÂºÂªÃ ÂºÂ²_Ã ÂºÂÃ ÂºÂ¶Ã ÂºÂÃ ÂºÂªÃ ÂºÂ°Ã ÂºÂÃ ÂºÂ²_Ã ÂºÂ¡Ã ÂºÂ´Ã ÂºÂÃ ÂºÂ¸Ã ÂºÂÃ ÂºÂ²_Ã ÂºÂÃ Â»ÂÃ ÂºÂ¥Ã ÂºÂ°Ã ÂºÂÃ ÂºÂ»Ã ÂºÂ_Ã ÂºÂªÃ ÂºÂ´Ã ÂºÂÃ ÂºÂ«Ã ÂºÂ²_Ã ÂºÂÃ ÂºÂ±Ã ÂºÂÃ ÂºÂÃ ÂºÂ²_Ã ÂºÂÃ ÂºÂ¸Ã ÂºÂ¥Ã ÂºÂ²_Ã ÂºÂÃ ÂºÂ°Ã ÂºÂÃ ÂºÂ´Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ±Ã ÂºÂÃ ÂºÂ§Ã ÂºÂ²'.split('_'),
        weekdays : 'Ã ÂºÂ­Ã ÂºÂ²Ã ÂºÂÃ ÂºÂ´Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ±Ã ÂºÂ_Ã ÂºÂ­Ã ÂºÂ±Ã ÂºÂÃ ÂºÂÃ ÂºÂ²Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ¸Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ°Ã ÂºÂ«Ã ÂºÂ±Ã ÂºÂ_Ã ÂºÂªÃ ÂºÂ¸Ã ÂºÂ_Ã Â»ÂÃ ÂºÂªÃ ÂºÂ»Ã ÂºÂ²'.split('_'),
        weekdaysShort : 'Ã ÂºÂÃ ÂºÂ´Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ±Ã ÂºÂ_Ã ÂºÂ­Ã ÂºÂ±Ã ÂºÂÃ ÂºÂÃ ÂºÂ²Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ¸Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ°Ã ÂºÂ«Ã ÂºÂ±Ã ÂºÂ_Ã ÂºÂªÃ ÂºÂ¸Ã ÂºÂ_Ã Â»ÂÃ ÂºÂªÃ ÂºÂ»Ã ÂºÂ²'.split('_'),
        weekdaysMin : 'Ã ÂºÂ_Ã ÂºÂ_Ã ÂºÂ­Ã ÂºÂ_Ã ÂºÂ_Ã ÂºÂÃ ÂºÂ«_Ã ÂºÂªÃ ÂºÂ_Ã ÂºÂª'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'Ã ÂºÂ§Ã ÂºÂ±Ã ÂºÂdddd D MMMM YYYY HH:mm'
        },
        meridiemParse: /Ã ÂºÂÃ ÂºÂ­Ã ÂºÂÃ Â»ÂÃ ÂºÂÃ ÂºÂ»Ã Â»ÂÃ ÂºÂ²|Ã ÂºÂÃ ÂºÂ­Ã ÂºÂÃ Â»ÂÃ ÂºÂ¥Ã ÂºÂ/,
        isPM: function (input) {
            return input === 'Ã ÂºÂÃ ÂºÂ­Ã ÂºÂÃ Â»ÂÃ ÂºÂ¥Ã ÂºÂ';
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 12) {
                return 'Ã ÂºÂÃ ÂºÂ­Ã ÂºÂÃ Â»ÂÃ ÂºÂÃ ÂºÂ»Ã Â»ÂÃ ÂºÂ²';
            } else {
                return 'Ã ÂºÂÃ ÂºÂ­Ã ÂºÂÃ Â»ÂÃ ÂºÂ¥Ã ÂºÂ';
            }
        },
        calendar : {
            sameDay : '[Ã ÂºÂ¡Ã ÂºÂ·Ã Â»ÂÃ ÂºÂÃ ÂºÂµÃ Â»ÂÃ Â»ÂÃ ÂºÂ§Ã ÂºÂ¥Ã ÂºÂ²] LT',
            nextDay : '[Ã ÂºÂ¡Ã ÂºÂ·Ã Â»ÂÃ ÂºÂ­Ã ÂºÂ·Ã Â»ÂÃ ÂºÂÃ Â»ÂÃ ÂºÂ§Ã ÂºÂ¥Ã ÂºÂ²] LT',
            nextWeek : '[Ã ÂºÂ§Ã ÂºÂ±Ã ÂºÂ]dddd[Ã Â»ÂÃ Â»ÂÃ ÂºÂ²Ã Â»ÂÃ ÂºÂ§Ã ÂºÂ¥Ã ÂºÂ²] LT',
            lastDay : '[Ã ÂºÂ¡Ã ÂºÂ·Ã Â»ÂÃ ÂºÂ§Ã ÂºÂ²Ã ÂºÂÃ ÂºÂÃ ÂºÂµÃ Â»ÂÃ Â»ÂÃ ÂºÂ§Ã ÂºÂ¥Ã ÂºÂ²] LT',
            lastWeek : '[Ã ÂºÂ§Ã ÂºÂ±Ã ÂºÂ]dddd[Ã Â»ÂÃ ÂºÂ¥Ã Â»ÂÃ ÂºÂ§Ã ÂºÂÃ ÂºÂµÃ Â»ÂÃ Â»ÂÃ ÂºÂ§Ã ÂºÂ¥Ã ÂºÂ²] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'Ã ÂºÂ­Ã ÂºÂµÃ ÂºÂ %s',
            past : '%sÃ ÂºÂÃ Â»ÂÃ ÂºÂ²Ã ÂºÂÃ ÂºÂ¡Ã ÂºÂ²',
            s : 'Ã ÂºÂÃ Â»ÂÃ Â»ÂÃ Â»ÂÃ ÂºÂÃ ÂºÂ»Ã Â»ÂÃ ÂºÂ²Ã Â»ÂÃ ÂºÂÃ ÂºÂ§Ã ÂºÂ´Ã ÂºÂÃ ÂºÂ²Ã ÂºÂÃ ÂºÂµ',
            m : '1 Ã ÂºÂÃ ÂºÂ²Ã ÂºÂÃ ÂºÂµ',
            mm : '%d Ã ÂºÂÃ ÂºÂ²Ã ÂºÂÃ ÂºÂµ',
            h : '1 Ã ÂºÂÃ ÂºÂ»Ã Â»ÂÃ ÂºÂ§Ã Â»ÂÃ ÂºÂ¡Ã ÂºÂ',
            hh : '%d Ã ÂºÂÃ ÂºÂ»Ã Â»ÂÃ ÂºÂ§Ã Â»ÂÃ ÂºÂ¡Ã ÂºÂ',
            d : '1 Ã ÂºÂ¡Ã ÂºÂ·Ã Â»Â',
            dd : '%d Ã ÂºÂ¡Ã ÂºÂ·Ã Â»Â',
            M : '1 Ã Â»ÂÃ ÂºÂÃ ÂºÂ·Ã ÂºÂ­Ã ÂºÂ',
            MM : '%d Ã Â»ÂÃ ÂºÂÃ ÂºÂ·Ã ÂºÂ­Ã ÂºÂ',
            y : '1 Ã ÂºÂÃ ÂºÂµ',
            yy : '%d Ã ÂºÂÃ ÂºÂµ'
        },
        ordinalParse: /(Ã ÂºÂÃ ÂºÂµÃ Â»Â)\d{1,2}/,
        ordinal : function (number) {
            return 'Ã ÂºÂÃ ÂºÂµÃ Â»Â' + number;
        }
    });

    //! moment.js locale configuration
    //! locale : Lithuanian (lt)
    //! author : Mindaugas MozÃÂ«ras : https://github.com/mmozuras

    var lt__units = {
        'm' : 'minutÃÂ_minutÃÂs_minutÃÂ',
        'mm': 'minutÃÂs_minuÃÂiÃÂ³_minutes',
        'h' : 'valanda_valandos_valandÃÂ',
        'hh': 'valandos_valandÃÂ³_valandas',
        'd' : 'diena_dienos_dienÃÂ',
        'dd': 'dienos_dienÃÂ³_dienas',
        'M' : 'mÃÂnuo_mÃÂnesio_mÃÂnesÃÂ¯',
        'MM': 'mÃÂnesiai_mÃÂnesiÃÂ³_mÃÂnesius',
        'y' : 'metai_metÃÂ³_metus',
        'yy': 'metai_metÃÂ³_metus'
    };
    function translateSeconds(number, withoutSuffix, key, isFuture) {
        if (withoutSuffix) {
            return 'kelios sekundÃÂs';
        } else {
            return isFuture ? 'keliÃÂ³ sekundÃÂ¾iÃÂ³' : 'kelias sekundes';
        }
    }
    function translateSingular(number, withoutSuffix, key, isFuture) {
        return withoutSuffix ? forms(key)[0] : (isFuture ? forms(key)[1] : forms(key)[2]);
    }
    function special(number) {
        return number % 10 === 0 || (number > 10 && number < 20);
    }
    function forms(key) {
        return lt__units[key].split('_');
    }
    function lt__translate(number, withoutSuffix, key, isFuture) {
        var result = number + ' ';
        if (number === 1) {
            return result + translateSingular(number, withoutSuffix, key[0], isFuture);
        } else if (withoutSuffix) {
            return result + (special(number) ? forms(key)[1] : forms(key)[0]);
        } else {
            if (isFuture) {
                return result + forms(key)[1];
            } else {
                return result + (special(number) ? forms(key)[1] : forms(key)[2]);
            }
        }
    }
    var lt = moment.defineLocale('lt', {
        months : {
            format: 'sausio_vasario_kovo_balandÃÂ¾io_geguÃÂ¾ÃÂs_birÃÂ¾elio_liepos_rugpjÃÂ«ÃÂio_rugsÃÂjo_spalio_lapkriÃÂio_gruodÃÂ¾io'.split('_'),
            standalone: 'sausis_vasaris_kovas_balandis_geguÃÂ¾ÃÂ_birÃÂ¾elis_liepa_rugpjÃÂ«tis_rugsÃÂjis_spalis_lapkritis_gruodis'.split('_')
        },
        monthsShort : 'sau_vas_kov_bal_geg_bir_lie_rgp_rgs_spa_lap_grd'.split('_'),
        weekdays : {
            format: 'sekmadienÃÂ¯_pirmadienÃÂ¯_antradienÃÂ¯_treÃÂiadienÃÂ¯_ketvirtadienÃÂ¯_penktadienÃÂ¯_ÃÂ¡eÃÂ¡tadienÃÂ¯'.split('_'),
            standalone: 'sekmadienis_pirmadienis_antradienis_treÃÂiadienis_ketvirtadienis_penktadienis_ÃÂ¡eÃÂ¡tadienis'.split('_'),
            isFormat: /dddd HH:mm/
        },
        weekdaysShort : 'Sek_Pir_Ant_Tre_Ket_Pen_ÃÂ eÃÂ¡'.split('_'),
        weekdaysMin : 'S_P_A_T_K_Pn_ÃÂ '.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'YYYY-MM-DD',
            LL : 'YYYY [m.] MMMM D [d.]',
            LLL : 'YYYY [m.] MMMM D [d.], HH:mm [val.]',
            LLLL : 'YYYY [m.] MMMM D [d.], dddd, HH:mm [val.]',
            l : 'YYYY-MM-DD',
            ll : 'YYYY [m.] MMMM D [d.]',
            lll : 'YYYY [m.] MMMM D [d.], HH:mm [val.]',
            llll : 'YYYY [m.] MMMM D [d.], ddd, HH:mm [val.]'
        },
        calendar : {
            sameDay : '[ÃÂ iandien] LT',
            nextDay : '[Rytoj] LT',
            nextWeek : 'dddd LT',
            lastDay : '[Vakar] LT',
            lastWeek : '[PraÃÂjusÃÂ¯] dddd LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'po %s',
            past : 'prieÃÂ¡ %s',
            s : translateSeconds,
            m : translateSingular,
            mm : lt__translate,
            h : translateSingular,
            hh : lt__translate,
            d : translateSingular,
            dd : lt__translate,
            M : translateSingular,
            MM : lt__translate,
            y : translateSingular,
            yy : lt__translate
        },
        ordinalParse: /\d{1,2}-oji/,
        ordinal : function (number) {
            return number + '-oji';
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : latvian (lv)
    //! author : Kristaps Karlsons : https://github.com/skakri
    //! author : JÃÂnis Elmeris : https://github.com/JanisE

    var lv__units = {
        'm': 'minÃÂ«tes_minÃÂ«tÃÂm_minÃÂ«te_minÃÂ«tes'.split('_'),
        'mm': 'minÃÂ«tes_minÃÂ«tÃÂm_minÃÂ«te_minÃÂ«tes'.split('_'),
        'h': 'stundas_stundÃÂm_stunda_stundas'.split('_'),
        'hh': 'stundas_stundÃÂm_stunda_stundas'.split('_'),
        'd': 'dienas_dienÃÂm_diena_dienas'.split('_'),
        'dd': 'dienas_dienÃÂm_diena_dienas'.split('_'),
        'M': 'mÃÂneÃÂ¡a_mÃÂneÃÂ¡iem_mÃÂnesis_mÃÂneÃÂ¡i'.split('_'),
        'MM': 'mÃÂneÃÂ¡a_mÃÂneÃÂ¡iem_mÃÂnesis_mÃÂneÃÂ¡i'.split('_'),
        'y': 'gada_gadiem_gads_gadi'.split('_'),
        'yy': 'gada_gadiem_gads_gadi'.split('_')
    };
    /**
     * @param withoutSuffix boolean true = a length of time; false = before/after a period of time.
     */
    function format(forms, number, withoutSuffix) {
        if (withoutSuffix) {
            // E.g. "21 minÃÂ«te", "3 minÃÂ«tes".
            return number % 10 === 1 && number !== 11 ? forms[2] : forms[3];
        } else {
            // E.g. "21 minÃÂ«tes" as in "pÃÂc 21 minÃÂ«tes".
            // E.g. "3 minÃÂ«tÃÂm" as in "pÃÂc 3 minÃÂ«tÃÂm".
            return number % 10 === 1 && number !== 11 ? forms[0] : forms[1];
        }
    }
    function lv__relativeTimeWithPlural(number, withoutSuffix, key) {
        return number + ' ' + format(lv__units[key], number, withoutSuffix);
    }
    function relativeTimeWithSingular(number, withoutSuffix, key) {
        return format(lv__units[key], number, withoutSuffix);
    }
    function relativeSeconds(number, withoutSuffix) {
        return withoutSuffix ? 'daÃÂ¾as sekundes' : 'daÃÂ¾ÃÂm sekundÃÂm';
    }

    var lv = moment.defineLocale('lv', {
        months : 'janvÃÂris_februÃÂris_marts_aprÃÂ«lis_maijs_jÃÂ«nijs_jÃÂ«lijs_augusts_septembris_oktobris_novembris_decembris'.split('_'),
        monthsShort : 'jan_feb_mar_apr_mai_jÃÂ«n_jÃÂ«l_aug_sep_okt_nov_dec'.split('_'),
        weekdays : 'svÃÂtdiena_pirmdiena_otrdiena_treÃÂ¡diena_ceturtdiena_piektdiena_sestdiena'.split('_'),
        weekdaysShort : 'Sv_P_O_T_C_Pk_S'.split('_'),
        weekdaysMin : 'Sv_P_O_T_C_Pk_S'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY.',
            LL : 'YYYY. [gada] D. MMMM',
            LLL : 'YYYY. [gada] D. MMMM, HH:mm',
            LLLL : 'YYYY. [gada] D. MMMM, dddd, HH:mm'
        },
        calendar : {
            sameDay : '[ÃÂ odien pulksten] LT',
            nextDay : '[RÃÂ«t pulksten] LT',
            nextWeek : 'dddd [pulksten] LT',
            lastDay : '[Vakar pulksten] LT',
            lastWeek : '[PagÃÂjuÃÂ¡ÃÂ] dddd [pulksten] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'pÃÂc %s',
            past : 'pirms %s',
            s : relativeSeconds,
            m : relativeTimeWithSingular,
            mm : lv__relativeTimeWithPlural,
            h : relativeTimeWithSingular,
            hh : lv__relativeTimeWithPlural,
            d : relativeTimeWithSingular,
            dd : lv__relativeTimeWithPlural,
            M : relativeTimeWithSingular,
            MM : lv__relativeTimeWithPlural,
            y : relativeTimeWithSingular,
            yy : lv__relativeTimeWithPlural
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Montenegrin (me)
    //! author : Miodrag NikaÃÂ <miodrag@restartit.me> : https://github.com/miodragnikac

    var me__translator = {
        words: { //Different grammatical cases
            m: ['jedan minut', 'jednog minuta'],
            mm: ['minut', 'minuta', 'minuta'],
            h: ['jedan sat', 'jednog sata'],
            hh: ['sat', 'sata', 'sati'],
            dd: ['dan', 'dana', 'dana'],
            MM: ['mjesec', 'mjeseca', 'mjeseci'],
            yy: ['godina', 'godine', 'godina']
        },
        correctGrammaticalCase: function (number, wordKey) {
            return number === 1 ? wordKey[0] : (number >= 2 && number <= 4 ? wordKey[1] : wordKey[2]);
        },
        translate: function (number, withoutSuffix, key) {
            var wordKey = me__translator.words[key];
            if (key.length === 1) {
                return withoutSuffix ? wordKey[0] : wordKey[1];
            } else {
                return number + ' ' + me__translator.correctGrammaticalCase(number, wordKey);
            }
        }
    };

    var me = moment.defineLocale('me', {
        months: 'januar_februar_mart_april_maj_jun_jul_avgust_septembar_oktobar_novembar_decembar'.split('_'),
        monthsShort: 'jan._feb._mar._apr._maj_jun_jul_avg._sep._okt._nov._dec.'.split('_'),
        monthsParseExact : true,
        weekdays: 'nedjelja_ponedjeljak_utorak_srijeda_ÃÂetvrtak_petak_subota'.split('_'),
        weekdaysShort: 'ned._pon._uto._sri._ÃÂet._pet._sub.'.split('_'),
        weekdaysMin: 'ne_po_ut_sr_ÃÂe_pe_su'.split('_'),
        weekdaysParseExact : true,
        longDateFormat: {
            LT: 'H:mm',
            LTS : 'H:mm:ss',
            L: 'DD. MM. YYYY',
            LL: 'D. MMMM YYYY',
            LLL: 'D. MMMM YYYY H:mm',
            LLLL: 'dddd, D. MMMM YYYY H:mm'
        },
        calendar: {
            sameDay: '[danas u] LT',
            nextDay: '[sjutra u] LT',

            nextWeek: function () {
                switch (this.day()) {
                case 0:
                    return '[u] [nedjelju] [u] LT';
                case 3:
                    return '[u] [srijedu] [u] LT';
                case 6:
                    return '[u] [subotu] [u] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[u] dddd [u] LT';
                }
            },
            lastDay  : '[juÃÂe u] LT',
            lastWeek : function () {
                var lastWeekDays = [
                    '[proÃÂ¡le] [nedjelje] [u] LT',
                    '[proÃÂ¡log] [ponedjeljka] [u] LT',
                    '[proÃÂ¡log] [utorka] [u] LT',
                    '[proÃÂ¡le] [srijede] [u] LT',
                    '[proÃÂ¡log] [ÃÂetvrtka] [u] LT',
                    '[proÃÂ¡log] [petka] [u] LT',
                    '[proÃÂ¡le] [subote] [u] LT'
                ];
                return lastWeekDays[this.day()];
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : 'za %s',
            past   : 'prije %s',
            s      : 'nekoliko sekundi',
            m      : me__translator.translate,
            mm     : me__translator.translate,
            h      : me__translator.translate,
            hh     : me__translator.translate,
            d      : 'dan',
            dd     : me__translator.translate,
            M      : 'mjesec',
            MM     : me__translator.translate,
            y      : 'godinu',
            yy     : me__translator.translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : macedonian (mk)
    //! author : Borislav Mickov : https://github.com/B0k0

    var mk = moment.defineLocale('mk', {
        months : 'ÃÂÃÂ°ÃÂ½ÃÂÃÂ°ÃÂÃÂ¸_ÃÂÃÂµÃÂ²ÃÂÃÂÃÂ°ÃÂÃÂ¸_ÃÂ¼ÃÂ°ÃÂÃÂ_ÃÂ°ÃÂ¿ÃÂÃÂ¸ÃÂ»_ÃÂ¼ÃÂ°ÃÂ_ÃÂÃÂÃÂ½ÃÂ¸_ÃÂÃÂÃÂ»ÃÂ¸_ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ¿ÃÂÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¸_ÃÂ¾ÃÂºÃÂÃÂ¾ÃÂ¼ÃÂ²ÃÂÃÂ¸_ÃÂ½ÃÂ¾ÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¸_ÃÂ´ÃÂµÃÂºÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¸'.split('_'),
        monthsShort : 'ÃÂÃÂ°ÃÂ½_ÃÂÃÂµÃÂ²_ÃÂ¼ÃÂ°ÃÂ_ÃÂ°ÃÂ¿ÃÂ_ÃÂ¼ÃÂ°ÃÂ_ÃÂÃÂÃÂ½_ÃÂÃÂÃÂ»_ÃÂ°ÃÂ²ÃÂ³_ÃÂÃÂµÃÂ¿_ÃÂ¾ÃÂºÃÂ_ÃÂ½ÃÂ¾ÃÂµ_ÃÂ´ÃÂµÃÂº'.split('_'),
        weekdays : 'ÃÂ½ÃÂµÃÂ´ÃÂµÃÂ»ÃÂ°_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂµÃÂ»ÃÂ½ÃÂ¸ÃÂº_ÃÂ²ÃÂÃÂ¾ÃÂÃÂ½ÃÂ¸ÃÂº_ÃÂÃÂÃÂµÃÂ´ÃÂ°_ÃÂÃÂµÃÂÃÂ²ÃÂÃÂÃÂ¾ÃÂº_ÃÂ¿ÃÂµÃÂÃÂ¾ÃÂº_ÃÂÃÂ°ÃÂ±ÃÂ¾ÃÂÃÂ°'.split('_'),
        weekdaysShort : 'ÃÂ½ÃÂµÃÂ´_ÃÂ¿ÃÂ¾ÃÂ½_ÃÂ²ÃÂÃÂ¾_ÃÂÃÂÃÂµ_ÃÂÃÂµÃÂ_ÃÂ¿ÃÂµÃÂ_ÃÂÃÂ°ÃÂ±'.split('_'),
        weekdaysMin : 'ÃÂ½e_ÃÂ¿o_ÃÂ²ÃÂ_ÃÂÃÂ_ÃÂÃÂµ_ÃÂ¿ÃÂµ_ÃÂa'.split('_'),
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'D.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY H:mm',
            LLLL : 'dddd, D MMMM YYYY H:mm'
        },
        calendar : {
            sameDay : '[ÃÂÃÂµÃÂ½ÃÂµÃÂ ÃÂ²ÃÂ¾] LT',
            nextDay : '[ÃÂ£ÃÂÃÂÃÂµ ÃÂ²ÃÂ¾] LT',
            nextWeek : '[ÃÂÃÂ¾] dddd [ÃÂ²ÃÂ¾] LT',
            lastDay : '[ÃÂÃÂÃÂµÃÂÃÂ° ÃÂ²ÃÂ¾] LT',
            lastWeek : function () {
                switch (this.day()) {
                case 0:
                case 3:
                case 6:
                    return '[ÃÂÃÂ·ÃÂ¼ÃÂ¸ÃÂ½ÃÂ°ÃÂÃÂ°ÃÂÃÂ°] dddd [ÃÂ²ÃÂ¾] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[ÃÂÃÂ·ÃÂ¼ÃÂ¸ÃÂ½ÃÂ°ÃÂÃÂ¸ÃÂ¾ÃÂ] dddd [ÃÂ²ÃÂ¾] LT';
                }
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂ¿ÃÂ¾ÃÂÃÂ»ÃÂµ %s',
            past : 'ÃÂ¿ÃÂÃÂµÃÂ´ %s',
            s : 'ÃÂ½ÃÂµÃÂºÃÂ¾ÃÂ»ÃÂºÃÂ ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´ÃÂ¸',
            m : 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ°',
            mm : '%d ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ¸',
            h : 'ÃÂÃÂ°ÃÂ',
            hh : '%d ÃÂÃÂ°ÃÂÃÂ°',
            d : 'ÃÂ´ÃÂµÃÂ½',
            dd : '%d ÃÂ´ÃÂµÃÂ½ÃÂ°',
            M : 'ÃÂ¼ÃÂµÃÂÃÂµÃÂ',
            MM : '%d ÃÂ¼ÃÂµÃÂÃÂµÃÂÃÂ¸',
            y : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°',
            yy : '%d ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ¸'
        },
        ordinalParse: /\d{1,2}-(ÃÂµÃÂ²|ÃÂµÃÂ½|ÃÂÃÂ¸|ÃÂ²ÃÂ¸|ÃÂÃÂ¸|ÃÂ¼ÃÂ¸)/,
        ordinal : function (number) {
            var lastDigit = number % 10,
                last2Digits = number % 100;
            if (number === 0) {
                return number + '-ÃÂµÃÂ²';
            } else if (last2Digits === 0) {
                return number + '-ÃÂµÃÂ½';
            } else if (last2Digits > 10 && last2Digits < 20) {
                return number + '-ÃÂÃÂ¸';
            } else if (lastDigit === 1) {
                return number + '-ÃÂ²ÃÂ¸';
            } else if (lastDigit === 2) {
                return number + '-ÃÂÃÂ¸';
            } else if (lastDigit === 7 || lastDigit === 8) {
                return number + '-ÃÂ¼ÃÂ¸';
            } else {
                return number + '-ÃÂÃÂ¸';
            }
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : malayalam (ml)
    //! author : Floyd Pink : https://github.com/floydpink

    var ml = moment.defineLocale('ml', {
        months : 'Ã Â´ÂÃ Â´Â¨Ã ÂµÂÃ Â´ÂµÃ Â´Â°Ã Â´Â¿_Ã Â´Â«Ã ÂµÂÃ Â´Â¬Ã ÂµÂÃ Â´Â°Ã ÂµÂÃ Â´ÂµÃ Â´Â°Ã Â´Â¿_Ã Â´Â®Ã Â´Â¾Ã ÂµÂ¼Ã Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂ_Ã Â´ÂÃ Â´ÂªÃ ÂµÂÃ Â´Â°Ã Â´Â¿Ã ÂµÂ½_Ã Â´Â®Ã ÂµÂÃ Â´Â¯Ã ÂµÂ_Ã Â´ÂÃ ÂµÂÃ ÂµÂº_Ã Â´ÂÃ ÂµÂÃ Â´Â²Ã ÂµÂ_Ã Â´ÂÃ Â´ÂÃ Â´Â¸Ã ÂµÂÃ Â´Â±Ã ÂµÂÃ Â´Â±Ã ÂµÂ_Ã Â´Â¸Ã ÂµÂÃ Â´ÂªÃ ÂµÂÃ Â´Â±Ã ÂµÂÃ Â´Â±Ã Â´ÂÃ Â´Â¬Ã ÂµÂ¼_Ã Â´ÂÃ Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂÃ Â´Â¬Ã ÂµÂ¼_Ã Â´Â¨Ã Â´ÂµÃ Â´ÂÃ Â´Â¬Ã ÂµÂ¼_Ã Â´Â¡Ã Â´Â¿Ã Â´Â¸Ã Â´ÂÃ Â´Â¬Ã ÂµÂ¼'.split('_'),
        monthsShort : 'Ã Â´ÂÃ Â´Â¨Ã ÂµÂ._Ã Â´Â«Ã ÂµÂÃ Â´Â¬Ã ÂµÂÃ Â´Â°Ã ÂµÂ._Ã Â´Â®Ã Â´Â¾Ã ÂµÂ¼._Ã Â´ÂÃ Â´ÂªÃ ÂµÂÃ Â´Â°Ã Â´Â¿._Ã Â´Â®Ã ÂµÂÃ Â´Â¯Ã ÂµÂ_Ã Â´ÂÃ ÂµÂÃ ÂµÂº_Ã Â´ÂÃ ÂµÂÃ Â´Â²Ã ÂµÂ._Ã Â´ÂÃ Â´Â._Ã Â´Â¸Ã ÂµÂÃ Â´ÂªÃ ÂµÂÃ Â´Â±Ã ÂµÂÃ Â´Â±._Ã Â´ÂÃ Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂ._Ã Â´Â¨Ã Â´ÂµÃ Â´Â._Ã Â´Â¡Ã Â´Â¿Ã Â´Â¸Ã Â´Â.'.split('_'),
        monthsParseExact : true,
        weekdays : 'Ã Â´ÂÃ Â´Â¾Ã Â´Â¯Ã Â´Â±Ã Â´Â¾Ã Â´Â´Ã ÂµÂÃ Â´Â_Ã Â´Â¤Ã Â´Â¿Ã Â´ÂÃ ÂµÂÃ Â´ÂÃ Â´Â³Ã Â´Â¾Ã Â´Â´Ã ÂµÂÃ Â´Â_Ã Â´ÂÃ ÂµÂÃ Â´ÂµÃ ÂµÂÃ Â´ÂµÃ Â´Â¾Ã Â´Â´Ã ÂµÂÃ Â´Â_Ã Â´Â¬Ã ÂµÂÃ Â´Â§Ã Â´Â¨Ã Â´Â¾Ã Â´Â´Ã ÂµÂÃ Â´Â_Ã Â´ÂµÃ ÂµÂÃ Â´Â¯Ã Â´Â¾Ã Â´Â´Ã Â´Â¾Ã Â´Â´Ã ÂµÂÃ Â´Â_Ã Â´ÂµÃ ÂµÂÃ Â´Â³Ã ÂµÂÃ Â´Â³Ã Â´Â¿Ã Â´Â¯Ã Â´Â¾Ã Â´Â´Ã ÂµÂÃ Â´Â_Ã Â´Â¶Ã Â´Â¨Ã Â´Â¿Ã Â´Â¯Ã Â´Â¾Ã Â´Â´Ã ÂµÂÃ Â´Â'.split('_'),
        weekdaysShort : 'Ã Â´ÂÃ Â´Â¾Ã Â´Â¯Ã ÂµÂ¼_Ã Â´Â¤Ã Â´Â¿Ã Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂ¾_Ã Â´ÂÃ ÂµÂÃ Â´ÂµÃ ÂµÂÃ Â´Âµ_Ã Â´Â¬Ã ÂµÂÃ Â´Â§Ã ÂµÂ»_Ã Â´ÂµÃ ÂµÂÃ Â´Â¯Ã Â´Â¾Ã Â´Â´Ã Â´Â_Ã Â´ÂµÃ ÂµÂÃ Â´Â³Ã ÂµÂÃ Â´Â³Ã Â´Â¿_Ã Â´Â¶Ã Â´Â¨Ã Â´Â¿'.split('_'),
        weekdaysMin : 'Ã Â´ÂÃ Â´Â¾_Ã Â´Â¤Ã Â´Â¿_Ã Â´ÂÃ ÂµÂ_Ã Â´Â¬Ã ÂµÂ_Ã Â´ÂµÃ ÂµÂÃ Â´Â¯Ã Â´Â¾_Ã Â´ÂµÃ ÂµÂ_Ã Â´Â¶'.split('_'),
        longDateFormat : {
            LT : 'A h:mm -Ã Â´Â¨Ã ÂµÂ',
            LTS : 'A h:mm:ss -Ã Â´Â¨Ã ÂµÂ',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY, A h:mm -Ã Â´Â¨Ã ÂµÂ',
            LLLL : 'dddd, D MMMM YYYY, A h:mm -Ã Â´Â¨Ã ÂµÂ'
        },
        calendar : {
            sameDay : '[Ã Â´ÂÃ Â´Â¨Ã ÂµÂÃ Â´Â¨Ã ÂµÂ] LT',
            nextDay : '[Ã Â´Â¨Ã Â´Â¾Ã Â´Â³Ã ÂµÂ] LT',
            nextWeek : 'dddd, LT',
            lastDay : '[Ã Â´ÂÃ Â´Â¨Ã ÂµÂÃ Â´Â¨Ã Â´Â²Ã ÂµÂ] LT',
            lastWeek : '[Ã Â´ÂÃ Â´Â´Ã Â´Â¿Ã Â´ÂÃ ÂµÂÃ Â´Â] dddd, LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s Ã Â´ÂÃ Â´Â´Ã Â´Â¿Ã Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂ',
            past : '%s Ã Â´Â®Ã ÂµÂÃ ÂµÂ»Ã Â´ÂªÃ ÂµÂ',
            s : 'Ã Â´ÂÃ ÂµÂ½Ã Â´Âª Ã Â´Â¨Ã Â´Â¿Ã Â´Â®Ã Â´Â¿Ã Â´Â·Ã Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂ¾',
            m : 'Ã Â´ÂÃ Â´Â°Ã ÂµÂ Ã Â´Â®Ã Â´Â¿Ã Â´Â¨Ã Â´Â¿Ã Â´Â±Ã ÂµÂÃ Â´Â±Ã ÂµÂ',
            mm : '%d Ã Â´Â®Ã Â´Â¿Ã Â´Â¨Ã Â´Â¿Ã Â´Â±Ã ÂµÂÃ Â´Â±Ã ÂµÂ',
            h : 'Ã Â´ÂÃ Â´Â°Ã ÂµÂ Ã Â´Â®Ã Â´Â£Ã Â´Â¿Ã Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂÃ ÂµÂ¼',
            hh : '%d Ã Â´Â®Ã Â´Â£Ã Â´Â¿Ã Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂÃ ÂµÂ¼',
            d : 'Ã Â´ÂÃ Â´Â°Ã ÂµÂ Ã Â´Â¦Ã Â´Â¿Ã Â´ÂµÃ Â´Â¸Ã Â´Â',
            dd : '%d Ã Â´Â¦Ã Â´Â¿Ã Â´ÂµÃ Â´Â¸Ã Â´Â',
            M : 'Ã Â´ÂÃ Â´Â°Ã ÂµÂ Ã Â´Â®Ã Â´Â¾Ã Â´Â¸Ã Â´Â',
            MM : '%d Ã Â´Â®Ã Â´Â¾Ã Â´Â¸Ã Â´Â',
            y : 'Ã Â´ÂÃ Â´Â°Ã ÂµÂ Ã Â´ÂµÃ ÂµÂ¼Ã Â´Â·Ã Â´Â',
            yy : '%d Ã Â´ÂµÃ ÂµÂ¼Ã Â´Â·Ã Â´Â'
        },
        meridiemParse: /Ã Â´Â°Ã Â´Â¾Ã Â´Â¤Ã ÂµÂÃ Â´Â°Ã Â´Â¿|Ã Â´Â°Ã Â´Â¾Ã Â´ÂµÃ Â´Â¿Ã Â´Â²Ã ÂµÂ|Ã Â´ÂÃ Â´ÂÃ ÂµÂÃ Â´Â Ã Â´ÂÃ Â´Â´Ã Â´Â¿Ã Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂ|Ã Â´ÂµÃ ÂµÂÃ Â´ÂÃ ÂµÂÃ Â´Â¨Ã ÂµÂÃ Â´Â¨Ã ÂµÂÃ Â´Â°Ã Â´Â|Ã Â´Â°Ã Â´Â¾Ã Â´Â¤Ã ÂµÂÃ Â´Â°Ã Â´Â¿/i,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if ((meridiem === 'Ã Â´Â°Ã Â´Â¾Ã Â´Â¤Ã ÂµÂÃ Â´Â°Ã Â´Â¿' && hour >= 4) ||
                    meridiem === 'Ã Â´ÂÃ Â´ÂÃ ÂµÂÃ Â´Â Ã Â´ÂÃ Â´Â´Ã Â´Â¿Ã Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂ' ||
                    meridiem === 'Ã Â´ÂµÃ ÂµÂÃ Â´ÂÃ ÂµÂÃ Â´Â¨Ã ÂµÂÃ Â´Â¨Ã ÂµÂÃ Â´Â°Ã Â´Â') {
                return hour + 12;
            } else {
                return hour;
            }
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'Ã Â´Â°Ã Â´Â¾Ã Â´Â¤Ã ÂµÂÃ Â´Â°Ã Â´Â¿';
            } else if (hour < 12) {
                return 'Ã Â´Â°Ã Â´Â¾Ã Â´ÂµÃ Â´Â¿Ã Â´Â²Ã ÂµÂ';
            } else if (hour < 17) {
                return 'Ã Â´ÂÃ Â´ÂÃ ÂµÂÃ Â´Â Ã Â´ÂÃ Â´Â´Ã Â´Â¿Ã Â´ÂÃ ÂµÂÃ Â´ÂÃ ÂµÂ';
            } else if (hour < 20) {
                return 'Ã Â´ÂµÃ ÂµÂÃ Â´ÂÃ ÂµÂÃ Â´Â¨Ã ÂµÂÃ Â´Â¨Ã ÂµÂÃ Â´Â°Ã Â´Â';
            } else {
                return 'Ã Â´Â°Ã Â´Â¾Ã Â´Â¤Ã ÂµÂÃ Â´Â°Ã Â´Â¿';
            }
        }
    });

    //! moment.js locale configuration
    //! locale : Marathi (mr)
    //! author : Harshad Kale : https://github.com/kalehv
    //! author : Vivek Athalye : https://github.com/vnathalye

    var mr__symbolMap = {
        '1': 'Ã Â¥Â§',
        '2': 'Ã Â¥Â¨',
        '3': 'Ã Â¥Â©',
        '4': 'Ã Â¥Âª',
        '5': 'Ã Â¥Â«',
        '6': 'Ã Â¥Â¬',
        '7': 'Ã Â¥Â­',
        '8': 'Ã Â¥Â®',
        '9': 'Ã Â¥Â¯',
        '0': 'Ã Â¥Â¦'
    },
    mr__numberMap = {
        'Ã Â¥Â§': '1',
        'Ã Â¥Â¨': '2',
        'Ã Â¥Â©': '3',
        'Ã Â¥Âª': '4',
        'Ã Â¥Â«': '5',
        'Ã Â¥Â¬': '6',
        'Ã Â¥Â­': '7',
        'Ã Â¥Â®': '8',
        'Ã Â¥Â¯': '9',
        'Ã Â¥Â¦': '0'
    };

    function relativeTimeMr(number, withoutSuffix, string, isFuture)
    {
        var output = '';
        if (withoutSuffix) {
            switch (string) {
                case 's': output = 'Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â¹Ã Â¥Â Ã Â¤Â¸Ã Â¥ÂÃ Â¤ÂÃ Â¤ÂÃ Â¤Â¦'; break;
                case 'm': output = 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¿Ã Â¤Â'; break;
                case 'mm': output = '%d Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¿Ã Â¤ÂÃ Â¥Â'; break;
                case 'h': output = 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â¤Ã Â¤Â¾Ã Â¤Â¸'; break;
                case 'hh': output = '%d Ã Â¤Â¤Ã Â¤Â¾Ã Â¤Â¸'; break;
                case 'd': output = 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¸'; break;
                case 'dd': output = '%d Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¸'; break;
                case 'M': output = 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â®Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¾'; break;
                case 'MM': output = '%d Ã Â¤Â®Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¨Ã Â¥Â'; break;
                case 'y': output = 'Ã Â¤ÂÃ Â¤Â Ã Â¤ÂµÃ Â¤Â°Ã Â¥ÂÃ Â¤Â·'; break;
                case 'yy': output = '%d Ã Â¤ÂµÃ Â¤Â°Ã Â¥ÂÃ Â¤Â·Ã Â¥Â'; break;
            }
        }
        else {
            switch (string) {
                case 's': output = 'Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â¹Ã Â¥Â Ã Â¤Â¸Ã Â¥ÂÃ Â¤ÂÃ Â¤ÂÃ Â¤Â¦Ã Â¤Â¾Ã Â¤Â'; break;
                case 'm': output = 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾ Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¿Ã Â¤ÂÃ Â¤Â¾'; break;
                case 'mm': output = '%d Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¿Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â'; break;
                case 'h': output = 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾ Ã Â¤Â¤Ã Â¤Â¾Ã Â¤Â¸Ã Â¤Â¾'; break;
                case 'hh': output = '%d Ã Â¤Â¤Ã Â¤Â¾Ã Â¤Â¸Ã Â¤Â¾Ã Â¤Â'; break;
                case 'd': output = 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾ Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¸Ã Â¤Â¾'; break;
                case 'dd': output = '%d Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¸Ã Â¤Â¾Ã Â¤Â'; break;
                case 'M': output = 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾ Ã Â¤Â®Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¨Ã Â¥ÂÃ Â¤Â¯Ã Â¤Â¾'; break;
                case 'MM': output = '%d Ã Â¤Â®Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¨Ã Â¥ÂÃ Â¤Â¯Ã Â¤Â¾Ã Â¤Â'; break;
                case 'y': output = 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾ Ã Â¤ÂµÃ Â¤Â°Ã Â¥ÂÃ Â¤Â·Ã Â¤Â¾'; break;
                case 'yy': output = '%d Ã Â¤ÂµÃ Â¤Â°Ã Â¥ÂÃ Â¤Â·Ã Â¤Â¾Ã Â¤Â'; break;
            }
        }
        return output.replace(/%d/i, number);
    }

    var mr = moment.defineLocale('mr', {
        months : 'Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â¨Ã Â¥ÂÃ Â¤ÂµÃ Â¤Â¾Ã Â¤Â°Ã Â¥Â_Ã Â¤Â«Ã Â¥ÂÃ Â¤Â¬Ã Â¥ÂÃ Â¤Â°Ã Â¥ÂÃ Â¤ÂµÃ Â¤Â¾Ã Â¤Â°Ã Â¥Â_Ã Â¤Â®Ã Â¤Â¾Ã Â¤Â°Ã Â¥ÂÃ Â¤Â_Ã Â¤ÂÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â°Ã Â¤Â¿Ã Â¤Â²_Ã Â¤Â®Ã Â¥Â_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â¨_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â²Ã Â¥Â_Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¸Ã Â¥ÂÃ Â¤Â_Ã Â¤Â¸Ã Â¤ÂªÃ Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤Â¨Ã Â¥ÂÃ Â¤ÂµÃ Â¥ÂÃ Â¤Â¹Ã Â¥ÂÃ Â¤ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤Â¡Ã Â¤Â¿Ã Â¤Â¸Ã Â¥ÂÃ Â¤ÂÃ Â¤Â¬Ã Â¤Â°'.split('_'),
        monthsShort: 'Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â¨Ã Â¥Â._Ã Â¤Â«Ã Â¥ÂÃ Â¤Â¬Ã Â¥ÂÃ Â¤Â°Ã Â¥Â._Ã Â¤Â®Ã Â¤Â¾Ã Â¤Â°Ã Â¥ÂÃ Â¤Â._Ã Â¤ÂÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â°Ã Â¤Â¿._Ã Â¤Â®Ã Â¥Â._Ã Â¤ÂÃ Â¥ÂÃ Â¤Â¨._Ã Â¤ÂÃ Â¥ÂÃ Â¤Â²Ã Â¥Â._Ã Â¤ÂÃ Â¤Â._Ã Â¤Â¸Ã Â¤ÂªÃ Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â._Ã Â¤ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¥Â._Ã Â¤Â¨Ã Â¥ÂÃ Â¤ÂµÃ Â¥ÂÃ Â¤Â¹Ã Â¥ÂÃ Â¤Â._Ã Â¤Â¡Ã Â¤Â¿Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â.'.split('_'),
        monthsParseExact : true,
        weekdays : 'Ã Â¤Â°Ã Â¤ÂµÃ Â¤Â¿Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â®Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â®Ã Â¤ÂÃ Â¤ÂÃ Â¤Â³Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¬Ã Â¥ÂÃ Â¤Â§Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â°Ã Â¥ÂÃ Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¶Ã Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â°Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¶Ã Â¤Â¨Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°'.split('_'),
        weekdaysShort : 'Ã Â¤Â°Ã Â¤ÂµÃ Â¤Â¿_Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â®_Ã Â¤Â®Ã Â¤ÂÃ Â¤ÂÃ Â¤Â³_Ã Â¤Â¬Ã Â¥ÂÃ Â¤Â§_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â°Ã Â¥Â_Ã Â¤Â¶Ã Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â°_Ã Â¤Â¶Ã Â¤Â¨Ã Â¤Â¿'.split('_'),
        weekdaysMin : 'Ã Â¤Â°_Ã Â¤Â¸Ã Â¥Â_Ã Â¤Â®Ã Â¤Â_Ã Â¤Â¬Ã Â¥Â_Ã Â¤ÂÃ Â¥Â_Ã Â¤Â¶Ã Â¥Â_Ã Â¤Â¶'.split('_'),
        longDateFormat : {
            LT : 'A h:mm Ã Â¤ÂµÃ Â¤Â¾Ã Â¤ÂÃ Â¤Â¤Ã Â¤Â¾',
            LTS : 'A h:mm:ss Ã Â¤ÂµÃ Â¤Â¾Ã Â¤ÂÃ Â¤Â¤Ã Â¤Â¾',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY, A h:mm Ã Â¤ÂµÃ Â¤Â¾Ã Â¤ÂÃ Â¤Â¤Ã Â¤Â¾',
            LLLL : 'dddd, D MMMM YYYY, A h:mm Ã Â¤ÂµÃ Â¤Â¾Ã Â¤ÂÃ Â¤Â¤Ã Â¤Â¾'
        },
        calendar : {
            sameDay : '[Ã Â¤ÂÃ Â¤Â] LT',
            nextDay : '[Ã Â¤ÂÃ Â¤Â¦Ã Â¥ÂÃ Â¤Â¯Ã Â¤Â¾] LT',
            nextWeek : 'dddd, LT',
            lastDay : '[Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â²] LT',
            lastWeek: '[Ã Â¤Â®Ã Â¤Â¾Ã Â¤ÂÃ Â¥ÂÃ Â¤Â²] dddd, LT',
            sameElse : 'L'
        },
        relativeTime : {
            future: '%sÃ Â¤Â®Ã Â¤Â§Ã Â¥ÂÃ Â¤Â¯Ã Â¥Â',
            past: '%sÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â°Ã Â¥ÂÃ Â¤ÂµÃ Â¥Â',
            s: relativeTimeMr,
            m: relativeTimeMr,
            mm: relativeTimeMr,
            h: relativeTimeMr,
            hh: relativeTimeMr,
            d: relativeTimeMr,
            dd: relativeTimeMr,
            M: relativeTimeMr,
            MM: relativeTimeMr,
            y: relativeTimeMr,
            yy: relativeTimeMr
        },
        preparse: function (string) {
            return string.replace(/[Ã Â¥Â§Ã Â¥Â¨Ã Â¥Â©Ã Â¥ÂªÃ Â¥Â«Ã Â¥Â¬Ã Â¥Â­Ã Â¥Â®Ã Â¥Â¯Ã Â¥Â¦]/g, function (match) {
                return mr__numberMap[match];
            });
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return mr__symbolMap[match];
            });
        },
        meridiemParse: /Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¥ÂÃ Â¤Â°Ã Â¥Â|Ã Â¤Â¸Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â|Ã Â¤Â¦Ã Â¥ÂÃ Â¤ÂªÃ Â¤Â¾Ã Â¤Â°Ã Â¥Â|Ã Â¤Â¸Ã Â¤Â¾Ã Â¤Â¯Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¥ÂÃ Â¤Â°Ã Â¥Â') {
                return hour < 4 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â¤Â¸Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â') {
                return hour;
            } else if (meridiem === 'Ã Â¤Â¦Ã Â¥ÂÃ Â¤ÂªÃ Â¤Â¾Ã Â¤Â°Ã Â¥Â') {
                return hour >= 10 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â¤Â¸Ã Â¤Â¾Ã Â¤Â¯Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â') {
                return hour + 12;
            }
        },
        meridiem: function (hour, minute, isLower) {
            if (hour < 4) {
                return 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¥ÂÃ Â¤Â°Ã Â¥Â';
            } else if (hour < 10) {
                return 'Ã Â¤Â¸Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â';
            } else if (hour < 17) {
                return 'Ã Â¤Â¦Ã Â¥ÂÃ Â¤ÂªÃ Â¤Â¾Ã Â¤Â°Ã Â¥Â';
            } else if (hour < 20) {
                return 'Ã Â¤Â¸Ã Â¤Â¾Ã Â¤Â¯Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â';
            } else {
                return 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¥ÂÃ Â¤Â°Ã Â¥Â';
            }
        },
        week : {
            dow : 0, // Sunday is the first day of the week.
            doy : 6  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Bahasa Malaysia (ms-MY)
    //! author : Weldan Jamili : https://github.com/weldan

    var ms_my = moment.defineLocale('ms-my', {
        months : 'Januari_Februari_Mac_April_Mei_Jun_Julai_Ogos_September_Oktober_November_Disember'.split('_'),
        monthsShort : 'Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ogs_Sep_Okt_Nov_Dis'.split('_'),
        weekdays : 'Ahad_Isnin_Selasa_Rabu_Khamis_Jumaat_Sabtu'.split('_'),
        weekdaysShort : 'Ahd_Isn_Sel_Rab_Kha_Jum_Sab'.split('_'),
        weekdaysMin : 'Ah_Is_Sl_Rb_Km_Jm_Sb'.split('_'),
        longDateFormat : {
            LT : 'HH.mm',
            LTS : 'HH.mm.ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY [pukul] HH.mm',
            LLLL : 'dddd, D MMMM YYYY [pukul] HH.mm'
        },
        meridiemParse: /pagi|tengahari|petang|malam/,
        meridiemHour: function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'pagi') {
                return hour;
            } else if (meridiem === 'tengahari') {
                return hour >= 11 ? hour : hour + 12;
            } else if (meridiem === 'petang' || meridiem === 'malam') {
                return hour + 12;
            }
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours < 11) {
                return 'pagi';
            } else if (hours < 15) {
                return 'tengahari';
            } else if (hours < 19) {
                return 'petang';
            } else {
                return 'malam';
            }
        },
        calendar : {
            sameDay : '[Hari ini pukul] LT',
            nextDay : '[Esok pukul] LT',
            nextWeek : 'dddd [pukul] LT',
            lastDay : '[Kelmarin pukul] LT',
            lastWeek : 'dddd [lepas pukul] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'dalam %s',
            past : '%s yang lepas',
            s : 'beberapa saat',
            m : 'seminit',
            mm : '%d minit',
            h : 'sejam',
            hh : '%d jam',
            d : 'sehari',
            dd : '%d hari',
            M : 'sebulan',
            MM : '%d bulan',
            y : 'setahun',
            yy : '%d tahun'
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Bahasa Malaysia (ms-MY)
    //! author : Weldan Jamili : https://github.com/weldan

    var ms = moment.defineLocale('ms', {
        months : 'Januari_Februari_Mac_April_Mei_Jun_Julai_Ogos_September_Oktober_November_Disember'.split('_'),
        monthsShort : 'Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ogs_Sep_Okt_Nov_Dis'.split('_'),
        weekdays : 'Ahad_Isnin_Selasa_Rabu_Khamis_Jumaat_Sabtu'.split('_'),
        weekdaysShort : 'Ahd_Isn_Sel_Rab_Kha_Jum_Sab'.split('_'),
        weekdaysMin : 'Ah_Is_Sl_Rb_Km_Jm_Sb'.split('_'),
        longDateFormat : {
            LT : 'HH.mm',
            LTS : 'HH.mm.ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY [pukul] HH.mm',
            LLLL : 'dddd, D MMMM YYYY [pukul] HH.mm'
        },
        meridiemParse: /pagi|tengahari|petang|malam/,
        meridiemHour: function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'pagi') {
                return hour;
            } else if (meridiem === 'tengahari') {
                return hour >= 11 ? hour : hour + 12;
            } else if (meridiem === 'petang' || meridiem === 'malam') {
                return hour + 12;
            }
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours < 11) {
                return 'pagi';
            } else if (hours < 15) {
                return 'tengahari';
            } else if (hours < 19) {
                return 'petang';
            } else {
                return 'malam';
            }
        },
        calendar : {
            sameDay : '[Hari ini pukul] LT',
            nextDay : '[Esok pukul] LT',
            nextWeek : 'dddd [pukul] LT',
            lastDay : '[Kelmarin pukul] LT',
            lastWeek : 'dddd [lepas pukul] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'dalam %s',
            past : '%s yang lepas',
            s : 'beberapa saat',
            m : 'seminit',
            mm : '%d minit',
            h : 'sejam',
            hh : '%d jam',
            d : 'sehari',
            dd : '%d hari',
            M : 'sebulan',
            MM : '%d bulan',
            y : 'setahun',
            yy : '%d tahun'
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Burmese (my)
    //! author : Squar team, mysquar.com

    var my__symbolMap = {
        '1': 'Ã¡ÂÂ',
        '2': 'Ã¡ÂÂ',
        '3': 'Ã¡ÂÂ',
        '4': 'Ã¡ÂÂ',
        '5': 'Ã¡ÂÂ',
        '6': 'Ã¡ÂÂ',
        '7': 'Ã¡ÂÂ',
        '8': 'Ã¡ÂÂ',
        '9': 'Ã¡ÂÂ',
        '0': 'Ã¡ÂÂ'
    }, my__numberMap = {
        'Ã¡ÂÂ': '1',
        'Ã¡ÂÂ': '2',
        'Ã¡ÂÂ': '3',
        'Ã¡ÂÂ': '4',
        'Ã¡ÂÂ': '5',
        'Ã¡ÂÂ': '6',
        'Ã¡ÂÂ': '7',
        'Ã¡ÂÂ': '8',
        'Ã¡ÂÂ': '9',
        'Ã¡ÂÂ': '0'
    };

    var my = moment.defineLocale('my', {
        months: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ«Ã¡ÂÂÃ¡ÂÂ®_Ã¡ÂÂÃ¡ÂÂ±Ã¡ÂÂÃ¡ÂÂ±Ã¡ÂÂ¬Ã¡ÂÂºÃ¡ÂÂÃ¡ÂÂ«Ã¡ÂÂÃ¡ÂÂ®_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂº_Ã¡ÂÂ§Ã¡ÂÂÃ¡ÂÂ¼Ã¡ÂÂ®_Ã¡ÂÂÃ¡ÂÂ±_Ã¡ÂÂÃ¡ÂÂ½Ã¡ÂÂÃ¡ÂÂº_Ã¡ÂÂÃ¡ÂÂ°Ã¡ÂÂÃ¡ÂÂ­Ã¡ÂÂ¯Ã¡ÂÂÃ¡ÂÂº_Ã¡ÂÂÃ¡ÂÂ¼Ã¡ÂÂÃ¡ÂÂ¯Ã¡ÂÂÃ¡ÂÂº_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂ¬_Ã¡ÂÂ¡Ã¡ÂÂ±Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂ­Ã¡ÂÂ¯Ã¡ÂÂÃ¡ÂÂ¬_Ã¡ÂÂÃ¡ÂÂ­Ã¡ÂÂ¯Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂ¬_Ã¡ÂÂÃ¡ÂÂ®Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂ¬'.split('_'),
        monthsShort: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂº_Ã¡ÂÂÃ¡ÂÂ±_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂº_Ã¡ÂÂÃ¡ÂÂ¼Ã¡ÂÂ®_Ã¡ÂÂÃ¡ÂÂ±_Ã¡ÂÂÃ¡ÂÂ½Ã¡ÂÂÃ¡ÂÂº_Ã¡ÂÂÃ¡ÂÂ­Ã¡ÂÂ¯Ã¡ÂÂÃ¡ÂÂº_Ã¡ÂÂÃ¡ÂÂ¼_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂº_Ã¡ÂÂ¡Ã¡ÂÂ±Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂº_Ã¡ÂÂÃ¡ÂÂ­Ã¡ÂÂ¯_Ã¡ÂÂÃ¡ÂÂ®'.split('_'),
        weekdays: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂ¹Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ½Ã¡ÂÂ±_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂ¹Ã¡ÂÂÃ¡ÂÂ¬_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂºÃ¡ÂÂ¹Ã¡ÂÂÃ¡ÂÂ«_Ã¡ÂÂÃ¡ÂÂ¯Ã¡ÂÂÃ¡ÂÂ¹Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ°Ã¡ÂÂ¸_Ã¡ÂÂÃ¡ÂÂ¼Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ±Ã¡ÂÂ¸_Ã¡ÂÂÃ¡ÂÂ±Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂ¼Ã¡ÂÂ¬_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ±'.split('_'),
        weekdaysShort: 'Ã¡ÂÂÃ¡ÂÂ½Ã¡ÂÂ±_Ã¡ÂÂÃ¡ÂÂ¬_Ã¡ÂÂÃ¡ÂÂ«_Ã¡ÂÂÃ¡ÂÂ°Ã¡ÂÂ¸_Ã¡ÂÂÃ¡ÂÂ¼Ã¡ÂÂ¬_Ã¡ÂÂÃ¡ÂÂ±Ã¡ÂÂ¬_Ã¡ÂÂÃ¡ÂÂ±'.split('_'),
        weekdaysMin: 'Ã¡ÂÂÃ¡ÂÂ½Ã¡ÂÂ±_Ã¡ÂÂÃ¡ÂÂ¬_Ã¡ÂÂÃ¡ÂÂ«_Ã¡ÂÂÃ¡ÂÂ°Ã¡ÂÂ¸_Ã¡ÂÂÃ¡ÂÂ¼Ã¡ÂÂ¬_Ã¡ÂÂÃ¡ÂÂ±Ã¡ÂÂ¬_Ã¡ÂÂÃ¡ÂÂ±'.split('_'),

        longDateFormat: {
            LT: 'HH:mm',
            LTS: 'HH:mm:ss',
            L: 'DD/MM/YYYY',
            LL: 'D MMMM YYYY',
            LLL: 'D MMMM YYYY HH:mm',
            LLLL: 'dddd D MMMM YYYY HH:mm'
        },
        calendar: {
            sameDay: '[Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ±.] LT [Ã¡ÂÂÃ¡ÂÂ¾Ã¡ÂÂ¬]',
            nextDay: '[Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂ¼Ã¡ÂÂÃ¡ÂÂº] LT [Ã¡ÂÂÃ¡ÂÂ¾Ã¡ÂÂ¬]',
            nextWeek: 'dddd LT [Ã¡ÂÂÃ¡ÂÂ¾Ã¡ÂÂ¬]',
            lastDay: '[Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ±.Ã¡ÂÂ] LT [Ã¡ÂÂÃ¡ÂÂ¾Ã¡ÂÂ¬]',
            lastWeek: '[Ã¡ÂÂÃ¡ÂÂ¼Ã¡ÂÂ®Ã¡ÂÂ¸Ã¡ÂÂÃ¡ÂÂ²Ã¡ÂÂ·Ã¡ÂÂÃ¡ÂÂ±Ã¡ÂÂ¬] dddd LT [Ã¡ÂÂÃ¡ÂÂ¾Ã¡ÂÂ¬]',
            sameElse: 'L'
        },
        relativeTime: {
            future: 'Ã¡ÂÂÃ¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂ· %s Ã¡ÂÂÃ¡ÂÂ¾Ã¡ÂÂ¬',
            past: 'Ã¡ÂÂÃ¡ÂÂ½Ã¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂ²Ã¡ÂÂ·Ã¡ÂÂÃ¡ÂÂ±Ã¡ÂÂ¬ %s Ã¡ÂÂ',
            s: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¹Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂº.Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂ¸Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂº',
            m: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂ­Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂº',
            mm: '%d Ã¡ÂÂÃ¡ÂÂ­Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂº',
            h: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂ®',
            hh: '%d Ã¡ÂÂÃ¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂ®',
            d: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂº',
            dd: '%d Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂº',
            M: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂ',
            MM: '%d Ã¡ÂÂ',
            y: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂºÃ¡ÂÂÃ¡ÂÂ¾Ã¡ÂÂÃ¡ÂÂº',
            yy: '%d Ã¡ÂÂÃ¡ÂÂ¾Ã¡ÂÂÃ¡ÂÂº'
        },
        preparse: function (string) {
            return string.replace(/[Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ]/g, function (match) {
                return my__numberMap[match];
            });
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return my__symbolMap[match];
            });
        },
        week: {
            dow: 1, // Monday is the first day of the week.
            doy: 4 // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : norwegian bokmÃÂ¥l (nb)
    //! authors : Espen Hovlandsdal : https://github.com/rexxars
    //!           Sigurd Gartmann : https://github.com/sigurdga

    var nb = moment.defineLocale('nb', {
        months : 'januar_februar_mars_april_mai_juni_juli_august_september_oktober_november_desember'.split('_'),
        monthsShort : 'jan._feb._mars_april_mai_juni_juli_aug._sep._okt._nov._des.'.split('_'),
        monthsParseExact : true,
        weekdays : 'sÃÂ¸ndag_mandag_tirsdag_onsdag_torsdag_fredag_lÃÂ¸rdag'.split('_'),
        weekdaysShort : 'sÃÂ¸._ma._ti._on._to._fr._lÃÂ¸.'.split('_'),
        weekdaysMin : 'sÃÂ¸_ma_ti_on_to_fr_lÃÂ¸'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY [kl.] HH:mm',
            LLLL : 'dddd D. MMMM YYYY [kl.] HH:mm'
        },
        calendar : {
            sameDay: '[i dag kl.] LT',
            nextDay: '[i morgen kl.] LT',
            nextWeek: 'dddd [kl.] LT',
            lastDay: '[i gÃÂ¥r kl.] LT',
            lastWeek: '[forrige] dddd [kl.] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'om %s',
            past : '%s siden',
            s : 'noen sekunder',
            m : 'ett minutt',
            mm : '%d minutter',
            h : 'en time',
            hh : '%d timer',
            d : 'en dag',
            dd : '%d dager',
            M : 'en mÃÂ¥ned',
            MM : '%d mÃÂ¥neder',
            y : 'ett ÃÂ¥r',
            yy : '%d ÃÂ¥r'
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : nepali/nepalese
    //! author : suvash : https://github.com/suvash

    var ne__symbolMap = {
        '1': 'Ã Â¥Â§',
        '2': 'Ã Â¥Â¨',
        '3': 'Ã Â¥Â©',
        '4': 'Ã Â¥Âª',
        '5': 'Ã Â¥Â«',
        '6': 'Ã Â¥Â¬',
        '7': 'Ã Â¥Â­',
        '8': 'Ã Â¥Â®',
        '9': 'Ã Â¥Â¯',
        '0': 'Ã Â¥Â¦'
    },
    ne__numberMap = {
        'Ã Â¥Â§': '1',
        'Ã Â¥Â¨': '2',
        'Ã Â¥Â©': '3',
        'Ã Â¥Âª': '4',
        'Ã Â¥Â«': '5',
        'Ã Â¥Â¬': '6',
        'Ã Â¥Â­': '7',
        'Ã Â¥Â®': '8',
        'Ã Â¥Â¯': '9',
        'Ã Â¥Â¦': '0'
    };

    var ne = moment.defineLocale('ne', {
        months : 'Ã Â¤ÂÃ Â¤Â¨Ã Â¤ÂµÃ Â¤Â°Ã Â¥Â_Ã Â¤Â«Ã Â¥ÂÃ Â¤Â¬Ã Â¥ÂÃ Â¤Â°Ã Â¥ÂÃ Â¤ÂµÃ Â¤Â°Ã Â¥Â_Ã Â¤Â®Ã Â¤Â¾Ã Â¤Â°Ã Â¥ÂÃ Â¤Â_Ã Â¤ÂÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â°Ã Â¤Â¿Ã Â¤Â²_Ã Â¤Â®Ã Â¤Â_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â¨_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â²Ã Â¤Â¾Ã Â¤Â_Ã Â¤ÂÃ Â¤ÂÃ Â¤Â·Ã Â¥ÂÃ Â¤Â_Ã Â¤Â¸Ã Â¥ÂÃ Â¤ÂªÃ Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â®Ã Â¥ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤Â¨Ã Â¥ÂÃ Â¤Â­Ã Â¥ÂÃ Â¤Â®Ã Â¥ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤Â¡Ã Â¤Â¿Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â®Ã Â¥ÂÃ Â¤Â¬Ã Â¤Â°'.split('_'),
        monthsShort : 'Ã Â¤ÂÃ Â¤Â¨._Ã Â¤Â«Ã Â¥ÂÃ Â¤Â¬Ã Â¥ÂÃ Â¤Â°Ã Â¥Â._Ã Â¤Â®Ã Â¤Â¾Ã Â¤Â°Ã Â¥ÂÃ Â¤Â_Ã Â¤ÂÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â°Ã Â¤Â¿._Ã Â¤Â®Ã Â¤Â_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â¨_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â²Ã Â¤Â¾Ã Â¤Â._Ã Â¤ÂÃ Â¤Â._Ã Â¤Â¸Ã Â¥ÂÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â._Ã Â¤ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¥Â._Ã Â¤Â¨Ã Â¥ÂÃ Â¤Â­Ã Â¥Â._Ã Â¤Â¡Ã Â¤Â¿Ã Â¤Â¸Ã Â¥Â.'.split('_'),
        monthsParseExact : true,
        weekdays : 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¤Ã Â¤Â¬Ã Â¤Â¾Ã Â¤Â°_Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â®Ã Â¤Â¬Ã Â¤Â¾Ã Â¤Â°_Ã Â¤Â®Ã Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¤Â²Ã Â¤Â¬Ã Â¤Â¾Ã Â¤Â°_Ã Â¤Â¬Ã Â¥ÂÃ Â¤Â§Ã Â¤Â¬Ã Â¤Â¾Ã Â¤Â°_Ã Â¤Â¬Ã Â¤Â¿Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¬Ã Â¤Â¾Ã Â¤Â°_Ã Â¤Â¶Ã Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â°Ã Â¤Â¬Ã Â¤Â¾Ã Â¤Â°_Ã Â¤Â¶Ã Â¤Â¨Ã Â¤Â¿Ã Â¤Â¬Ã Â¤Â¾Ã Â¤Â°'.split('_'),
        weekdaysShort : 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¤._Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â®._Ã Â¤Â®Ã Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¤Â²._Ã Â¤Â¬Ã Â¥ÂÃ Â¤Â§._Ã Â¤Â¬Ã Â¤Â¿Ã Â¤Â¹Ã Â¤Â¿._Ã Â¤Â¶Ã Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â°._Ã Â¤Â¶Ã Â¤Â¨Ã Â¤Â¿.'.split('_'),
        weekdaysMin : 'Ã Â¤Â._Ã Â¤Â¸Ã Â¥Â._Ã Â¤Â®Ã Â¤Â._Ã Â¤Â¬Ã Â¥Â._Ã Â¤Â¬Ã Â¤Â¿._Ã Â¤Â¶Ã Â¥Â._Ã Â¤Â¶.'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'AÃ Â¤ÂÃ Â¥Â h:mm Ã Â¤Â¬Ã Â¤ÂÃ Â¥Â',
            LTS : 'AÃ Â¤ÂÃ Â¥Â h:mm:ss Ã Â¤Â¬Ã Â¤ÂÃ Â¥Â',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY, AÃ Â¤ÂÃ Â¥Â h:mm Ã Â¤Â¬Ã Â¤ÂÃ Â¥Â',
            LLLL : 'dddd, D MMMM YYYY, AÃ Â¤ÂÃ Â¥Â h:mm Ã Â¤Â¬Ã Â¤ÂÃ Â¥Â'
        },
        preparse: function (string) {
            return string.replace(/[Ã Â¥Â§Ã Â¥Â¨Ã Â¥Â©Ã Â¥ÂªÃ Â¥Â«Ã Â¥Â¬Ã Â¥Â­Ã Â¥Â®Ã Â¥Â¯Ã Â¥Â¦]/g, function (match) {
                return ne__numberMap[match];
            });
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return ne__symbolMap[match];
            });
        },
        meridiemParse: /Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¤Â¿|Ã Â¤Â¬Ã Â¤Â¿Ã Â¤Â¹Ã Â¤Â¾Ã Â¤Â¨|Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¸Ã Â¥Â|Ã Â¤Â¸Ã Â¤Â¾Ã Â¤ÂÃ Â¤Â/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¤Â¿') {
                return hour < 4 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â¤Â¬Ã Â¤Â¿Ã Â¤Â¹Ã Â¤Â¾Ã Â¤Â¨') {
                return hour;
            } else if (meridiem === 'Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¸Ã Â¥Â') {
                return hour >= 10 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â¤Â¸Ã Â¤Â¾Ã Â¤ÂÃ Â¤Â') {
                return hour + 12;
            }
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 3) {
                return 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¤Â¿';
            } else if (hour < 12) {
                return 'Ã Â¤Â¬Ã Â¤Â¿Ã Â¤Â¹Ã Â¤Â¾Ã Â¤Â¨';
            } else if (hour < 16) {
                return 'Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¸Ã Â¥Â';
            } else if (hour < 20) {
                return 'Ã Â¤Â¸Ã Â¤Â¾Ã Â¤ÂÃ Â¤Â';
            } else {
                return 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¤Â¿';
            }
        },
        calendar : {
            sameDay : '[Ã Â¤ÂÃ Â¤Â] LT',
            nextDay : '[Ã Â¤Â­Ã Â¥ÂÃ Â¤Â²Ã Â¤Â¿] LT',
            nextWeek : '[Ã Â¤ÂÃ Â¤ÂÃ Â¤ÂÃ Â¤Â¦Ã Â¥Â] dddd[,] LT',
            lastDay : '[Ã Â¤Â¹Ã Â¤Â¿Ã Â¤ÂÃ Â¥Â] LT',
            lastWeek : '[Ã Â¤ÂÃ Â¤ÂÃ Â¤ÂÃ Â¥Â] dddd[,] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%sÃ Â¤Â®Ã Â¤Â¾',
            past : '%s Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾Ã Â¤Â¡Ã Â¤Â¿',
            s : 'Ã Â¤ÂÃ Â¥ÂÃ Â¤Â¹Ã Â¥Â Ã Â¤ÂÃ Â¥ÂÃ Â¤Â·Ã Â¤Â£',
            m : 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¥ÂÃ Â¤Â',
            mm : '%d Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¥ÂÃ Â¤Â',
            h : 'Ã Â¤ÂÃ Â¤Â Ã Â¤ÂÃ Â¤Â£Ã Â¥ÂÃ Â¤ÂÃ Â¤Â¾',
            hh : '%d Ã Â¤ÂÃ Â¤Â£Ã Â¥ÂÃ Â¤ÂÃ Â¤Â¾',
            d : 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â¦Ã Â¤Â¿Ã Â¤Â¨',
            dd : '%d Ã Â¤Â¦Ã Â¤Â¿Ã Â¤Â¨',
            M : 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â®Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¾',
            MM : '%d Ã Â¤Â®Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¾',
            y : 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â¬Ã Â¤Â°Ã Â¥ÂÃ Â¤Â·',
            yy : '%d Ã Â¤Â¬Ã Â¤Â°Ã Â¥ÂÃ Â¤Â·'
        },
        week : {
            dow : 0, // Sunday is the first day of the week.
            doy : 6  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : dutch (nl)
    //! author : Joris RÃÂ¶ling : https://github.com/jjupiter

    var nl__monthsShortWithDots = 'jan._feb._mrt._apr._mei_jun._jul._aug._sep._okt._nov._dec.'.split('_'),
        nl__monthsShortWithoutDots = 'jan_feb_mrt_apr_mei_jun_jul_aug_sep_okt_nov_dec'.split('_');

    var nl = moment.defineLocale('nl', {
        months : 'januari_februari_maart_april_mei_juni_juli_augustus_september_oktober_november_december'.split('_'),
        monthsShort : function (m, format) {
            if (/-MMM-/.test(format)) {
                return nl__monthsShortWithoutDots[m.month()];
            } else {
                return nl__monthsShortWithDots[m.month()];
            }
        },
        monthsParseExact : true,
        weekdays : 'zondag_maandag_dinsdag_woensdag_donderdag_vrijdag_zaterdag'.split('_'),
        weekdaysShort : 'zo._ma._di._wo._do._vr._za.'.split('_'),
        weekdaysMin : 'Zo_Ma_Di_Wo_Do_Vr_Za'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD-MM-YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[vandaag om] LT',
            nextDay: '[morgen om] LT',
            nextWeek: 'dddd [om] LT',
            lastDay: '[gisteren om] LT',
            lastWeek: '[afgelopen] dddd [om] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'over %s',
            past : '%s geleden',
            s : 'een paar seconden',
            m : 'ÃÂ©ÃÂ©n minuut',
            mm : '%d minuten',
            h : 'ÃÂ©ÃÂ©n uur',
            hh : '%d uur',
            d : 'ÃÂ©ÃÂ©n dag',
            dd : '%d dagen',
            M : 'ÃÂ©ÃÂ©n maand',
            MM : '%d maanden',
            y : 'ÃÂ©ÃÂ©n jaar',
            yy : '%d jaar'
        },
        ordinalParse: /\d{1,2}(ste|de)/,
        ordinal : function (number) {
            return number + ((number === 1 || number === 8 || number >= 20) ? 'ste' : 'de');
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : norwegian nynorsk (nn)
    //! author : https://github.com/mechuwind

    var nn = moment.defineLocale('nn', {
        months : 'januar_februar_mars_april_mai_juni_juli_august_september_oktober_november_desember'.split('_'),
        monthsShort : 'jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des'.split('_'),
        weekdays : 'sundag_mÃÂ¥ndag_tysdag_onsdag_torsdag_fredag_laurdag'.split('_'),
        weekdaysShort : 'sun_mÃÂ¥n_tys_ons_tor_fre_lau'.split('_'),
        weekdaysMin : 'su_mÃÂ¥_ty_on_to_fr_lÃÂ¸'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY [kl.] H:mm',
            LLLL : 'dddd D. MMMM YYYY [kl.] HH:mm'
        },
        calendar : {
            sameDay: '[I dag klokka] LT',
            nextDay: '[I morgon klokka] LT',
            nextWeek: 'dddd [klokka] LT',
            lastDay: '[I gÃÂ¥r klokka] LT',
            lastWeek: '[FÃÂ¸regÃÂ¥ande] dddd [klokka] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'om %s',
            past : '%s sidan',
            s : 'nokre sekund',
            m : 'eit minutt',
            mm : '%d minutt',
            h : 'ein time',
            hh : '%d timar',
            d : 'ein dag',
            dd : '%d dagar',
            M : 'ein mÃÂ¥nad',
            MM : '%d mÃÂ¥nader',
            y : 'eit ÃÂ¥r',
            yy : '%d ÃÂ¥r'
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : punjabi india (pa-in)
    //! author : Harpreet Singh : https://github.com/harpreetkhalsagtbit

    var pa_in__symbolMap = {
        '1': 'Ã Â©Â§',
        '2': 'Ã Â©Â¨',
        '3': 'Ã Â©Â©',
        '4': 'Ã Â©Âª',
        '5': 'Ã Â©Â«',
        '6': 'Ã Â©Â¬',
        '7': 'Ã Â©Â­',
        '8': 'Ã Â©Â®',
        '9': 'Ã Â©Â¯',
        '0': 'Ã Â©Â¦'
    },
    pa_in__numberMap = {
        'Ã Â©Â§': '1',
        'Ã Â©Â¨': '2',
        'Ã Â©Â©': '3',
        'Ã Â©Âª': '4',
        'Ã Â©Â«': '5',
        'Ã Â©Â¬': '6',
        'Ã Â©Â­': '7',
        'Ã Â©Â®': '8',
        'Ã Â©Â¯': '9',
        'Ã Â©Â¦': '0'
    };

    var pa_in = moment.defineLocale('pa-in', {
        // There are months name as per Nanakshahi Calender but they are not used as rigidly in modern Punjabi.
        months : 'Ã Â¨ÂÃ Â¨Â¨Ã Â¨ÂµÃ Â¨Â°Ã Â©Â_Ã Â¨Â«Ã Â¨Â¼Ã Â¨Â°Ã Â¨ÂµÃ Â¨Â°Ã Â©Â_Ã Â¨Â®Ã Â¨Â¾Ã Â¨Â°Ã Â¨Â_Ã Â¨ÂÃ Â¨ÂªÃ Â©ÂÃ Â¨Â°Ã Â©ÂÃ Â¨Â²_Ã Â¨Â®Ã Â¨Â_Ã Â¨ÂÃ Â©ÂÃ Â¨Â¨_Ã Â¨ÂÃ Â©ÂÃ Â¨Â²Ã Â¨Â¾Ã Â¨Â_Ã Â¨ÂÃ Â¨ÂÃ Â¨Â¸Ã Â¨Â¤_Ã Â¨Â¸Ã Â¨Â¤Ã Â©Â°Ã Â¨Â¬Ã Â¨Â°_Ã Â¨ÂÃ Â¨ÂÃ Â¨Â¤Ã Â©ÂÃ Â¨Â¬Ã Â¨Â°_Ã Â¨Â¨Ã Â¨ÂµÃ Â©Â°Ã Â¨Â¬Ã Â¨Â°_Ã Â¨Â¦Ã Â¨Â¸Ã Â©Â°Ã Â¨Â¬Ã Â¨Â°'.split('_'),
        monthsShort : 'Ã Â¨ÂÃ Â¨Â¨Ã Â¨ÂµÃ Â¨Â°Ã Â©Â_Ã Â¨Â«Ã Â¨Â¼Ã Â¨Â°Ã Â¨ÂµÃ Â¨Â°Ã Â©Â_Ã Â¨Â®Ã Â¨Â¾Ã Â¨Â°Ã Â¨Â_Ã Â¨ÂÃ Â¨ÂªÃ Â©ÂÃ Â¨Â°Ã Â©ÂÃ Â¨Â²_Ã Â¨Â®Ã Â¨Â_Ã Â¨ÂÃ Â©ÂÃ Â¨Â¨_Ã Â¨ÂÃ Â©ÂÃ Â¨Â²Ã Â¨Â¾Ã Â¨Â_Ã Â¨ÂÃ Â¨ÂÃ Â¨Â¸Ã Â¨Â¤_Ã Â¨Â¸Ã Â¨Â¤Ã Â©Â°Ã Â¨Â¬Ã Â¨Â°_Ã Â¨ÂÃ Â¨ÂÃ Â¨Â¤Ã Â©ÂÃ Â¨Â¬Ã Â¨Â°_Ã Â¨Â¨Ã Â¨ÂµÃ Â©Â°Ã Â¨Â¬Ã Â¨Â°_Ã Â¨Â¦Ã Â¨Â¸Ã Â©Â°Ã Â¨Â¬Ã Â¨Â°'.split('_'),
        weekdays : 'Ã Â¨ÂÃ Â¨Â¤Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨Â¸Ã Â©ÂÃ Â¨Â®Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨Â®Ã Â©Â°Ã Â¨ÂÃ Â¨Â²Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨Â¬Ã Â©ÂÃ Â¨Â§Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â©ÂÃ Â©Â±Ã Â¨ÂÃ Â¨Â°Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¨Ã Â©ÂÃ Â¨ÂÃ Â¨Â°Ã Â¨ÂµÃ Â¨Â¾Ã Â¨Â°'.split('_'),
        weekdaysShort : 'Ã Â¨ÂÃ Â¨Â¤_Ã Â¨Â¸Ã Â©ÂÃ Â¨Â®_Ã Â¨Â®Ã Â©Â°Ã Â¨ÂÃ Â¨Â²_Ã Â¨Â¬Ã Â©ÂÃ Â¨Â§_Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â©ÂÃ Â¨ÂÃ Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¨Ã Â©Â'.split('_'),
        weekdaysMin : 'Ã Â¨ÂÃ Â¨Â¤_Ã Â¨Â¸Ã Â©ÂÃ Â¨Â®_Ã Â¨Â®Ã Â©Â°Ã Â¨ÂÃ Â¨Â²_Ã Â¨Â¬Ã Â©ÂÃ Â¨Â§_Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â©ÂÃ Â¨ÂÃ Â¨Â°_Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¨Ã Â©Â'.split('_'),
        longDateFormat : {
            LT : 'A h:mm Ã Â¨ÂµÃ Â¨ÂÃ Â©Â',
            LTS : 'A h:mm:ss Ã Â¨ÂµÃ Â¨ÂÃ Â©Â',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY, A h:mm Ã Â¨ÂµÃ Â¨ÂÃ Â©Â',
            LLLL : 'dddd, D MMMM YYYY, A h:mm Ã Â¨ÂµÃ Â¨ÂÃ Â©Â'
        },
        calendar : {
            sameDay : '[Ã Â¨ÂÃ Â¨Â] LT',
            nextDay : '[Ã Â¨ÂÃ Â¨Â²] LT',
            nextWeek : 'dddd, LT',
            lastDay : '[Ã Â¨ÂÃ Â¨Â²] LT',
            lastWeek : '[Ã Â¨ÂªÃ Â¨Â¿Ã Â¨ÂÃ Â¨Â²Ã Â©Â] dddd, LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s Ã Â¨ÂµÃ Â¨Â¿Ã Â©Â±Ã Â¨Â',
            past : '%s Ã Â¨ÂªÃ Â¨Â¿Ã Â¨ÂÃ Â¨Â²Ã Â©Â',
            s : 'Ã Â¨ÂÃ Â©ÂÃ Â¨Â Ã Â¨Â¸Ã Â¨ÂÃ Â¨Â¿Ã Â©Â°Ã Â¨Â',
            m : 'Ã Â¨ÂÃ Â¨Â Ã Â¨Â®Ã Â¨Â¿Ã Â©Â°Ã Â¨Â',
            mm : '%d Ã Â¨Â®Ã Â¨Â¿Ã Â©Â°Ã Â¨Â',
            h : 'Ã Â¨ÂÃ Â©Â±Ã Â¨Â Ã Â¨ÂÃ Â©Â°Ã Â¨ÂÃ Â¨Â¾',
            hh : '%d Ã Â¨ÂÃ Â©Â°Ã Â¨ÂÃ Â©Â',
            d : 'Ã Â¨ÂÃ Â©Â±Ã Â¨Â Ã Â¨Â¦Ã Â¨Â¿Ã Â¨Â¨',
            dd : '%d Ã Â¨Â¦Ã Â¨Â¿Ã Â¨Â¨',
            M : 'Ã Â¨ÂÃ Â©Â±Ã Â¨Â Ã Â¨Â®Ã Â¨Â¹Ã Â©ÂÃ Â¨Â¨Ã Â¨Â¾',
            MM : '%d Ã Â¨Â®Ã Â¨Â¹Ã Â©ÂÃ Â¨Â¨Ã Â©Â',
            y : 'Ã Â¨ÂÃ Â©Â±Ã Â¨Â Ã Â¨Â¸Ã Â¨Â¾Ã Â¨Â²',
            yy : '%d Ã Â¨Â¸Ã Â¨Â¾Ã Â¨Â²'
        },
        preparse: function (string) {
            return string.replace(/[Ã Â©Â§Ã Â©Â¨Ã Â©Â©Ã Â©ÂªÃ Â©Â«Ã Â©Â¬Ã Â©Â­Ã Â©Â®Ã Â©Â¯Ã Â©Â¦]/g, function (match) {
                return pa_in__numberMap[match];
            });
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return pa_in__symbolMap[match];
            });
        },
        // Punjabi notation for meridiems are quite fuzzy in practice. While there exists
        // a rigid notion of a 'Pahar' it is not used as rigidly in modern Punjabi.
        meridiemParse: /Ã Â¨Â°Ã Â¨Â¾Ã Â¨Â¤|Ã Â¨Â¸Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°|Ã Â¨Â¦Ã Â©ÂÃ Â¨ÂªÃ Â¨Â¹Ã Â¨Â¿Ã Â¨Â°|Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¾Ã Â¨Â®/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'Ã Â¨Â°Ã Â¨Â¾Ã Â¨Â¤') {
                return hour < 4 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â¨Â¸Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°') {
                return hour;
            } else if (meridiem === 'Ã Â¨Â¦Ã Â©ÂÃ Â¨ÂªÃ Â¨Â¹Ã Â¨Â¿Ã Â¨Â°') {
                return hour >= 10 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¾Ã Â¨Â®') {
                return hour + 12;
            }
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'Ã Â¨Â°Ã Â¨Â¾Ã Â¨Â¤';
            } else if (hour < 10) {
                return 'Ã Â¨Â¸Ã Â¨ÂµÃ Â©ÂÃ Â¨Â°';
            } else if (hour < 17) {
                return 'Ã Â¨Â¦Ã Â©ÂÃ Â¨ÂªÃ Â¨Â¹Ã Â¨Â¿Ã Â¨Â°';
            } else if (hour < 20) {
                return 'Ã Â¨Â¸Ã Â¨Â¼Ã Â¨Â¾Ã Â¨Â®';
            } else {
                return 'Ã Â¨Â°Ã Â¨Â¾Ã Â¨Â¤';
            }
        },
        week : {
            dow : 0, // Sunday is the first day of the week.
            doy : 6  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : polish (pl)
    //! author : Rafal Hirsz : https://github.com/evoL

    var monthsNominative = 'styczeÃÂ_luty_marzec_kwiecieÃÂ_maj_czerwiec_lipiec_sierpieÃÂ_wrzesieÃÂ_paÃÂºdziernik_listopad_grudzieÃÂ'.split('_'),
        monthsSubjective = 'stycznia_lutego_marca_kwietnia_maja_czerwca_lipca_sierpnia_wrzeÃÂnia_paÃÂºdziernika_listopada_grudnia'.split('_');
    function pl__plural(n) {
        return (n % 10 < 5) && (n % 10 > 1) && ((~~(n / 10) % 10) !== 1);
    }
    function pl__translate(number, withoutSuffix, key) {
        var result = number + ' ';
        switch (key) {
        case 'm':
            return withoutSuffix ? 'minuta' : 'minutÃÂ';
        case 'mm':
            return result + (pl__plural(number) ? 'minuty' : 'minut');
        case 'h':
            return withoutSuffix  ? 'godzina'  : 'godzinÃÂ';
        case 'hh':
            return result + (pl__plural(number) ? 'godziny' : 'godzin');
        case 'MM':
            return result + (pl__plural(number) ? 'miesiÃÂce' : 'miesiÃÂcy');
        case 'yy':
            return result + (pl__plural(number) ? 'lata' : 'lat');
        }
    }

    var pl = moment.defineLocale('pl', {
        months : function (momentToFormat, format) {
            if (format === '') {
                // Hack: if format empty we know this is used to generate
                // RegExp by moment. Give then back both valid forms of months
                // in RegExp ready format.
                return '(' + monthsSubjective[momentToFormat.month()] + '|' + monthsNominative[momentToFormat.month()] + ')';
            } else if (/D MMMM/.test(format)) {
                return monthsSubjective[momentToFormat.month()];
            } else {
                return monthsNominative[momentToFormat.month()];
            }
        },
        monthsShort : 'sty_lut_mar_kwi_maj_cze_lip_sie_wrz_paÃÂº_lis_gru'.split('_'),
        weekdays : 'niedziela_poniedziaÃÂek_wtorek_ÃÂroda_czwartek_piÃÂtek_sobota'.split('_'),
        weekdaysShort : 'nie_pon_wt_ÃÂr_czw_pt_sb'.split('_'),
        weekdaysMin : 'Nd_Pn_Wt_ÃÂr_Cz_Pt_So'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[DziÃÂ o] LT',
            nextDay: '[Jutro o] LT',
            nextWeek: '[W] dddd [o] LT',
            lastDay: '[Wczoraj o] LT',
            lastWeek: function () {
                switch (this.day()) {
                case 0:
                    return '[W zeszÃÂÃÂ niedzielÃÂ o] LT';
                case 3:
                    return '[W zeszÃÂÃÂ ÃÂrodÃÂ o] LT';
                case 6:
                    return '[W zeszÃÂÃÂ sobotÃÂ o] LT';
                default:
                    return '[W zeszÃÂy] dddd [o] LT';
                }
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'za %s',
            past : '%s temu',
            s : 'kilka sekund',
            m : pl__translate,
            mm : pl__translate,
            h : pl__translate,
            hh : pl__translate,
            d : '1 dzieÃÂ',
            dd : '%d dni',
            M : 'miesiÃÂc',
            MM : pl__translate,
            y : 'rok',
            yy : pl__translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : brazilian portuguese (pt-br)
    //! author : Caio Ribeiro Pereira : https://github.com/caio-ribeiro-pereira

    var pt_br = moment.defineLocale('pt-br', {
        months : 'Janeiro_Fevereiro_MarÃÂ§o_Abril_Maio_Junho_Julho_Agosto_Setembro_Outubro_Novembro_Dezembro'.split('_'),
        monthsShort : 'Jan_Fev_Mar_Abr_Mai_Jun_Jul_Ago_Set_Out_Nov_Dez'.split('_'),
        weekdays : 'Domingo_Segunda-feira_TerÃÂ§a-feira_Quarta-feira_Quinta-feira_Sexta-feira_SÃÂ¡bado'.split('_'),
        weekdaysShort : 'Dom_Seg_Ter_Qua_Qui_Sex_SÃÂ¡b'.split('_'),
        weekdaysMin : 'Dom_2ÃÂª_3ÃÂª_4ÃÂª_5ÃÂª_6ÃÂª_SÃÂ¡b'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D [de] MMMM [de] YYYY',
            LLL : 'D [de] MMMM [de] YYYY [ÃÂ s] HH:mm',
            LLLL : 'dddd, D [de] MMMM [de] YYYY [ÃÂ s] HH:mm'
        },
        calendar : {
            sameDay: '[Hoje ÃÂ s] LT',
            nextDay: '[AmanhÃÂ£ ÃÂ s] LT',
            nextWeek: 'dddd [ÃÂ s] LT',
            lastDay: '[Ontem ÃÂ s] LT',
            lastWeek: function () {
                return (this.day() === 0 || this.day() === 6) ?
                    '[ÃÂltimo] dddd [ÃÂ s] LT' : // Saturday + Sunday
                    '[ÃÂltima] dddd [ÃÂ s] LT'; // Monday - Friday
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'em %s',
            past : '%s atrÃÂ¡s',
            s : 'poucos segundos',
            m : 'um minuto',
            mm : '%d minutos',
            h : 'uma hora',
            hh : '%d horas',
            d : 'um dia',
            dd : '%d dias',
            M : 'um mÃÂªs',
            MM : '%d meses',
            y : 'um ano',
            yy : '%d anos'
        },
        ordinalParse: /\d{1,2}ÃÂº/,
        ordinal : '%dÃÂº'
    });

    //! moment.js locale configuration
    //! locale : portuguese (pt)
    //! author : Jefferson : https://github.com/jalex79

    var pt = moment.defineLocale('pt', {
        months : 'Janeiro_Fevereiro_MarÃÂ§o_Abril_Maio_Junho_Julho_Agosto_Setembro_Outubro_Novembro_Dezembro'.split('_'),
        monthsShort : 'Jan_Fev_Mar_Abr_Mai_Jun_Jul_Ago_Set_Out_Nov_Dez'.split('_'),
        weekdays : 'Domingo_Segunda-Feira_TerÃÂ§a-Feira_Quarta-Feira_Quinta-Feira_Sexta-Feira_SÃÂ¡bado'.split('_'),
        weekdaysShort : 'Dom_Seg_Ter_Qua_Qui_Sex_SÃÂ¡b'.split('_'),
        weekdaysMin : 'Dom_2ÃÂª_3ÃÂª_4ÃÂª_5ÃÂª_6ÃÂª_SÃÂ¡b'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D [de] MMMM [de] YYYY',
            LLL : 'D [de] MMMM [de] YYYY HH:mm',
            LLLL : 'dddd, D [de] MMMM [de] YYYY HH:mm'
        },
        calendar : {
            sameDay: '[Hoje ÃÂ s] LT',
            nextDay: '[AmanhÃÂ£ ÃÂ s] LT',
            nextWeek: 'dddd [ÃÂ s] LT',
            lastDay: '[Ontem ÃÂ s] LT',
            lastWeek: function () {
                return (this.day() === 0 || this.day() === 6) ?
                    '[ÃÂltimo] dddd [ÃÂ s] LT' : // Saturday + Sunday
                    '[ÃÂltima] dddd [ÃÂ s] LT'; // Monday - Friday
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'em %s',
            past : 'hÃÂ¡ %s',
            s : 'segundos',
            m : 'um minuto',
            mm : '%d minutos',
            h : 'uma hora',
            hh : '%d horas',
            d : 'um dia',
            dd : '%d dias',
            M : 'um mÃÂªs',
            MM : '%d meses',
            y : 'um ano',
            yy : '%d anos'
        },
        ordinalParse: /\d{1,2}ÃÂº/,
        ordinal : '%dÃÂº',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : romanian (ro)
    //! author : Vlad Gurdiga : https://github.com/gurdiga
    //! author : Valentin Agachi : https://github.com/avaly

    function ro__relativeTimeWithPlural(number, withoutSuffix, key) {
        var format = {
                'mm': 'minute',
                'hh': 'ore',
                'dd': 'zile',
                'MM': 'luni',
                'yy': 'ani'
            },
            separator = ' ';
        if (number % 100 >= 20 || (number >= 100 && number % 100 === 0)) {
            separator = ' de ';
        }
        return number + separator + format[key];
    }

    var ro = moment.defineLocale('ro', {
        months : 'ianuarie_februarie_martie_aprilie_mai_iunie_iulie_august_septembrie_octombrie_noiembrie_decembrie'.split('_'),
        monthsShort : 'ian._febr._mart._apr._mai_iun._iul._aug._sept._oct._nov._dec.'.split('_'),
        monthsParseExact: true,
        weekdays : 'duminicÃÂ_luni_marÃÂi_miercuri_joi_vineri_sÃÂ¢mbÃÂtÃÂ'.split('_'),
        weekdaysShort : 'Dum_Lun_Mar_Mie_Joi_Vin_SÃÂ¢m'.split('_'),
        weekdaysMin : 'Du_Lu_Ma_Mi_Jo_Vi_SÃÂ¢'.split('_'),
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY H:mm',
            LLLL : 'dddd, D MMMM YYYY H:mm'
        },
        calendar : {
            sameDay: '[azi la] LT',
            nextDay: '[mÃÂ¢ine la] LT',
            nextWeek: 'dddd [la] LT',
            lastDay: '[ieri la] LT',
            lastWeek: '[fosta] dddd [la] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'peste %s',
            past : '%s ÃÂ®n urmÃÂ',
            s : 'cÃÂ¢teva secunde',
            m : 'un minut',
            mm : ro__relativeTimeWithPlural,
            h : 'o orÃÂ',
            hh : ro__relativeTimeWithPlural,
            d : 'o zi',
            dd : ro__relativeTimeWithPlural,
            M : 'o lunÃÂ',
            MM : ro__relativeTimeWithPlural,
            y : 'un an',
            yy : ro__relativeTimeWithPlural
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : russian (ru)
    //! author : Viktorminator : https://github.com/Viktorminator
    //! Author : Menelion ElensÃÂºle : https://github.com/Oire
    //! author : ÃÂÃÂ¾ÃÂÃÂµÃÂ½ÃÂ±ÃÂµÃÂÃÂ³ ÃÂÃÂ°ÃÂÃÂº : https://github.com/socketpair

    function ru__plural(word, num) {
        var forms = word.split('_');
        return num % 10 === 1 && num % 100 !== 11 ? forms[0] : (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) ? forms[1] : forms[2]);
    }
    function ru__relativeTimeWithPlural(number, withoutSuffix, key) {
        var format = {
            'mm': withoutSuffix ? 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ°_ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ_ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ' : 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ_ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ_ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ',
            'hh': 'ÃÂÃÂ°ÃÂ_ÃÂÃÂ°ÃÂÃÂ°_ÃÂÃÂ°ÃÂÃÂ¾ÃÂ²',
            'dd': 'ÃÂ´ÃÂµÃÂ½ÃÂ_ÃÂ´ÃÂ½ÃÂ_ÃÂ´ÃÂ½ÃÂµÃÂ¹',
            'MM': 'ÃÂ¼ÃÂµÃÂÃÂÃÂ_ÃÂ¼ÃÂµÃÂÃÂÃÂÃÂ°_ÃÂ¼ÃÂµÃÂÃÂÃÂÃÂµÃÂ²',
            'yy': 'ÃÂ³ÃÂ¾ÃÂ´_ÃÂ³ÃÂ¾ÃÂ´ÃÂ°_ÃÂ»ÃÂµÃÂ'
        };
        if (key === 'm') {
            return withoutSuffix ? 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ°' : 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ';
        }
        else {
            return number + ' ' + ru__plural(format[key], +number);
        }
    }
    var monthsParse = [/^ÃÂÃÂ½ÃÂ²/i, /^ÃÂÃÂµÃÂ²/i, /^ÃÂ¼ÃÂ°ÃÂ/i, /^ÃÂ°ÃÂ¿ÃÂ/i, /^ÃÂ¼ÃÂ°[ÃÂ¹ÃÂ]/i, /^ÃÂ¸ÃÂÃÂ½/i, /^ÃÂ¸ÃÂÃÂ»/i, /^ÃÂ°ÃÂ²ÃÂ³/i, /^ÃÂÃÂµÃÂ½/i, /^ÃÂ¾ÃÂºÃÂ/i, /^ÃÂ½ÃÂ¾ÃÂ/i, /^ÃÂ´ÃÂµÃÂº/i];

    // http://new.gramota.ru/spravka/rules/139-prop : ÃÂ§ 103
    // ÃÂ¡ÃÂ¾ÃÂºÃÂÃÂ°ÃÂÃÂµÃÂ½ÃÂ¸ÃÂ ÃÂ¼ÃÂµÃÂÃÂÃÂÃÂµÃÂ²: http://new.gramota.ru/spravka/buro/search-answer?s=242637
    // CLDR data:          http://www.unicode.org/cldr/charts/28/summary/ru.html#1753
    var ru = moment.defineLocale('ru', {
        months : {
            format: 'ÃÂÃÂ½ÃÂ²ÃÂ°ÃÂÃÂ_ÃÂÃÂµÃÂ²ÃÂÃÂ°ÃÂ»ÃÂ_ÃÂ¼ÃÂ°ÃÂÃÂÃÂ°_ÃÂ°ÃÂ¿ÃÂÃÂµÃÂ»ÃÂ_ÃÂ¼ÃÂ°ÃÂ_ÃÂ¸ÃÂÃÂ½ÃÂ_ÃÂ¸ÃÂÃÂ»ÃÂ_ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂÃÂ°_ÃÂÃÂµÃÂ½ÃÂÃÂÃÂ±ÃÂÃÂ_ÃÂ¾ÃÂºÃÂÃÂÃÂ±ÃÂÃÂ_ÃÂ½ÃÂ¾ÃÂÃÂ±ÃÂÃÂ_ÃÂ´ÃÂµÃÂºÃÂ°ÃÂ±ÃÂÃÂ'.split('_'),
            standalone: 'ÃÂÃÂ½ÃÂ²ÃÂ°ÃÂÃÂ_ÃÂÃÂµÃÂ²ÃÂÃÂ°ÃÂ»ÃÂ_ÃÂ¼ÃÂ°ÃÂÃÂ_ÃÂ°ÃÂ¿ÃÂÃÂµÃÂ»ÃÂ_ÃÂ¼ÃÂ°ÃÂ¹_ÃÂ¸ÃÂÃÂ½ÃÂ_ÃÂ¸ÃÂÃÂ»ÃÂ_ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ½ÃÂÃÂÃÂ±ÃÂÃÂ_ÃÂ¾ÃÂºÃÂÃÂÃÂ±ÃÂÃÂ_ÃÂ½ÃÂ¾ÃÂÃÂ±ÃÂÃÂ_ÃÂ´ÃÂµÃÂºÃÂ°ÃÂ±ÃÂÃÂ'.split('_')
        },
        monthsShort : {
            // ÃÂ¿ÃÂ¾ CLDR ÃÂ¸ÃÂ¼ÃÂµÃÂ½ÃÂ½ÃÂ¾ "ÃÂ¸ÃÂÃÂ»." ÃÂ¸ "ÃÂ¸ÃÂÃÂ½.", ÃÂ½ÃÂ¾ ÃÂºÃÂ°ÃÂºÃÂ¾ÃÂ¹ ÃÂÃÂ¼ÃÂÃÂÃÂ» ÃÂ¼ÃÂµÃÂ½ÃÂÃÂÃÂ ÃÂ±ÃÂÃÂºÃÂ²ÃÂ ÃÂ½ÃÂ° ÃÂÃÂ¾ÃÂÃÂºÃÂ ?
            format: 'ÃÂÃÂ½ÃÂ²._ÃÂÃÂµÃÂ²ÃÂ._ÃÂ¼ÃÂ°ÃÂ._ÃÂ°ÃÂ¿ÃÂ._ÃÂ¼ÃÂ°ÃÂ_ÃÂ¸ÃÂÃÂ½ÃÂ_ÃÂ¸ÃÂÃÂ»ÃÂ_ÃÂ°ÃÂ²ÃÂ³._ÃÂÃÂµÃÂ½ÃÂ._ÃÂ¾ÃÂºÃÂ._ÃÂ½ÃÂ¾ÃÂÃÂ±._ÃÂ´ÃÂµÃÂº.'.split('_'),
            standalone: 'ÃÂÃÂ½ÃÂ²._ÃÂÃÂµÃÂ²ÃÂ._ÃÂ¼ÃÂ°ÃÂÃÂ_ÃÂ°ÃÂ¿ÃÂ._ÃÂ¼ÃÂ°ÃÂ¹_ÃÂ¸ÃÂÃÂ½ÃÂ_ÃÂ¸ÃÂÃÂ»ÃÂ_ÃÂ°ÃÂ²ÃÂ³._ÃÂÃÂµÃÂ½ÃÂ._ÃÂ¾ÃÂºÃÂ._ÃÂ½ÃÂ¾ÃÂÃÂ±._ÃÂ´ÃÂµÃÂº.'.split('_')
        },
        weekdays : {
            standalone: 'ÃÂ²ÃÂ¾ÃÂÃÂºÃÂÃÂµÃÂÃÂµÃÂ½ÃÂÃÂµ_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂµÃÂ»ÃÂÃÂ½ÃÂ¸ÃÂº_ÃÂ²ÃÂÃÂ¾ÃÂÃÂ½ÃÂ¸ÃÂº_ÃÂÃÂÃÂµÃÂ´ÃÂ°_ÃÂÃÂµÃÂÃÂ²ÃÂµÃÂÃÂ³_ÃÂ¿ÃÂÃÂÃÂ½ÃÂ¸ÃÂÃÂ°_ÃÂÃÂÃÂ±ÃÂ±ÃÂ¾ÃÂÃÂ°'.split('_'),
            format: 'ÃÂ²ÃÂ¾ÃÂÃÂºÃÂÃÂµÃÂÃÂµÃÂ½ÃÂÃÂµ_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂµÃÂ»ÃÂÃÂ½ÃÂ¸ÃÂº_ÃÂ²ÃÂÃÂ¾ÃÂÃÂ½ÃÂ¸ÃÂº_ÃÂÃÂÃÂµÃÂ´ÃÂ_ÃÂÃÂµÃÂÃÂ²ÃÂµÃÂÃÂ³_ÃÂ¿ÃÂÃÂÃÂ½ÃÂ¸ÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂ±ÃÂ¾ÃÂÃÂ'.split('_'),
            isFormat: /\[ ?[ÃÂÃÂ²] ?(?:ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂÃÂ|ÃÂÃÂ»ÃÂµÃÂ´ÃÂÃÂÃÂÃÂÃÂ|ÃÂÃÂÃÂ)? ?\] ?dddd/
        },
        weekdaysShort : 'ÃÂ²ÃÂ_ÃÂ¿ÃÂ½_ÃÂ²ÃÂ_ÃÂÃÂ_ÃÂÃÂ_ÃÂ¿ÃÂ_ÃÂÃÂ±'.split('_'),
        weekdaysMin : 'ÃÂ²ÃÂ_ÃÂ¿ÃÂ½_ÃÂ²ÃÂ_ÃÂÃÂ_ÃÂÃÂ_ÃÂ¿ÃÂ_ÃÂÃÂ±'.split('_'),
        monthsParse : monthsParse,
        longMonthsParse : monthsParse,
        shortMonthsParse : monthsParse,
        monthsRegex: /^(ÃÂÃÂµÃÂ½ÃÂÃÂÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂ¾ÃÂºÃÂÃÂÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂ´ÃÂµÃÂºÃÂ°ÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂÃÂµÃÂ²ÃÂÃÂ°ÃÂ»[ÃÂÃÂ]|ÃÂÃÂ½ÃÂ²ÃÂ°ÃÂ[ÃÂÃÂ]|ÃÂ°ÃÂ¿ÃÂÃÂµÃÂ»[ÃÂÃÂ]|ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂÃÂ°?|ÃÂ½ÃÂ¾ÃÂÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂÃÂµÃÂ½ÃÂ\.|ÃÂÃÂµÃÂ²ÃÂ\.|ÃÂ½ÃÂ¾ÃÂÃÂ±\.|ÃÂ¸ÃÂÃÂ½ÃÂ|ÃÂÃÂ½ÃÂ².|ÃÂ¸ÃÂÃÂ»ÃÂ|ÃÂ´ÃÂµÃÂº.|ÃÂ°ÃÂ²ÃÂ³.|ÃÂ°ÃÂ¿ÃÂ.|ÃÂ¼ÃÂ°ÃÂÃÂÃÂ°|ÃÂ¼ÃÂ°ÃÂ[.ÃÂ]|ÃÂ¾ÃÂºÃÂ.|ÃÂ¸ÃÂÃÂ½[ÃÂÃÂ]|ÃÂ¸ÃÂÃÂ»[ÃÂÃÂ]|ÃÂ¼ÃÂ°[ÃÂÃÂ¹])/i,
        monthsShortRegex: /^(ÃÂÃÂµÃÂ½ÃÂÃÂÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂ¾ÃÂºÃÂÃÂÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂ´ÃÂµÃÂºÃÂ°ÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂÃÂµÃÂ²ÃÂÃÂ°ÃÂ»[ÃÂÃÂ]|ÃÂÃÂ½ÃÂ²ÃÂ°ÃÂ[ÃÂÃÂ]|ÃÂ°ÃÂ¿ÃÂÃÂµÃÂ»[ÃÂÃÂ]|ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂÃÂ°?|ÃÂ½ÃÂ¾ÃÂÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂÃÂµÃÂ½ÃÂ\.|ÃÂÃÂµÃÂ²ÃÂ\.|ÃÂ½ÃÂ¾ÃÂÃÂ±\.|ÃÂ¸ÃÂÃÂ½ÃÂ|ÃÂÃÂ½ÃÂ².|ÃÂ¸ÃÂÃÂ»ÃÂ|ÃÂ´ÃÂµÃÂº.|ÃÂ°ÃÂ²ÃÂ³.|ÃÂ°ÃÂ¿ÃÂ.|ÃÂ¼ÃÂ°ÃÂÃÂÃÂ°|ÃÂ¼ÃÂ°ÃÂ[.ÃÂ]|ÃÂ¾ÃÂºÃÂ.|ÃÂ¸ÃÂÃÂ½[ÃÂÃÂ]|ÃÂ¸ÃÂÃÂ»[ÃÂÃÂ]|ÃÂ¼ÃÂ°[ÃÂÃÂ¹])/i,
        monthsStrictRegex: /^(ÃÂÃÂµÃÂ½ÃÂÃÂÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂ¾ÃÂºÃÂÃÂÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂ´ÃÂµÃÂºÃÂ°ÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂÃÂµÃÂ²ÃÂÃÂ°ÃÂ»[ÃÂÃÂ]|ÃÂÃÂ½ÃÂ²ÃÂ°ÃÂ[ÃÂÃÂ]|ÃÂ°ÃÂ¿ÃÂÃÂµÃÂ»[ÃÂÃÂ]|ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂÃÂ°?|ÃÂ½ÃÂ¾ÃÂÃÂ±ÃÂ[ÃÂÃÂ]|ÃÂ¼ÃÂ°ÃÂÃÂÃÂ°?|ÃÂ¸ÃÂÃÂ½[ÃÂÃÂ]|ÃÂ¸ÃÂÃÂ»[ÃÂÃÂ]|ÃÂ¼ÃÂ°[ÃÂÃÂ¹])/i,
        monthsShortStrictRegex: /^(ÃÂ½ÃÂ¾ÃÂÃÂ±\.|ÃÂÃÂµÃÂ²ÃÂ\.|ÃÂÃÂµÃÂ½ÃÂ\.|ÃÂ¸ÃÂÃÂ»ÃÂ|ÃÂÃÂ½ÃÂ²\.|ÃÂ¸ÃÂÃÂ½[ÃÂÃÂ]|ÃÂ¼ÃÂ°ÃÂ[.ÃÂ]|ÃÂ°ÃÂ²ÃÂ³\.|ÃÂ°ÃÂ¿ÃÂ\.|ÃÂ¾ÃÂºÃÂ\.|ÃÂ´ÃÂµÃÂº\.|ÃÂ¼ÃÂ°[ÃÂÃÂ¹])/i,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY ÃÂ³.',
            LLL : 'D MMMM YYYY ÃÂ³., HH:mm',
            LLLL : 'dddd, D MMMM YYYY ÃÂ³., HH:mm'
        },
        calendar : {
            sameDay: '[ÃÂ¡ÃÂµÃÂ³ÃÂ¾ÃÂ´ÃÂ½ÃÂ ÃÂ²] LT',
            nextDay: '[ÃÂÃÂ°ÃÂ²ÃÂÃÂÃÂ° ÃÂ²] LT',
            lastDay: '[ÃÂÃÂÃÂµÃÂÃÂ° ÃÂ²] LT',
            nextWeek: function (now) {
                if (now.week() !== this.week()) {
                    switch (this.day()) {
                    case 0:
                        return '[ÃÂ ÃÂÃÂ»ÃÂµÃÂ´ÃÂÃÂÃÂÃÂµÃÂµ] dddd [ÃÂ²] LT';
                    case 1:
                    case 2:
                    case 4:
                        return '[ÃÂ ÃÂÃÂ»ÃÂµÃÂ´ÃÂÃÂÃÂÃÂ¸ÃÂ¹] dddd [ÃÂ²] LT';
                    case 3:
                    case 5:
                    case 6:
                        return '[ÃÂ ÃÂÃÂ»ÃÂµÃÂ´ÃÂÃÂÃÂÃÂÃÂ] dddd [ÃÂ²] LT';
                    }
                } else {
                    if (this.day() === 2) {
                        return '[ÃÂÃÂ¾] dddd [ÃÂ²] LT';
                    } else {
                        return '[ÃÂ] dddd [ÃÂ²] LT';
                    }
                }
            },
            lastWeek: function (now) {
                if (now.week() !== this.week()) {
                    switch (this.day()) {
                    case 0:
                        return '[ÃÂ ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂ¾ÃÂµ] dddd [ÃÂ²] LT';
                    case 1:
                    case 2:
                    case 4:
                        return '[ÃÂ ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂÃÂ¹] dddd [ÃÂ²] LT';
                    case 3:
                    case 5:
                    case 6:
                        return '[ÃÂ ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂÃÂ] dddd [ÃÂ²] LT';
                    }
                } else {
                    if (this.day() === 2) {
                        return '[ÃÂÃÂ¾] dddd [ÃÂ²] LT';
                    } else {
                        return '[ÃÂ] dddd [ÃÂ²] LT';
                    }
                }
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'ÃÂÃÂµÃÂÃÂµÃÂ· %s',
            past : '%s ÃÂ½ÃÂ°ÃÂ·ÃÂ°ÃÂ´',
            s : 'ÃÂ½ÃÂµÃÂÃÂºÃÂ¾ÃÂ»ÃÂÃÂºÃÂ¾ ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´',
            m : ru__relativeTimeWithPlural,
            mm : ru__relativeTimeWithPlural,
            h : 'ÃÂÃÂ°ÃÂ',
            hh : ru__relativeTimeWithPlural,
            d : 'ÃÂ´ÃÂµÃÂ½ÃÂ',
            dd : ru__relativeTimeWithPlural,
            M : 'ÃÂ¼ÃÂµÃÂÃÂÃÂ',
            MM : ru__relativeTimeWithPlural,
            y : 'ÃÂ³ÃÂ¾ÃÂ´',
            yy : ru__relativeTimeWithPlural
        },
        meridiemParse: /ÃÂ½ÃÂ¾ÃÂÃÂ¸|ÃÂÃÂÃÂÃÂ°|ÃÂ´ÃÂ½ÃÂ|ÃÂ²ÃÂµÃÂÃÂµÃÂÃÂ°/i,
        isPM : function (input) {
            return /^(ÃÂ´ÃÂ½ÃÂ|ÃÂ²ÃÂµÃÂÃÂµÃÂÃÂ°)$/.test(input);
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'ÃÂ½ÃÂ¾ÃÂÃÂ¸';
            } else if (hour < 12) {
                return 'ÃÂÃÂÃÂÃÂ°';
            } else if (hour < 17) {
                return 'ÃÂ´ÃÂ½ÃÂ';
            } else {
                return 'ÃÂ²ÃÂµÃÂÃÂµÃÂÃÂ°';
            }
        },
        ordinalParse: /\d{1,2}-(ÃÂ¹|ÃÂ³ÃÂ¾|ÃÂ)/,
        ordinal: function (number, period) {
            switch (period) {
            case 'M':
            case 'd':
            case 'DDD':
                return number + '-ÃÂ¹';
            case 'D':
                return number + '-ÃÂ³ÃÂ¾';
            case 'w':
            case 'W':
                return number + '-ÃÂ';
            default:
                return number;
            }
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Northern Sami (se)
    //! authors : BÃÂ¥rd Rolstad Henriksen : https://github.com/karamell


    var se = moment.defineLocale('se', {
        months : 'oÃÂÃÂajagemÃÂ¡nnu_guovvamÃÂ¡nnu_njukÃÂamÃÂ¡nnu_cuoÃÂomÃÂ¡nnu_miessemÃÂ¡nnu_geassemÃÂ¡nnu_suoidnemÃÂ¡nnu_borgemÃÂ¡nnu_ÃÂakÃÂamÃÂ¡nnu_golggotmÃÂ¡nnu_skÃÂ¡bmamÃÂ¡nnu_juovlamÃÂ¡nnu'.split('_'),
        monthsShort : 'oÃÂÃÂj_guov_njuk_cuo_mies_geas_suoi_borg_ÃÂakÃÂ_golg_skÃÂ¡b_juov'.split('_'),
        weekdays : 'sotnabeaivi_vuossÃÂ¡rga_maÃÂÃÂebÃÂ¡rga_gaskavahkku_duorastat_bearjadat_lÃÂ¡vvardat'.split('_'),
        weekdaysShort : 'sotn_vuos_maÃÂ_gask_duor_bear_lÃÂ¡v'.split('_'),
        weekdaysMin : 's_v_m_g_d_b_L'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'MMMM D. [b.] YYYY',
            LLL : 'MMMM D. [b.] YYYY [ti.] HH:mm',
            LLLL : 'dddd, MMMM D. [b.] YYYY [ti.] HH:mm'
        },
        calendar : {
            sameDay: '[otne ti] LT',
            nextDay: '[ihttin ti] LT',
            nextWeek: 'dddd [ti] LT',
            lastDay: '[ikte ti] LT',
            lastWeek: '[ovddit] dddd [ti] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : '%s geaÃÂ¾es',
            past : 'maÃÂit %s',
            s : 'moadde sekunddat',
            m : 'okta minuhta',
            mm : '%d minuhtat',
            h : 'okta diimmu',
            hh : '%d diimmut',
            d : 'okta beaivi',
            dd : '%d beaivvit',
            M : 'okta mÃÂ¡nnu',
            MM : '%d mÃÂ¡nut',
            y : 'okta jahki',
            yy : '%d jagit'
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Sinhalese (si)
    //! author : Sampath Sitinamaluwa : https://github.com/sampathsris

    /*jshint -W100*/
    var si = moment.defineLocale('si', {
        months : 'Ã Â¶Â¢Ã Â¶Â±Ã Â·ÂÃ Â·ÂÃ Â¶Â»Ã Â·Â_Ã Â¶Â´Ã Â·ÂÃ Â¶Â¶Ã Â¶Â»Ã Â·ÂÃ Â·ÂÃ Â¶Â»Ã Â·Â_Ã Â¶Â¸Ã Â·ÂÃ Â¶Â»Ã Â·ÂÃ Â¶Â­Ã Â·Â_Ã Â¶ÂÃ Â¶Â´Ã Â·ÂÃ¢ÂÂÃ Â¶Â»Ã Â·ÂÃ Â¶Â½Ã Â·Â_Ã Â¶Â¸Ã Â·ÂÃ Â¶ÂºÃ Â·Â_Ã Â¶Â¢Ã Â·ÂÃ Â¶Â±Ã Â·Â_Ã Â¶Â¢Ã Â·ÂÃ Â¶Â½Ã Â·Â_Ã Â¶ÂÃ Â¶ÂÃ Â·ÂÃ Â·ÂÃ Â·ÂÃ Â¶Â­Ã Â·Â_Ã Â·ÂÃ Â·ÂÃ Â¶Â´Ã Â·ÂÃ Â¶Â­Ã Â·ÂÃ Â¶Â¸Ã Â·ÂÃ Â¶Â¶Ã Â¶Â»Ã Â·Â_Ã Â¶ÂÃ Â¶ÂÃ Â·ÂÃ Â¶Â­Ã Â·ÂÃ Â¶Â¶Ã Â¶Â»Ã Â·Â_Ã Â¶Â±Ã Â·ÂÃ Â·ÂÃ Â·ÂÃ Â¶Â¸Ã Â·ÂÃ Â¶Â¶Ã Â¶Â»Ã Â·Â_Ã Â¶Â¯Ã Â·ÂÃ Â·ÂÃ Â·ÂÃ Â¶Â¸Ã Â·ÂÃ Â¶Â¶Ã Â¶Â»Ã Â·Â'.split('_'),
        monthsShort : 'Ã Â¶Â¢Ã Â¶Â±_Ã Â¶Â´Ã Â·ÂÃ Â¶Â¶_Ã Â¶Â¸Ã Â·ÂÃ Â¶Â»Ã Â·Â_Ã Â¶ÂÃ Â¶Â´Ã Â·Â_Ã Â¶Â¸Ã Â·ÂÃ Â¶ÂºÃ Â·Â_Ã Â¶Â¢Ã Â·ÂÃ Â¶Â±Ã Â·Â_Ã Â¶Â¢Ã Â·ÂÃ Â¶Â½Ã Â·Â_Ã Â¶ÂÃ Â¶ÂÃ Â·Â_Ã Â·ÂÃ Â·ÂÃ Â¶Â´Ã Â·Â_Ã Â¶ÂÃ Â¶ÂÃ Â·Â_Ã Â¶Â±Ã Â·ÂÃ Â·ÂÃ Â·Â_Ã Â¶Â¯Ã Â·ÂÃ Â·ÂÃ Â·Â'.split('_'),
        weekdays : 'Ã Â¶ÂÃ Â¶Â»Ã Â·ÂÃ Â¶Â¯Ã Â·Â_Ã Â·ÂÃ Â¶Â³Ã Â·ÂÃ Â¶Â¯Ã Â·Â_Ã Â¶ÂÃ Â¶ÂÃ Â·ÂÃ Â¶Â»Ã Â·ÂÃ Â·ÂÃ Â·ÂÃ Â¶Â¯Ã Â·Â_Ã Â¶Â¶Ã Â¶Â¯Ã Â·ÂÃ Â¶Â¯Ã Â·Â_Ã Â¶Â¶Ã Â·ÂÃ¢ÂÂÃ Â¶Â»Ã Â·ÂÃ Â·ÂÃ Â·ÂÃ Â¶Â´Ã Â¶Â­Ã Â·ÂÃ Â¶Â±Ã Â·ÂÃ Â¶Â¯Ã Â·Â_Ã Â·ÂÃ Â·ÂÃ Â¶ÂÃ Â·ÂÃ Â¶Â»Ã Â·ÂÃ Â¶Â¯Ã Â·Â_Ã Â·ÂÃ Â·ÂÃ Â¶Â±Ã Â·ÂÃ Â·ÂÃ Â¶Â»Ã Â·ÂÃ Â¶Â¯Ã Â·Â'.split('_'),
        weekdaysShort : 'Ã Â¶ÂÃ Â¶Â»Ã Â·Â_Ã Â·ÂÃ Â¶Â³Ã Â·Â_Ã Â¶ÂÃ Â¶Â_Ã Â¶Â¶Ã Â¶Â¯Ã Â·Â_Ã Â¶Â¶Ã Â·ÂÃ¢ÂÂÃ Â¶Â»Ã Â·Â_Ã Â·ÂÃ Â·ÂÃ Â¶ÂÃ Â·Â_Ã Â·ÂÃ Â·ÂÃ Â¶Â±'.split('_'),
        weekdaysMin : 'Ã Â¶Â_Ã Â·Â_Ã Â¶Â_Ã Â¶Â¶_Ã Â¶Â¶Ã Â·ÂÃ¢ÂÂÃ Â¶Â»_Ã Â·ÂÃ Â·Â_Ã Â·ÂÃ Â·Â'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'a h:mm',
            LTS : 'a h:mm:ss',
            L : 'YYYY/MM/DD',
            LL : 'YYYY MMMM D',
            LLL : 'YYYY MMMM D, a h:mm',
            LLLL : 'YYYY MMMM D [Ã Â·ÂÃ Â·ÂÃ Â¶Â±Ã Â·Â] dddd, a h:mm:ss'
        },
        calendar : {
            sameDay : '[Ã Â¶ÂÃ Â¶Â¯] LT[Ã Â¶Â§]',
            nextDay : '[Ã Â·ÂÃ Â·ÂÃ Â¶Â§] LT[Ã Â¶Â§]',
            nextWeek : 'dddd LT[Ã Â¶Â§]',
            lastDay : '[Ã Â¶ÂÃ Â¶ÂºÃ Â·Â] LT[Ã Â¶Â§]',
            lastWeek : '[Ã Â¶Â´Ã Â·ÂÃ Â·ÂÃ Â¶ÂÃ Â·ÂÃ Â¶Âº] dddd LT[Ã Â¶Â§]',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%sÃ Â¶ÂÃ Â·ÂÃ Â¶Â±Ã Â·Â',
            past : '%sÃ Â¶ÂÃ Â¶Â§ Ã Â¶Â´Ã Â·ÂÃ Â¶Â»',
            s : 'Ã Â¶Â­Ã Â¶Â­Ã Â·ÂÃ Â¶Â´Ã Â¶Â» Ã Â¶ÂÃ Â·ÂÃ Â·ÂÃ Â·ÂÃ Â¶Â´Ã Â¶Âº',
            m : 'Ã Â¶Â¸Ã Â·ÂÃ Â¶Â±Ã Â·ÂÃ Â¶Â­Ã Â·ÂÃ Â¶Â­Ã Â·ÂÃ Â·Â',
            mm : 'Ã Â¶Â¸Ã Â·ÂÃ Â¶Â±Ã Â·ÂÃ Â¶Â­Ã Â·ÂÃ Â¶Â­Ã Â·Â %d',
            h : 'Ã Â¶Â´Ã Â·ÂÃ Â¶Âº',
            hh : 'Ã Â¶Â´Ã Â·ÂÃ Â¶Âº %d',
            d : 'Ã Â¶Â¯Ã Â·ÂÃ Â¶Â±Ã Â¶Âº',
            dd : 'Ã Â¶Â¯Ã Â·ÂÃ Â¶Â± %d',
            M : 'Ã Â¶Â¸Ã Â·ÂÃ Â·ÂÃ Â¶Âº',
            MM : 'Ã Â¶Â¸Ã Â·ÂÃ Â·Â %d',
            y : 'Ã Â·ÂÃ Â·ÂÃ Â¶Â»',
            yy : 'Ã Â·ÂÃ Â·ÂÃ Â¶Â» %d'
        },
        ordinalParse: /\d{1,2} Ã Â·ÂÃ Â·ÂÃ Â¶Â±Ã Â·Â/,
        ordinal : function (number) {
            return number + ' Ã Â·ÂÃ Â·ÂÃ Â¶Â±Ã Â·Â';
        },
        meridiemParse : /Ã Â¶Â´Ã Â·ÂÃ Â¶Â» Ã Â·ÂÃ Â¶Â»Ã Â·Â|Ã Â¶Â´Ã Â·ÂÃ Â·Â Ã Â·ÂÃ Â¶Â»Ã Â·Â|Ã Â¶Â´Ã Â·Â.Ã Â·Â|Ã Â¶Â´.Ã Â·Â./,
        isPM : function (input) {
            return input === 'Ã Â¶Â´.Ã Â·Â.' || input === 'Ã Â¶Â´Ã Â·ÂÃ Â·Â Ã Â·ÂÃ Â¶Â»Ã Â·Â';
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours > 11) {
                return isLower ? 'Ã Â¶Â´.Ã Â·Â.' : 'Ã Â¶Â´Ã Â·ÂÃ Â·Â Ã Â·ÂÃ Â¶Â»Ã Â·Â';
            } else {
                return isLower ? 'Ã Â¶Â´Ã Â·Â.Ã Â·Â.' : 'Ã Â¶Â´Ã Â·ÂÃ Â¶Â» Ã Â·ÂÃ Â¶Â»Ã Â·Â';
            }
        }
    });

    //! moment.js locale configuration
    //! locale : slovak (sk)
    //! author : Martin Minka : https://github.com/k2s
    //! based on work of petrbela : https://github.com/petrbela

    var sk__months = 'januÃÂ¡r_februÃÂ¡r_marec_aprÃÂ­l_mÃÂ¡j_jÃÂºn_jÃÂºl_august_september_oktÃÂ³ber_november_december'.split('_'),
        sk__monthsShort = 'jan_feb_mar_apr_mÃÂ¡j_jÃÂºn_jÃÂºl_aug_sep_okt_nov_dec'.split('_');
    function sk__plural(n) {
        return (n > 1) && (n < 5);
    }
    function sk__translate(number, withoutSuffix, key, isFuture) {
        var result = number + ' ';
        switch (key) {
        case 's':  // a few seconds / in a few seconds / a few seconds ago
            return (withoutSuffix || isFuture) ? 'pÃÂ¡r sekÃÂºnd' : 'pÃÂ¡r sekundami';
        case 'm':  // a minute / in a minute / a minute ago
            return withoutSuffix ? 'minÃÂºta' : (isFuture ? 'minÃÂºtu' : 'minÃÂºtou');
        case 'mm': // 9 minutes / in 9 minutes / 9 minutes ago
            if (withoutSuffix || isFuture) {
                return result + (sk__plural(number) ? 'minÃÂºty' : 'minÃÂºt');
            } else {
                return result + 'minÃÂºtami';
            }
            break;
        case 'h':  // an hour / in an hour / an hour ago
            return withoutSuffix ? 'hodina' : (isFuture ? 'hodinu' : 'hodinou');
        case 'hh': // 9 hours / in 9 hours / 9 hours ago
            if (withoutSuffix || isFuture) {
                return result + (sk__plural(number) ? 'hodiny' : 'hodÃÂ­n');
            } else {
                return result + 'hodinami';
            }
            break;
        case 'd':  // a day / in a day / a day ago
            return (withoutSuffix || isFuture) ? 'deÃÂ' : 'dÃÂom';
        case 'dd': // 9 days / in 9 days / 9 days ago
            if (withoutSuffix || isFuture) {
                return result + (sk__plural(number) ? 'dni' : 'dnÃÂ­');
            } else {
                return result + 'dÃÂami';
            }
            break;
        case 'M':  // a month / in a month / a month ago
            return (withoutSuffix || isFuture) ? 'mesiac' : 'mesiacom';
        case 'MM': // 9 months / in 9 months / 9 months ago
            if (withoutSuffix || isFuture) {
                return result + (sk__plural(number) ? 'mesiace' : 'mesiacov');
            } else {
                return result + 'mesiacmi';
            }
            break;
        case 'y':  // a year / in a year / a year ago
            return (withoutSuffix || isFuture) ? 'rok' : 'rokom';
        case 'yy': // 9 years / in 9 years / 9 years ago
            if (withoutSuffix || isFuture) {
                return result + (sk__plural(number) ? 'roky' : 'rokov');
            } else {
                return result + 'rokmi';
            }
            break;
        }
    }

    var sk = moment.defineLocale('sk', {
        months : sk__months,
        monthsShort : sk__monthsShort,
        weekdays : 'nedeÃÂ¾a_pondelok_utorok_streda_ÃÂ¡tvrtok_piatok_sobota'.split('_'),
        weekdaysShort : 'ne_po_ut_st_ÃÂ¡t_pi_so'.split('_'),
        weekdaysMin : 'ne_po_ut_st_ÃÂ¡t_pi_so'.split('_'),
        longDateFormat : {
            LT: 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY H:mm',
            LLLL : 'dddd D. MMMM YYYY H:mm'
        },
        calendar : {
            sameDay: '[dnes o] LT',
            nextDay: '[zajtra o] LT',
            nextWeek: function () {
                switch (this.day()) {
                case 0:
                    return '[v nedeÃÂ¾u o] LT';
                case 1:
                case 2:
                    return '[v] dddd [o] LT';
                case 3:
                    return '[v stredu o] LT';
                case 4:
                    return '[vo ÃÂ¡tvrtok o] LT';
                case 5:
                    return '[v piatok o] LT';
                case 6:
                    return '[v sobotu o] LT';
                }
            },
            lastDay: '[vÃÂera o] LT',
            lastWeek: function () {
                switch (this.day()) {
                case 0:
                    return '[minulÃÂº nedeÃÂ¾u o] LT';
                case 1:
                case 2:
                    return '[minulÃÂ½] dddd [o] LT';
                case 3:
                    return '[minulÃÂº stredu o] LT';
                case 4:
                case 5:
                    return '[minulÃÂ½] dddd [o] LT';
                case 6:
                    return '[minulÃÂº sobotu o] LT';
                }
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'za %s',
            past : 'pred %s',
            s : sk__translate,
            m : sk__translate,
            mm : sk__translate,
            h : sk__translate,
            hh : sk__translate,
            d : sk__translate,
            dd : sk__translate,
            M : sk__translate,
            MM : sk__translate,
            y : sk__translate,
            yy : sk__translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : slovenian (sl)
    //! author : Robert SedovÃÂ¡ek : https://github.com/sedovsek

    function sl__processRelativeTime(number, withoutSuffix, key, isFuture) {
        var result = number + ' ';
        switch (key) {
        case 's':
            return withoutSuffix || isFuture ? 'nekaj sekund' : 'nekaj sekundami';
        case 'm':
            return withoutSuffix ? 'ena minuta' : 'eno minuto';
        case 'mm':
            if (number === 1) {
                result += withoutSuffix ? 'minuta' : 'minuto';
            } else if (number === 2) {
                result += withoutSuffix || isFuture ? 'minuti' : 'minutama';
            } else if (number < 5) {
                result += withoutSuffix || isFuture ? 'minute' : 'minutami';
            } else {
                result += withoutSuffix || isFuture ? 'minut' : 'minutami';
            }
            return result;
        case 'h':
            return withoutSuffix ? 'ena ura' : 'eno uro';
        case 'hh':
            if (number === 1) {
                result += withoutSuffix ? 'ura' : 'uro';
            } else if (number === 2) {
                result += withoutSuffix || isFuture ? 'uri' : 'urama';
            } else if (number < 5) {
                result += withoutSuffix || isFuture ? 'ure' : 'urami';
            } else {
                result += withoutSuffix || isFuture ? 'ur' : 'urami';
            }
            return result;
        case 'd':
            return withoutSuffix || isFuture ? 'en dan' : 'enim dnem';
        case 'dd':
            if (number === 1) {
                result += withoutSuffix || isFuture ? 'dan' : 'dnem';
            } else if (number === 2) {
                result += withoutSuffix || isFuture ? 'dni' : 'dnevoma';
            } else {
                result += withoutSuffix || isFuture ? 'dni' : 'dnevi';
            }
            return result;
        case 'M':
            return withoutSuffix || isFuture ? 'en mesec' : 'enim mesecem';
        case 'MM':
            if (number === 1) {
                result += withoutSuffix || isFuture ? 'mesec' : 'mesecem';
            } else if (number === 2) {
                result += withoutSuffix || isFuture ? 'meseca' : 'mesecema';
            } else if (number < 5) {
                result += withoutSuffix || isFuture ? 'mesece' : 'meseci';
            } else {
                result += withoutSuffix || isFuture ? 'mesecev' : 'meseci';
            }
            return result;
        case 'y':
            return withoutSuffix || isFuture ? 'eno leto' : 'enim letom';
        case 'yy':
            if (number === 1) {
                result += withoutSuffix || isFuture ? 'leto' : 'letom';
            } else if (number === 2) {
                result += withoutSuffix || isFuture ? 'leti' : 'letoma';
            } else if (number < 5) {
                result += withoutSuffix || isFuture ? 'leta' : 'leti';
            } else {
                result += withoutSuffix || isFuture ? 'let' : 'leti';
            }
            return result;
        }
    }

    var sl = moment.defineLocale('sl', {
        months : 'januar_februar_marec_april_maj_junij_julij_avgust_september_oktober_november_december'.split('_'),
        monthsShort : 'jan._feb._mar._apr._maj._jun._jul._avg._sep._okt._nov._dec.'.split('_'),
        monthsParseExact: true,
        weekdays : 'nedelja_ponedeljek_torek_sreda_ÃÂetrtek_petek_sobota'.split('_'),
        weekdaysShort : 'ned._pon._tor._sre._ÃÂet._pet._sob.'.split('_'),
        weekdaysMin : 'ne_po_to_sr_ÃÂe_pe_so'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'H:mm',
            LTS : 'H:mm:ss',
            L : 'DD. MM. YYYY',
            LL : 'D. MMMM YYYY',
            LLL : 'D. MMMM YYYY H:mm',
            LLLL : 'dddd, D. MMMM YYYY H:mm'
        },
        calendar : {
            sameDay  : '[danes ob] LT',
            nextDay  : '[jutri ob] LT',

            nextWeek : function () {
                switch (this.day()) {
                case 0:
                    return '[v] [nedeljo] [ob] LT';
                case 3:
                    return '[v] [sredo] [ob] LT';
                case 6:
                    return '[v] [soboto] [ob] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[v] dddd [ob] LT';
                }
            },
            lastDay  : '[vÃÂeraj ob] LT',
            lastWeek : function () {
                switch (this.day()) {
                case 0:
                    return '[prejÃÂ¡njo] [nedeljo] [ob] LT';
                case 3:
                    return '[prejÃÂ¡njo] [sredo] [ob] LT';
                case 6:
                    return '[prejÃÂ¡njo] [soboto] [ob] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[prejÃÂ¡nji] dddd [ob] LT';
                }
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂez %s',
            past   : 'pred %s',
            s      : sl__processRelativeTime,
            m      : sl__processRelativeTime,
            mm     : sl__processRelativeTime,
            h      : sl__processRelativeTime,
            hh     : sl__processRelativeTime,
            d      : sl__processRelativeTime,
            dd     : sl__processRelativeTime,
            M      : sl__processRelativeTime,
            MM     : sl__processRelativeTime,
            y      : sl__processRelativeTime,
            yy     : sl__processRelativeTime
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Albanian (sq)
    //! author : FlakÃÂ«rim Ismani : https://github.com/flakerimi
    //! author: Menelion ElensÃÂºle: https://github.com/Oire (tests)
    //! author : Oerd Cukalla : https://github.com/oerd (fixes)

    var sq = moment.defineLocale('sq', {
        months : 'Janar_Shkurt_Mars_Prill_Maj_Qershor_Korrik_Gusht_Shtator_Tetor_NÃÂ«ntor_Dhjetor'.split('_'),
        monthsShort : 'Jan_Shk_Mar_Pri_Maj_Qer_Kor_Gus_Sht_Tet_NÃÂ«n_Dhj'.split('_'),
        weekdays : 'E Diel_E HÃÂ«nÃÂ«_E MartÃÂ«_E MÃÂ«rkurÃÂ«_E Enjte_E Premte_E ShtunÃÂ«'.split('_'),
        weekdaysShort : 'Die_HÃÂ«n_Mar_MÃÂ«r_Enj_Pre_Sht'.split('_'),
        weekdaysMin : 'D_H_Ma_MÃÂ«_E_P_Sh'.split('_'),
        weekdaysParseExact : true,
        meridiemParse: /PD|MD/,
        isPM: function (input) {
            return input.charAt(0) === 'M';
        },
        meridiem : function (hours, minutes, isLower) {
            return hours < 12 ? 'PD' : 'MD';
        },
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[Sot nÃÂ«] LT',
            nextDay : '[NesÃÂ«r nÃÂ«] LT',
            nextWeek : 'dddd [nÃÂ«] LT',
            lastDay : '[Dje nÃÂ«] LT',
            lastWeek : 'dddd [e kaluar nÃÂ«] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'nÃÂ« %s',
            past : '%s mÃÂ« parÃÂ«',
            s : 'disa sekonda',
            m : 'njÃÂ« minutÃÂ«',
            mm : '%d minuta',
            h : 'njÃÂ« orÃÂ«',
            hh : '%d orÃÂ«',
            d : 'njÃÂ« ditÃÂ«',
            dd : '%d ditÃÂ«',
            M : 'njÃÂ« muaj',
            MM : '%d muaj',
            y : 'njÃÂ« vit',
            yy : '%d vite'
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Serbian-cyrillic (sr-cyrl)
    //! author : Milan JanaÃÂkoviÃÂ<milanjanackovic@gmail.com> : https://github.com/milan-j

    var sr_cyrl__translator = {
        words: { //Different grammatical cases
            m: ['ÃÂÃÂµÃÂ´ÃÂ°ÃÂ½ ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ', 'ÃÂÃÂµÃÂ´ÃÂ½ÃÂµ ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂµ'],
            mm: ['ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ', 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂµ', 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ°'],
            h: ['ÃÂÃÂµÃÂ´ÃÂ°ÃÂ½ ÃÂÃÂ°ÃÂ', 'ÃÂÃÂµÃÂ´ÃÂ½ÃÂ¾ÃÂ³ ÃÂÃÂ°ÃÂÃÂ°'],
            hh: ['ÃÂÃÂ°ÃÂ', 'ÃÂÃÂ°ÃÂÃÂ°', 'ÃÂÃÂ°ÃÂÃÂ¸'],
            dd: ['ÃÂ´ÃÂ°ÃÂ½', 'ÃÂ´ÃÂ°ÃÂ½ÃÂ°', 'ÃÂ´ÃÂ°ÃÂ½ÃÂ°'],
            MM: ['ÃÂ¼ÃÂµÃÂÃÂµÃÂ', 'ÃÂ¼ÃÂµÃÂÃÂµÃÂÃÂ°', 'ÃÂ¼ÃÂµÃÂÃÂµÃÂÃÂ¸'],
            yy: ['ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°', 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂµ', 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°']
        },
        correctGrammaticalCase: function (number, wordKey) {
            return number === 1 ? wordKey[0] : (number >= 2 && number <= 4 ? wordKey[1] : wordKey[2]);
        },
        translate: function (number, withoutSuffix, key) {
            var wordKey = sr_cyrl__translator.words[key];
            if (key.length === 1) {
                return withoutSuffix ? wordKey[0] : wordKey[1];
            } else {
                return number + ' ' + sr_cyrl__translator.correctGrammaticalCase(number, wordKey);
            }
        }
    };

    var sr_cyrl = moment.defineLocale('sr-cyrl', {
        months: 'ÃÂÃÂ°ÃÂ½ÃÂÃÂ°ÃÂ_ÃÂÃÂµÃÂ±ÃÂÃÂÃÂ°ÃÂ_ÃÂ¼ÃÂ°ÃÂÃÂ_ÃÂ°ÃÂ¿ÃÂÃÂ¸ÃÂ»_ÃÂ¼ÃÂ°ÃÂ_ÃÂÃÂÃÂ½_ÃÂÃÂÃÂ»_ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ¿ÃÂÃÂµÃÂ¼ÃÂ±ÃÂ°ÃÂ_ÃÂ¾ÃÂºÃÂÃÂ¾ÃÂ±ÃÂ°ÃÂ_ÃÂ½ÃÂ¾ÃÂ²ÃÂµÃÂ¼ÃÂ±ÃÂ°ÃÂ_ÃÂ´ÃÂµÃÂÃÂµÃÂ¼ÃÂ±ÃÂ°ÃÂ'.split('_'),
        monthsShort: 'ÃÂÃÂ°ÃÂ½._ÃÂÃÂµÃÂ±._ÃÂ¼ÃÂ°ÃÂ._ÃÂ°ÃÂ¿ÃÂ._ÃÂ¼ÃÂ°ÃÂ_ÃÂÃÂÃÂ½_ÃÂÃÂÃÂ»_ÃÂ°ÃÂ²ÃÂ³._ÃÂÃÂµÃÂ¿._ÃÂ¾ÃÂºÃÂ._ÃÂ½ÃÂ¾ÃÂ²._ÃÂ´ÃÂµÃÂ.'.split('_'),
        monthsParseExact: true,
        weekdays: 'ÃÂ½ÃÂµÃÂ´ÃÂµÃÂÃÂ°_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂµÃÂÃÂ°ÃÂº_ÃÂÃÂÃÂ¾ÃÂÃÂ°ÃÂº_ÃÂÃÂÃÂµÃÂ´ÃÂ°_ÃÂÃÂµÃÂÃÂ²ÃÂÃÂÃÂ°ÃÂº_ÃÂ¿ÃÂµÃÂÃÂ°ÃÂº_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ°'.split('_'),
        weekdaysShort: 'ÃÂ½ÃÂµÃÂ´._ÃÂ¿ÃÂ¾ÃÂ½._ÃÂÃÂÃÂ¾._ÃÂÃÂÃÂµ._ÃÂÃÂµÃÂ._ÃÂ¿ÃÂµÃÂ._ÃÂÃÂÃÂ±.'.split('_'),
        weekdaysMin: 'ÃÂ½ÃÂµ_ÃÂ¿ÃÂ¾_ÃÂÃÂ_ÃÂÃÂ_ÃÂÃÂµ_ÃÂ¿ÃÂµ_ÃÂÃÂ'.split('_'),
        weekdaysParseExact : true,
        longDateFormat: {
            LT: 'H:mm',
            LTS : 'H:mm:ss',
            L: 'DD. MM. YYYY',
            LL: 'D. MMMM YYYY',
            LLL: 'D. MMMM YYYY H:mm',
            LLLL: 'dddd, D. MMMM YYYY H:mm'
        },
        calendar: {
            sameDay: '[ÃÂ´ÃÂ°ÃÂ½ÃÂ°ÃÂ ÃÂ] LT',
            nextDay: '[ÃÂÃÂÃÂÃÂÃÂ° ÃÂ] LT',
            nextWeek: function () {
                switch (this.day()) {
                case 0:
                    return '[ÃÂ] [ÃÂ½ÃÂµÃÂ´ÃÂµÃÂÃÂ] [ÃÂ] LT';
                case 3:
                    return '[ÃÂ] [ÃÂÃÂÃÂµÃÂ´ÃÂ] [ÃÂ] LT';
                case 6:
                    return '[ÃÂ] [ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ] [ÃÂ] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[ÃÂ] dddd [ÃÂ] LT';
                }
            },
            lastDay  : '[ÃÂÃÂÃÂÃÂµ ÃÂ] LT',
            lastWeek : function () {
                var lastWeekDays = [
                    '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂµ] [ÃÂ½ÃÂµÃÂ´ÃÂµÃÂÃÂµ] [ÃÂ] LT',
                    '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂ¾ÃÂ³] [ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂµÃÂÃÂºÃÂ°] [ÃÂ] LT',
                    '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂ¾ÃÂ³] [ÃÂÃÂÃÂ¾ÃÂÃÂºÃÂ°] [ÃÂ] LT',
                    '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂµ] [ÃÂÃÂÃÂµÃÂ´ÃÂµ] [ÃÂ] LT',
                    '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂ¾ÃÂ³] [ÃÂÃÂµÃÂÃÂ²ÃÂÃÂÃÂºÃÂ°] [ÃÂ] LT',
                    '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂ¾ÃÂ³] [ÃÂ¿ÃÂµÃÂÃÂºÃÂ°] [ÃÂ] LT',
                    '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂµ] [ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂµ] [ÃÂ] LT'
                ];
                return lastWeekDays[this.day()];
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂ·ÃÂ° %s',
            past   : 'ÃÂ¿ÃÂÃÂµ %s',
            s      : 'ÃÂ½ÃÂµÃÂºÃÂ¾ÃÂ»ÃÂ¸ÃÂºÃÂ¾ ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´ÃÂ¸',
            m      : sr_cyrl__translator.translate,
            mm     : sr_cyrl__translator.translate,
            h      : sr_cyrl__translator.translate,
            hh     : sr_cyrl__translator.translate,
            d      : 'ÃÂ´ÃÂ°ÃÂ½',
            dd     : sr_cyrl__translator.translate,
            M      : 'ÃÂ¼ÃÂµÃÂÃÂµÃÂ',
            MM     : sr_cyrl__translator.translate,
            y      : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ',
            yy     : sr_cyrl__translator.translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Serbian-latin (sr)
    //! author : Milan JanaÃÂkoviÃÂ<milanjanackovic@gmail.com> : https://github.com/milan-j

    var sr__translator = {
        words: { //Different grammatical cases
            m: ['jedan minut', 'jedne minute'],
            mm: ['minut', 'minute', 'minuta'],
            h: ['jedan sat', 'jednog sata'],
            hh: ['sat', 'sata', 'sati'],
            dd: ['dan', 'dana', 'dana'],
            MM: ['mesec', 'meseca', 'meseci'],
            yy: ['godina', 'godine', 'godina']
        },
        correctGrammaticalCase: function (number, wordKey) {
            return number === 1 ? wordKey[0] : (number >= 2 && number <= 4 ? wordKey[1] : wordKey[2]);
        },
        translate: function (number, withoutSuffix, key) {
            var wordKey = sr__translator.words[key];
            if (key.length === 1) {
                return withoutSuffix ? wordKey[0] : wordKey[1];
            } else {
                return number + ' ' + sr__translator.correctGrammaticalCase(number, wordKey);
            }
        }
    };

    var sr = moment.defineLocale('sr', {
        months: 'januar_februar_mart_april_maj_jun_jul_avgust_septembar_oktobar_novembar_decembar'.split('_'),
        monthsShort: 'jan._feb._mar._apr._maj_jun_jul_avg._sep._okt._nov._dec.'.split('_'),
        monthsParseExact: true,
        weekdays: 'nedelja_ponedeljak_utorak_sreda_ÃÂetvrtak_petak_subota'.split('_'),
        weekdaysShort: 'ned._pon._uto._sre._ÃÂet._pet._sub.'.split('_'),
        weekdaysMin: 'ne_po_ut_sr_ÃÂe_pe_su'.split('_'),
        weekdaysParseExact : true,
        longDateFormat: {
            LT: 'H:mm',
            LTS : 'H:mm:ss',
            L: 'DD. MM. YYYY',
            LL: 'D. MMMM YYYY',
            LLL: 'D. MMMM YYYY H:mm',
            LLLL: 'dddd, D. MMMM YYYY H:mm'
        },
        calendar: {
            sameDay: '[danas u] LT',
            nextDay: '[sutra u] LT',
            nextWeek: function () {
                switch (this.day()) {
                case 0:
                    return '[u] [nedelju] [u] LT';
                case 3:
                    return '[u] [sredu] [u] LT';
                case 6:
                    return '[u] [subotu] [u] LT';
                case 1:
                case 2:
                case 4:
                case 5:
                    return '[u] dddd [u] LT';
                }
            },
            lastDay  : '[juÃÂe u] LT',
            lastWeek : function () {
                var lastWeekDays = [
                    '[proÃÂ¡le] [nedelje] [u] LT',
                    '[proÃÂ¡log] [ponedeljka] [u] LT',
                    '[proÃÂ¡log] [utorka] [u] LT',
                    '[proÃÂ¡le] [srede] [u] LT',
                    '[proÃÂ¡log] [ÃÂetvrtka] [u] LT',
                    '[proÃÂ¡log] [petka] [u] LT',
                    '[proÃÂ¡le] [subote] [u] LT'
                ];
                return lastWeekDays[this.day()];
            },
            sameElse : 'L'
        },
        relativeTime : {
            future : 'za %s',
            past   : 'pre %s',
            s      : 'nekoliko sekundi',
            m      : sr__translator.translate,
            mm     : sr__translator.translate,
            h      : sr__translator.translate,
            hh     : sr__translator.translate,
            d      : 'dan',
            dd     : sr__translator.translate,
            M      : 'mesec',
            MM     : sr__translator.translate,
            y      : 'godinu',
            yy     : sr__translator.translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : siSwati (ss)
    //! author : Nicolai Davies<mail@nicolai.io> : https://github.com/nicolaidavies


    var ss = moment.defineLocale('ss', {
        months : "Bhimbidvwane_Indlovana_Indlov'lenkhulu_Mabasa_Inkhwekhweti_Inhlaba_Kholwane_Ingci_Inyoni_Imphala_Lweti_Ingongoni".split('_'),
        monthsShort : 'Bhi_Ina_Inu_Mab_Ink_Inh_Kho_Igc_Iny_Imp_Lwe_Igo'.split('_'),
        weekdays : 'Lisontfo_Umsombuluko_Lesibili_Lesitsatfu_Lesine_Lesihlanu_Umgcibelo'.split('_'),
        weekdaysShort : 'Lis_Umb_Lsb_Les_Lsi_Lsh_Umg'.split('_'),
        weekdaysMin : 'Li_Us_Lb_Lt_Ls_Lh_Ug'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'h:mm A',
            LTS : 'h:mm:ss A',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY h:mm A',
            LLLL : 'dddd, D MMMM YYYY h:mm A'
        },
        calendar : {
            sameDay : '[Namuhla nga] LT',
            nextDay : '[Kusasa nga] LT',
            nextWeek : 'dddd [nga] LT',
            lastDay : '[Itolo nga] LT',
            lastWeek : 'dddd [leliphelile] [nga] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'nga %s',
            past : 'wenteka nga %s',
            s : 'emizuzwana lomcane',
            m : 'umzuzu',
            mm : '%d emizuzu',
            h : 'lihora',
            hh : '%d emahora',
            d : 'lilanga',
            dd : '%d emalanga',
            M : 'inyanga',
            MM : '%d tinyanga',
            y : 'umnyaka',
            yy : '%d iminyaka'
        },
        meridiemParse: /ekuseni|emini|entsambama|ebusuku/,
        meridiem : function (hours, minutes, isLower) {
            if (hours < 11) {
                return 'ekuseni';
            } else if (hours < 15) {
                return 'emini';
            } else if (hours < 19) {
                return 'entsambama';
            } else {
                return 'ebusuku';
            }
        },
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'ekuseni') {
                return hour;
            } else if (meridiem === 'emini') {
                return hour >= 11 ? hour : hour + 12;
            } else if (meridiem === 'entsambama' || meridiem === 'ebusuku') {
                if (hour === 0) {
                    return 0;
                }
                return hour + 12;
            }
        },
        ordinalParse: /\d{1,2}/,
        ordinal : '%d',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : swedish (sv)
    //! author : Jens Alm : https://github.com/ulmus

    var sv = moment.defineLocale('sv', {
        months : 'januari_februari_mars_april_maj_juni_juli_augusti_september_oktober_november_december'.split('_'),
        monthsShort : 'jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec'.split('_'),
        weekdays : 'sÃÂ¶ndag_mÃÂ¥ndag_tisdag_onsdag_torsdag_fredag_lÃÂ¶rdag'.split('_'),
        weekdaysShort : 'sÃÂ¶n_mÃÂ¥n_tis_ons_tor_fre_lÃÂ¶r'.split('_'),
        weekdaysMin : 'sÃÂ¶_mÃÂ¥_ti_on_to_fr_lÃÂ¶'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'YYYY-MM-DD',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY [kl.] HH:mm',
            LLLL : 'dddd D MMMM YYYY [kl.] HH:mm',
            lll : 'D MMM YYYY HH:mm',
            llll : 'ddd D MMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[Idag] LT',
            nextDay: '[Imorgon] LT',
            lastDay: '[IgÃÂ¥r] LT',
            nextWeek: '[PÃÂ¥] dddd LT',
            lastWeek: '[I] dddd[s] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'om %s',
            past : 'fÃÂ¶r %s sedan',
            s : 'nÃÂ¥gra sekunder',
            m : 'en minut',
            mm : '%d minuter',
            h : 'en timme',
            hh : '%d timmar',
            d : 'en dag',
            dd : '%d dagar',
            M : 'en mÃÂ¥nad',
            MM : '%d mÃÂ¥nader',
            y : 'ett ÃÂ¥r',
            yy : '%d ÃÂ¥r'
        },
        ordinalParse: /\d{1,2}(e|a)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (~~(number % 100 / 10) === 1) ? 'e' :
                (b === 1) ? 'a' :
                (b === 2) ? 'a' :
                (b === 3) ? 'e' : 'e';
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : swahili (sw)
    //! author : Fahad Kassim : https://github.com/fadsel

    var sw = moment.defineLocale('sw', {
        months : 'Januari_Februari_Machi_Aprili_Mei_Juni_Julai_Agosti_Septemba_Oktoba_Novemba_Desemba'.split('_'),
        monthsShort : 'Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ago_Sep_Okt_Nov_Des'.split('_'),
        weekdays : 'Jumapili_Jumatatu_Jumanne_Jumatano_Alhamisi_Ijumaa_Jumamosi'.split('_'),
        weekdaysShort : 'Jpl_Jtat_Jnne_Jtan_Alh_Ijm_Jmos'.split('_'),
        weekdaysMin : 'J2_J3_J4_J5_Al_Ij_J1'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[leo saa] LT',
            nextDay : '[kesho saa] LT',
            nextWeek : '[wiki ijayo] dddd [saat] LT',
            lastDay : '[jana] LT',
            lastWeek : '[wiki iliyopita] dddd [saat] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s baadaye',
            past : 'tokea %s',
            s : 'hivi punde',
            m : 'dakika moja',
            mm : 'dakika %d',
            h : 'saa limoja',
            hh : 'masaa %d',
            d : 'siku moja',
            dd : 'masiku %d',
            M : 'mwezi mmoja',
            MM : 'miezi %d',
            y : 'mwaka mmoja',
            yy : 'miaka %d'
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : tamil (ta)
    //! author : Arjunkumar Krishnamoorthy : https://github.com/tk120404

    var ta__symbolMap = {
        '1': 'Ã Â¯Â§',
        '2': 'Ã Â¯Â¨',
        '3': 'Ã Â¯Â©',
        '4': 'Ã Â¯Âª',
        '5': 'Ã Â¯Â«',
        '6': 'Ã Â¯Â¬',
        '7': 'Ã Â¯Â­',
        '8': 'Ã Â¯Â®',
        '9': 'Ã Â¯Â¯',
        '0': 'Ã Â¯Â¦'
    }, ta__numberMap = {
        'Ã Â¯Â§': '1',
        'Ã Â¯Â¨': '2',
        'Ã Â¯Â©': '3',
        'Ã Â¯Âª': '4',
        'Ã Â¯Â«': '5',
        'Ã Â¯Â¬': '6',
        'Ã Â¯Â­': '7',
        'Ã Â¯Â®': '8',
        'Ã Â¯Â¯': '9',
        'Ã Â¯Â¦': '0'
    };

    var ta = moment.defineLocale('ta', {
        months : 'Ã Â®ÂÃ Â®Â©Ã Â®ÂµÃ Â®Â°Ã Â®Â¿_Ã Â®ÂªÃ Â®Â¿Ã Â®ÂªÃ Â¯ÂÃ Â®Â°Ã Â®ÂµÃ Â®Â°Ã Â®Â¿_Ã Â®Â®Ã Â®Â¾Ã Â®Â°Ã Â¯ÂÃ Â®ÂÃ Â¯Â_Ã Â®ÂÃ Â®ÂªÃ Â¯ÂÃ Â®Â°Ã Â®Â²Ã Â¯Â_Ã Â®Â®Ã Â¯Â_Ã Â®ÂÃ Â¯ÂÃ Â®Â©Ã Â¯Â_Ã Â®ÂÃ Â¯ÂÃ Â®Â²Ã Â¯Â_Ã Â®ÂÃ Â®ÂÃ Â®Â¸Ã Â¯ÂÃ Â®ÂÃ Â¯Â_Ã Â®ÂÃ Â¯ÂÃ Â®ÂªÃ Â¯ÂÃ Â®ÂÃ Â¯ÂÃ Â®Â®Ã Â¯ÂÃ Â®ÂªÃ Â®Â°Ã Â¯Â_Ã Â®ÂÃ Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â¯ÂÃ Â®Â¾Ã Â®ÂªÃ Â®Â°Ã Â¯Â_Ã Â®Â¨Ã Â®ÂµÃ Â®Â®Ã Â¯ÂÃ Â®ÂªÃ Â®Â°Ã Â¯Â_Ã Â®ÂÃ Â®Â¿Ã Â®ÂÃ Â®Â®Ã Â¯ÂÃ Â®ÂªÃ Â®Â°Ã Â¯Â'.split('_'),
        monthsShort : 'Ã Â®ÂÃ Â®Â©Ã Â®ÂµÃ Â®Â°Ã Â®Â¿_Ã Â®ÂªÃ Â®Â¿Ã Â®ÂªÃ Â¯ÂÃ Â®Â°Ã Â®ÂµÃ Â®Â°Ã Â®Â¿_Ã Â®Â®Ã Â®Â¾Ã Â®Â°Ã Â¯ÂÃ Â®ÂÃ Â¯Â_Ã Â®ÂÃ Â®ÂªÃ Â¯ÂÃ Â®Â°Ã Â®Â²Ã Â¯Â_Ã Â®Â®Ã Â¯Â_Ã Â®ÂÃ Â¯ÂÃ Â®Â©Ã Â¯Â_Ã Â®ÂÃ Â¯ÂÃ Â®Â²Ã Â¯Â_Ã Â®ÂÃ Â®ÂÃ Â®Â¸Ã Â¯ÂÃ Â®ÂÃ Â¯Â_Ã Â®ÂÃ Â¯ÂÃ Â®ÂªÃ Â¯ÂÃ Â®ÂÃ Â¯ÂÃ Â®Â®Ã Â¯ÂÃ Â®ÂªÃ Â®Â°Ã Â¯Â_Ã Â®ÂÃ Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â¯ÂÃ Â®Â¾Ã Â®ÂªÃ Â®Â°Ã Â¯Â_Ã Â®Â¨Ã Â®ÂµÃ Â®Â®Ã Â¯ÂÃ Â®ÂªÃ Â®Â°Ã Â¯Â_Ã Â®ÂÃ Â®Â¿Ã Â®ÂÃ Â®Â®Ã Â¯ÂÃ Â®ÂªÃ Â®Â°Ã Â¯Â'.split('_'),
        weekdays : 'Ã Â®ÂÃ Â®Â¾Ã Â®Â¯Ã Â®Â¿Ã Â®Â±Ã Â¯ÂÃ Â®Â±Ã Â¯ÂÃ Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®Â¿Ã Â®Â´Ã Â®Â®Ã Â¯Â_Ã Â®Â¤Ã Â®Â¿Ã Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®Â¿Ã Â®Â´Ã Â®Â®Ã Â¯Â_Ã Â®ÂÃ Â¯ÂÃ Â®ÂµÃ Â¯ÂÃ Â®ÂµÃ Â®Â¾Ã Â®Â¯Ã Â¯ÂÃ Â®ÂÃ Â®Â¿Ã Â®Â´Ã Â®Â®Ã Â¯Â_Ã Â®ÂªÃ Â¯ÂÃ Â®Â¤Ã Â®Â©Ã Â¯ÂÃ Â®ÂÃ Â®Â¿Ã Â®Â´Ã Â®Â®Ã Â¯Â_Ã Â®ÂµÃ Â®Â¿Ã Â®Â¯Ã Â®Â¾Ã Â®Â´Ã Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®Â¿Ã Â®Â´Ã Â®Â®Ã Â¯Â_Ã Â®ÂµÃ Â¯ÂÃ Â®Â³Ã Â¯ÂÃ Â®Â³Ã Â®Â¿Ã Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®Â¿Ã Â®Â´Ã Â®Â®Ã Â¯Â_Ã Â®ÂÃ Â®Â©Ã Â®Â¿Ã Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®Â¿Ã Â®Â´Ã Â®Â®Ã Â¯Â'.split('_'),
        weekdaysShort : 'Ã Â®ÂÃ Â®Â¾Ã Â®Â¯Ã Â®Â¿Ã Â®Â±Ã Â¯Â_Ã Â®Â¤Ã Â®Â¿Ã Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®Â³Ã Â¯Â_Ã Â®ÂÃ Â¯ÂÃ Â®ÂµÃ Â¯ÂÃ Â®ÂµÃ Â®Â¾Ã Â®Â¯Ã Â¯Â_Ã Â®ÂªÃ Â¯ÂÃ Â®Â¤Ã Â®Â©Ã Â¯Â_Ã Â®ÂµÃ Â®Â¿Ã Â®Â¯Ã Â®Â¾Ã Â®Â´Ã Â®Â©Ã Â¯Â_Ã Â®ÂµÃ Â¯ÂÃ Â®Â³Ã Â¯ÂÃ Â®Â³Ã Â®Â¿_Ã Â®ÂÃ Â®Â©Ã Â®Â¿'.split('_'),
        weekdaysMin : 'Ã Â®ÂÃ Â®Â¾_Ã Â®Â¤Ã Â®Â¿_Ã Â®ÂÃ Â¯Â_Ã Â®ÂªÃ Â¯Â_Ã Â®ÂµÃ Â®Â¿_Ã Â®ÂµÃ Â¯Â_Ã Â®Â'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY, HH:mm',
            LLLL : 'dddd, D MMMM YYYY, HH:mm'
        },
        calendar : {
            sameDay : '[Ã Â®ÂÃ Â®Â©Ã Â¯ÂÃ Â®Â±Ã Â¯Â] LT',
            nextDay : '[Ã Â®Â¨Ã Â®Â¾Ã Â®Â³Ã Â¯Â] LT',
            nextWeek : 'dddd, LT',
            lastDay : '[Ã Â®Â¨Ã Â¯ÂÃ Â®Â±Ã Â¯ÂÃ Â®Â±Ã Â¯Â] LT',
            lastWeek : '[Ã Â®ÂÃ Â®ÂÃ Â®Â¨Ã Â¯ÂÃ Â®Â¤ Ã Â®ÂµÃ Â®Â¾Ã Â®Â°Ã Â®Â®Ã Â¯Â] dddd, LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s Ã Â®ÂÃ Â®Â²Ã Â¯Â',
            past : '%s Ã Â®Â®Ã Â¯ÂÃ Â®Â©Ã Â¯Â',
            s : 'Ã Â®ÂÃ Â®Â°Ã Â¯Â Ã Â®ÂÃ Â®Â¿Ã Â®Â² Ã Â®ÂµÃ Â®Â¿Ã Â®Â¨Ã Â®Â¾Ã Â®ÂÃ Â®Â¿Ã Â®ÂÃ Â®Â³Ã Â¯Â',
            m : 'Ã Â®ÂÃ Â®Â°Ã Â¯Â Ã Â®Â¨Ã Â®Â¿Ã Â®Â®Ã Â®Â¿Ã Â®ÂÃ Â®Â®Ã Â¯Â',
            mm : '%d Ã Â®Â¨Ã Â®Â¿Ã Â®Â®Ã Â®Â¿Ã Â®ÂÃ Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®Â³Ã Â¯Â',
            h : 'Ã Â®ÂÃ Â®Â°Ã Â¯Â Ã Â®Â®Ã Â®Â£Ã Â®Â¿ Ã Â®Â¨Ã Â¯ÂÃ Â®Â°Ã Â®Â®Ã Â¯Â',
            hh : '%d Ã Â®Â®Ã Â®Â£Ã Â®Â¿ Ã Â®Â¨Ã Â¯ÂÃ Â®Â°Ã Â®Â®Ã Â¯Â',
            d : 'Ã Â®ÂÃ Â®Â°Ã Â¯Â Ã Â®Â¨Ã Â®Â¾Ã Â®Â³Ã Â¯Â',
            dd : '%d Ã Â®Â¨Ã Â®Â¾Ã Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®Â³Ã Â¯Â',
            M : 'Ã Â®ÂÃ Â®Â°Ã Â¯Â Ã Â®Â®Ã Â®Â¾Ã Â®Â¤Ã Â®Â®Ã Â¯Â',
            MM : '%d Ã Â®Â®Ã Â®Â¾Ã Â®Â¤Ã Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®Â³Ã Â¯Â',
            y : 'Ã Â®ÂÃ Â®Â°Ã Â¯Â Ã Â®ÂµÃ Â®Â°Ã Â¯ÂÃ Â®ÂÃ Â®Â®Ã Â¯Â',
            yy : '%d Ã Â®ÂÃ Â®Â£Ã Â¯ÂÃ Â®ÂÃ Â¯ÂÃ Â®ÂÃ Â®Â³Ã Â¯Â'
        },
        ordinalParse: /\d{1,2}Ã Â®ÂµÃ Â®Â¤Ã Â¯Â/,
        ordinal : function (number) {
            return number + 'Ã Â®ÂµÃ Â®Â¤Ã Â¯Â';
        },
        preparse: function (string) {
            return string.replace(/[Ã Â¯Â§Ã Â¯Â¨Ã Â¯Â©Ã Â¯ÂªÃ Â¯Â«Ã Â¯Â¬Ã Â¯Â­Ã Â¯Â®Ã Â¯Â¯Ã Â¯Â¦]/g, function (match) {
                return ta__numberMap[match];
            });
        },
        postformat: function (string) {
            return string.replace(/\d/g, function (match) {
                return ta__symbolMap[match];
            });
        },
        // refer http://ta.wikipedia.org/s/1er1
        meridiemParse: /Ã Â®Â¯Ã Â®Â¾Ã Â®Â®Ã Â®Â®Ã Â¯Â|Ã Â®ÂµÃ Â¯ÂÃ Â®ÂÃ Â®Â±Ã Â¯Â|Ã Â®ÂÃ Â®Â¾Ã Â®Â²Ã Â¯Â|Ã Â®Â¨Ã Â®Â£Ã Â¯ÂÃ Â®ÂªÃ Â®ÂÃ Â®Â²Ã Â¯Â|Ã Â®ÂÃ Â®Â±Ã Â¯ÂÃ Â®ÂªÃ Â®Â¾Ã Â®ÂÃ Â¯Â|Ã Â®Â®Ã Â®Â¾Ã Â®Â²Ã Â¯Â/,
        meridiem : function (hour, minute, isLower) {
            if (hour < 2) {
                return ' Ã Â®Â¯Ã Â®Â¾Ã Â®Â®Ã Â®Â®Ã Â¯Â';
            } else if (hour < 6) {
                return ' Ã Â®ÂµÃ Â¯ÂÃ Â®ÂÃ Â®Â±Ã Â¯Â';  // Ã Â®ÂµÃ Â¯ÂÃ Â®ÂÃ Â®Â±Ã Â¯Â
            } else if (hour < 10) {
                return ' Ã Â®ÂÃ Â®Â¾Ã Â®Â²Ã Â¯Â'; // Ã Â®ÂÃ Â®Â¾Ã Â®Â²Ã Â¯Â
            } else if (hour < 14) {
                return ' Ã Â®Â¨Ã Â®Â£Ã Â¯ÂÃ Â®ÂªÃ Â®ÂÃ Â®Â²Ã Â¯Â'; // Ã Â®Â¨Ã Â®Â£Ã Â¯ÂÃ Â®ÂªÃ Â®ÂÃ Â®Â²Ã Â¯Â
            } else if (hour < 18) {
                return ' Ã Â®ÂÃ Â®Â±Ã Â¯ÂÃ Â®ÂªÃ Â®Â¾Ã Â®ÂÃ Â¯Â'; // Ã Â®ÂÃ Â®Â±Ã Â¯ÂÃ Â®ÂªÃ Â®Â¾Ã Â®ÂÃ Â¯Â
            } else if (hour < 22) {
                return ' Ã Â®Â®Ã Â®Â¾Ã Â®Â²Ã Â¯Â'; // Ã Â®Â®Ã Â®Â¾Ã Â®Â²Ã Â¯Â
            } else {
                return ' Ã Â®Â¯Ã Â®Â¾Ã Â®Â®Ã Â®Â®Ã Â¯Â';
            }
        },
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'Ã Â®Â¯Ã Â®Â¾Ã Â®Â®Ã Â®Â®Ã Â¯Â') {
                return hour < 2 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â®ÂµÃ Â¯ÂÃ Â®ÂÃ Â®Â±Ã Â¯Â' || meridiem === 'Ã Â®ÂÃ Â®Â¾Ã Â®Â²Ã Â¯Â') {
                return hour;
            } else if (meridiem === 'Ã Â®Â¨Ã Â®Â£Ã Â¯ÂÃ Â®ÂªÃ Â®ÂÃ Â®Â²Ã Â¯Â') {
                return hour >= 10 ? hour : hour + 12;
            } else {
                return hour + 12;
            }
        },
        week : {
            dow : 0, // Sunday is the first day of the week.
            doy : 6  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : telugu (te)
    //! author : Krishna Chaitanya Thota : https://github.com/kcthota

    var te = moment.defineLocale('te', {
        months : 'Ã Â°ÂÃ Â°Â¨Ã Â°ÂµÃ Â°Â°Ã Â°Â¿_Ã Â°Â«Ã Â°Â¿Ã Â°Â¬Ã Â±ÂÃ Â°Â°Ã Â°ÂµÃ Â°Â°Ã Â°Â¿_Ã Â°Â®Ã Â°Â¾Ã Â°Â°Ã Â±ÂÃ Â°ÂÃ Â°Â¿_Ã Â°ÂÃ Â°ÂªÃ Â±ÂÃ Â°Â°Ã Â°Â¿Ã Â°Â²Ã Â±Â_Ã Â°Â®Ã Â±Â_Ã Â°ÂÃ Â±ÂÃ Â°Â¨Ã Â±Â_Ã Â°ÂÃ Â±ÂÃ Â°Â²Ã Â±ÂÃ Â±Â_Ã Â°ÂÃ Â°ÂÃ Â°Â¸Ã Â±ÂÃ Â°ÂÃ Â±Â_Ã Â°Â¸Ã Â±ÂÃ Â°ÂªÃ Â±ÂÃ Â°ÂÃ Â±ÂÃ Â°ÂÃ Â°Â¬Ã Â°Â°Ã Â±Â_Ã Â°ÂÃ Â°ÂÃ Â±ÂÃ Â°ÂÃ Â±ÂÃ Â°Â¬Ã Â°Â°Ã Â±Â_Ã Â°Â¨Ã Â°ÂµÃ Â°ÂÃ Â°Â¬Ã Â°Â°Ã Â±Â_Ã Â°Â¡Ã Â°Â¿Ã Â°Â¸Ã Â±ÂÃ Â°ÂÃ Â°Â¬Ã Â°Â°Ã Â±Â'.split('_'),
        monthsShort : 'Ã Â°ÂÃ Â°Â¨._Ã Â°Â«Ã Â°Â¿Ã Â°Â¬Ã Â±ÂÃ Â°Â°._Ã Â°Â®Ã Â°Â¾Ã Â°Â°Ã Â±ÂÃ Â°ÂÃ Â°Â¿_Ã Â°ÂÃ Â°ÂªÃ Â±ÂÃ Â°Â°Ã Â°Â¿._Ã Â°Â®Ã Â±Â_Ã Â°ÂÃ Â±ÂÃ Â°Â¨Ã Â±Â_Ã Â°ÂÃ Â±ÂÃ Â°Â²Ã Â±ÂÃ Â±Â_Ã Â°ÂÃ Â°Â._Ã Â°Â¸Ã Â±ÂÃ Â°ÂªÃ Â±Â._Ã Â°ÂÃ Â°ÂÃ Â±ÂÃ Â°ÂÃ Â±Â._Ã Â°Â¨Ã Â°Âµ._Ã Â°Â¡Ã Â°Â¿Ã Â°Â¸Ã Â±Â.'.split('_'),
        monthsParseExact : true,
        weekdays : 'Ã Â°ÂÃ Â°Â¦Ã Â°Â¿Ã Â°ÂµÃ Â°Â¾Ã Â°Â°Ã Â°Â_Ã Â°Â¸Ã Â±ÂÃ Â°Â®Ã Â°ÂµÃ Â°Â¾Ã Â°Â°Ã Â°Â_Ã Â°Â®Ã Â°ÂÃ Â°ÂÃ Â°Â³Ã Â°ÂµÃ Â°Â¾Ã Â°Â°Ã Â°Â_Ã Â°Â¬Ã Â±ÂÃ Â°Â§Ã Â°ÂµÃ Â°Â¾Ã Â°Â°Ã Â°Â_Ã Â°ÂÃ Â±ÂÃ Â°Â°Ã Â±ÂÃ Â°ÂµÃ Â°Â¾Ã Â°Â°Ã Â°Â_Ã Â°Â¶Ã Â±ÂÃ Â°ÂÃ Â±ÂÃ Â°Â°Ã Â°ÂµÃ Â°Â¾Ã Â°Â°Ã Â°Â_Ã Â°Â¶Ã Â°Â¨Ã Â°Â¿Ã Â°ÂµÃ Â°Â¾Ã Â°Â°Ã Â°Â'.split('_'),
        weekdaysShort : 'Ã Â°ÂÃ Â°Â¦Ã Â°Â¿_Ã Â°Â¸Ã Â±ÂÃ Â°Â®_Ã Â°Â®Ã Â°ÂÃ Â°ÂÃ Â°Â³_Ã Â°Â¬Ã Â±ÂÃ Â°Â§_Ã Â°ÂÃ Â±ÂÃ Â°Â°Ã Â±Â_Ã Â°Â¶Ã Â±ÂÃ Â°ÂÃ Â±ÂÃ Â°Â°_Ã Â°Â¶Ã Â°Â¨Ã Â°Â¿'.split('_'),
        weekdaysMin : 'Ã Â°Â_Ã Â°Â¸Ã Â±Â_Ã Â°Â®Ã Â°Â_Ã Â°Â¬Ã Â±Â_Ã Â°ÂÃ Â±Â_Ã Â°Â¶Ã Â±Â_Ã Â°Â¶'.split('_'),
        longDateFormat : {
            LT : 'A h:mm',
            LTS : 'A h:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY, A h:mm',
            LLLL : 'dddd, D MMMM YYYY, A h:mm'
        },
        calendar : {
            sameDay : '[Ã Â°Â¨Ã Â±ÂÃ Â°Â¡Ã Â±Â] LT',
            nextDay : '[Ã Â°Â°Ã Â±ÂÃ Â°ÂªÃ Â±Â] LT',
            nextWeek : 'dddd, LT',
            lastDay : '[Ã Â°Â¨Ã Â°Â¿Ã Â°Â¨Ã Â±ÂÃ Â°Â¨] LT',
            lastWeek : '[Ã Â°ÂÃ Â°Â¤] dddd, LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s Ã Â°Â²Ã Â±Â',
            past : '%s Ã Â°ÂÃ Â±ÂÃ Â°Â°Ã Â°Â¿Ã Â°Â¤Ã Â°Â',
            s : 'Ã Â°ÂÃ Â±ÂÃ Â°Â¨Ã Â±ÂÃ Â°Â¨Ã Â°Â¿ Ã Â°ÂÃ Â±ÂÃ Â°Â·Ã Â°Â£Ã Â°Â¾Ã Â°Â²Ã Â±Â',
            m : 'Ã Â°ÂÃ Â°Â Ã Â°Â¨Ã Â°Â¿Ã Â°Â®Ã Â°Â¿Ã Â°Â·Ã Â°Â',
            mm : '%d Ã Â°Â¨Ã Â°Â¿Ã Â°Â®Ã Â°Â¿Ã Â°Â·Ã Â°Â¾Ã Â°Â²Ã Â±Â',
            h : 'Ã Â°ÂÃ Â°Â Ã Â°ÂÃ Â°ÂÃ Â°Â',
            hh : '%d Ã Â°ÂÃ Â°ÂÃ Â°ÂÃ Â°Â²Ã Â±Â',
            d : 'Ã Â°ÂÃ Â°Â Ã Â°Â°Ã Â±ÂÃ Â°ÂÃ Â±Â',
            dd : '%d Ã Â°Â°Ã Â±ÂÃ Â°ÂÃ Â±ÂÃ Â°Â²Ã Â±Â',
            M : 'Ã Â°ÂÃ Â°Â Ã Â°Â¨Ã Â±ÂÃ Â°Â²',
            MM : '%d Ã Â°Â¨Ã Â±ÂÃ Â°Â²Ã Â°Â²Ã Â±Â',
            y : 'Ã Â°ÂÃ Â°Â Ã Â°Â¸Ã Â°ÂÃ Â°ÂµÃ Â°Â¤Ã Â±ÂÃ Â°Â¸Ã Â°Â°Ã Â°Â',
            yy : '%d Ã Â°Â¸Ã Â°ÂÃ Â°ÂµÃ Â°Â¤Ã Â±ÂÃ Â°Â¸Ã Â°Â°Ã Â°Â¾Ã Â°Â²Ã Â±Â'
        },
        ordinalParse : /\d{1,2}Ã Â°Âµ/,
        ordinal : '%dÃ Â°Âµ',
        meridiemParse: /Ã Â°Â°Ã Â°Â¾Ã Â°Â¤Ã Â±ÂÃ Â°Â°Ã Â°Â¿|Ã Â°ÂÃ Â°Â¦Ã Â°Â¯Ã Â°Â|Ã Â°Â®Ã Â°Â§Ã Â±ÂÃ Â°Â¯Ã Â°Â¾Ã Â°Â¹Ã Â±ÂÃ Â°Â¨Ã Â°Â|Ã Â°Â¸Ã Â°Â¾Ã Â°Â¯Ã Â°ÂÃ Â°Â¤Ã Â±ÂÃ Â°Â°Ã Â°Â/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'Ã Â°Â°Ã Â°Â¾Ã Â°Â¤Ã Â±ÂÃ Â°Â°Ã Â°Â¿') {
                return hour < 4 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â°ÂÃ Â°Â¦Ã Â°Â¯Ã Â°Â') {
                return hour;
            } else if (meridiem === 'Ã Â°Â®Ã Â°Â§Ã Â±ÂÃ Â°Â¯Ã Â°Â¾Ã Â°Â¹Ã Â±ÂÃ Â°Â¨Ã Â°Â') {
                return hour >= 10 ? hour : hour + 12;
            } else if (meridiem === 'Ã Â°Â¸Ã Â°Â¾Ã Â°Â¯Ã Â°ÂÃ Â°Â¤Ã Â±ÂÃ Â°Â°Ã Â°Â') {
                return hour + 12;
            }
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'Ã Â°Â°Ã Â°Â¾Ã Â°Â¤Ã Â±ÂÃ Â°Â°Ã Â°Â¿';
            } else if (hour < 10) {
                return 'Ã Â°ÂÃ Â°Â¦Ã Â°Â¯Ã Â°Â';
            } else if (hour < 17) {
                return 'Ã Â°Â®Ã Â°Â§Ã Â±ÂÃ Â°Â¯Ã Â°Â¾Ã Â°Â¹Ã Â±ÂÃ Â°Â¨Ã Â°Â';
            } else if (hour < 20) {
                return 'Ã Â°Â¸Ã Â°Â¾Ã Â°Â¯Ã Â°ÂÃ Â°Â¤Ã Â±ÂÃ Â°Â°Ã Â°Â';
            } else {
                return 'Ã Â°Â°Ã Â°Â¾Ã Â°Â¤Ã Â±ÂÃ Â°Â°Ã Â°Â¿';
            }
        },
        week : {
            dow : 0, // Sunday is the first day of the week.
            doy : 6  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : thai (th)
    //! author : Kridsada Thanabulpong : https://github.com/sirn

    var th = moment.defineLocale('th', {
        months : 'Ã Â¸Â¡Ã Â¸ÂÃ Â¸Â£Ã Â¸Â²Ã Â¸ÂÃ Â¸Â¡_Ã Â¸ÂÃ Â¸Â¸Ã Â¸Â¡Ã Â¸Â Ã Â¸Â²Ã Â¸ÂÃ Â¸Â±Ã Â¸ÂÃ Â¸ÂÃ Â¹Â_Ã Â¸Â¡Ã Â¸ÂµÃ Â¸ÂÃ Â¸Â²Ã Â¸ÂÃ Â¸Â¡_Ã Â¹ÂÃ Â¸Â¡Ã Â¸Â©Ã Â¸Â²Ã Â¸Â¢Ã Â¸Â_Ã Â¸ÂÃ Â¸Â¤Ã Â¸Â©Ã Â¸Â Ã Â¸Â²Ã Â¸ÂÃ Â¸Â¡_Ã Â¸Â¡Ã Â¸Â´Ã Â¸ÂÃ Â¸Â¸Ã Â¸ÂÃ Â¸Â²Ã Â¸Â¢Ã Â¸Â_Ã Â¸ÂÃ Â¸Â£Ã Â¸ÂÃ Â¸ÂÃ Â¸Â²Ã Â¸ÂÃ Â¸Â¡_Ã Â¸ÂªÃ Â¸Â´Ã Â¸ÂÃ Â¸Â«Ã Â¸Â²Ã Â¸ÂÃ Â¸Â¡_Ã Â¸ÂÃ Â¸Â±Ã Â¸ÂÃ Â¸Â¢Ã Â¸Â²Ã Â¸Â¢Ã Â¸Â_Ã Â¸ÂÃ Â¸Â¸Ã Â¸Â¥Ã Â¸Â²Ã Â¸ÂÃ Â¸Â¡_Ã Â¸ÂÃ Â¸Â¤Ã Â¸Â¨Ã Â¸ÂÃ Â¸Â´Ã Â¸ÂÃ Â¸Â²Ã Â¸Â¢Ã Â¸Â_Ã Â¸ÂÃ Â¸Â±Ã Â¸ÂÃ Â¸Â§Ã Â¸Â²Ã Â¸ÂÃ Â¸Â¡'.split('_'),
        monthsShort : 'Ã Â¸Â¡Ã Â¸ÂÃ Â¸Â£Ã Â¸Â²_Ã Â¸ÂÃ Â¸Â¸Ã Â¸Â¡Ã Â¸Â Ã Â¸Â²_Ã Â¸Â¡Ã Â¸ÂµÃ Â¸ÂÃ Â¸Â²_Ã Â¹ÂÃ Â¸Â¡Ã Â¸Â©Ã Â¸Â²_Ã Â¸ÂÃ Â¸Â¤Ã Â¸Â©Ã Â¸Â Ã Â¸Â²_Ã Â¸Â¡Ã Â¸Â´Ã Â¸ÂÃ Â¸Â¸Ã Â¸ÂÃ Â¸Â²_Ã Â¸ÂÃ Â¸Â£Ã Â¸ÂÃ Â¸ÂÃ Â¸Â²_Ã Â¸ÂªÃ Â¸Â´Ã Â¸ÂÃ Â¸Â«Ã Â¸Â²_Ã Â¸ÂÃ Â¸Â±Ã Â¸ÂÃ Â¸Â¢Ã Â¸Â²_Ã Â¸ÂÃ Â¸Â¸Ã Â¸Â¥Ã Â¸Â²_Ã Â¸ÂÃ Â¸Â¤Ã Â¸Â¨Ã Â¸ÂÃ Â¸Â´Ã Â¸ÂÃ Â¸Â²_Ã Â¸ÂÃ Â¸Â±Ã Â¸ÂÃ Â¸Â§Ã Â¸Â²'.split('_'),
        monthsParseExact: true,
        weekdays : 'Ã Â¸Â­Ã Â¸Â²Ã Â¸ÂÃ Â¸Â´Ã Â¸ÂÃ Â¸Â¢Ã Â¹Â_Ã Â¸ÂÃ Â¸Â±Ã Â¸ÂÃ Â¸ÂÃ Â¸Â£Ã Â¹Â_Ã Â¸Â­Ã Â¸Â±Ã Â¸ÂÃ Â¸ÂÃ Â¸Â²Ã Â¸Â£_Ã Â¸ÂÃ Â¸Â¸Ã Â¸Â_Ã Â¸ÂÃ Â¸Â¤Ã Â¸Â«Ã Â¸Â±Ã Â¸ÂªÃ Â¸ÂÃ Â¸ÂÃ Â¸Âµ_Ã Â¸Â¨Ã Â¸Â¸Ã Â¸ÂÃ Â¸Â£Ã Â¹Â_Ã Â¹ÂÃ Â¸ÂªÃ Â¸Â²Ã Â¸Â£Ã Â¹Â'.split('_'),
        weekdaysShort : 'Ã Â¸Â­Ã Â¸Â²Ã Â¸ÂÃ Â¸Â´Ã Â¸ÂÃ Â¸Â¢Ã Â¹Â_Ã Â¸ÂÃ Â¸Â±Ã Â¸ÂÃ Â¸ÂÃ Â¸Â£Ã Â¹Â_Ã Â¸Â­Ã Â¸Â±Ã Â¸ÂÃ Â¸ÂÃ Â¸Â²Ã Â¸Â£_Ã Â¸ÂÃ Â¸Â¸Ã Â¸Â_Ã Â¸ÂÃ Â¸Â¤Ã Â¸Â«Ã Â¸Â±Ã Â¸Âª_Ã Â¸Â¨Ã Â¸Â¸Ã Â¸ÂÃ Â¸Â£Ã Â¹Â_Ã Â¹ÂÃ Â¸ÂªÃ Â¸Â²Ã Â¸Â£Ã Â¹Â'.split('_'), // yes, three characters difference
        weekdaysMin : 'Ã Â¸Â­Ã Â¸Â²._Ã Â¸Â._Ã Â¸Â­._Ã Â¸Â._Ã Â¸ÂÃ Â¸Â¤._Ã Â¸Â¨._Ã Â¸Âª.'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'H Ã Â¸ÂÃ Â¸Â²Ã Â¸Â¬Ã Â¸Â´Ã Â¸ÂÃ Â¸Â² m Ã Â¸ÂÃ Â¸Â²Ã Â¸ÂÃ Â¸Âµ',
            LTS : 'H Ã Â¸ÂÃ Â¸Â²Ã Â¸Â¬Ã Â¸Â´Ã Â¸ÂÃ Â¸Â² m Ã Â¸ÂÃ Â¸Â²Ã Â¸ÂÃ Â¸Âµ s Ã Â¸Â§Ã Â¸Â´Ã Â¸ÂÃ Â¸Â²Ã Â¸ÂÃ Â¸Âµ',
            L : 'YYYY/MM/DD',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY Ã Â¹ÂÃ Â¸Â§Ã Â¸Â¥Ã Â¸Â² H Ã Â¸ÂÃ Â¸Â²Ã Â¸Â¬Ã Â¸Â´Ã Â¸ÂÃ Â¸Â² m Ã Â¸ÂÃ Â¸Â²Ã Â¸ÂÃ Â¸Âµ',
            LLLL : 'Ã Â¸Â§Ã Â¸Â±Ã Â¸ÂddddÃ Â¸ÂÃ Â¸ÂµÃ Â¹Â D MMMM YYYY Ã Â¹ÂÃ Â¸Â§Ã Â¸Â¥Ã Â¸Â² H Ã Â¸ÂÃ Â¸Â²Ã Â¸Â¬Ã Â¸Â´Ã Â¸ÂÃ Â¸Â² m Ã Â¸ÂÃ Â¸Â²Ã Â¸ÂÃ Â¸Âµ'
        },
        meridiemParse: /Ã Â¸ÂÃ Â¹ÂÃ Â¸Â­Ã Â¸ÂÃ Â¹ÂÃ Â¸ÂÃ Â¸ÂµÃ Â¹ÂÃ Â¸Â¢Ã Â¸Â|Ã Â¸Â«Ã Â¸Â¥Ã Â¸Â±Ã Â¸ÂÃ Â¹ÂÃ Â¸ÂÃ Â¸ÂµÃ Â¹ÂÃ Â¸Â¢Ã Â¸Â/,
        isPM: function (input) {
            return input === 'Ã Â¸Â«Ã Â¸Â¥Ã Â¸Â±Ã Â¸ÂÃ Â¹ÂÃ Â¸ÂÃ Â¸ÂµÃ Â¹ÂÃ Â¸Â¢Ã Â¸Â';
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 12) {
                return 'Ã Â¸ÂÃ Â¹ÂÃ Â¸Â­Ã Â¸ÂÃ Â¹ÂÃ Â¸ÂÃ Â¸ÂµÃ Â¹ÂÃ Â¸Â¢Ã Â¸Â';
            } else {
                return 'Ã Â¸Â«Ã Â¸Â¥Ã Â¸Â±Ã Â¸ÂÃ Â¹ÂÃ Â¸ÂÃ Â¸ÂµÃ Â¹ÂÃ Â¸Â¢Ã Â¸Â';
            }
        },
        calendar : {
            sameDay : '[Ã Â¸Â§Ã Â¸Â±Ã Â¸ÂÃ Â¸ÂÃ Â¸ÂµÃ Â¹Â Ã Â¹ÂÃ Â¸Â§Ã Â¸Â¥Ã Â¸Â²] LT',
            nextDay : '[Ã Â¸ÂÃ Â¸Â£Ã Â¸Â¸Ã Â¹ÂÃ Â¸ÂÃ Â¸ÂÃ Â¸ÂµÃ Â¹Â Ã Â¹ÂÃ Â¸Â§Ã Â¸Â¥Ã Â¸Â²] LT',
            nextWeek : 'dddd[Ã Â¸Â«Ã Â¸ÂÃ Â¹ÂÃ Â¸Â² Ã Â¹ÂÃ Â¸Â§Ã Â¸Â¥Ã Â¸Â²] LT',
            lastDay : '[Ã Â¹ÂÃ Â¸Â¡Ã Â¸Â·Ã Â¹ÂÃ Â¸Â­Ã Â¸Â§Ã Â¸Â²Ã Â¸ÂÃ Â¸ÂÃ Â¸ÂµÃ Â¹Â Ã Â¹ÂÃ Â¸Â§Ã Â¸Â¥Ã Â¸Â²] LT',
            lastWeek : '[Ã Â¸Â§Ã Â¸Â±Ã Â¸Â]dddd[Ã Â¸ÂÃ Â¸ÂµÃ Â¹ÂÃ Â¹ÂÃ Â¸Â¥Ã Â¹ÂÃ Â¸Â§ Ã Â¹ÂÃ Â¸Â§Ã Â¸Â¥Ã Â¸Â²] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'Ã Â¸Â­Ã Â¸ÂµÃ Â¸Â %s',
            past : '%sÃ Â¸ÂÃ Â¸ÂµÃ Â¹ÂÃ Â¹ÂÃ Â¸Â¥Ã Â¹ÂÃ Â¸Â§',
            s : 'Ã Â¹ÂÃ Â¸Â¡Ã Â¹ÂÃ Â¸ÂÃ Â¸ÂµÃ Â¹ÂÃ Â¸Â§Ã Â¸Â´Ã Â¸ÂÃ Â¸Â²Ã Â¸ÂÃ Â¸Âµ',
            m : '1 Ã Â¸ÂÃ Â¸Â²Ã Â¸ÂÃ Â¸Âµ',
            mm : '%d Ã Â¸ÂÃ Â¸Â²Ã Â¸ÂÃ Â¸Âµ',
            h : '1 Ã Â¸ÂÃ Â¸Â±Ã Â¹ÂÃ Â¸Â§Ã Â¹ÂÃ Â¸Â¡Ã Â¸Â',
            hh : '%d Ã Â¸ÂÃ Â¸Â±Ã Â¹ÂÃ Â¸Â§Ã Â¹ÂÃ Â¸Â¡Ã Â¸Â',
            d : '1 Ã Â¸Â§Ã Â¸Â±Ã Â¸Â',
            dd : '%d Ã Â¸Â§Ã Â¸Â±Ã Â¸Â',
            M : '1 Ã Â¹ÂÃ Â¸ÂÃ Â¸Â·Ã Â¸Â­Ã Â¸Â',
            MM : '%d Ã Â¹ÂÃ Â¸ÂÃ Â¸Â·Ã Â¸Â­Ã Â¸Â',
            y : '1 Ã Â¸ÂÃ Â¸Âµ',
            yy : '%d Ã Â¸ÂÃ Â¸Âµ'
        }
    });

    //! moment.js locale configuration
    //! locale : Tagalog/Filipino (tl-ph)
    //! author : Dan Hagman

    var tl_ph = moment.defineLocale('tl-ph', {
        months : 'Enero_Pebrero_Marso_Abril_Mayo_Hunyo_Hulyo_Agosto_Setyembre_Oktubre_Nobyembre_Disyembre'.split('_'),
        monthsShort : 'Ene_Peb_Mar_Abr_May_Hun_Hul_Ago_Set_Okt_Nob_Dis'.split('_'),
        weekdays : 'Linggo_Lunes_Martes_Miyerkules_Huwebes_Biyernes_Sabado'.split('_'),
        weekdaysShort : 'Lin_Lun_Mar_Miy_Huw_Biy_Sab'.split('_'),
        weekdaysMin : 'Li_Lu_Ma_Mi_Hu_Bi_Sab'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'MM/D/YYYY',
            LL : 'MMMM D, YYYY',
            LLL : 'MMMM D, YYYY HH:mm',
            LLLL : 'dddd, MMMM DD, YYYY HH:mm'
        },
        calendar : {
            sameDay: '[Ngayon sa] LT',
            nextDay: '[Bukas sa] LT',
            nextWeek: 'dddd [sa] LT',
            lastDay: '[Kahapon sa] LT',
            lastWeek: 'dddd [huling linggo] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'sa loob ng %s',
            past : '%s ang nakalipas',
            s : 'ilang segundo',
            m : 'isang minuto',
            mm : '%d minuto',
            h : 'isang oras',
            hh : '%d oras',
            d : 'isang araw',
            dd : '%d araw',
            M : 'isang buwan',
            MM : '%d buwan',
            y : 'isang taon',
            yy : '%d taon'
        },
        ordinalParse: /\d{1,2}/,
        ordinal : function (number) {
            return number;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Klingon (tlh)
    //! author : Dominika Kruk : https://github.com/amaranthrose

    var numbersNouns = 'pagh_waÃ¢ÂÂ_chaÃ¢ÂÂ_wej_loS_vagh_jav_Soch_chorgh_Hut'.split('_');

    function translateFuture(output) {
        var time = output;
        time = (output.indexOf('jaj') !== -1) ?
    	time.slice(0, -3) + 'leS' :
    	(output.indexOf('jar') !== -1) ?
    	time.slice(0, -3) + 'waQ' :
    	(output.indexOf('DIS') !== -1) ?
    	time.slice(0, -3) + 'nem' :
    	time + ' pIq';
        return time;
    }

    function translatePast(output) {
        var time = output;
        time = (output.indexOf('jaj') !== -1) ?
    	time.slice(0, -3) + 'HuÃ¢ÂÂ' :
    	(output.indexOf('jar') !== -1) ?
    	time.slice(0, -3) + 'wen' :
    	(output.indexOf('DIS') !== -1) ?
    	time.slice(0, -3) + 'ben' :
    	time + ' ret';
        return time;
    }

    function tlh__translate(number, withoutSuffix, string, isFuture) {
        var numberNoun = numberAsNoun(number);
        switch (string) {
            case 'mm':
                return numberNoun + ' tup';
            case 'hh':
                return numberNoun + ' rep';
            case 'dd':
                return numberNoun + ' jaj';
            case 'MM':
                return numberNoun + ' jar';
            case 'yy':
                return numberNoun + ' DIS';
        }
    }

    function numberAsNoun(number) {
        var hundred = Math.floor((number % 1000) / 100),
    	ten = Math.floor((number % 100) / 10),
    	one = number % 10,
    	word = '';
        if (hundred > 0) {
            word += numbersNouns[hundred] + 'vatlh';
        }
        if (ten > 0) {
            word += ((word !== '') ? ' ' : '') + numbersNouns[ten] + 'maH';
        }
        if (one > 0) {
            word += ((word !== '') ? ' ' : '') + numbersNouns[one];
        }
        return (word === '') ? 'pagh' : word;
    }

    var tlh = moment.defineLocale('tlh', {
        months : 'teraÃ¢ÂÂ jar waÃ¢ÂÂ_teraÃ¢ÂÂ jar chaÃ¢ÂÂ_teraÃ¢ÂÂ jar wej_teraÃ¢ÂÂ jar loS_teraÃ¢ÂÂ jar vagh_teraÃ¢ÂÂ jar jav_teraÃ¢ÂÂ jar Soch_teraÃ¢ÂÂ jar chorgh_teraÃ¢ÂÂ jar Hut_teraÃ¢ÂÂ jar waÃ¢ÂÂmaH_teraÃ¢ÂÂ jar waÃ¢ÂÂmaH waÃ¢ÂÂ_teraÃ¢ÂÂ jar waÃ¢ÂÂmaH chaÃ¢ÂÂ'.split('_'),
        monthsShort : 'jar waÃ¢ÂÂ_jar chaÃ¢ÂÂ_jar wej_jar loS_jar vagh_jar jav_jar Soch_jar chorgh_jar Hut_jar waÃ¢ÂÂmaH_jar waÃ¢ÂÂmaH waÃ¢ÂÂ_jar waÃ¢ÂÂmaH chaÃ¢ÂÂ'.split('_'),
        monthsParseExact : true,
        weekdays : 'lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj'.split('_'),
        weekdaysShort : 'lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj'.split('_'),
        weekdaysMin : 'lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[DaHjaj] LT',
            nextDay: '[waÃ¢ÂÂleS] LT',
            nextWeek: 'LLL',
            lastDay: '[waÃ¢ÂÂHuÃ¢ÂÂ] LT',
            lastWeek: 'LLL',
            sameElse: 'L'
        },
        relativeTime : {
            future : translateFuture,
            past : translatePast,
            s : 'puS lup',
            m : 'waÃ¢ÂÂ tup',
            mm : tlh__translate,
            h : 'waÃ¢ÂÂ rep',
            hh : tlh__translate,
            d : 'waÃ¢ÂÂ jaj',
            dd : tlh__translate,
            M : 'waÃ¢ÂÂ jar',
            MM : tlh__translate,
            y : 'waÃ¢ÂÂ DIS',
            yy : tlh__translate
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : turkish (tr)
    //! authors : Erhan Gundogan : https://github.com/erhangundogan,
    //!           Burak YiÃÂit Kaya: https://github.com/BYK

    var tr__suffixes = {
        1: '\'inci',
        5: '\'inci',
        8: '\'inci',
        70: '\'inci',
        80: '\'inci',
        2: '\'nci',
        7: '\'nci',
        20: '\'nci',
        50: '\'nci',
        3: '\'ÃÂ¼ncÃÂ¼',
        4: '\'ÃÂ¼ncÃÂ¼',
        100: '\'ÃÂ¼ncÃÂ¼',
        6: '\'ncÃÂ±',
        9: '\'uncu',
        10: '\'uncu',
        30: '\'uncu',
        60: '\'ÃÂ±ncÃÂ±',
        90: '\'ÃÂ±ncÃÂ±'
    };

    var tr = moment.defineLocale('tr', {
        months : 'Ocak_ÃÂubat_Mart_Nisan_MayÃÂ±s_Haziran_Temmuz_AÃÂustos_EylÃÂ¼l_Ekim_KasÃÂ±m_AralÃÂ±k'.split('_'),
        monthsShort : 'Oca_ÃÂub_Mar_Nis_May_Haz_Tem_AÃÂu_Eyl_Eki_Kas_Ara'.split('_'),
        weekdays : 'Pazar_Pazartesi_SalÃÂ±_ÃÂarÃÂamba_PerÃÂembe_Cuma_Cumartesi'.split('_'),
        weekdaysShort : 'Paz_Pts_Sal_ÃÂar_Per_Cum_Cts'.split('_'),
        weekdaysMin : 'Pz_Pt_Sa_ÃÂa_Pe_Cu_Ct'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[bugÃÂ¼n saat] LT',
            nextDay : '[yarÃÂ±n saat] LT',
            nextWeek : '[haftaya] dddd [saat] LT',
            lastDay : '[dÃÂ¼n] LT',
            lastWeek : '[geÃÂ§en hafta] dddd [saat] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : '%s sonra',
            past : '%s ÃÂ¶nce',
            s : 'birkaÃÂ§ saniye',
            m : 'bir dakika',
            mm : '%d dakika',
            h : 'bir saat',
            hh : '%d saat',
            d : 'bir gÃÂ¼n',
            dd : '%d gÃÂ¼n',
            M : 'bir ay',
            MM : '%d ay',
            y : 'bir yÃÂ±l',
            yy : '%d yÃÂ±l'
        },
        ordinalParse: /\d{1,2}'(inci|nci|ÃÂ¼ncÃÂ¼|ncÃÂ±|uncu|ÃÂ±ncÃÂ±)/,
        ordinal : function (number) {
            if (number === 0) {  // special case for zero
                return number + '\'ÃÂ±ncÃÂ±';
            }
            var a = number % 10,
                b = number % 100 - a,
                c = number >= 100 ? 100 : null;
            return number + (tr__suffixes[a] || tr__suffixes[b] || tr__suffixes[c]);
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : talossan (tzl)
    //! author : Robin van der Vliet : https://github.com/robin0van0der0v with the help of IustÃÂ¬ Canun


    // After the year there should be a slash and the amount of years since December 26, 1979 in Roman numerals.
    // This is currently too difficult (maybe even impossible) to add.
    var tzl = moment.defineLocale('tzl', {
        months : 'Januar_Fevraglh_MarÃÂ§_AvrÃÂ¯u_Mai_GÃÂ¼n_Julia_Guscht_Setemvar_ListopÃÂ¤ts_Noemvar_Zecemvar'.split('_'),
        monthsShort : 'Jan_Fev_Mar_Avr_Mai_GÃÂ¼n_Jul_Gus_Set_Lis_Noe_Zec'.split('_'),
        weekdays : 'SÃÂºladi_LÃÂºneÃÂ§i_Maitzi_MÃÂ¡rcuri_XhÃÂºadi_ViÃÂ©nerÃÂ§i_SÃÂ¡turi'.split('_'),
        weekdaysShort : 'SÃÂºl_LÃÂºn_Mai_MÃÂ¡r_XhÃÂº_ViÃÂ©_SÃÂ¡t'.split('_'),
        weekdaysMin : 'SÃÂº_LÃÂº_Ma_MÃÂ¡_Xh_Vi_SÃÂ¡'.split('_'),
        longDateFormat : {
            LT : 'HH.mm',
            LTS : 'HH.mm.ss',
            L : 'DD.MM.YYYY',
            LL : 'D. MMMM [dallas] YYYY',
            LLL : 'D. MMMM [dallas] YYYY HH.mm',
            LLLL : 'dddd, [li] D. MMMM [dallas] YYYY HH.mm'
        },
        meridiemParse: /d\'o|d\'a/i,
        isPM : function (input) {
            return 'd\'o' === input.toLowerCase();
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours > 11) {
                return isLower ? 'd\'o' : 'D\'O';
            } else {
                return isLower ? 'd\'a' : 'D\'A';
            }
        },
        calendar : {
            sameDay : '[oxhi ÃÂ ] LT',
            nextDay : '[demÃÂ  ÃÂ ] LT',
            nextWeek : 'dddd [ÃÂ ] LT',
            lastDay : '[ieiri ÃÂ ] LT',
            lastWeek : '[sÃÂ¼r el] dddd [lasteu ÃÂ ] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'osprei %s',
            past : 'ja%s',
            s : tzl__processRelativeTime,
            m : tzl__processRelativeTime,
            mm : tzl__processRelativeTime,
            h : tzl__processRelativeTime,
            hh : tzl__processRelativeTime,
            d : tzl__processRelativeTime,
            dd : tzl__processRelativeTime,
            M : tzl__processRelativeTime,
            MM : tzl__processRelativeTime,
            y : tzl__processRelativeTime,
            yy : tzl__processRelativeTime
        },
        ordinalParse: /\d{1,2}\./,
        ordinal : '%d.',
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    function tzl__processRelativeTime(number, withoutSuffix, key, isFuture) {
        var format = {
            's': ['viensas secunds', '\'iensas secunds'],
            'm': ['\'n mÃÂ­ut', '\'iens mÃÂ­ut'],
            'mm': [number + ' mÃÂ­uts', '' + number + ' mÃÂ­uts'],
            'h': ['\'n ÃÂ¾ora', '\'iensa ÃÂ¾ora'],
            'hh': [number + ' ÃÂ¾oras', '' + number + ' ÃÂ¾oras'],
            'd': ['\'n ziua', '\'iensa ziua'],
            'dd': [number + ' ziuas', '' + number + ' ziuas'],
            'M': ['\'n mes', '\'iens mes'],
            'MM': [number + ' mesen', '' + number + ' mesen'],
            'y': ['\'n ar', '\'iens ar'],
            'yy': [number + ' ars', '' + number + ' ars']
        };
        return isFuture ? format[key][0] : (withoutSuffix ? format[key][0] : format[key][1]);
    }

    //! moment.js locale configuration
    //! locale : Morocco Central Atlas TamaziÃÂ£t in Latin (tzm-latn)
    //! author : Abdel Said : https://github.com/abdelsaid

    var tzm_latn = moment.defineLocale('tzm-latn', {
        months : 'innayr_brÃÂ¤ayrÃÂ¤_marÃÂ¤sÃÂ¤_ibrir_mayyw_ywnyw_ywlywz_ÃÂ£wÃÂ¡t_ÃÂ¡wtanbir_ktÃÂ¤wbrÃÂ¤_nwwanbir_dwjnbir'.split('_'),
        monthsShort : 'innayr_brÃÂ¤ayrÃÂ¤_marÃÂ¤sÃÂ¤_ibrir_mayyw_ywnyw_ywlywz_ÃÂ£wÃÂ¡t_ÃÂ¡wtanbir_ktÃÂ¤wbrÃÂ¤_nwwanbir_dwjnbir'.split('_'),
        weekdays : 'asamas_aynas_asinas_akras_akwas_asimwas_asiÃ¡Â¸Âyas'.split('_'),
        weekdaysShort : 'asamas_aynas_asinas_akras_akwas_asimwas_asiÃ¡Â¸Âyas'.split('_'),
        weekdaysMin : 'asamas_aynas_asinas_akras_akwas_asimwas_asiÃ¡Â¸Âyas'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[asdkh g] LT',
            nextDay: '[aska g] LT',
            nextWeek: 'dddd [g] LT',
            lastDay: '[assant g] LT',
            lastWeek: 'dddd [g] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'dadkh s yan %s',
            past : 'yan %s',
            s : 'imik',
            m : 'minuÃ¡Â¸Â',
            mm : '%d minuÃ¡Â¸Â',
            h : 'saÃÂa',
            hh : '%d tassaÃÂin',
            d : 'ass',
            dd : '%d ossan',
            M : 'ayowr',
            MM : '%d iyyirn',
            y : 'asgas',
            yy : '%d isgasn'
        },
        week : {
            dow : 6, // Saturday is the first day of the week.
            doy : 12  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : Morocco Central Atlas TamaziÃÂ£t (tzm)
    //! author : Abdel Said : https://github.com/abdelsaid

    var tzm = moment.defineLocale('tzm', {
        months : 'Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ¢Ã¢ÂµÂ_Ã¢Â´Â±Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ¢Ã¢ÂµÂ_Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂ_Ã¢ÂµÂÃ¢Â´Â±Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ_Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ¢Ã¢ÂµÂ¢Ã¢ÂµÂ_Ã¢ÂµÂ¢Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ¢Ã¢ÂµÂ_Ã¢ÂµÂ¢Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ¢Ã¢ÂµÂÃ¢ÂµÂ£_Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ_Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢Â´Â±Ã¢ÂµÂÃ¢ÂµÂ_Ã¢Â´Â½Ã¢ÂµÂÃ¢ÂµÂÃ¢Â´Â±Ã¢ÂµÂ_Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ¡Ã¢Â´Â°Ã¢ÂµÂÃ¢Â´Â±Ã¢ÂµÂÃ¢ÂµÂ_Ã¢Â´Â·Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â±Ã¢ÂµÂÃ¢ÂµÂ'.split('_'),
        monthsShort : 'Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ¢Ã¢ÂµÂ_Ã¢Â´Â±Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ¢Ã¢ÂµÂ_Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂ_Ã¢ÂµÂÃ¢Â´Â±Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ_Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ¢Ã¢ÂµÂ¢Ã¢ÂµÂ_Ã¢ÂµÂ¢Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ¢Ã¢ÂµÂ_Ã¢ÂµÂ¢Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ¢Ã¢ÂµÂÃ¢ÂµÂ£_Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ_Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢Â´Â±Ã¢ÂµÂÃ¢ÂµÂ_Ã¢Â´Â½Ã¢ÂµÂÃ¢ÂµÂÃ¢Â´Â±Ã¢ÂµÂ_Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ¡Ã¢Â´Â°Ã¢ÂµÂÃ¢Â´Â±Ã¢ÂµÂÃ¢ÂµÂ_Ã¢Â´Â·Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â±Ã¢ÂµÂÃ¢ÂµÂ'.split('_'),
        weekdays : 'Ã¢Â´Â°Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂ¢Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢Â´Â½Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢Â´Â½Ã¢ÂµÂ¡Ã¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ¡Ã¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢Â´Â¹Ã¢ÂµÂ¢Ã¢Â´Â°Ã¢ÂµÂ'.split('_'),
        weekdaysShort : 'Ã¢Â´Â°Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂ¢Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢Â´Â½Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢Â´Â½Ã¢ÂµÂ¡Ã¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ¡Ã¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢Â´Â¹Ã¢ÂµÂ¢Ã¢Â´Â°Ã¢ÂµÂ'.split('_'),
        weekdaysMin : 'Ã¢Â´Â°Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂ¢Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢Â´Â½Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢Â´Â½Ã¢ÂµÂ¡Ã¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ¡Ã¢Â´Â°Ã¢ÂµÂ_Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢Â´Â¹Ã¢ÂµÂ¢Ã¢Â´Â°Ã¢ÂµÂ'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS: 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[Ã¢Â´Â°Ã¢ÂµÂÃ¢Â´Â·Ã¢ÂµÂ Ã¢Â´Â´] LT',
            nextDay: '[Ã¢Â´Â°Ã¢ÂµÂÃ¢Â´Â½Ã¢Â´Â° Ã¢Â´Â´] LT',
            nextWeek: 'dddd [Ã¢Â´Â´] LT',
            lastDay: '[Ã¢Â´Â°Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂ Ã¢Â´Â´] LT',
            lastWeek: 'dddd [Ã¢Â´Â´] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : 'Ã¢Â´Â·Ã¢Â´Â°Ã¢Â´Â·Ã¢ÂµÂ Ã¢ÂµÂ Ã¢ÂµÂ¢Ã¢Â´Â°Ã¢ÂµÂ %s',
            past : 'Ã¢ÂµÂ¢Ã¢Â´Â°Ã¢ÂµÂ %s',
            s : 'Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â½',
            m : 'Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Âº',
            mm : '%d Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Âº',
            h : 'Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢Â´Â°',
            hh : '%d Ã¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ',
            d : 'Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂ',
            dd : '%d oÃ¢ÂµÂÃ¢ÂµÂÃ¢Â´Â°Ã¢ÂµÂ',
            M : 'Ã¢Â´Â°Ã¢ÂµÂ¢oÃ¢ÂµÂÃ¢ÂµÂ',
            MM : '%d Ã¢ÂµÂÃ¢ÂµÂ¢Ã¢ÂµÂ¢Ã¢ÂµÂÃ¢ÂµÂÃ¢ÂµÂ',
            y : 'Ã¢Â´Â°Ã¢ÂµÂÃ¢Â´Â³Ã¢Â´Â°Ã¢ÂµÂ',
            yy : '%d Ã¢ÂµÂÃ¢ÂµÂÃ¢Â´Â³Ã¢Â´Â°Ã¢ÂµÂÃ¢ÂµÂ'
        },
        week : {
            dow : 6, // Saturday is the first day of the week.
            doy : 12  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : ukrainian (uk)
    //! author : zemlanin : https://github.com/zemlanin
    //! Author : Menelion ElensÃÂºle : https://github.com/Oire

    function uk__plural(word, num) {
        var forms = word.split('_');
        return num % 10 === 1 && num % 100 !== 11 ? forms[0] : (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) ? forms[1] : forms[2]);
    }
    function uk__relativeTimeWithPlural(number, withoutSuffix, key) {
        var format = {
            'mm': withoutSuffix ? 'ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ°_ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ¸_ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½' : 'ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ_ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ¸_ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½',
            'hh': withoutSuffix ? 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°_ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ¸_ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½' : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ_ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ¸_ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½',
            'dd': 'ÃÂ´ÃÂµÃÂ½ÃÂ_ÃÂ´ÃÂ½ÃÂ_ÃÂ´ÃÂ½ÃÂÃÂ²',
            'MM': 'ÃÂ¼ÃÂÃÂÃÂÃÂÃÂ_ÃÂ¼ÃÂÃÂÃÂÃÂÃÂ_ÃÂ¼ÃÂÃÂÃÂÃÂÃÂÃÂ²',
            'yy': 'ÃÂÃÂÃÂº_ÃÂÃÂ¾ÃÂºÃÂ¸_ÃÂÃÂ¾ÃÂºÃÂÃÂ²'
        };
        if (key === 'm') {
            return withoutSuffix ? 'ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ°' : 'ÃÂÃÂ²ÃÂ¸ÃÂ»ÃÂ¸ÃÂ½ÃÂ';
        }
        else if (key === 'h') {
            return withoutSuffix ? 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°' : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ';
        }
        else {
            return number + ' ' + uk__plural(format[key], +number);
        }
    }
    function weekdaysCaseReplace(m, format) {
        var weekdays = {
            'nominative': 'ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂ_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂ¾ÃÂº_ÃÂ²ÃÂÃÂ²ÃÂÃÂ¾ÃÂÃÂ¾ÃÂº_ÃÂÃÂµÃÂÃÂµÃÂ´ÃÂ°_ÃÂÃÂµÃÂÃÂ²ÃÂµÃÂ_ÃÂ¿Ã¢ÂÂÃÂÃÂÃÂ½ÃÂ¸ÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ°'.split('_'),
            'accusative': 'ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂ_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂ¾ÃÂº_ÃÂ²ÃÂÃÂ²ÃÂÃÂ¾ÃÂÃÂ¾ÃÂº_ÃÂÃÂµÃÂÃÂµÃÂ´ÃÂ_ÃÂÃÂµÃÂÃÂ²ÃÂµÃÂ_ÃÂ¿Ã¢ÂÂÃÂÃÂÃÂ½ÃÂ¸ÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ'.split('_'),
            'genitive': 'ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂ_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂÃÂ»ÃÂºÃÂ°_ÃÂ²ÃÂÃÂ²ÃÂÃÂ¾ÃÂÃÂºÃÂ°_ÃÂÃÂµÃÂÃÂµÃÂ´ÃÂ¸_ÃÂÃÂµÃÂÃÂ²ÃÂµÃÂÃÂ³ÃÂ°_ÃÂ¿Ã¢ÂÂÃÂÃÂÃÂ½ÃÂ¸ÃÂÃÂ_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ¸'.split('_')
        },
        nounCase = (/(\[[ÃÂÃÂ²ÃÂ£ÃÂ]\]) ?dddd/).test(format) ?
            'accusative' :
            ((/\[?(?:ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ»ÃÂ¾ÃÂ|ÃÂ½ÃÂ°ÃÂÃÂÃÂÃÂ¿ÃÂ½ÃÂ¾ÃÂ)? ?\] ?dddd/).test(format) ?
                'genitive' :
                'nominative');
        return weekdays[nounCase][m.day()];
    }
    function processHoursFunction(str) {
        return function () {
            return str + 'ÃÂ¾' + (this.hours() === 11 ? 'ÃÂ±' : '') + '] LT';
        };
    }

    var uk = moment.defineLocale('uk', {
        months : {
            'format': 'ÃÂÃÂÃÂÃÂ½ÃÂ_ÃÂ»ÃÂÃÂÃÂ¾ÃÂ³ÃÂ¾_ÃÂ±ÃÂµÃÂÃÂµÃÂ·ÃÂ½ÃÂ_ÃÂºÃÂ²ÃÂÃÂÃÂ½ÃÂ_ÃÂÃÂÃÂ°ÃÂ²ÃÂ½ÃÂ_ÃÂÃÂµÃÂÃÂ²ÃÂ½ÃÂ_ÃÂ»ÃÂ¸ÃÂ¿ÃÂ½ÃÂ_ÃÂÃÂµÃÂÃÂ¿ÃÂ½ÃÂ_ÃÂ²ÃÂµÃÂÃÂµÃÂÃÂ½ÃÂ_ÃÂ¶ÃÂ¾ÃÂ²ÃÂÃÂ½ÃÂ_ÃÂ»ÃÂ¸ÃÂÃÂÃÂ¾ÃÂ¿ÃÂ°ÃÂ´ÃÂ°_ÃÂ³ÃÂÃÂÃÂ´ÃÂ½ÃÂ'.split('_'),
            'standalone': 'ÃÂÃÂÃÂÃÂµÃÂ½ÃÂ_ÃÂ»ÃÂÃÂÃÂ¸ÃÂ¹_ÃÂ±ÃÂµÃÂÃÂµÃÂ·ÃÂµÃÂ½ÃÂ_ÃÂºÃÂ²ÃÂÃÂÃÂµÃÂ½ÃÂ_ÃÂÃÂÃÂ°ÃÂ²ÃÂµÃÂ½ÃÂ_ÃÂÃÂµÃÂÃÂ²ÃÂµÃÂ½ÃÂ_ÃÂ»ÃÂ¸ÃÂ¿ÃÂµÃÂ½ÃÂ_ÃÂÃÂµÃÂÃÂ¿ÃÂµÃÂ½ÃÂ_ÃÂ²ÃÂµÃÂÃÂµÃÂÃÂµÃÂ½ÃÂ_ÃÂ¶ÃÂ¾ÃÂ²ÃÂÃÂµÃÂ½ÃÂ_ÃÂ»ÃÂ¸ÃÂÃÂÃÂ¾ÃÂ¿ÃÂ°ÃÂ´_ÃÂ³ÃÂÃÂÃÂ´ÃÂµÃÂ½ÃÂ'.split('_')
        },
        monthsShort : 'ÃÂÃÂÃÂ_ÃÂ»ÃÂÃÂ_ÃÂ±ÃÂµÃÂ_ÃÂºÃÂ²ÃÂÃÂ_ÃÂÃÂÃÂ°ÃÂ²_ÃÂÃÂµÃÂÃÂ²_ÃÂ»ÃÂ¸ÃÂ¿_ÃÂÃÂµÃÂÃÂ¿_ÃÂ²ÃÂµÃÂ_ÃÂ¶ÃÂ¾ÃÂ²ÃÂ_ÃÂ»ÃÂ¸ÃÂÃÂ_ÃÂ³ÃÂÃÂÃÂ´'.split('_'),
        weekdays : weekdaysCaseReplace,
        weekdaysShort : 'ÃÂ½ÃÂ´_ÃÂ¿ÃÂ½_ÃÂ²ÃÂ_ÃÂÃÂ_ÃÂÃÂ_ÃÂ¿ÃÂ_ÃÂÃÂ±'.split('_'),
        weekdaysMin : 'ÃÂ½ÃÂ´_ÃÂ¿ÃÂ½_ÃÂ²ÃÂ_ÃÂÃÂ_ÃÂÃÂ_ÃÂ¿ÃÂ_ÃÂÃÂ±'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD.MM.YYYY',
            LL : 'D MMMM YYYY ÃÂ.',
            LLL : 'D MMMM YYYY ÃÂ., HH:mm',
            LLLL : 'dddd, D MMMM YYYY ÃÂ., HH:mm'
        },
        calendar : {
            sameDay: processHoursFunction('[ÃÂ¡ÃÂÃÂ¾ÃÂ³ÃÂ¾ÃÂ´ÃÂ½ÃÂ '),
            nextDay: processHoursFunction('[ÃÂÃÂ°ÃÂ²ÃÂÃÂÃÂ° '),
            lastDay: processHoursFunction('[ÃÂÃÂÃÂ¾ÃÂÃÂ° '),
            nextWeek: processHoursFunction('[ÃÂ£] dddd ['),
            lastWeek: function () {
                switch (this.day()) {
                case 0:
                case 3:
                case 5:
                case 6:
                    return processHoursFunction('[ÃÂÃÂ¸ÃÂ½ÃÂÃÂ»ÃÂ¾ÃÂ] dddd [').call(this);
                case 1:
                case 2:
                case 4:
                    return processHoursFunction('[ÃÂÃÂ¸ÃÂ½ÃÂÃÂ»ÃÂ¾ÃÂ³ÃÂ¾] dddd [').call(this);
                }
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'ÃÂ·ÃÂ° %s',
            past : '%s ÃÂÃÂ¾ÃÂ¼ÃÂ',
            s : 'ÃÂ´ÃÂµÃÂºÃÂÃÂ»ÃÂÃÂºÃÂ° ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´',
            m : uk__relativeTimeWithPlural,
            mm : uk__relativeTimeWithPlural,
            h : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ',
            hh : uk__relativeTimeWithPlural,
            d : 'ÃÂ´ÃÂµÃÂ½ÃÂ',
            dd : uk__relativeTimeWithPlural,
            M : 'ÃÂ¼ÃÂÃÂÃÂÃÂÃÂ',
            MM : uk__relativeTimeWithPlural,
            y : 'ÃÂÃÂÃÂº',
            yy : uk__relativeTimeWithPlural
        },
        // M. E.: those two are virtually unused but a user might want to implement them for his/her website for some reason
        meridiemParse: /ÃÂ½ÃÂ¾ÃÂÃÂ|ÃÂÃÂ°ÃÂ½ÃÂºÃÂ|ÃÂ´ÃÂ½ÃÂ|ÃÂ²ÃÂµÃÂÃÂ¾ÃÂÃÂ°/,
        isPM: function (input) {
            return /^(ÃÂ´ÃÂ½ÃÂ|ÃÂ²ÃÂµÃÂÃÂ¾ÃÂÃÂ°)$/.test(input);
        },
        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return 'ÃÂ½ÃÂ¾ÃÂÃÂ';
            } else if (hour < 12) {
                return 'ÃÂÃÂ°ÃÂ½ÃÂºÃÂ';
            } else if (hour < 17) {
                return 'ÃÂ´ÃÂ½ÃÂ';
            } else {
                return 'ÃÂ²ÃÂµÃÂÃÂ¾ÃÂÃÂ°';
            }
        },
        ordinalParse: /\d{1,2}-(ÃÂ¹|ÃÂ³ÃÂ¾)/,
        ordinal: function (number, period) {
            switch (period) {
            case 'M':
            case 'd':
            case 'DDD':
            case 'w':
            case 'W':
                return number + '-ÃÂ¹';
            case 'D':
                return number + '-ÃÂ³ÃÂ¾';
            default:
                return number;
            }
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : uzbek (uz)
    //! author : Sardor Muminov : https://github.com/muminoff

    var uz = moment.defineLocale('uz', {
        months : 'ÃÂÃÂ½ÃÂ²ÃÂ°ÃÂ_ÃÂÃÂµÃÂ²ÃÂÃÂ°ÃÂ»_ÃÂ¼ÃÂ°ÃÂÃÂ_ÃÂ°ÃÂ¿ÃÂÃÂµÃÂ»_ÃÂ¼ÃÂ°ÃÂ¹_ÃÂ¸ÃÂÃÂ½_ÃÂ¸ÃÂÃÂ»_ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ½ÃÂÃÂÃÂ±ÃÂ_ÃÂ¾ÃÂºÃÂÃÂÃÂ±ÃÂ_ÃÂ½ÃÂ¾ÃÂÃÂ±ÃÂ_ÃÂ´ÃÂµÃÂºÃÂ°ÃÂ±ÃÂ'.split('_'),
        monthsShort : 'ÃÂÃÂ½ÃÂ²_ÃÂÃÂµÃÂ²_ÃÂ¼ÃÂ°ÃÂ_ÃÂ°ÃÂ¿ÃÂ_ÃÂ¼ÃÂ°ÃÂ¹_ÃÂ¸ÃÂÃÂ½_ÃÂ¸ÃÂÃÂ»_ÃÂ°ÃÂ²ÃÂ³_ÃÂÃÂµÃÂ½_ÃÂ¾ÃÂºÃÂ_ÃÂ½ÃÂ¾ÃÂ_ÃÂ´ÃÂµÃÂº'.split('_'),
        weekdays : 'ÃÂ¯ÃÂºÃÂÃÂ°ÃÂ½ÃÂ±ÃÂ°_ÃÂÃÂÃÂÃÂ°ÃÂ½ÃÂ±ÃÂ°_ÃÂ¡ÃÂµÃÂÃÂ°ÃÂ½ÃÂ±ÃÂ°_ÃÂ§ÃÂ¾ÃÂÃÂÃÂ°ÃÂ½ÃÂ±ÃÂ°_ÃÂÃÂ°ÃÂ¹ÃÂÃÂ°ÃÂ½ÃÂ±ÃÂ°_ÃÂÃÂÃÂ¼ÃÂ°_ÃÂ¨ÃÂ°ÃÂ½ÃÂ±ÃÂ°'.split('_'),
        weekdaysShort : 'ÃÂ¯ÃÂºÃÂ_ÃÂÃÂÃÂ_ÃÂ¡ÃÂµÃÂ_ÃÂ§ÃÂ¾ÃÂ_ÃÂÃÂ°ÃÂ¹_ÃÂÃÂÃÂ¼_ÃÂ¨ÃÂ°ÃÂ½'.split('_'),
        weekdaysMin : 'ÃÂ¯ÃÂº_ÃÂÃÂ_ÃÂ¡ÃÂµ_ÃÂ§ÃÂ¾_ÃÂÃÂ°_ÃÂÃÂ_ÃÂ¨ÃÂ°'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'D MMMM YYYY, dddd HH:mm'
        },
        calendar : {
            sameDay : '[ÃÂÃÂÃÂ³ÃÂÃÂ½ ÃÂÃÂ¾ÃÂ°ÃÂ] LT [ÃÂ´ÃÂ°]',
            nextDay : '[ÃÂ­ÃÂÃÂÃÂ°ÃÂ³ÃÂ°] LT [ÃÂ´ÃÂ°]',
            nextWeek : 'dddd [ÃÂºÃÂÃÂ½ÃÂ¸ ÃÂÃÂ¾ÃÂ°ÃÂ] LT [ÃÂ´ÃÂ°]',
            lastDay : '[ÃÂÃÂµÃÂÃÂ° ÃÂÃÂ¾ÃÂ°ÃÂ] LT [ÃÂ´ÃÂ°]',
            lastWeek : '[ÃÂ£ÃÂÃÂ³ÃÂ°ÃÂ½] dddd [ÃÂºÃÂÃÂ½ÃÂ¸ ÃÂÃÂ¾ÃÂ°ÃÂ] LT [ÃÂ´ÃÂ°]',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂ¯ÃÂºÃÂ¸ÃÂ½ %s ÃÂ¸ÃÂÃÂ¸ÃÂ´ÃÂ°',
            past : 'ÃÂÃÂ¸ÃÂ ÃÂ½ÃÂµÃÂÃÂ° %s ÃÂ¾ÃÂ»ÃÂ´ÃÂ¸ÃÂ½',
            s : 'ÃÂÃÂÃÂÃÂÃÂ°ÃÂ',
            m : 'ÃÂ±ÃÂ¸ÃÂ ÃÂ´ÃÂ°ÃÂºÃÂ¸ÃÂºÃÂ°',
            mm : '%d ÃÂ´ÃÂ°ÃÂºÃÂ¸ÃÂºÃÂ°',
            h : 'ÃÂ±ÃÂ¸ÃÂ ÃÂÃÂ¾ÃÂ°ÃÂ',
            hh : '%d ÃÂÃÂ¾ÃÂ°ÃÂ',
            d : 'ÃÂ±ÃÂ¸ÃÂ ÃÂºÃÂÃÂ½',
            dd : '%d ÃÂºÃÂÃÂ½',
            M : 'ÃÂ±ÃÂ¸ÃÂ ÃÂ¾ÃÂ¹',
            MM : '%d ÃÂ¾ÃÂ¹',
            y : 'ÃÂ±ÃÂ¸ÃÂ ÃÂ¹ÃÂ¸ÃÂ»',
            yy : '%d ÃÂ¹ÃÂ¸ÃÂ»'
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : vietnamese (vi)
    //! author : Bang Nguyen : https://github.com/bangnk

    var vi = moment.defineLocale('vi', {
        months : 'thÃÂ¡ng 1_thÃÂ¡ng 2_thÃÂ¡ng 3_thÃÂ¡ng 4_thÃÂ¡ng 5_thÃÂ¡ng 6_thÃÂ¡ng 7_thÃÂ¡ng 8_thÃÂ¡ng 9_thÃÂ¡ng 10_thÃÂ¡ng 11_thÃÂ¡ng 12'.split('_'),
        monthsShort : 'Th01_Th02_Th03_Th04_Th05_Th06_Th07_Th08_Th09_Th10_Th11_Th12'.split('_'),
        monthsParseExact : true,
        weekdays : 'chÃ¡Â»Â§ nhÃ¡ÂºÂ­t_thÃ¡Â»Â© hai_thÃ¡Â»Â© ba_thÃ¡Â»Â© tÃÂ°_thÃ¡Â»Â© nÃÂm_thÃ¡Â»Â© sÃÂ¡u_thÃ¡Â»Â© bÃ¡ÂºÂ£y'.split('_'),
        weekdaysShort : 'CN_T2_T3_T4_T5_T6_T7'.split('_'),
        weekdaysMin : 'CN_T2_T3_T4_T5_T6_T7'.split('_'),
        weekdaysParseExact : true,
        meridiemParse: /sa|ch/i,
        isPM : function (input) {
            return /^ch$/i.test(input);
        },
        meridiem : function (hours, minutes, isLower) {
            if (hours < 12) {
                return isLower ? 'sa' : 'SA';
            } else {
                return isLower ? 'ch' : 'CH';
            }
        },
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM [nÃÂm] YYYY',
            LLL : 'D MMMM [nÃÂm] YYYY HH:mm',
            LLLL : 'dddd, D MMMM [nÃÂm] YYYY HH:mm',
            l : 'DD/M/YYYY',
            ll : 'D MMM YYYY',
            lll : 'D MMM YYYY HH:mm',
            llll : 'ddd, D MMM YYYY HH:mm'
        },
        calendar : {
            sameDay: '[HÃÂ´m nay lÃÂºc] LT',
            nextDay: '[NgÃÂ y mai lÃÂºc] LT',
            nextWeek: 'dddd [tuÃ¡ÂºÂ§n tÃ¡Â»Âi lÃÂºc] LT',
            lastDay: '[HÃÂ´m qua lÃÂºc] LT',
            lastWeek: 'dddd [tuÃ¡ÂºÂ§n rÃ¡Â»Âi lÃÂºc] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future : '%s tÃ¡Â»Âi',
            past : '%s trÃÂ°Ã¡Â»Âc',
            s : 'vÃÂ i giÃÂ¢y',
            m : 'mÃ¡Â»Ât phÃÂºt',
            mm : '%d phÃÂºt',
            h : 'mÃ¡Â»Ât giÃ¡Â»Â',
            hh : '%d giÃ¡Â»Â',
            d : 'mÃ¡Â»Ât ngÃÂ y',
            dd : '%d ngÃÂ y',
            M : 'mÃ¡Â»Ât thÃÂ¡ng',
            MM : '%d thÃÂ¡ng',
            y : 'mÃ¡Â»Ât nÃÂm',
            yy : '%d nÃÂm'
        },
        ordinalParse: /\d{1,2}/,
        ordinal : function (number) {
            return number;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : pseudo (x-pseudo)
    //! author : Andrew Hood : https://github.com/andrewhood125

    var x_pseudo = moment.defineLocale('x-pseudo', {
        months : 'J~ÃÂ¡ÃÂ±ÃÂºÃÂ¡~rÃÂ½_F~ÃÂ©brÃÂº~ÃÂ¡rÃÂ½_~MÃÂ¡rc~h_ÃÂp~rÃÂ­l_~MÃÂ¡ÃÂ½_~JÃÂºÃÂ±ÃÂ©~_JÃÂºl~ÃÂ½_ÃÂÃÂº~gÃÂºst~_SÃÂ©p~tÃÂ©mb~ÃÂ©r_ÃÂ~ctÃÂ³b~ÃÂ©r_ÃÂ~ÃÂ³vÃÂ©m~bÃÂ©r_~DÃÂ©cÃÂ©~mbÃÂ©r'.split('_'),
        monthsShort : 'J~ÃÂ¡ÃÂ±_~FÃÂ©b_~MÃÂ¡r_~ÃÂpr_~MÃÂ¡ÃÂ½_~JÃÂºÃÂ±_~JÃÂºl_~ÃÂÃÂºg_~SÃÂ©p_~ÃÂct_~ÃÂÃÂ³v_~DÃÂ©c'.split('_'),
        monthsParseExact : true,
        weekdays : 'S~ÃÂºÃÂ±dÃÂ¡~ÃÂ½_MÃÂ³~ÃÂ±dÃÂ¡ÃÂ½~_TÃÂºÃÂ©~sdÃÂ¡ÃÂ½~_WÃÂ©d~ÃÂ±ÃÂ©sd~ÃÂ¡ÃÂ½_T~hÃÂºrs~dÃÂ¡ÃÂ½_~FrÃÂ­d~ÃÂ¡ÃÂ½_S~ÃÂ¡tÃÂºr~dÃÂ¡ÃÂ½'.split('_'),
        weekdaysShort : 'S~ÃÂºÃÂ±_~MÃÂ³ÃÂ±_~TÃÂºÃÂ©_~WÃÂ©d_~ThÃÂº_~FrÃÂ­_~SÃÂ¡t'.split('_'),
        weekdaysMin : 'S~ÃÂº_MÃÂ³~_TÃÂº_~WÃÂ©_T~h_Fr~_SÃÂ¡'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd, D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[T~ÃÂ³dÃÂ¡~ÃÂ½ ÃÂ¡t] LT',
            nextDay : '[T~ÃÂ³mÃÂ³~rrÃÂ³~w ÃÂ¡t] LT',
            nextWeek : 'dddd [ÃÂ¡t] LT',
            lastDay : '[ÃÂ~ÃÂ©st~ÃÂ©rdÃÂ¡~ÃÂ½ ÃÂ¡t] LT',
            lastWeek : '[L~ÃÂ¡st] dddd [ÃÂ¡t] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'ÃÂ­~ÃÂ± %s',
            past : '%s ÃÂ¡~gÃÂ³',
            s : 'ÃÂ¡ ~fÃÂ©w ~sÃÂ©cÃÂ³~ÃÂ±ds',
            m : 'ÃÂ¡ ~mÃÂ­ÃÂ±~ÃÂºtÃÂ©',
            mm : '%d m~ÃÂ­ÃÂ±ÃÂº~tÃÂ©s',
            h : 'ÃÂ¡~ÃÂ± hÃÂ³~ÃÂºr',
            hh : '%d h~ÃÂ³ÃÂºrs',
            d : 'ÃÂ¡ ~dÃÂ¡ÃÂ½',
            dd : '%d d~ÃÂ¡ÃÂ½s',
            M : 'ÃÂ¡ ~mÃÂ³ÃÂ±~th',
            MM : '%d m~ÃÂ³ÃÂ±t~hs',
            y : 'ÃÂ¡ ~ÃÂ½ÃÂ©ÃÂ¡r',
            yy : '%d ÃÂ½~ÃÂ©ÃÂ¡rs'
        },
        ordinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
            return number + output;
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : chinese (zh-cn)
    //! author : suupic : https://github.com/suupic
    //! author : Zeno Zeng : https://github.com/zenozeng

    var zh_cn = moment.defineLocale('zh-cn', {
        months : 'Ã¤Â¸ÂÃ¦ÂÂ_Ã¤ÂºÂÃ¦ÂÂ_Ã¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂÃ¦ÂÂ_Ã¤ÂºÂÃ¦ÂÂ_Ã¥ÂÂ­Ã¦ÂÂ_Ã¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂ«Ã¦ÂÂ_Ã¤Â¹ÂÃ¦ÂÂ_Ã¥ÂÂÃ¦ÂÂ_Ã¥ÂÂÃ¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂÃ¤ÂºÂÃ¦ÂÂ'.split('_'),
        monthsShort : '1Ã¦ÂÂ_2Ã¦ÂÂ_3Ã¦ÂÂ_4Ã¦ÂÂ_5Ã¦ÂÂ_6Ã¦ÂÂ_7Ã¦ÂÂ_8Ã¦ÂÂ_9Ã¦ÂÂ_10Ã¦ÂÂ_11Ã¦ÂÂ_12Ã¦ÂÂ'.split('_'),
        weekdays : 'Ã¦ÂÂÃ¦ÂÂÃ¦ÂÂ¥_Ã¦ÂÂÃ¦ÂÂÃ¤Â¸Â_Ã¦ÂÂÃ¦ÂÂÃ¤ÂºÂ_Ã¦ÂÂÃ¦ÂÂÃ¤Â¸Â_Ã¦ÂÂÃ¦ÂÂÃ¥ÂÂ_Ã¦ÂÂÃ¦ÂÂÃ¤ÂºÂ_Ã¦ÂÂÃ¦ÂÂÃ¥ÂÂ­'.split('_'),
        weekdaysShort : 'Ã¥ÂÂ¨Ã¦ÂÂ¥_Ã¥ÂÂ¨Ã¤Â¸Â_Ã¥ÂÂ¨Ã¤ÂºÂ_Ã¥ÂÂ¨Ã¤Â¸Â_Ã¥ÂÂ¨Ã¥ÂÂ_Ã¥ÂÂ¨Ã¤ÂºÂ_Ã¥ÂÂ¨Ã¥ÂÂ­'.split('_'),
        weekdaysMin : 'Ã¦ÂÂ¥_Ã¤Â¸Â_Ã¤ÂºÂ_Ã¤Â¸Â_Ã¥ÂÂ_Ã¤ÂºÂ_Ã¥ÂÂ­'.split('_'),
        longDateFormat : {
            LT : 'AhÃ§ÂÂ¹mmÃ¥ÂÂ',
            LTS : 'AhÃ§ÂÂ¹mÃ¥ÂÂsÃ§Â§Â',
            L : 'YYYY-MM-DD',
            LL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
            LLL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥AhÃ§ÂÂ¹mmÃ¥ÂÂ',
            LLLL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥ddddAhÃ§ÂÂ¹mmÃ¥ÂÂ',
            l : 'YYYY-MM-DD',
            ll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
            lll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥AhÃ§ÂÂ¹mmÃ¥ÂÂ',
            llll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥ddddAhÃ§ÂÂ¹mmÃ¥ÂÂ'
        },
        meridiemParse: /Ã¥ÂÂÃ¦ÂÂ¨|Ã¦ÂÂ©Ã¤Â¸Â|Ã¤Â¸ÂÃ¥ÂÂ|Ã¤Â¸Â­Ã¥ÂÂ|Ã¤Â¸ÂÃ¥ÂÂ|Ã¦ÂÂÃ¤Â¸Â/,
        meridiemHour: function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'Ã¥ÂÂÃ¦ÂÂ¨' || meridiem === 'Ã¦ÂÂ©Ã¤Â¸Â' ||
                    meridiem === 'Ã¤Â¸ÂÃ¥ÂÂ') {
                return hour;
            } else if (meridiem === 'Ã¤Â¸ÂÃ¥ÂÂ' || meridiem === 'Ã¦ÂÂÃ¤Â¸Â') {
                return hour + 12;
            } else {
                // 'Ã¤Â¸Â­Ã¥ÂÂ'
                return hour >= 11 ? hour : hour + 12;
            }
        },
        meridiem : function (hour, minute, isLower) {
            var hm = hour * 100 + minute;
            if (hm < 600) {
                return 'Ã¥ÂÂÃ¦ÂÂ¨';
            } else if (hm < 900) {
                return 'Ã¦ÂÂ©Ã¤Â¸Â';
            } else if (hm < 1130) {
                return 'Ã¤Â¸ÂÃ¥ÂÂ';
            } else if (hm < 1230) {
                return 'Ã¤Â¸Â­Ã¥ÂÂ';
            } else if (hm < 1800) {
                return 'Ã¤Â¸ÂÃ¥ÂÂ';
            } else {
                return 'Ã¦ÂÂÃ¤Â¸Â';
            }
        },
        calendar : {
            sameDay : function () {
                return this.minutes() === 0 ? '[Ã¤Â»ÂÃ¥Â¤Â©]Ah[Ã§ÂÂ¹Ã¦ÂÂ´]' : '[Ã¤Â»ÂÃ¥Â¤Â©]LT';
            },
            nextDay : function () {
                return this.minutes() === 0 ? '[Ã¦ÂÂÃ¥Â¤Â©]Ah[Ã§ÂÂ¹Ã¦ÂÂ´]' : '[Ã¦ÂÂÃ¥Â¤Â©]LT';
            },
            lastDay : function () {
                return this.minutes() === 0 ? '[Ã¦ÂÂ¨Ã¥Â¤Â©]Ah[Ã§ÂÂ¹Ã¦ÂÂ´]' : '[Ã¦ÂÂ¨Ã¥Â¤Â©]LT';
            },
            nextWeek : function () {
                var startOfWeek, prefix;
                startOfWeek = moment().startOf('week');
                prefix = this.diff(startOfWeek, 'days') >= 7 ? '[Ã¤Â¸Â]' : '[Ã¦ÂÂ¬]';
                return this.minutes() === 0 ? prefix + 'dddAhÃ§ÂÂ¹Ã¦ÂÂ´' : prefix + 'dddAhÃ§ÂÂ¹mm';
            },
            lastWeek : function () {
                var startOfWeek, prefix;
                startOfWeek = moment().startOf('week');
                prefix = this.unix() < startOfWeek.unix()  ? '[Ã¤Â¸Â]' : '[Ã¦ÂÂ¬]';
                return this.minutes() === 0 ? prefix + 'dddAhÃ§ÂÂ¹Ã¦ÂÂ´' : prefix + 'dddAhÃ§ÂÂ¹mm';
            },
            sameElse : 'LL'
        },
        ordinalParse: /\d{1,2}(Ã¦ÂÂ¥|Ã¦ÂÂ|Ã¥ÂÂ¨)/,
        ordinal : function (number, period) {
            switch (period) {
            case 'd':
            case 'D':
            case 'DDD':
                return number + 'Ã¦ÂÂ¥';
            case 'M':
                return number + 'Ã¦ÂÂ';
            case 'w':
            case 'W':
                return number + 'Ã¥ÂÂ¨';
            default:
                return number;
            }
        },
        relativeTime : {
            future : '%sÃ¥ÂÂ',
            past : '%sÃ¥ÂÂ',
            s : 'Ã¥ÂÂ Ã§Â§Â',
            m : '1 Ã¥ÂÂÃ©ÂÂ',
            mm : '%d Ã¥ÂÂÃ©ÂÂ',
            h : '1 Ã¥Â°ÂÃ¦ÂÂ¶',
            hh : '%d Ã¥Â°ÂÃ¦ÂÂ¶',
            d : '1 Ã¥Â¤Â©',
            dd : '%d Ã¥Â¤Â©',
            M : '1 Ã¤Â¸ÂªÃ¦ÂÂ',
            MM : '%d Ã¤Â¸ÂªÃ¦ÂÂ',
            y : '1 Ã¥Â¹Â´',
            yy : '%d Ã¥Â¹Â´'
        },
        week : {
            // GB/T 7408-1994Ã£ÂÂÃ¦ÂÂ°Ã¦ÂÂ®Ã¥ÂÂÃ¥ÂÂÃ¤ÂºÂ¤Ã¦ÂÂ¢Ã¦Â Â¼Ã¥Â¼ÂÃÂ·Ã¤Â¿Â¡Ã¦ÂÂ¯Ã¤ÂºÂ¤Ã¦ÂÂ¢ÃÂ·Ã¦ÂÂ¥Ã¦ÂÂÃ¥ÂÂÃ¦ÂÂ¶Ã©ÂÂ´Ã¨Â¡Â¨Ã§Â¤ÂºÃ¦Â³ÂÃ£ÂÂÃ¤Â¸ÂISO 8601:1988Ã§Â­ÂÃ¦ÂÂ
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    //! moment.js locale configuration
    //! locale : traditional chinese (zh-tw)
    //! author : Ben : https://github.com/ben-lin

    var zh_tw = moment.defineLocale('zh-tw', {
        months : 'Ã¤Â¸ÂÃ¦ÂÂ_Ã¤ÂºÂÃ¦ÂÂ_Ã¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂÃ¦ÂÂ_Ã¤ÂºÂÃ¦ÂÂ_Ã¥ÂÂ­Ã¦ÂÂ_Ã¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂ«Ã¦ÂÂ_Ã¤Â¹ÂÃ¦ÂÂ_Ã¥ÂÂÃ¦ÂÂ_Ã¥ÂÂÃ¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂÃ¤ÂºÂÃ¦ÂÂ'.split('_'),
        monthsShort : '1Ã¦ÂÂ_2Ã¦ÂÂ_3Ã¦ÂÂ_4Ã¦ÂÂ_5Ã¦ÂÂ_6Ã¦ÂÂ_7Ã¦ÂÂ_8Ã¦ÂÂ_9Ã¦ÂÂ_10Ã¦ÂÂ_11Ã¦ÂÂ_12Ã¦ÂÂ'.split('_'),
        weekdays : 'Ã¦ÂÂÃ¦ÂÂÃ¦ÂÂ¥_Ã¦ÂÂÃ¦ÂÂÃ¤Â¸Â_Ã¦ÂÂÃ¦ÂÂÃ¤ÂºÂ_Ã¦ÂÂÃ¦ÂÂÃ¤Â¸Â_Ã¦ÂÂÃ¦ÂÂÃ¥ÂÂ_Ã¦ÂÂÃ¦ÂÂÃ¤ÂºÂ_Ã¦ÂÂÃ¦ÂÂÃ¥ÂÂ­'.split('_'),
        weekdaysShort : 'Ã©ÂÂ±Ã¦ÂÂ¥_Ã©ÂÂ±Ã¤Â¸Â_Ã©ÂÂ±Ã¤ÂºÂ_Ã©ÂÂ±Ã¤Â¸Â_Ã©ÂÂ±Ã¥ÂÂ_Ã©ÂÂ±Ã¤ÂºÂ_Ã©ÂÂ±Ã¥ÂÂ­'.split('_'),
        weekdaysMin : 'Ã¦ÂÂ¥_Ã¤Â¸Â_Ã¤ÂºÂ_Ã¤Â¸Â_Ã¥ÂÂ_Ã¤ÂºÂ_Ã¥ÂÂ­'.split('_'),
        longDateFormat : {
            LT : 'AhÃ©Â»ÂmmÃ¥ÂÂ',
            LTS : 'AhÃ©Â»ÂmÃ¥ÂÂsÃ§Â§Â',
            L : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
            LL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
            LLL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥AhÃ©Â»ÂmmÃ¥ÂÂ',
            LLLL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥ddddAhÃ©Â»ÂmmÃ¥ÂÂ',
            l : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
            ll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
            lll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥AhÃ©Â»ÂmmÃ¥ÂÂ',
            llll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥ddddAhÃ©Â»ÂmmÃ¥ÂÂ'
        },
        meridiemParse: /Ã¦ÂÂ©Ã¤Â¸Â|Ã¤Â¸ÂÃ¥ÂÂ|Ã¤Â¸Â­Ã¥ÂÂ|Ã¤Â¸ÂÃ¥ÂÂ|Ã¦ÂÂÃ¤Â¸Â/,
        meridiemHour : function (hour, meridiem) {
            if (hour === 12) {
                hour = 0;
            }
            if (meridiem === 'Ã¦ÂÂ©Ã¤Â¸Â' || meridiem === 'Ã¤Â¸ÂÃ¥ÂÂ') {
                return hour;
            } else if (meridiem === 'Ã¤Â¸Â­Ã¥ÂÂ') {
                return hour >= 11 ? hour : hour + 12;
            } else if (meridiem === 'Ã¤Â¸ÂÃ¥ÂÂ' || meridiem === 'Ã¦ÂÂÃ¤Â¸Â') {
                return hour + 12;
            }
        },
        meridiem : function (hour, minute, isLower) {
            var hm = hour * 100 + minute;
            if (hm < 900) {
                return 'Ã¦ÂÂ©Ã¤Â¸Â';
            } else if (hm < 1130) {
                return 'Ã¤Â¸ÂÃ¥ÂÂ';
            } else if (hm < 1230) {
                return 'Ã¤Â¸Â­Ã¥ÂÂ';
            } else if (hm < 1800) {
                return 'Ã¤Â¸ÂÃ¥ÂÂ';
            } else {
                return 'Ã¦ÂÂÃ¤Â¸Â';
            }
        },
        calendar : {
            sameDay : '[Ã¤Â»ÂÃ¥Â¤Â©]LT',
            nextDay : '[Ã¦ÂÂÃ¥Â¤Â©]LT',
            nextWeek : '[Ã¤Â¸Â]ddddLT',
            lastDay : '[Ã¦ÂÂ¨Ã¥Â¤Â©]LT',
            lastWeek : '[Ã¤Â¸Â]ddddLT',
            sameElse : 'L'
        },
        ordinalParse: /\d{1,2}(Ã¦ÂÂ¥|Ã¦ÂÂ|Ã©ÂÂ±)/,
        ordinal : function (number, period) {
            switch (period) {
            case 'd' :
            case 'D' :
            case 'DDD' :
                return number + 'Ã¦ÂÂ¥';
            case 'M' :
                return number + 'Ã¦ÂÂ';
            case 'w' :
            case 'W' :
                return number + 'Ã©ÂÂ±';
            default :
                return number;
            }
        },
        relativeTime : {
            future : '%sÃ¥ÂÂ§',
            past : '%sÃ¥ÂÂ',
            s : 'Ã¥Â¹Â¾Ã§Â§Â',
            m : '1Ã¥ÂÂÃ©ÂÂ',
            mm : '%dÃ¥ÂÂÃ©ÂÂ',
            h : '1Ã¥Â°ÂÃ¦ÂÂ',
            hh : '%dÃ¥Â°ÂÃ¦ÂÂ',
            d : '1Ã¥Â¤Â©',
            dd : '%dÃ¥Â¤Â©',
            M : '1Ã¥ÂÂÃ¦ÂÂ',
            MM : '%dÃ¥ÂÂÃ¦ÂÂ',
            y : '1Ã¥Â¹Â´',
            yy : '%dÃ¥Â¹Â´'
        }
    });

    moment.locale('en');

}));