//! moment.js locale configuration
//! locale : Arabic Saudi Arabia (ar-sa)
//! author : Suhail Alkowaileet : https://github.com/xsoh

import moment from '../moment';

var symbolMap = {
    '1': 'ÃÂ¡',
    '2': 'ÃÂ¢',
    '3': 'ÃÂ£',
    '4': 'ÃÂ¤',
    '5': 'ÃÂ¥',
    '6': 'ÃÂ¦',
    '7': 'ÃÂ§',
    '8': 'ÃÂ¨',
    '9': 'ÃÂ©',
    '0': 'ÃÂ '
}, numberMap = {
    'ÃÂ¡': '1',
    'ÃÂ¢': '2',
    'ÃÂ£': '3',
    'ÃÂ¤': '4',
    'ÃÂ¥': '5',
    'ÃÂ¦': '6',
    'ÃÂ§': '7',
    'ÃÂ¨': '8',
    'ÃÂ©': '9',
    'ÃÂ ': '0'
};

export default moment.defineLocale('ar-sa', {
    months : 'ÃÂÃÂÃÂ§ÃÂÃÂ±_ÃÂÃÂ¨ÃÂ±ÃÂ§ÃÂÃÂ±_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ£ÃÂ¨ÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂ_ÃÂ£ÃÂºÃÂ³ÃÂ·ÃÂ³_ÃÂ³ÃÂ¨ÃÂªÃÂÃÂ¨ÃÂ±_ÃÂ£ÃÂÃÂªÃÂÃÂ¨ÃÂ±_ÃÂÃÂÃÂÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂÃÂ³ÃÂÃÂ¨ÃÂ±'.split('_'),
    monthsShort : 'ÃÂÃÂÃÂ§ÃÂÃÂ±_ÃÂÃÂ¨ÃÂ±ÃÂ§ÃÂÃÂ±_ÃÂÃÂ§ÃÂ±ÃÂ³_ÃÂ£ÃÂ¨ÃÂ±ÃÂÃÂ_ÃÂÃÂ§ÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂ_ÃÂÃÂÃÂÃÂÃÂ_ÃÂ£ÃÂºÃÂ³ÃÂ·ÃÂ³_ÃÂ³ÃÂ¨ÃÂªÃÂÃÂ¨ÃÂ±_ÃÂ£ÃÂÃÂªÃÂÃÂ¨ÃÂ±_ÃÂÃÂÃÂÃÂÃÂ¨ÃÂ±_ÃÂ¯ÃÂÃÂ³ÃÂÃÂ¨ÃÂ±'.split('_'),
    weekdays : 'ÃÂ§ÃÂÃÂ£ÃÂ­ÃÂ¯_ÃÂ§ÃÂÃÂ¥ÃÂ«ÃÂÃÂÃÂ_ÃÂ§ÃÂÃÂ«ÃÂÃÂ§ÃÂ«ÃÂ§ÃÂ¡_ÃÂ§ÃÂÃÂ£ÃÂ±ÃÂ¨ÃÂ¹ÃÂ§ÃÂ¡_ÃÂ§ÃÂÃÂ®ÃÂÃÂÃÂ³_ÃÂ§ÃÂÃÂ¬ÃÂÃÂ¹ÃÂ©_ÃÂ§ÃÂÃÂ³ÃÂ¨ÃÂª'.split('_'),
    weekdaysShort : 'ÃÂ£ÃÂ­ÃÂ¯_ÃÂ¥ÃÂ«ÃÂÃÂÃÂ_ÃÂ«ÃÂÃÂ§ÃÂ«ÃÂ§ÃÂ¡_ÃÂ£ÃÂ±ÃÂ¨ÃÂ¹ÃÂ§ÃÂ¡_ÃÂ®ÃÂÃÂÃÂ³_ÃÂ¬ÃÂÃÂ¹ÃÂ©_ÃÂ³ÃÂ¨ÃÂª'.split('_'),
    weekdaysMin : 'ÃÂ­_ÃÂ_ÃÂ«_ÃÂ±_ÃÂ®_ÃÂ¬_ÃÂ³'.split('_'),
    weekdaysParseExact : true,
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD/MM/YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY HH:mm',
        LLLL : 'dddd D MMMM YYYY HH:mm'
    },
    meridiemParse: /ÃÂµ|ÃÂ/,
    isPM : function (input) {
        return 'ÃÂ' === input;
    },
    meridiem : function (hour, minute, isLower) {
        if (hour < 12) {
            return 'ÃÂµ';
        } else {
            return 'ÃÂ';
        }
    },
    calendar : {
        sameDay: '[ÃÂ§ÃÂÃÂÃÂÃÂ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
        nextDay: '[ÃÂºÃÂ¯ÃÂ§ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
        nextWeek: 'dddd [ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
        lastDay: '[ÃÂ£ÃÂÃÂ³ ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
        lastWeek: 'dddd [ÃÂ¹ÃÂÃÂ ÃÂ§ÃÂÃÂ³ÃÂ§ÃÂ¹ÃÂ©] LT',
        sameElse: 'L'
    },
    relativeTime : {
        future : 'ÃÂÃÂ %s',
        past : 'ÃÂÃÂÃÂ° %s',
        s : 'ÃÂ«ÃÂÃÂ§ÃÂ',
        m : 'ÃÂ¯ÃÂÃÂÃÂÃÂ©',
        mm : '%d ÃÂ¯ÃÂÃÂ§ÃÂ¦ÃÂ',
        h : 'ÃÂ³ÃÂ§ÃÂ¹ÃÂ©',
        hh : '%d ÃÂ³ÃÂ§ÃÂ¹ÃÂ§ÃÂª',
        d : 'ÃÂÃÂÃÂ',
        dd : '%d ÃÂ£ÃÂÃÂ§ÃÂ',
        M : 'ÃÂ´ÃÂÃÂ±',
        MM : '%d ÃÂ£ÃÂ´ÃÂÃÂ±',
        y : 'ÃÂ³ÃÂÃÂ©',
        yy : '%d ÃÂ³ÃÂÃÂÃÂ§ÃÂª'
    },
    preparse: function (string) {
        return string.replace(/[ÃÂ¡ÃÂ¢ÃÂ£ÃÂ¤ÃÂ¥ÃÂ¦ÃÂ§ÃÂ¨ÃÂ©ÃÂ ]/g, function (match) {
            return numberMap[match];
        }).replace(/ÃÂ/g, ',');
    },
    postformat: function (string) {
        return string.replace(/\d/g, function (match) {
            return symbolMap[match];
        }).replace(/,/g, 'ÃÂ');
    },
    week : {
        dow : 6, // Saturday is the first day of the week.
        doy : 12  // The week that contains Jan 1st is the first week of the year.
    }
});

