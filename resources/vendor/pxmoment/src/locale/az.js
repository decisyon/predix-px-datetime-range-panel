//! moment.js locale configuration
//! locale : azerbaijani (az)
//! author : topchiyev : https://github.com/topchiyev

import moment from '../moment';

var suffixes = {
    1: '-inci',
    5: '-inci',
    8: '-inci',
    70: '-inci',
    80: '-inci',
    2: '-nci',
    7: '-nci',
    20: '-nci',
    50: '-nci',
    3: '-ÃÂ¼ncÃÂ¼',
    4: '-ÃÂ¼ncÃÂ¼',
    100: '-ÃÂ¼ncÃÂ¼',
    6: '-ncÃÂ±',
    9: '-uncu',
    10: '-uncu',
    30: '-uncu',
    60: '-ÃÂ±ncÃÂ±',
    90: '-ÃÂ±ncÃÂ±'
};

export default moment.defineLocale('az', {
    months : 'yanvar_fevral_mart_aprel_may_iyun_iyul_avqust_sentyabr_oktyabr_noyabr_dekabr'.split('_'),
    monthsShort : 'yan_fev_mar_apr_may_iyn_iyl_avq_sen_okt_noy_dek'.split('_'),
    weekdays : 'Bazar_Bazar ertÃÂsi_ÃÂÃÂrÃÂÃÂnbÃÂ axÃÂamÃÂ±_ÃÂÃÂrÃÂÃÂnbÃÂ_CÃÂ¼mÃÂ axÃÂamÃÂ±_CÃÂ¼mÃÂ_ÃÂÃÂnbÃÂ'.split('_'),
    weekdaysShort : 'Baz_BzE_ÃÂAx_ÃÂÃÂr_CAx_CÃÂ¼m_ÃÂÃÂn'.split('_'),
    weekdaysMin : 'Bz_BE_ÃÂA_ÃÂÃÂ_CA_CÃÂ¼_ÃÂÃÂ'.split('_'),
    weekdaysParseExact : true,
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD.MM.YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY HH:mm',
        LLLL : 'dddd, D MMMM YYYY HH:mm'
    },
    calendar : {
        sameDay : '[bugÃÂ¼n saat] LT',
        nextDay : '[sabah saat] LT',
        nextWeek : '[gÃÂlÃÂn hÃÂftÃÂ] dddd [saat] LT',
        lastDay : '[dÃÂ¼nÃÂn] LT',
        lastWeek : '[keÃÂ§ÃÂn hÃÂftÃÂ] dddd [saat] LT',
        sameElse : 'L'
    },
    relativeTime : {
        future : '%s sonra',
        past : '%s ÃÂvvÃÂl',
        s : 'birneÃÂ§ÃÂ saniyyÃÂ',
        m : 'bir dÃÂqiqÃÂ',
        mm : '%d dÃÂqiqÃÂ',
        h : 'bir saat',
        hh : '%d saat',
        d : 'bir gÃÂ¼n',
        dd : '%d gÃÂ¼n',
        M : 'bir ay',
        MM : '%d ay',
        y : 'bir il',
        yy : '%d il'
    },
    meridiemParse: /gecÃÂ|sÃÂhÃÂr|gÃÂ¼ndÃÂ¼z|axÃÂam/,
    isPM : function (input) {
        return /^(gÃÂ¼ndÃÂ¼z|axÃÂam)$/.test(input);
    },
    meridiem : function (hour, minute, isLower) {
        if (hour < 4) {
            return 'gecÃÂ';
        } else if (hour < 12) {
            return 'sÃÂhÃÂr';
        } else if (hour < 17) {
            return 'gÃÂ¼ndÃÂ¼z';
        } else {
            return 'axÃÂam';
        }
    },
    ordinalParse: /\d{1,2}-(ÃÂ±ncÃÂ±|inci|nci|ÃÂ¼ncÃÂ¼|ncÃÂ±|uncu)/,
    ordinal : function (number) {
        if (number === 0) {  // special case for zero
            return number + '-ÃÂ±ncÃÂ±';
        }
        var a = number % 10,
            b = number % 100 - a,
            c = number >= 100 ? 100 : null;
        return number + (suffixes[a] || suffixes[b] || suffixes[c]);
    },
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 7  // The week that contains Jan 1st is the first week of the year.
    }
});

