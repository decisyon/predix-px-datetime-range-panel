//! moment.js locale configuration
//! locale : faroese (fo)
//! author : Ragnar Johannesen : https://github.com/ragnar123

import moment from '../moment';

export default moment.defineLocale('fo', {
    months : 'januar_februar_mars_aprÃÂ­l_mai_juni_juli_august_september_oktober_november_desember'.split('_'),
    monthsShort : 'jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des'.split('_'),
    weekdays : 'sunnudagur_mÃÂ¡nadagur_tÃÂ½sdagur_mikudagur_hÃÂ³sdagur_frÃÂ­ggjadagur_leygardagur'.split('_'),
    weekdaysShort : 'sun_mÃÂ¡n_tÃÂ½s_mik_hÃÂ³s_frÃÂ­_ley'.split('_'),
    weekdaysMin : 'su_mÃÂ¡_tÃÂ½_mi_hÃÂ³_fr_le'.split('_'),
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD/MM/YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY HH:mm',
        LLLL : 'dddd D. MMMM, YYYY HH:mm'
    },
    calendar : {
        sameDay : '[ÃÂ dag kl.] LT',
        nextDay : '[ÃÂ morgin kl.] LT',
        nextWeek : 'dddd [kl.] LT',
        lastDay : '[ÃÂ gjÃÂ¡r kl.] LT',
        lastWeek : '[sÃÂ­ÃÂ°stu] dddd [kl] LT',
        sameElse : 'L'
    },
    relativeTime : {
        future : 'um %s',
        past : '%s sÃÂ­ÃÂ°ani',
        s : 'fÃÂ¡ sekund',
        m : 'ein minutt',
        mm : '%d minuttir',
        h : 'ein tÃÂ­mi',
        hh : '%d tÃÂ­mar',
        d : 'ein dagur',
        dd : '%d dagar',
        M : 'ein mÃÂ¡naÃÂ°i',
        MM : '%d mÃÂ¡naÃÂ°ir',
        y : 'eitt ÃÂ¡r',
        yy : '%d ÃÂ¡r'
    },
    ordinalParse: /\d{1,2}\./,
    ordinal : '%d.',
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
    }
});

