//! moment.js locale configuration
//! locale : great britain scottish gealic (gd)
//! author : Jon Ashdown : https://github.com/jonashdown

import moment from '../moment';

var months = [
    'Am Faoilleach', 'An Gearran', 'Am MÃÂ rt', 'An Giblean', 'An CÃÂ¨itean', 'An t-ÃÂgmhios', 'An t-Iuchar', 'An LÃÂ¹nastal', 'An t-Sultain', 'An DÃÂ mhair', 'An t-Samhain', 'An DÃÂ¹bhlachd'
];

var monthsShort = ['Faoi', 'Gear', 'MÃÂ rt', 'Gibl', 'CÃÂ¨it', 'ÃÂgmh', 'Iuch', 'LÃÂ¹n', 'Sult', 'DÃÂ mh', 'Samh', 'DÃÂ¹bh'];

var weekdays = ['DidÃÂ²mhnaich', 'Diluain', 'DimÃÂ irt', 'Diciadain', 'Diardaoin', 'Dihaoine', 'Disathairne'];

var weekdaysShort = ['Did', 'Dil', 'Dim', 'Dic', 'Dia', 'Dih', 'Dis'];

var weekdaysMin = ['DÃÂ²', 'Lu', 'MÃÂ ', 'Ci', 'Ar', 'Ha', 'Sa'];

export default moment.defineLocale('gd', {
    months : months,
    monthsShort : monthsShort,
    monthsParseExact : true,
    weekdays : weekdays,
    weekdaysShort : weekdaysShort,
    weekdaysMin : weekdaysMin,
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD/MM/YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY HH:mm',
        LLLL : 'dddd, D MMMM YYYY HH:mm'
    },
    calendar : {
        sameDay : '[An-diugh aig] LT',
        nextDay : '[A-mÃÂ ireach aig] LT',
        nextWeek : 'dddd [aig] LT',
        lastDay : '[An-dÃÂ¨ aig] LT',
        lastWeek : 'dddd [seo chaidh] [aig] LT',
        sameElse : 'L'
    },
    relativeTime : {
        future : 'ann an %s',
        past : 'bho chionn %s',
        s : 'beagan diogan',
        m : 'mionaid',
        mm : '%d mionaidean',
        h : 'uair',
        hh : '%d uairean',
        d : 'latha',
        dd : '%d latha',
        M : 'mÃÂ¬os',
        MM : '%d mÃÂ¬osan',
        y : 'bliadhna',
        yy : '%d bliadhna'
    },
    ordinalParse : /\d{1,2}(d|na|mh)/,
    ordinal : function (number) {
        var output = number === 1 ? 'd' : number % 10 === 2 ? 'na' : 'mh';
        return number + output;
    },
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
    }
});

