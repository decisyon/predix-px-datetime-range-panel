//! moment.js locale configuration
//! locale : hungarian (hu)
//! author : Adam Brunner : https://github.com/adambrunner

import moment from '../moment';

var weekEndings = 'vasÃÂ¡rnap hÃÂ©tfÃÂn kedden szerdÃÂ¡n csÃÂ¼tÃÂ¶rtÃÂ¶kÃÂ¶n pÃÂ©nteken szombaton'.split(' ');
function translate(number, withoutSuffix, key, isFuture) {
    var num = number,
        suffix;
    switch (key) {
    case 's':
        return (isFuture || withoutSuffix) ? 'nÃÂ©hÃÂ¡ny mÃÂ¡sodperc' : 'nÃÂ©hÃÂ¡ny mÃÂ¡sodperce';
    case 'm':
        return 'egy' + (isFuture || withoutSuffix ? ' perc' : ' perce');
    case 'mm':
        return num + (isFuture || withoutSuffix ? ' perc' : ' perce');
    case 'h':
        return 'egy' + (isFuture || withoutSuffix ? ' ÃÂ³ra' : ' ÃÂ³rÃÂ¡ja');
    case 'hh':
        return num + (isFuture || withoutSuffix ? ' ÃÂ³ra' : ' ÃÂ³rÃÂ¡ja');
    case 'd':
        return 'egy' + (isFuture || withoutSuffix ? ' nap' : ' napja');
    case 'dd':
        return num + (isFuture || withoutSuffix ? ' nap' : ' napja');
    case 'M':
        return 'egy' + (isFuture || withoutSuffix ? ' hÃÂ³nap' : ' hÃÂ³napja');
    case 'MM':
        return num + (isFuture || withoutSuffix ? ' hÃÂ³nap' : ' hÃÂ³napja');
    case 'y':
        return 'egy' + (isFuture || withoutSuffix ? ' ÃÂ©v' : ' ÃÂ©ve');
    case 'yy':
        return num + (isFuture || withoutSuffix ? ' ÃÂ©v' : ' ÃÂ©ve');
    }
    return '';
}
function week(isFuture) {
    return (isFuture ? '' : '[mÃÂºlt] ') + '[' + weekEndings[this.day()] + '] LT[-kor]';
}

export default moment.defineLocale('hu', {
    months : 'januÃÂ¡r_februÃÂ¡r_mÃÂ¡rcius_ÃÂ¡prilis_mÃÂ¡jus_jÃÂºnius_jÃÂºlius_augusztus_szeptember_oktÃÂ³ber_november_december'.split('_'),
    monthsShort : 'jan_feb_mÃÂ¡rc_ÃÂ¡pr_mÃÂ¡j_jÃÂºn_jÃÂºl_aug_szept_okt_nov_dec'.split('_'),
    weekdays : 'vasÃÂ¡rnap_hÃÂ©tfÃÂ_kedd_szerda_csÃÂ¼tÃÂ¶rtÃÂ¶k_pÃÂ©ntek_szombat'.split('_'),
    weekdaysShort : 'vas_hÃÂ©t_kedd_sze_csÃÂ¼t_pÃÂ©n_szo'.split('_'),
    weekdaysMin : 'v_h_k_sze_cs_p_szo'.split('_'),
    longDateFormat : {
        LT : 'H:mm',
        LTS : 'H:mm:ss',
        L : 'YYYY.MM.DD.',
        LL : 'YYYY. MMMM D.',
        LLL : 'YYYY. MMMM D. H:mm',
        LLLL : 'YYYY. MMMM D., dddd H:mm'
    },
    meridiemParse: /de|du/i,
    isPM: function (input) {
        return input.charAt(1).toLowerCase() === 'u';
    },
    meridiem : function (hours, minutes, isLower) {
        if (hours < 12) {
            return isLower === true ? 'de' : 'DE';
        } else {
            return isLower === true ? 'du' : 'DU';
        }
    },
    calendar : {
        sameDay : '[ma] LT[-kor]',
        nextDay : '[holnap] LT[-kor]',
        nextWeek : function () {
            return week.call(this, true);
        },
        lastDay : '[tegnap] LT[-kor]',
        lastWeek : function () {
            return week.call(this, false);
        },
        sameElse : 'L'
    },
    relativeTime : {
        future : '%s mÃÂºlva',
        past : '%s',
        s : translate,
        m : translate,
        mm : translate,
        h : translate,
        hh : translate,
        d : translate,
        dd : translate,
        M : translate,
        MM : translate,
        y : translate,
        yy : translate
    },
    ordinalParse: /\d{1,2}\./,
    ordinal : '%d.',
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 7  // The week that contains Jan 1st is the first week of the year.
    }
});

