//! moment.js locale configuration
//! locale : Georgian (ka)
//! author : Irakli Janiashvili : https://github.com/irakli-janiashvili

import moment from '../moment';

export default moment.defineLocale('ka', {
    months : {
        standalone: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¢Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ¢Ã¡ÂÂ_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂ¥Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ¥Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ'.split('_'),
        format: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¢Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂ¢Ã¡ÂÂ¡_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂ¥Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂ¥Ã¡ÂÂ¢Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¡'.split('_')
    },
    monthsShort : 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ _Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ _Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂ¥_Ã¡ÂÂÃ¡ÂÂ¥Ã¡ÂÂ¢_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ'.split('_'),
    weekdays : {
        standalone: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ®Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ®Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ'.split('_'),
        format: 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ®Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂ®Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂÃ¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡_Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡'.split('_'),
        isFormat: /(Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ)/
    },
    weekdaysShort : 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¨_Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ®_Ã¡ÂÂ®Ã¡ÂÂ£Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ _Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂ'.split('_'),
    weekdaysMin : 'Ã¡ÂÂÃ¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ _Ã¡ÂÂ¡Ã¡ÂÂ_Ã¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ®Ã¡ÂÂ£_Ã¡ÂÂÃ¡ÂÂ_Ã¡ÂÂ¨Ã¡ÂÂ'.split('_'),
    longDateFormat : {
        LT : 'h:mm A',
        LTS : 'h:mm:ss A',
        L : 'DD/MM/YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY h:mm A',
        LLLL : 'dddd, D MMMM YYYY h:mm A'
    },
    calendar : {
        sameDay : '[Ã¡ÂÂÃ¡ÂÂ¦Ã¡ÂÂÃ¡ÂÂ¡] LT[-Ã¡ÂÂÃ¡ÂÂ]',
        nextDay : '[Ã¡ÂÂ®Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ] LT[-Ã¡ÂÂÃ¡ÂÂ]',
        lastDay : '[Ã¡ÂÂÃ¡ÂÂ£Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂ] LT[-Ã¡ÂÂÃ¡ÂÂ]',
        nextWeek : '[Ã¡ÂÂ¨Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ] dddd LT[-Ã¡ÂÂÃ¡ÂÂ]',
        lastWeek : '[Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ] dddd LT-Ã¡ÂÂÃ¡ÂÂ',
        sameElse : 'L'
    },
    relativeTime : {
        future : function (s) {
            return (/(Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¬Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ)/).test(s) ?
                s.replace(/Ã¡ÂÂ$/, 'Ã¡ÂÂ¨Ã¡ÂÂ') :
                s + 'Ã¡ÂÂ¨Ã¡ÂÂ';
        },
        past : function (s) {
            if ((/(Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¬Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ|Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ|Ã¡ÂÂÃ¡ÂÂ¦Ã¡ÂÂ|Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ)/).test(s)) {
                return s.replace(/(Ã¡ÂÂ|Ã¡ÂÂ)$/, 'Ã¡ÂÂÃ¡ÂÂ¡ Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂ');
            }
            if ((/Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ/).test(s)) {
                return s.replace(/Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ$/, 'Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ¡ Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂ');
            }
        },
        s : 'Ã¡ÂÂ Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
        m : 'Ã¡ÂÂ¬Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ',
        mm : '%d Ã¡ÂÂ¬Ã¡ÂÂ£Ã¡ÂÂÃ¡ÂÂ',
        h : 'Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
        hh : '%d Ã¡ÂÂ¡Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
        d : 'Ã¡ÂÂÃ¡ÂÂ¦Ã¡ÂÂ',
        dd : '%d Ã¡ÂÂÃ¡ÂÂ¦Ã¡ÂÂ',
        M : 'Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
        MM : '%d Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
        y : 'Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ',
        yy : '%d Ã¡ÂÂ¬Ã¡ÂÂÃ¡ÂÂÃ¡ÂÂ'
    },
    ordinalParse: /0|1-Ã¡ÂÂÃ¡ÂÂ|Ã¡ÂÂÃ¡ÂÂ-\d{1,2}|\d{1,2}-Ã¡ÂÂ/,
    ordinal : function (number) {
        if (number === 0) {
            return number;
        }
        if (number === 1) {
            return number + '-Ã¡ÂÂÃ¡ÂÂ';
        }
        if ((number < 20) || (number <= 100 && (number % 20 === 0)) || (number % 100 === 0)) {
            return 'Ã¡ÂÂÃ¡ÂÂ-' + number;
        }
        return number + '-Ã¡ÂÂ';
    },
    week : {
        dow : 1,
        doy : 7
    }
});

