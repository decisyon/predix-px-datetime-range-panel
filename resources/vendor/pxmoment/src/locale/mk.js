//! moment.js locale configuration
//! locale : macedonian (mk)
//! author : Borislav Mickov : https://github.com/B0k0

import moment from '../moment';

export default moment.defineLocale('mk', {
    months : 'ÃÂÃÂ°ÃÂ½ÃÂÃÂ°ÃÂÃÂ¸_ÃÂÃÂµÃÂ²ÃÂÃÂÃÂ°ÃÂÃÂ¸_ÃÂ¼ÃÂ°ÃÂÃÂ_ÃÂ°ÃÂ¿ÃÂÃÂ¸ÃÂ»_ÃÂ¼ÃÂ°ÃÂ_ÃÂÃÂÃÂ½ÃÂ¸_ÃÂÃÂÃÂ»ÃÂ¸_ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ¿ÃÂÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¸_ÃÂ¾ÃÂºÃÂÃÂ¾ÃÂ¼ÃÂ²ÃÂÃÂ¸_ÃÂ½ÃÂ¾ÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¸_ÃÂ´ÃÂµÃÂºÃÂµÃÂ¼ÃÂ²ÃÂÃÂ¸'.split('_'),
    monthsShort : 'ÃÂÃÂ°ÃÂ½_ÃÂÃÂµÃÂ²_ÃÂ¼ÃÂ°ÃÂ_ÃÂ°ÃÂ¿ÃÂ_ÃÂ¼ÃÂ°ÃÂ_ÃÂÃÂÃÂ½_ÃÂÃÂÃÂ»_ÃÂ°ÃÂ²ÃÂ³_ÃÂÃÂµÃÂ¿_ÃÂ¾ÃÂºÃÂ_ÃÂ½ÃÂ¾ÃÂµ_ÃÂ´ÃÂµÃÂº'.split('_'),
    weekdays : 'ÃÂ½ÃÂµÃÂ´ÃÂµÃÂ»ÃÂ°_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂµÃÂ»ÃÂ½ÃÂ¸ÃÂº_ÃÂ²ÃÂÃÂ¾ÃÂÃÂ½ÃÂ¸ÃÂº_ÃÂÃÂÃÂµÃÂ´ÃÂ°_ÃÂÃÂµÃÂÃÂ²ÃÂÃÂÃÂ¾ÃÂº_ÃÂ¿ÃÂµÃÂÃÂ¾ÃÂº_ÃÂÃÂ°ÃÂ±ÃÂ¾ÃÂÃÂ°'.split('_'),
    weekdaysShort : 'ÃÂ½ÃÂµÃÂ´_ÃÂ¿ÃÂ¾ÃÂ½_ÃÂ²ÃÂÃÂ¾_ÃÂÃÂÃÂµ_ÃÂÃÂµÃÂ_ÃÂ¿ÃÂµÃÂ_ÃÂÃÂ°ÃÂ±'.split('_'),
    weekdaysMin : 'ÃÂ½e_ÃÂ¿o_ÃÂ²ÃÂ_ÃÂÃÂ_ÃÂÃÂµ_ÃÂ¿ÃÂµ_ÃÂa'.split('_'),
    longDateFormat : {
        LT : 'H:mm',
        LTS : 'H:mm:ss',
        L : 'D.MM.YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY H:mm',
        LLLL : 'dddd, D MMMM YYYY H:mm'
    },
    calendar : {
        sameDay : '[ÃÂÃÂµÃÂ½ÃÂµÃÂ ÃÂ²ÃÂ¾] LT',
        nextDay : '[ÃÂ£ÃÂÃÂÃÂµ ÃÂ²ÃÂ¾] LT',
        nextWeek : '[ÃÂÃÂ¾] dddd [ÃÂ²ÃÂ¾] LT',
        lastDay : '[ÃÂÃÂÃÂµÃÂÃÂ° ÃÂ²ÃÂ¾] LT',
        lastWeek : function () {
            switch (this.day()) {
            case 0:
            case 3:
            case 6:
                return '[ÃÂÃÂ·ÃÂ¼ÃÂ¸ÃÂ½ÃÂ°ÃÂÃÂ°ÃÂÃÂ°] dddd [ÃÂ²ÃÂ¾] LT';
            case 1:
            case 2:
            case 4:
            case 5:
                return '[ÃÂÃÂ·ÃÂ¼ÃÂ¸ÃÂ½ÃÂ°ÃÂÃÂ¸ÃÂ¾ÃÂ] dddd [ÃÂ²ÃÂ¾] LT';
            }
        },
        sameElse : 'L'
    },
    relativeTime : {
        future : 'ÃÂ¿ÃÂ¾ÃÂÃÂ»ÃÂµ %s',
        past : 'ÃÂ¿ÃÂÃÂµÃÂ´ %s',
        s : 'ÃÂ½ÃÂµÃÂºÃÂ¾ÃÂ»ÃÂºÃÂ ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´ÃÂ¸',
        m : 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ°',
        mm : '%d ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ¸',
        h : 'ÃÂÃÂ°ÃÂ',
        hh : '%d ÃÂÃÂ°ÃÂÃÂ°',
        d : 'ÃÂ´ÃÂµÃÂ½',
        dd : '%d ÃÂ´ÃÂµÃÂ½ÃÂ°',
        M : 'ÃÂ¼ÃÂµÃÂÃÂµÃÂ',
        MM : '%d ÃÂ¼ÃÂµÃÂÃÂµÃÂÃÂ¸',
        y : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°',
        yy : '%d ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ¸'
    },
    ordinalParse: /\d{1,2}-(ÃÂµÃÂ²|ÃÂµÃÂ½|ÃÂÃÂ¸|ÃÂ²ÃÂ¸|ÃÂÃÂ¸|ÃÂ¼ÃÂ¸)/,
    ordinal : function (number) {
        var lastDigit = number % 10,
            last2Digits = number % 100;
        if (number === 0) {
            return number + '-ÃÂµÃÂ²';
        } else if (last2Digits === 0) {
            return number + '-ÃÂµÃÂ½';
        } else if (last2Digits > 10 && last2Digits < 20) {
            return number + '-ÃÂÃÂ¸';
        } else if (lastDigit === 1) {
            return number + '-ÃÂ²ÃÂ¸';
        } else if (lastDigit === 2) {
            return number + '-ÃÂÃÂ¸';
        } else if (lastDigit === 7 || lastDigit === 8) {
            return number + '-ÃÂ¼ÃÂ¸';
        } else {
            return number + '-ÃÂÃÂ¸';
        }
    },
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 7  // The week that contains Jan 1st is the first week of the year.
    }
});

