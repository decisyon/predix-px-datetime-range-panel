//! moment.js locale configuration
//! locale : Marathi (mr)
//! author : Harshad Kale : https://github.com/kalehv
//! author : Vivek Athalye : https://github.com/vnathalye

import moment from '../moment';

var symbolMap = {
    '1': 'Ã Â¥Â§',
    '2': 'Ã Â¥Â¨',
    '3': 'Ã Â¥Â©',
    '4': 'Ã Â¥Âª',
    '5': 'Ã Â¥Â«',
    '6': 'Ã Â¥Â¬',
    '7': 'Ã Â¥Â­',
    '8': 'Ã Â¥Â®',
    '9': 'Ã Â¥Â¯',
    '0': 'Ã Â¥Â¦'
},
numberMap = {
    'Ã Â¥Â§': '1',
    'Ã Â¥Â¨': '2',
    'Ã Â¥Â©': '3',
    'Ã Â¥Âª': '4',
    'Ã Â¥Â«': '5',
    'Ã Â¥Â¬': '6',
    'Ã Â¥Â­': '7',
    'Ã Â¥Â®': '8',
    'Ã Â¥Â¯': '9',
    'Ã Â¥Â¦': '0'
};

function relativeTimeMr(number, withoutSuffix, string, isFuture)
{
    var output = '';
    if (withoutSuffix) {
        switch (string) {
            case 's': output = 'Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â¹Ã Â¥Â Ã Â¤Â¸Ã Â¥ÂÃ Â¤ÂÃ Â¤ÂÃ Â¤Â¦'; break;
            case 'm': output = 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¿Ã Â¤Â'; break;
            case 'mm': output = '%d Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¿Ã Â¤ÂÃ Â¥Â'; break;
            case 'h': output = 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â¤Ã Â¤Â¾Ã Â¤Â¸'; break;
            case 'hh': output = '%d Ã Â¤Â¤Ã Â¤Â¾Ã Â¤Â¸'; break;
            case 'd': output = 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¸'; break;
            case 'dd': output = '%d Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¸'; break;
            case 'M': output = 'Ã Â¤ÂÃ Â¤Â Ã Â¤Â®Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¾'; break;
            case 'MM': output = '%d Ã Â¤Â®Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¨Ã Â¥Â'; break;
            case 'y': output = 'Ã Â¤ÂÃ Â¤Â Ã Â¤ÂµÃ Â¤Â°Ã Â¥ÂÃ Â¤Â·'; break;
            case 'yy': output = '%d Ã Â¤ÂµÃ Â¤Â°Ã Â¥ÂÃ Â¤Â·Ã Â¥Â'; break;
        }
    }
    else {
        switch (string) {
            case 's': output = 'Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â¹Ã Â¥Â Ã Â¤Â¸Ã Â¥ÂÃ Â¤ÂÃ Â¤ÂÃ Â¤Â¦Ã Â¤Â¾Ã Â¤Â'; break;
            case 'm': output = 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾ Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¿Ã Â¤ÂÃ Â¤Â¾'; break;
            case 'mm': output = '%d Ã Â¤Â®Ã Â¤Â¿Ã Â¤Â¨Ã Â¤Â¿Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â'; break;
            case 'h': output = 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾ Ã Â¤Â¤Ã Â¤Â¾Ã Â¤Â¸Ã Â¤Â¾'; break;
            case 'hh': output = '%d Ã Â¤Â¤Ã Â¤Â¾Ã Â¤Â¸Ã Â¤Â¾Ã Â¤Â'; break;
            case 'd': output = 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾ Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¸Ã Â¤Â¾'; break;
            case 'dd': output = '%d Ã Â¤Â¦Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¸Ã Â¤Â¾Ã Â¤Â'; break;
            case 'M': output = 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾ Ã Â¤Â®Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¨Ã Â¥ÂÃ Â¤Â¯Ã Â¤Â¾'; break;
            case 'MM': output = '%d Ã Â¤Â®Ã Â¤Â¹Ã Â¤Â¿Ã Â¤Â¨Ã Â¥ÂÃ Â¤Â¯Ã Â¤Â¾Ã Â¤Â'; break;
            case 'y': output = 'Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾ Ã Â¤ÂµÃ Â¤Â°Ã Â¥ÂÃ Â¤Â·Ã Â¤Â¾'; break;
            case 'yy': output = '%d Ã Â¤ÂµÃ Â¤Â°Ã Â¥ÂÃ Â¤Â·Ã Â¤Â¾Ã Â¤Â'; break;
        }
    }
    return output.replace(/%d/i, number);
}

export default moment.defineLocale('mr', {
    months : 'Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â¨Ã Â¥ÂÃ Â¤ÂµÃ Â¤Â¾Ã Â¤Â°Ã Â¥Â_Ã Â¤Â«Ã Â¥ÂÃ Â¤Â¬Ã Â¥ÂÃ Â¤Â°Ã Â¥ÂÃ Â¤ÂµÃ Â¤Â¾Ã Â¤Â°Ã Â¥Â_Ã Â¤Â®Ã Â¤Â¾Ã Â¤Â°Ã Â¥ÂÃ Â¤Â_Ã Â¤ÂÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â°Ã Â¤Â¿Ã Â¤Â²_Ã Â¤Â®Ã Â¥Â_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â¨_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â²Ã Â¥Â_Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¸Ã Â¥ÂÃ Â¤Â_Ã Â¤Â¸Ã Â¤ÂªÃ Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤Â¨Ã Â¥ÂÃ Â¤ÂµÃ Â¥ÂÃ Â¤Â¹Ã Â¥ÂÃ Â¤ÂÃ Â¤Â¬Ã Â¤Â°_Ã Â¤Â¡Ã Â¤Â¿Ã Â¤Â¸Ã Â¥ÂÃ Â¤ÂÃ Â¤Â¬Ã Â¤Â°'.split('_'),
    monthsShort: 'Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â¨Ã Â¥Â._Ã Â¤Â«Ã Â¥ÂÃ Â¤Â¬Ã Â¥ÂÃ Â¤Â°Ã Â¥Â._Ã Â¤Â®Ã Â¤Â¾Ã Â¤Â°Ã Â¥ÂÃ Â¤Â._Ã Â¤ÂÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â°Ã Â¤Â¿._Ã Â¤Â®Ã Â¥Â._Ã Â¤ÂÃ Â¥ÂÃ Â¤Â¨._Ã Â¤ÂÃ Â¥ÂÃ Â¤Â²Ã Â¥Â._Ã Â¤ÂÃ Â¤Â._Ã Â¤Â¸Ã Â¤ÂªÃ Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â._Ã Â¤ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤ÂÃ Â¥Â._Ã Â¤Â¨Ã Â¥ÂÃ Â¤ÂµÃ Â¥ÂÃ Â¤Â¹Ã Â¥ÂÃ Â¤Â._Ã Â¤Â¡Ã Â¤Â¿Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â.'.split('_'),
    monthsParseExact : true,
    weekdays : 'Ã Â¤Â°Ã Â¤ÂµÃ Â¤Â¿Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â®Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â®Ã Â¤ÂÃ Â¤ÂÃ Â¤Â³Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¬Ã Â¥ÂÃ Â¤Â§Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â°Ã Â¥ÂÃ Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¶Ã Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â°Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°_Ã Â¤Â¶Ã Â¤Â¨Ã Â¤Â¿Ã Â¤ÂµÃ Â¤Â¾Ã Â¤Â°'.split('_'),
    weekdaysShort : 'Ã Â¤Â°Ã Â¤ÂµÃ Â¤Â¿_Ã Â¤Â¸Ã Â¥ÂÃ Â¤Â®_Ã Â¤Â®Ã Â¤ÂÃ Â¤ÂÃ Â¤Â³_Ã Â¤Â¬Ã Â¥ÂÃ Â¤Â§_Ã Â¤ÂÃ Â¥ÂÃ Â¤Â°Ã Â¥Â_Ã Â¤Â¶Ã Â¥ÂÃ Â¤ÂÃ Â¥ÂÃ Â¤Â°_Ã Â¤Â¶Ã Â¤Â¨Ã Â¤Â¿'.split('_'),
    weekdaysMin : 'Ã Â¤Â°_Ã Â¤Â¸Ã Â¥Â_Ã Â¤Â®Ã Â¤Â_Ã Â¤Â¬Ã Â¥Â_Ã Â¤ÂÃ Â¥Â_Ã Â¤Â¶Ã Â¥Â_Ã Â¤Â¶'.split('_'),
    longDateFormat : {
        LT : 'A h:mm Ã Â¤ÂµÃ Â¤Â¾Ã Â¤ÂÃ Â¤Â¤Ã Â¤Â¾',
        LTS : 'A h:mm:ss Ã Â¤ÂµÃ Â¤Â¾Ã Â¤ÂÃ Â¤Â¤Ã Â¤Â¾',
        L : 'DD/MM/YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY, A h:mm Ã Â¤ÂµÃ Â¤Â¾Ã Â¤ÂÃ Â¤Â¤Ã Â¤Â¾',
        LLLL : 'dddd, D MMMM YYYY, A h:mm Ã Â¤ÂµÃ Â¤Â¾Ã Â¤ÂÃ Â¤Â¤Ã Â¤Â¾'
    },
    calendar : {
        sameDay : '[Ã Â¤ÂÃ Â¤Â] LT',
        nextDay : '[Ã Â¤ÂÃ Â¤Â¦Ã Â¥ÂÃ Â¤Â¯Ã Â¤Â¾] LT',
        nextWeek : 'dddd, LT',
        lastDay : '[Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â²] LT',
        lastWeek: '[Ã Â¤Â®Ã Â¤Â¾Ã Â¤ÂÃ Â¥ÂÃ Â¤Â²] dddd, LT',
        sameElse : 'L'
    },
    relativeTime : {
        future: '%sÃ Â¤Â®Ã Â¤Â§Ã Â¥ÂÃ Â¤Â¯Ã Â¥Â',
        past: '%sÃ Â¤ÂªÃ Â¥ÂÃ Â¤Â°Ã Â¥ÂÃ Â¤ÂµÃ Â¥Â',
        s: relativeTimeMr,
        m: relativeTimeMr,
        mm: relativeTimeMr,
        h: relativeTimeMr,
        hh: relativeTimeMr,
        d: relativeTimeMr,
        dd: relativeTimeMr,
        M: relativeTimeMr,
        MM: relativeTimeMr,
        y: relativeTimeMr,
        yy: relativeTimeMr
    },
    preparse: function (string) {
        return string.replace(/[Ã Â¥Â§Ã Â¥Â¨Ã Â¥Â©Ã Â¥ÂªÃ Â¥Â«Ã Â¥Â¬Ã Â¥Â­Ã Â¥Â®Ã Â¥Â¯Ã Â¥Â¦]/g, function (match) {
            return numberMap[match];
        });
    },
    postformat: function (string) {
        return string.replace(/\d/g, function (match) {
            return symbolMap[match];
        });
    },
    meridiemParse: /Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¥ÂÃ Â¤Â°Ã Â¥Â|Ã Â¤Â¸Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â|Ã Â¤Â¦Ã Â¥ÂÃ Â¤ÂªÃ Â¤Â¾Ã Â¤Â°Ã Â¥Â|Ã Â¤Â¸Ã Â¤Â¾Ã Â¤Â¯Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â/,
    meridiemHour : function (hour, meridiem) {
        if (hour === 12) {
            hour = 0;
        }
        if (meridiem === 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¥ÂÃ Â¤Â°Ã Â¥Â') {
            return hour < 4 ? hour : hour + 12;
        } else if (meridiem === 'Ã Â¤Â¸Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â') {
            return hour;
        } else if (meridiem === 'Ã Â¤Â¦Ã Â¥ÂÃ Â¤ÂªÃ Â¤Â¾Ã Â¤Â°Ã Â¥Â') {
            return hour >= 10 ? hour : hour + 12;
        } else if (meridiem === 'Ã Â¤Â¸Ã Â¤Â¾Ã Â¤Â¯Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â') {
            return hour + 12;
        }
    },
    meridiem: function (hour, minute, isLower) {
        if (hour < 4) {
            return 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¥ÂÃ Â¤Â°Ã Â¥Â';
        } else if (hour < 10) {
            return 'Ã Â¤Â¸Ã Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â';
        } else if (hour < 17) {
            return 'Ã Â¤Â¦Ã Â¥ÂÃ Â¤ÂªÃ Â¤Â¾Ã Â¤Â°Ã Â¥Â';
        } else if (hour < 20) {
            return 'Ã Â¤Â¸Ã Â¤Â¾Ã Â¤Â¯Ã Â¤ÂÃ Â¤ÂÃ Â¤Â¾Ã Â¤Â³Ã Â¥Â';
        } else {
            return 'Ã Â¤Â°Ã Â¤Â¾Ã Â¤Â¤Ã Â¥ÂÃ Â¤Â°Ã Â¥Â';
        }
    },
    week : {
        dow : 0, // Sunday is the first day of the week.
        doy : 6  // The week that contains Jan 1st is the first week of the year.
    }
});

