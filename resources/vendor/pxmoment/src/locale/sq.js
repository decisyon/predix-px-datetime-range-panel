//! moment.js locale configuration
//! locale : Albanian (sq)
//! author : FlakÃÂ«rim Ismani : https://github.com/flakerimi
//! author: Menelion ElensÃÂºle: https://github.com/Oire (tests)
//! author : Oerd Cukalla : https://github.com/oerd (fixes)

import moment from '../moment';

export default moment.defineLocale('sq', {
    months : 'Janar_Shkurt_Mars_Prill_Maj_Qershor_Korrik_Gusht_Shtator_Tetor_NÃÂ«ntor_Dhjetor'.split('_'),
    monthsShort : 'Jan_Shk_Mar_Pri_Maj_Qer_Kor_Gus_Sht_Tet_NÃÂ«n_Dhj'.split('_'),
    weekdays : 'E Diel_E HÃÂ«nÃÂ«_E MartÃÂ«_E MÃÂ«rkurÃÂ«_E Enjte_E Premte_E ShtunÃÂ«'.split('_'),
    weekdaysShort : 'Die_HÃÂ«n_Mar_MÃÂ«r_Enj_Pre_Sht'.split('_'),
    weekdaysMin : 'D_H_Ma_MÃÂ«_E_P_Sh'.split('_'),
    weekdaysParseExact : true,
    meridiemParse: /PD|MD/,
    isPM: function (input) {
        return input.charAt(0) === 'M';
    },
    meridiem : function (hours, minutes, isLower) {
        return hours < 12 ? 'PD' : 'MD';
    },
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD/MM/YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY HH:mm',
        LLLL : 'dddd, D MMMM YYYY HH:mm'
    },
    calendar : {
        sameDay : '[Sot nÃÂ«] LT',
        nextDay : '[NesÃÂ«r nÃÂ«] LT',
        nextWeek : 'dddd [nÃÂ«] LT',
        lastDay : '[Dje nÃÂ«] LT',
        lastWeek : 'dddd [e kaluar nÃÂ«] LT',
        sameElse : 'L'
    },
    relativeTime : {
        future : 'nÃÂ« %s',
        past : '%s mÃÂ« parÃÂ«',
        s : 'disa sekonda',
        m : 'njÃÂ« minutÃÂ«',
        mm : '%d minuta',
        h : 'njÃÂ« orÃÂ«',
        hh : '%d orÃÂ«',
        d : 'njÃÂ« ditÃÂ«',
        dd : '%d ditÃÂ«',
        M : 'njÃÂ« muaj',
        MM : '%d muaj',
        y : 'njÃÂ« vit',
        yy : '%d vite'
    },
    ordinalParse: /\d{1,2}\./,
    ordinal : '%d.',
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
    }
});

