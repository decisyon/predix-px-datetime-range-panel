//! moment.js locale configuration
//! locale : Serbian-cyrillic (sr-cyrl)
//! author : Milan JanaÃÂkoviÃÂ<milanjanackovic@gmail.com> : https://github.com/milan-j

import moment from '../moment';

var translator = {
    words: { //Different grammatical cases
        m: ['ÃÂÃÂµÃÂ´ÃÂ°ÃÂ½ ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ', 'ÃÂÃÂµÃÂ´ÃÂ½ÃÂµ ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂµ'],
        mm: ['ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂ', 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂµ', 'ÃÂ¼ÃÂ¸ÃÂ½ÃÂÃÂÃÂ°'],
        h: ['ÃÂÃÂµÃÂ´ÃÂ°ÃÂ½ ÃÂÃÂ°ÃÂ', 'ÃÂÃÂµÃÂ´ÃÂ½ÃÂ¾ÃÂ³ ÃÂÃÂ°ÃÂÃÂ°'],
        hh: ['ÃÂÃÂ°ÃÂ', 'ÃÂÃÂ°ÃÂÃÂ°', 'ÃÂÃÂ°ÃÂÃÂ¸'],
        dd: ['ÃÂ´ÃÂ°ÃÂ½', 'ÃÂ´ÃÂ°ÃÂ½ÃÂ°', 'ÃÂ´ÃÂ°ÃÂ½ÃÂ°'],
        MM: ['ÃÂ¼ÃÂµÃÂÃÂµÃÂ', 'ÃÂ¼ÃÂµÃÂÃÂµÃÂÃÂ°', 'ÃÂ¼ÃÂµÃÂÃÂµÃÂÃÂ¸'],
        yy: ['ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°', 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂµ', 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ°']
    },
    correctGrammaticalCase: function (number, wordKey) {
        return number === 1 ? wordKey[0] : (number >= 2 && number <= 4 ? wordKey[1] : wordKey[2]);
    },
    translate: function (number, withoutSuffix, key) {
        var wordKey = translator.words[key];
        if (key.length === 1) {
            return withoutSuffix ? wordKey[0] : wordKey[1];
        } else {
            return number + ' ' + translator.correctGrammaticalCase(number, wordKey);
        }
    }
};

export default moment.defineLocale('sr-cyrl', {
    months: 'ÃÂÃÂ°ÃÂ½ÃÂÃÂ°ÃÂ_ÃÂÃÂµÃÂ±ÃÂÃÂÃÂ°ÃÂ_ÃÂ¼ÃÂ°ÃÂÃÂ_ÃÂ°ÃÂ¿ÃÂÃÂ¸ÃÂ»_ÃÂ¼ÃÂ°ÃÂ_ÃÂÃÂÃÂ½_ÃÂÃÂÃÂ»_ÃÂ°ÃÂ²ÃÂ³ÃÂÃÂÃÂ_ÃÂÃÂµÃÂ¿ÃÂÃÂµÃÂ¼ÃÂ±ÃÂ°ÃÂ_ÃÂ¾ÃÂºÃÂÃÂ¾ÃÂ±ÃÂ°ÃÂ_ÃÂ½ÃÂ¾ÃÂ²ÃÂµÃÂ¼ÃÂ±ÃÂ°ÃÂ_ÃÂ´ÃÂµÃÂÃÂµÃÂ¼ÃÂ±ÃÂ°ÃÂ'.split('_'),
    monthsShort: 'ÃÂÃÂ°ÃÂ½._ÃÂÃÂµÃÂ±._ÃÂ¼ÃÂ°ÃÂ._ÃÂ°ÃÂ¿ÃÂ._ÃÂ¼ÃÂ°ÃÂ_ÃÂÃÂÃÂ½_ÃÂÃÂÃÂ»_ÃÂ°ÃÂ²ÃÂ³._ÃÂÃÂµÃÂ¿._ÃÂ¾ÃÂºÃÂ._ÃÂ½ÃÂ¾ÃÂ²._ÃÂ´ÃÂµÃÂ.'.split('_'),
    monthsParseExact: true,
    weekdays: 'ÃÂ½ÃÂµÃÂ´ÃÂµÃÂÃÂ°_ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂµÃÂÃÂ°ÃÂº_ÃÂÃÂÃÂ¾ÃÂÃÂ°ÃÂº_ÃÂÃÂÃÂµÃÂ´ÃÂ°_ÃÂÃÂµÃÂÃÂ²ÃÂÃÂÃÂ°ÃÂº_ÃÂ¿ÃÂµÃÂÃÂ°ÃÂº_ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ°'.split('_'),
    weekdaysShort: 'ÃÂ½ÃÂµÃÂ´._ÃÂ¿ÃÂ¾ÃÂ½._ÃÂÃÂÃÂ¾._ÃÂÃÂÃÂµ._ÃÂÃÂµÃÂ._ÃÂ¿ÃÂµÃÂ._ÃÂÃÂÃÂ±.'.split('_'),
    weekdaysMin: 'ÃÂ½ÃÂµ_ÃÂ¿ÃÂ¾_ÃÂÃÂ_ÃÂÃÂ_ÃÂÃÂµ_ÃÂ¿ÃÂµ_ÃÂÃÂ'.split('_'),
    weekdaysParseExact : true,
    longDateFormat: {
        LT: 'H:mm',
        LTS : 'H:mm:ss',
        L: 'DD. MM. YYYY',
        LL: 'D. MMMM YYYY',
        LLL: 'D. MMMM YYYY H:mm',
        LLLL: 'dddd, D. MMMM YYYY H:mm'
    },
    calendar: {
        sameDay: '[ÃÂ´ÃÂ°ÃÂ½ÃÂ°ÃÂ ÃÂ] LT',
        nextDay: '[ÃÂÃÂÃÂÃÂÃÂ° ÃÂ] LT',
        nextWeek: function () {
            switch (this.day()) {
            case 0:
                return '[ÃÂ] [ÃÂ½ÃÂµÃÂ´ÃÂµÃÂÃÂ] [ÃÂ] LT';
            case 3:
                return '[ÃÂ] [ÃÂÃÂÃÂµÃÂ´ÃÂ] [ÃÂ] LT';
            case 6:
                return '[ÃÂ] [ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂ] [ÃÂ] LT';
            case 1:
            case 2:
            case 4:
            case 5:
                return '[ÃÂ] dddd [ÃÂ] LT';
            }
        },
        lastDay  : '[ÃÂÃÂÃÂÃÂµ ÃÂ] LT',
        lastWeek : function () {
            var lastWeekDays = [
                '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂµ] [ÃÂ½ÃÂµÃÂ´ÃÂµÃÂÃÂµ] [ÃÂ] LT',
                '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂ¾ÃÂ³] [ÃÂ¿ÃÂ¾ÃÂ½ÃÂµÃÂ´ÃÂµÃÂÃÂºÃÂ°] [ÃÂ] LT',
                '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂ¾ÃÂ³] [ÃÂÃÂÃÂ¾ÃÂÃÂºÃÂ°] [ÃÂ] LT',
                '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂµ] [ÃÂÃÂÃÂµÃÂ´ÃÂµ] [ÃÂ] LT',
                '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂ¾ÃÂ³] [ÃÂÃÂµÃÂÃÂ²ÃÂÃÂÃÂºÃÂ°] [ÃÂ] LT',
                '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂ¾ÃÂ³] [ÃÂ¿ÃÂµÃÂÃÂºÃÂ°] [ÃÂ] LT',
                '[ÃÂ¿ÃÂÃÂ¾ÃÂÃÂ»ÃÂµ] [ÃÂÃÂÃÂ±ÃÂ¾ÃÂÃÂµ] [ÃÂ] LT'
            ];
            return lastWeekDays[this.day()];
        },
        sameElse : 'L'
    },
    relativeTime : {
        future : 'ÃÂ·ÃÂ° %s',
        past   : 'ÃÂ¿ÃÂÃÂµ %s',
        s      : 'ÃÂ½ÃÂµÃÂºÃÂ¾ÃÂ»ÃÂ¸ÃÂºÃÂ¾ ÃÂÃÂµÃÂºÃÂÃÂ½ÃÂ´ÃÂ¸',
        m      : translator.translate,
        mm     : translator.translate,
        h      : translator.translate,
        hh     : translator.translate,
        d      : 'ÃÂ´ÃÂ°ÃÂ½',
        dd     : translator.translate,
        M      : 'ÃÂ¼ÃÂµÃÂÃÂµÃÂ',
        MM     : translator.translate,
        y      : 'ÃÂ³ÃÂ¾ÃÂ´ÃÂ¸ÃÂ½ÃÂ',
        yy     : translator.translate
    },
    ordinalParse: /\d{1,2}\./,
    ordinal : '%d.',
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 7  // The week that contains Jan 1st is the first week of the year.
    }
});

