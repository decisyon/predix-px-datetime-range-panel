//! moment.js locale configuration
//! locale : Klingon (tlh)
//! author : Dominika Kruk : https://github.com/amaranthrose

import moment from '../moment';

var numbersNouns = 'pagh_waÃ¢ÂÂ_chaÃ¢ÂÂ_wej_loS_vagh_jav_Soch_chorgh_Hut'.split('_');

function translateFuture(output) {
    var time = output;
    time = (output.indexOf('jaj') !== -1) ?
	time.slice(0, -3) + 'leS' :
	(output.indexOf('jar') !== -1) ?
	time.slice(0, -3) + 'waQ' :
	(output.indexOf('DIS') !== -1) ?
	time.slice(0, -3) + 'nem' :
	time + ' pIq';
    return time;
}

function translatePast(output) {
    var time = output;
    time = (output.indexOf('jaj') !== -1) ?
	time.slice(0, -3) + 'HuÃ¢ÂÂ' :
	(output.indexOf('jar') !== -1) ?
	time.slice(0, -3) + 'wen' :
	(output.indexOf('DIS') !== -1) ?
	time.slice(0, -3) + 'ben' :
	time + ' ret';
    return time;
}

function translate(number, withoutSuffix, string, isFuture) {
    var numberNoun = numberAsNoun(number);
    switch (string) {
        case 'mm':
            return numberNoun + ' tup';
        case 'hh':
            return numberNoun + ' rep';
        case 'dd':
            return numberNoun + ' jaj';
        case 'MM':
            return numberNoun + ' jar';
        case 'yy':
            return numberNoun + ' DIS';
    }
}

function numberAsNoun(number) {
    var hundred = Math.floor((number % 1000) / 100),
	ten = Math.floor((number % 100) / 10),
	one = number % 10,
	word = '';
    if (hundred > 0) {
        word += numbersNouns[hundred] + 'vatlh';
    }
    if (ten > 0) {
        word += ((word !== '') ? ' ' : '') + numbersNouns[ten] + 'maH';
    }
    if (one > 0) {
        word += ((word !== '') ? ' ' : '') + numbersNouns[one];
    }
    return (word === '') ? 'pagh' : word;
}

export default moment.defineLocale('tlh', {
    months : 'teraÃ¢ÂÂ jar waÃ¢ÂÂ_teraÃ¢ÂÂ jar chaÃ¢ÂÂ_teraÃ¢ÂÂ jar wej_teraÃ¢ÂÂ jar loS_teraÃ¢ÂÂ jar vagh_teraÃ¢ÂÂ jar jav_teraÃ¢ÂÂ jar Soch_teraÃ¢ÂÂ jar chorgh_teraÃ¢ÂÂ jar Hut_teraÃ¢ÂÂ jar waÃ¢ÂÂmaH_teraÃ¢ÂÂ jar waÃ¢ÂÂmaH waÃ¢ÂÂ_teraÃ¢ÂÂ jar waÃ¢ÂÂmaH chaÃ¢ÂÂ'.split('_'),
    monthsShort : 'jar waÃ¢ÂÂ_jar chaÃ¢ÂÂ_jar wej_jar loS_jar vagh_jar jav_jar Soch_jar chorgh_jar Hut_jar waÃ¢ÂÂmaH_jar waÃ¢ÂÂmaH waÃ¢ÂÂ_jar waÃ¢ÂÂmaH chaÃ¢ÂÂ'.split('_'),
    monthsParseExact : true,
    weekdays : 'lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj'.split('_'),
    weekdaysShort : 'lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj'.split('_'),
    weekdaysMin : 'lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj'.split('_'),
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD.MM.YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY HH:mm',
        LLLL : 'dddd, D MMMM YYYY HH:mm'
    },
    calendar : {
        sameDay: '[DaHjaj] LT',
        nextDay: '[waÃ¢ÂÂleS] LT',
        nextWeek: 'LLL',
        lastDay: '[waÃ¢ÂÂHuÃ¢ÂÂ] LT',
        lastWeek: 'LLL',
        sameElse: 'L'
    },
    relativeTime : {
        future : translateFuture,
        past : translatePast,
        s : 'puS lup',
        m : 'waÃ¢ÂÂ tup',
        mm : translate,
        h : 'waÃ¢ÂÂ rep',
        hh : translate,
        d : 'waÃ¢ÂÂ jaj',
        dd : translate,
        M : 'waÃ¢ÂÂ jar',
        MM : translate,
        y : 'waÃ¢ÂÂ DIS',
        yy : translate
    },
    ordinalParse: /\d{1,2}\./,
    ordinal : '%d.',
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
    }
});
