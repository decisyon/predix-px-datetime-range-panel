//! moment.js locale configuration
//! locale : Morocco Central Atlas TamaziÃÂ£t in Latin (tzm-latn)
//! author : Abdel Said : https://github.com/abdelsaid

import moment from '../moment';

export default moment.defineLocale('tzm-latn', {
    months : 'innayr_brÃÂ¤ayrÃÂ¤_marÃÂ¤sÃÂ¤_ibrir_mayyw_ywnyw_ywlywz_ÃÂ£wÃÂ¡t_ÃÂ¡wtanbir_ktÃÂ¤wbrÃÂ¤_nwwanbir_dwjnbir'.split('_'),
    monthsShort : 'innayr_brÃÂ¤ayrÃÂ¤_marÃÂ¤sÃÂ¤_ibrir_mayyw_ywnyw_ywlywz_ÃÂ£wÃÂ¡t_ÃÂ¡wtanbir_ktÃÂ¤wbrÃÂ¤_nwwanbir_dwjnbir'.split('_'),
    weekdays : 'asamas_aynas_asinas_akras_akwas_asimwas_asiÃ¡Â¸Âyas'.split('_'),
    weekdaysShort : 'asamas_aynas_asinas_akras_akwas_asimwas_asiÃ¡Â¸Âyas'.split('_'),
    weekdaysMin : 'asamas_aynas_asinas_akras_akwas_asimwas_asiÃ¡Â¸Âyas'.split('_'),
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD/MM/YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY HH:mm',
        LLLL : 'dddd D MMMM YYYY HH:mm'
    },
    calendar : {
        sameDay: '[asdkh g] LT',
        nextDay: '[aska g] LT',
        nextWeek: 'dddd [g] LT',
        lastDay: '[assant g] LT',
        lastWeek: 'dddd [g] LT',
        sameElse: 'L'
    },
    relativeTime : {
        future : 'dadkh s yan %s',
        past : 'yan %s',
        s : 'imik',
        m : 'minuÃ¡Â¸Â',
        mm : '%d minuÃ¡Â¸Â',
        h : 'saÃÂa',
        hh : '%d tassaÃÂin',
        d : 'ass',
        dd : '%d ossan',
        M : 'ayowr',
        MM : '%d iyyirn',
        y : 'asgas',
        yy : '%d isgasn'
    },
    week : {
        dow : 6, // Saturday is the first day of the week.
        doy : 12  // The week that contains Jan 1st is the first week of the year.
    }
});

