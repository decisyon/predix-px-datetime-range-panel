//! moment.js locale configuration
//! locale : chinese (zh-cn)
//! author : suupic : https://github.com/suupic
//! author : Zeno Zeng : https://github.com/zenozeng

import moment from '../moment';

export default moment.defineLocale('zh-cn', {
    months : 'Ã¤Â¸ÂÃ¦ÂÂ_Ã¤ÂºÂÃ¦ÂÂ_Ã¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂÃ¦ÂÂ_Ã¤ÂºÂÃ¦ÂÂ_Ã¥ÂÂ­Ã¦ÂÂ_Ã¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂ«Ã¦ÂÂ_Ã¤Â¹ÂÃ¦ÂÂ_Ã¥ÂÂÃ¦ÂÂ_Ã¥ÂÂÃ¤Â¸ÂÃ¦ÂÂ_Ã¥ÂÂÃ¤ÂºÂÃ¦ÂÂ'.split('_'),
    monthsShort : '1Ã¦ÂÂ_2Ã¦ÂÂ_3Ã¦ÂÂ_4Ã¦ÂÂ_5Ã¦ÂÂ_6Ã¦ÂÂ_7Ã¦ÂÂ_8Ã¦ÂÂ_9Ã¦ÂÂ_10Ã¦ÂÂ_11Ã¦ÂÂ_12Ã¦ÂÂ'.split('_'),
    weekdays : 'Ã¦ÂÂÃ¦ÂÂÃ¦ÂÂ¥_Ã¦ÂÂÃ¦ÂÂÃ¤Â¸Â_Ã¦ÂÂÃ¦ÂÂÃ¤ÂºÂ_Ã¦ÂÂÃ¦ÂÂÃ¤Â¸Â_Ã¦ÂÂÃ¦ÂÂÃ¥ÂÂ_Ã¦ÂÂÃ¦ÂÂÃ¤ÂºÂ_Ã¦ÂÂÃ¦ÂÂÃ¥ÂÂ­'.split('_'),
    weekdaysShort : 'Ã¥ÂÂ¨Ã¦ÂÂ¥_Ã¥ÂÂ¨Ã¤Â¸Â_Ã¥ÂÂ¨Ã¤ÂºÂ_Ã¥ÂÂ¨Ã¤Â¸Â_Ã¥ÂÂ¨Ã¥ÂÂ_Ã¥ÂÂ¨Ã¤ÂºÂ_Ã¥ÂÂ¨Ã¥ÂÂ­'.split('_'),
    weekdaysMin : 'Ã¦ÂÂ¥_Ã¤Â¸Â_Ã¤ÂºÂ_Ã¤Â¸Â_Ã¥ÂÂ_Ã¤ÂºÂ_Ã¥ÂÂ­'.split('_'),
    longDateFormat : {
        LT : 'AhÃ§ÂÂ¹mmÃ¥ÂÂ',
        LTS : 'AhÃ§ÂÂ¹mÃ¥ÂÂsÃ§Â§Â',
        L : 'YYYY-MM-DD',
        LL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
        LLL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥AhÃ§ÂÂ¹mmÃ¥ÂÂ',
        LLLL : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥ddddAhÃ§ÂÂ¹mmÃ¥ÂÂ',
        l : 'YYYY-MM-DD',
        ll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥',
        lll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥AhÃ§ÂÂ¹mmÃ¥ÂÂ',
        llll : 'YYYYÃ¥Â¹Â´MMMDÃ¦ÂÂ¥ddddAhÃ§ÂÂ¹mmÃ¥ÂÂ'
    },
    meridiemParse: /Ã¥ÂÂÃ¦ÂÂ¨|Ã¦ÂÂ©Ã¤Â¸Â|Ã¤Â¸ÂÃ¥ÂÂ|Ã¤Â¸Â­Ã¥ÂÂ|Ã¤Â¸ÂÃ¥ÂÂ|Ã¦ÂÂÃ¤Â¸Â/,
    meridiemHour: function (hour, meridiem) {
        if (hour === 12) {
            hour = 0;
        }
        if (meridiem === 'Ã¥ÂÂÃ¦ÂÂ¨' || meridiem === 'Ã¦ÂÂ©Ã¤Â¸Â' ||
                meridiem === 'Ã¤Â¸ÂÃ¥ÂÂ') {
            return hour;
        } else if (meridiem === 'Ã¤Â¸ÂÃ¥ÂÂ' || meridiem === 'Ã¦ÂÂÃ¤Â¸Â') {
            return hour + 12;
        } else {
            // 'Ã¤Â¸Â­Ã¥ÂÂ'
            return hour >= 11 ? hour : hour + 12;
        }
    },
    meridiem : function (hour, minute, isLower) {
        var hm = hour * 100 + minute;
        if (hm < 600) {
            return 'Ã¥ÂÂÃ¦ÂÂ¨';
        } else if (hm < 900) {
            return 'Ã¦ÂÂ©Ã¤Â¸Â';
        } else if (hm < 1130) {
            return 'Ã¤Â¸ÂÃ¥ÂÂ';
        } else if (hm < 1230) {
            return 'Ã¤Â¸Â­Ã¥ÂÂ';
        } else if (hm < 1800) {
            return 'Ã¤Â¸ÂÃ¥ÂÂ';
        } else {
            return 'Ã¦ÂÂÃ¤Â¸Â';
        }
    },
    calendar : {
        sameDay : function () {
            return this.minutes() === 0 ? '[Ã¤Â»ÂÃ¥Â¤Â©]Ah[Ã§ÂÂ¹Ã¦ÂÂ´]' : '[Ã¤Â»ÂÃ¥Â¤Â©]LT';
        },
        nextDay : function () {
            return this.minutes() === 0 ? '[Ã¦ÂÂÃ¥Â¤Â©]Ah[Ã§ÂÂ¹Ã¦ÂÂ´]' : '[Ã¦ÂÂÃ¥Â¤Â©]LT';
        },
        lastDay : function () {
            return this.minutes() === 0 ? '[Ã¦ÂÂ¨Ã¥Â¤Â©]Ah[Ã§ÂÂ¹Ã¦ÂÂ´]' : '[Ã¦ÂÂ¨Ã¥Â¤Â©]LT';
        },
        nextWeek : function () {
            var startOfWeek, prefix;
            startOfWeek = moment().startOf('week');
            prefix = this.diff(startOfWeek, 'days') >= 7 ? '[Ã¤Â¸Â]' : '[Ã¦ÂÂ¬]';
            return this.minutes() === 0 ? prefix + 'dddAhÃ§ÂÂ¹Ã¦ÂÂ´' : prefix + 'dddAhÃ§ÂÂ¹mm';
        },
        lastWeek : function () {
            var startOfWeek, prefix;
            startOfWeek = moment().startOf('week');
            prefix = this.unix() < startOfWeek.unix()  ? '[Ã¤Â¸Â]' : '[Ã¦ÂÂ¬]';
            return this.minutes() === 0 ? prefix + 'dddAhÃ§ÂÂ¹Ã¦ÂÂ´' : prefix + 'dddAhÃ§ÂÂ¹mm';
        },
        sameElse : 'LL'
    },
    ordinalParse: /\d{1,2}(Ã¦ÂÂ¥|Ã¦ÂÂ|Ã¥ÂÂ¨)/,
    ordinal : function (number, period) {
        switch (period) {
        case 'd':
        case 'D':
        case 'DDD':
            return number + 'Ã¦ÂÂ¥';
        case 'M':
            return number + 'Ã¦ÂÂ';
        case 'w':
        case 'W':
            return number + 'Ã¥ÂÂ¨';
        default:
            return number;
        }
    },
    relativeTime : {
        future : '%sÃ¥ÂÂ',
        past : '%sÃ¥ÂÂ',
        s : 'Ã¥ÂÂ Ã§Â§Â',
        m : '1 Ã¥ÂÂÃ©ÂÂ',
        mm : '%d Ã¥ÂÂÃ©ÂÂ',
        h : '1 Ã¥Â°ÂÃ¦ÂÂ¶',
        hh : '%d Ã¥Â°ÂÃ¦ÂÂ¶',
        d : '1 Ã¥Â¤Â©',
        dd : '%d Ã¥Â¤Â©',
        M : '1 Ã¤Â¸ÂªÃ¦ÂÂ',
        MM : '%d Ã¤Â¸ÂªÃ¦ÂÂ',
        y : '1 Ã¥Â¹Â´',
        yy : '%d Ã¥Â¹Â´'
    },
    week : {
        // GB/T 7408-1994Ã£ÂÂÃ¦ÂÂ°Ã¦ÂÂ®Ã¥ÂÂÃ¥ÂÂÃ¤ÂºÂ¤Ã¦ÂÂ¢Ã¦Â Â¼Ã¥Â¼ÂÃÂ·Ã¤Â¿Â¡Ã¦ÂÂ¯Ã¤ÂºÂ¤Ã¦ÂÂ¢ÃÂ·Ã¦ÂÂ¥Ã¦ÂÂÃ¥ÂÂÃ¦ÂÂ¶Ã©ÂÂ´Ã¨Â¡Â¨Ã§Â¤ÂºÃ¦Â³ÂÃ£ÂÂÃ¤Â¸ÂISO 8601:1988Ã§Â­ÂÃ¦ÂÂ
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
    }
});

